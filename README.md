
# MicroLAM

![screenshot](doc/screenshot-1.png)

## Disclaimer

This is an old project, not being maintained for years. Continue at your own risk and please mind the license in case you decide to use it. This project is purposefully licensed under the [GNU GPLv3](LICENSE) to allow for very restrictive use due to its academic nature and the desire to keep the knowledge here open to everybody. In case the license does not attend your needs, please contact the [original author](https://luciopaiva.com).

The project was written in Portuguese and so are the following instructions.

## Como usar

Clone este repositório e abra o projeto no IntelliJ. Qualquer IDE servirá, porém as instruções que se seguem são exclusivas para o IntelliJ.

### Configuração do projeto

Para configurar o projeto:

1. Entre no menu *File -> Project Structure...*;
2. Na aba *Project*, escolha o SDK atualmente instalado no seu sistema;
3. Ainda nesta aba, escolha *Project language level* para um nível compatível com o seu SDK;
4. Na mesma aba, Preencha *Project compiler output* com o caminho para uma pasta local onde o IntelliJ deve salvar os arquivos de build (sugestão: criar uma pasta `bin/` dentro da pasta do projeto);
5. Mudar para a aba *Modules* e marcar a pasta `src` com a tag `Sources`.

Agora compile o projeto através do menu *Build -> Build Project*.

Existem duas maneiras de rodar o simulador:

- aplicativo visual: ajuda a entender o funcionamento da simulação e observar comportamentos, mas não gera dados;
- aplicativo de console: Executa o apenas simulador; não há parte gráfica neste modo. Aceita parâmetros de linha de comando para configurar a simulação. Ao final da execução, o simulador gera arquivos com os dados obtidos. 

Veja abaixo como configurar cada um deles.

### Aplicativo desktop

![screenshot](doc/screenshot-2.png)

Use este aplicativo para visualizar o funcionamento do simulador em tempo real.

Para executar via IntelliJ:

1. menu *Run -> Edit Configurations...*
2. no topo esquerdo, clique no botão "+" e escolha *Application*
3. no campo *Name*, escreva "MicroLAM desktop"
4. na aba *Configuration*, campo *Main class": `br.ufrj.lam.microlam.ui.MicroLAMDesktopApp`
5. ainda em *Configuration*, campo *Working directory*, escolha a pasta-raiz do projeto
6. clique em *Ok*

Com o aplicativo configurado, basta agora selecioná-lo e clicar no botão *play*.

Durante a execução, clique com o botão esquerdo e arraste o mouse para navegar pelo mapa. Clique com o botão direito e arraste o mouse para manipular o zoom. Clique em um veículo para selecioná-lo.

Atalhos do teclado:

- `[`: desacelera simulação
- `]`: acelera simulação
- `p`: pausa simulação

#### Como mudar o cenário

A mudança do cenário (mapa com configuração das ruas e outros parâmetros) é feita manualmente e necessita recompilação do código. Navegue até a classe `br.ufrj.lam.microlam.ui.MicroLAMPanel`, método `init()` e altere a seguinte linha:

    engine = new Engine(XmlScenarioLoader.load("scenarios/manhattan_smer.xml"), FRAME_DURATION_MS);

Para checar os cenários disponíveis, navegue até a pasta `/scenarios` a partir da pasta-raiz do projeto.

### Aplicativo de console

Use este aplicativo quando for necessário gerar dados sobre a simulação. Neste modo, não há visualização e a simulação executa em background, gerando arquivos de log e planilhas para posterior análise.

Para executar via IntelliJ, siga os mesmos passos da aplicação desktop (veja acima), porém mude o campo *Main class* para `br.ufrj.lam.microlam.MicroLAMApplication`. É necessário também configurar o campo *Program arguments*. Para a listagem dos parâmetros disponíveis, rode o aplicativo sem argumentos. Um exemplo de argumentos:

    scenarios/singlecar.xml 1 10 30 4 1 50 1 1 1 1 42 teste

Ao fim da execução, o simulador irá gerar os seguintes arquivos:

- `<scenario_name>_<suffix>.csv`: arquivo em formato CSV contendo um registro para cada via sendo simulada no cenário com dados consolidados ao fim da simulação;
- `<scenario_name>_<suffix>.verbose.csv`: arquivo em formato CSV contendo snapshots dos dados, tirados ao longo da execução com período ditado pelo parâmetro `<interval>`;
- `<scenario_name>_<suffix>.log`: mensagens informativas geradas pelo simulador ao longo da execução;
- `<scenario_name>_<suffix>.err`: quaisquer mensagens de erro geradas pelo simulador. Após uma rodada bem sucedida, espera-se que este arquivo esteja vazio. 

*Nota: os arquivos estão sendo gerados dentro da pasta `/scenarios`. Um importante ponto de melhoria é fazer o simulador escrever os arquivos numa pasta própria para isso, garantindo que tais arquivos sejam ignorados pelo sistema de versionamento.*
 