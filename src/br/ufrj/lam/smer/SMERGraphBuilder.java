package br.ufrj.lam.smer;

public class SMERGraphBuilder {

    private SMERGraph graph;

    public SMERGraphBuilder() {
        graph = new SMERGraph();
    }

    public void addNode(Object id, long reversibility) {
        graph.addNode(id, reversibility);
    }

    public void addNode(Object id) {
        this.addNode(id, 1);
    }

    public void removeNode(Object id) {
        this.graph.removeNode(id);
    }

    /**
     * Adiciona aresta identificada.
     */
    void addEdge(long edgeId, String nodeIdA, long nodeCountA, String nodeIdB, long nodeCountB) throws SMERException {
        graph.addEdge(edgeId, nodeIdA, nodeCountA, nodeIdB, nodeCountB);
    }

    /**
     * Adiciona aresta sem identificação.
     */
    void addEdge(String nodeIdA, long nodeCountA, String nodeIdB, long nodeCountB) throws SMERException {
        graph.addEdge(nodeIdA, nodeCountA, nodeIdB, nodeCountB);
    }

    /**
     * Adiciona aresta sem identificação e com número de tokens não inicializados (serão criados após rodar o AlgArestas).
     */
    public void addEdge(Object nodeIdA, Object nodeIdB) throws SMERException {
        graph.addEdge(nodeIdA, SMEREdge.UNINITIALIZED_EDGE, nodeIdB, SMEREdge.UNINITIALIZED_EDGE);
    }

    public SMERGraph graph() {
        return graph;
    }

    /**
     * @return retorna true se existe uma aresta conectando a com b.
     */
    private boolean existsEdge(SMERNode a, SMERNode b) {
        for (SMEREdge e : graph.edges) {
            if ((e.getNodeA() == a && e.getNodeB() == b) ||
                    (e.getNodeA() == b && e.getNodeB() == a))
                return true;
        }
        return false;
    }

    /**
     * Gera um grafo completo, criando arestas que liguem nós que ainda
     * não estiverem conectados. Arestas já existentes são aproveitadas
     * e mantidas.
     */
    public void makeCompleteGraph() throws SMERException {
        for (int i = 0; i < graph.nodes.size(); i++) {
            SMERNode m = graph.nodes.get(i);
            for (int j = i + 1; j < graph.nodes.size(); j++) {
                SMERNode n = graph.nodes.get(j);

                if (!existsEdge(m, n))
                    graph.addEdge(m.getId(), Long.MAX_VALUE, n.getId(), Long.MAX_VALUE);
            }
        }
    }

}
