package br.ufrj.lam.smer;

/**
 * Representa uma aresta no grafo SMER.
 * <p>
 * Observe que o SMER é um multigrafo e que na verdade esta classe
 * pode representar um conjunto de arestas que estão ligadas a um
 * mesmo par de vértices. Uma maneira de ver esta aresta é como se
 * ela possuísse "contas" que são deslizadas de um lado para outro
 * conforme os nós processam, porém a concepção original dessas contas
 * representa na verdade o sentido atual de cada aresta direcionada
 * no multigrafo.
 *
 * @author Lucio Paiva
 * @since Jan-2012
 */
public class SMEREdge {

    static final long UNINITIALIZED_EDGE = -1;

    private long id;

    /**
     * Uma aresta SMER sempre está ligada a um par de vértices.
     **/
    private SMERNode nodeA, nodeB;

    /**
     * Contagem atual de contas (ou tokens) em cada ponta da aresta.
     **/
    private long nodeCountA, nodeCountB;
    private long tempCountA, tempCountB;

    private boolean hasTransferToCommit;

    SMEREdge(long id, SMERNode nodeA, long nodeCountA, SMERNode nodeB, long nodeCountB) {
        this.id = id;
        this.nodeA = nodeA;
        this.nodeB = nodeB;
        this.nodeCountA = nodeCountA;
        this.nodeCountB = nodeCountB;
        this.hasTransferToCommit = false;
    }

    /**
     * @param id Identificador do nó em questão.
     * @return true se a aresta está conectada a um nó com identificador "id".
     */
    @SuppressWarnings("unused")
    private boolean isConnectedToNodeId(Object id) {
        return (nodeA.getId().equals(id)) || (nodeB.getId().equals(id));
    }

    /**
     * Duas arestas são consideradas iguais se possuem mesmo id, estão ligadas
     * ao mesmo par de nós (os nós são identificados pelos seus ids) e possuem
     * a mesma quantidade de tokens em cada ponta.
     */
    boolean isEqualTo(SMEREdge otherEdge) {
        if (this.getId() != otherEdge.getId())
            return false;

        // A==A, B==B
        if ((this.getNodeA().getId().equals(otherEdge.getNodeA().getId())) &&
                (this.getNodeB().getId().equals(otherEdge.getNodeB().getId()))) {
            // verifica se possuem a mesma contagem de tokens
            if (this.nodeCountA != otherEdge.nodeCountA)
                return false;
            return this.nodeCountB == otherEdge.nodeCountB;
        }
        // A==B, B==A
        else if ((this.getNodeB().getId().equals(otherEdge.getNodeA().getId())) &&
                (this.getNodeA().getId().equals(otherEdge.getNodeB().getId()))) {
            // verifica se possuem a mesma contagem de tokens
            if (this.nodeCountB != otherEdge.nodeCountA)
                return false;
            return this.nodeCountA == otherEdge.nodeCountB;
        }

        // se chegou aqui, passou por todos os testes
        return true;
    }

    // commita o novo estado da aresta
    private void commitOpenTokenTransfers() {
        if (hasTransferToCommit) {
            nodeCountA = tempCountA;
            nodeCountB = tempCountB;
            hasTransferToCommit = false;
        }
    }

    protected void step() {
        commitOpenTokenTransfers();
    }

    long getTokenCountForNodeById(Object id) {
        if (id.equals(nodeA.getId()))
            return getTokenCountForNode(nodeA);
        else if (id.equals(nodeB.getId()))
            return getTokenCountForNode(nodeB);
        else
            return 0; // TODO deveria gerar uma exception aqui
    }

    // chamado quando um nó quer saber quantos tokens ele possui disponíveis nessa aresta
    long getTokenCountForNode(SMERNode node) {
        if (node == nodeA)
            return nodeCountA;
        else if (node == nodeB)
            return nodeCountB;
        else
            return 0; // should not happen!
    }

    // called when node 'node' is executing to revert its tokens
    void revert(SMERNode node) throws SMERException {
        if (node == nodeA) {
            if (nodeCountA >= node.getReversibility()) {
                tempCountA = nodeCountA - node.getReversibility();
                tempCountB = nodeCountB + node.getReversibility();
            } else {
                // node is trying to revert, but it hasn't enough tokens to do so
                // should not happen!
                throw new SMERException("Node<" + nodeA.getId().toString() + "> não é sink mas tentou executar mesmo assim!");
            }
        } else if (node == nodeB) {
            if (nodeCountB >= node.getReversibility()) {
                tempCountB = nodeCountB - node.getReversibility();
                tempCountA = nodeCountA + node.getReversibility();
            } else {
                // node is trying to revert, but it hasn't enough tokens to do so
                // should not happen!
                throw new SMERException("Node<" + nodeB.getId() + "> não é sink mas tentou executar mesmo assim!");
            }
        } else {
            // should not happen!
            throw new SMERException("Node<" + node.getId() + "> tentou executar em uma aresta que não lhe pertence!");
        }

        hasTransferToCommit = true;
    }

    SMERNode getNodeA() {
        return nodeA;
    }

    SMERNode getNodeB() {
        return nodeB;
    }

    boolean isConnectedTo(SMERNode node) {
        return (this.nodeA == node) || (this.nodeB == node);
    }

    protected long getId() {
        return this.id;
    }

    public String toString() {
        String nca = (nodeCountA == SMEREdge.UNINITIALIZED_EDGE) ? "?" : String.valueOf(nodeCountA);
        String ncb = (nodeCountB == SMEREdge.UNINITIALIZED_EDGE) ? "?" : String.valueOf(nodeCountB);
        return "Edge<" + id + "> [" + nodeA.getId() + "](" + nca + ")---(" + ncb + ")[" + nodeB.getId() + "]";
    }

    /**
     * Orienta aresta para o nó especificado. No caso generalizado do SMER,
     * reverte o número de arestas necesário para que o nó possa executar.
     * Esse número é calculado subtraindo-se da reversibilidade dele, quantas
     * arestas faltam ser revertidas para que ele possa operar.
     */
    void setOrientationTo(SMERNode n) throws SMERException {
        if (n == nodeA) {
            nodeCountA = 1;
            nodeCountB = 0;
        } else if (n == nodeB) {
            nodeCountA = 0;
            nodeCountB = 1;
        } else {
            throw new SMERException("Cannot set orientation to node <" + n.getId().toString() + ">");
        }
    }

    /**
     * @return o nó que teria condições de executar se ele
     * só dependesse desta aresta para tal.
     */
    SMERNode getOrientation() {
        if (nodeCountA >= nodeA.getReversibility())
            return nodeA;
        else if (nodeCountB >= nodeB.getReversibility())
            return nodeB;
        else
            return null;
    }

    /**
     * Usado quando um nó muda sua reversibilidade para consertar o número de tokens.
     */
    void setTokenCountForNode(SMERNode node, long numberOfTokensToInsert) {
        if (node == nodeA)
            nodeCountA = numberOfTokensToInsert;
        else if (node == nodeB)
            nodeCountB = numberOfTokensToInsert;
    }

    SMERNode getOtherNode(SMERNode node) {
        if (node == nodeA)
            return nodeB;
        else if (node == nodeB)
            return nodeA;
        else
            return null;
    }
}
