package br.ufrj.lam.smer;

import java.util.ArrayList;
import java.util.HashMap;

import br.ufrj.lam.microlam.core.Rand;
import br.ufrj.lam.microlam.core.Utils;
import br.ufrj.lam.microlam.report.ReportManager;

/**
 * Engine principal que execute um grafo SMER.
 * Recebe como entrada o grafo a ser executado.
 *
 * @author Lucio Paiva
 * @since Jan-2012
 */
public class SMEREngine {

    private class ReversibilityChangeInfo {
        private SMERNode node;
        private long oldReversibility;
        private long newReversibility;

        private ReversibilityChangeInfo(SMERNode node, long oldReversibility, long newReversibility) {
            this.node = node;
            this.oldReversibility = oldReversibility;
            this.newReversibility = newReversibility;
        }
    }

    private ArrayList<SMERGraph> executionHistory;
    private HashMap<Integer, ArrayList<ReversibilityChangeInfo>> reversibilityChangeHistory;
    private SMERGraph graph;

    /**
     * Quando {@code true}, indica que o período do grafo foi encontrado.
     * Nesse caso, {@link SMEREngine#periodBegin} e {@link SMEREngine#periodEnd} guardam o índice
     * do primeiro e último estados do período. Não necessariamente
     * {@code periodBegin} será igual a 0, pois pode ser que exista uma
     * tripa advinda do estado inicial do grafo e que não faz parte do
     * período.
     **/
    private boolean foundPeriod = false;
    private long periodBegin;
    private long periodEnd;
    private int lookForAPeriodStartingHere;
    /**
     * Esta variável precisa ser {@code true} para que o histórico seja
     * gravado e o período do grafo possa ser impresso numa execução em
     * que se quer observar como o grafo evolui. Para a utilização da
     * engine SMER no auxílio do funcionamento de outros sistemas (como
     * o MicroLAM), onde o período do grafo não será impresso, esta va-
     * riável deve ficar como {@code false}.
     */
    private boolean wantsToRecordHistory;
    /**
     * Variável usada durante execução do método de look ahead para
     * avisar à engine que certas ações não devem ser executadas por
     * se tratar de uma simulação do futuro.
     */
    private boolean offTheRecord = false;

    private long outputFieldWidth;
    private long outputLineNumWidth;

    private boolean opportunistMode;

    private static final boolean DEBUG_INFO = false;

    public SMEREngine(SMERGraph graph) {
        this.executionHistory = new ArrayList<>();
        this.reversibilityChangeHistory = new HashMap<>();
        this.graph = graph;
    }

    /**
     * Deve ser chamado antes do grafo começar a executar
     * caso seja necessário garantir que o mesmo não contém
     * ciclos.
     */
    public void prepareToRun() throws SMERException {
        reset();
        runAlgColor();
    }

    private void reset() {
        executionHistory.clear();
        reversibilityChangeHistory.clear();
        wantsToRecordHistory = false;
        foundPeriod = false;
    }

    /**
     * Acumula estado atual do grafo no histórico de estados.
     */
    private void addCurrentStateToHistory() {
        if (foundPeriod)
            return;

        for (int i = lookForAPeriodStartingHere; i < executionHistory.size(); i++) {
            SMERGraph prevGraph = executionHistory.get(i);

            if (prevGraph.isEqualTo(graph)) {
                // achou o periodo do grafo
                foundPeriod = true;
                periodBegin = i;
                periodEnd = executionHistory.size() - 1;
                return;
            }
        }

        // se chegou aqui, o periodo ainda nao fechou... continua gravando os estados:
        executionHistory.add(new SMERGraph(graph));
    }

    /**
     * Gera um clone do grafo passado como parâmetro e roda sobre
     * esse clone n iterações, retornando ao final o estado do grafo.
     * <br /><br />
     * <span style="color: red; font-weight: bold">ATENÇÃO</span>: o
     * resultado pode não refletir o que realmente vai ocorrer no
     * futuro caso o grafo sofra uma mudança de reversibilidades!
     */
    private SMERGraph lookAhead(SMERGraph originalGraph, int n) throws SMERException {
        SMERGraph lookAheadGraph = new SMERGraph(originalGraph);

        offTheRecord = true;

        for (int i = 0; i < n; i++) {
            step(lookAheadGraph);
        }

        offTheRecord = false;

        return lookAheadGraph;
    }

    public void step() throws SMERException {
        step(this.graph);
    }

    /**
     * Executa uma iteração síncrona do grafo SMER passado como parâmetro.
     * <p>
     * Este método é usado tanto para iterar o grafo principal da engine
     * como também para iterar estados futuros dele quando requisitada a
     * função de look ahead da engine.
     *
     * @param g SMERGraph a ser iterado.
     */
    private void step(SMERGraph g) throws SMERException {

        if (opportunistMode)
            runOpportunistNodes();

        if (wantsToRecordHistory && !offTheRecord)
            addCurrentStateToHistory();

        // para cada nó, descobre quais podem executar:
        for (SMERNode node : g.nodes) {
            /*
             * Antigamente eu simplesmente checava por uma mudança no
             * valor da reversibilidade para saber se trocou a reversibilidade.
             * Porém, às vezes é possível que o nó troque para a mesma re-
             * versibilidade. Quando isso acontece, não é possível invalidar
             * corretamente o histórico, pois o código acha que não houve
             * mudança, porém isso pode causar erros de cálculo do período,
             * logo é importante registrar mesmo esses casos. Para isso, eu
             * passei a checar diretamente o booleano da mudança.
             */
            boolean wantsToChangeReversibility = node.doesWantToChangeReversibility();
            long oldReversibility = node.getReversibility();

            node.step();

            /* Se a reversibilidade do nó mudou após sua
             * execução, precisamos invalidar a gravação do
             * período do grafo, pois a partir de agora ele
             * entrará em outro período.
             */
            if (wantsToChangeReversibility && (!node.doesWantToChangeReversibility())) {
                invalidateHistory();
                logNodeReversibilityChange(node, oldReversibility, node.getReversibility());
            }
        }

        // Após a execução completa do grafo, devemos agora atualizar os tokens das arestas.
        // Eles não são atualizados DURANTE a execução do grafo pois isso pode gerar um estado inconsistente.
        // É preciso processar todo mundo e só DEPOIS disso atualizar.
        for (SMEREdge edge : g.edges) {
            edge.step();
        }

    }

    /**
     * Registra uma ocorrência de mudança de reversibilidade, apenas para efeito de log.
     */
    private void logNodeReversibilityChange(SMERNode node, long oldReversibility, long newReversibility) {
        int currentPeriodIndex = executionHistory.size() - 1;
        reversibilityChangeHistory.computeIfAbsent(currentPeriodIndex, k -> new ArrayList<>());
        ArrayList<ReversibilityChangeInfo> changeList = reversibilityChangeHistory.get(currentPeriodIndex);
        changeList.add(new ReversibilityChangeInfo(node, oldReversibility, newReversibility));
    }

    /**
     * Chamado quando se deseja esquecer o histórico
     * que já foi gravado e começar tudo de novo. Isso
     * é desejado quando algum nó mudou sua reversibi-
     * lidade.
     */
    private void invalidateHistory() {
        if (wantsToRecordHistory) {
            foundPeriod = false;
            /*
             * Passei a não apagar mais o histórico, assim eu não
             * escondo as iterações de transição da reversibilidade
             * que antes não apareciam no dump. O que eu estou
             * fazendo agora é manter um ponteiro indicando para o
             * código onde ele deve começar a procurar por um pe-
             * ríodo, assim eu garanto que ele não vai identificar
             * um período falso olhando para algum estado anterior
             * às mudanças de reversibilidade que possa por coin-
             * cidência ser igual a um estado pós-mudanças.
             */
            lookForAPeriodStartingHere = executionHistory.size();
        }
    }

    /**
     * Este método implementa o algoritmo do "Nó Oportunista".
     * Essa foi uma ideia que tive enquanto modelava um cru-
     * zamento de trânsito usando um grafo SER. Percebi que
     * alguns nós que não estavam executando num determinado
     * estado do grafo poderiam executar, se arestas, que es-
     * tavam apontando para nós que também não estavam execu-
     * tando, fossem revertidas temporariamente.
     * <p>
     * O algoritmo ainda não está redondo. Veja tag "TO DO"
     * que pode ser encontrada no meio do algoritmo abaixo.
     */
    private void runOpportunistNodes() throws SMERException {
        /*
         * Pré-etapa dos nós oportunistas
         * Antes de começar a executar os sinks, verifica
         * se existe algum nó que também poderia ser sink,
         * mas não é por conta de alguma aresta ociosa que
         * está apontada no sentido oposto.
         */
        for (SMERNode node : graph.nodes) {
            if (!node.isSink()) {
                int availableEdges = 0;

                /*
                 * Conta quantas arestas disponíveis o nó tem a seu
                 * dispor, já contando as ociosas:
                 */
                for (SMEREdge edge : node.getEdges()) {
                    if (edge.getOrientation() == node)
                        // esta aresta já está orientada para mim:
                        availableEdges++;
                    else if (edge.getOrientation() != node &&
                            !edge.getOrientation().isSink())
                        // esta aresta não está orientada para mim, porém
                        // ela está ociosa e eu poderia utilizá-la:
                        availableEdges++;
                }

                // TODO pode existir um caso em que A está revertendo
                // uma aresta ociosa que apontava para B, porém pode
                // ser que se B revertesse uma outra aresta ociosa que
                // apontava para C, ele também conseguisse executar.
                // Nesse caso, tanto A quanto B poderiam roubar uma
                // aresta e executar, porém os dois não conseguiriam
                // fazer isso ao mesmo tempo. Nesse caso, como decidir
                // quem tem a vez? Teria que criar um metagrafo para
                // isso? Haveria uma aresta especial entre A e B que
                // resolveria esse impasse caso ele ocorresse. Essa
                // aresta também seria orientada, e assim que um deles
                // a utilizasse, reverteria para que da próxima vez
                // o outro pudesse ganhar na decisão de um impasse de
                // aresta ociosa.
                // se o nó conseguir que todas as arestas apontem para ele:
                if (availableEdges == node.getEdges().size()) {
                    for (SMEREdge edge : node.getEdges()) {
                        edge.setOrientationTo(node);
                    }
                }

            }
        }
    }

    private String appendTokenCount(long value, char append) {
        StringBuilder result = new StringBuilder(String.valueOf(value));
        while (result.length() < this.outputFieldWidth)
            result.insert(0, append);
        return result.toString();
    }

    /**
     * Exemplo de impressão que é gerada pelo código abaixo:
     * <p>
     * A , B  |  B , C
     * 000. [3], 0  |  3 , 0
     * 001.  0 ,[3] | [3], 0
     * 002.  1 ,[2] | [2], 1  <-+
     * 003.  2 ,[1] | [1], 2    |
     * 004. [3], 0  |  0 ,[3]   |
     * 005.  0 , 3  |  1 ,[2]   |
     * 006.  0 , 3  |  2 ,[1] --+
     */
    private String dumpGraph(SMERGraph graph) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < graph.edges.size(); i++) {
            SMEREdge edge = graph.edges.get(i);

            String nA;
            if (edge.getNodeA().isSink())
                nA = String.format("[%s]", appendTokenCount(edge.getTokenCountForNode(edge.getNodeA()), '_'));
            else
                nA = String.format(" %s ", appendTokenCount(edge.getTokenCountForNode(edge.getNodeA()), ' '));

            String nB;
            if (edge.getNodeB().isSink())
                nB = String.format("[%s]", appendTokenCount(edge.getTokenCountForNode(edge.getNodeB()), '_'));
            else
                nB = String.format(" %s ", appendTokenCount(edge.getTokenCountForNode(edge.getNodeB()), ' '));

            String edgeStr = nA + "," + nB;

            result.append(edgeStr);

            if (i != graph.edges.size() - 1)
                result.append(" | ");
        }

        return result.toString();
    }

    private String dumpListOfSinks(SMERGraph g) {
        StringBuilder result = new StringBuilder();

        ArrayList<Object> sinks = getSinkNodes(g);

        if (sinks.size() > 0) {
            for (int i = 0; i < sinks.size() - 1; i++)
                result.append(sinks.get(i).toString()).append(", ");
            result.append(sinks.get(sinks.size() - 1));
        }

        return result.toString();
    }

    private String dumpHeader(SMERGraph graph) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < graph.edges.size(); i++) {
            SMEREdge edge = graph.edges.get(i);

            StringBuilder idStr;
            idStr = new StringBuilder(edge.getNodeA().getId().toString());
            while (idStr.length() < this.outputFieldWidth)
                idStr.insert(0, " ");
            String nA = String.format(" %s ", idStr.toString());
            idStr = new StringBuilder(edge.getNodeB().getId().toString());
            while (idStr.length() < this.outputFieldWidth)
                idStr.insert(0, " ");
            String nB = String.format(" %s ", idStr.toString());

            String edgeStr = nA + "," + nB;

            result.append(edgeStr);

            if (i != graph.edges.size() - 1)
                result.append(" | ");
        }

        long headerWidth = result.length();
        result.append('\n');
        for (int i = 0; i < outputLineNumWidth + 2; i++)
            result.append(' ');
        for (int i = 0; i < headerWidth; i++)
            result.append('-');

        return result.toString();
    }

    @SuppressWarnings("unused")
    private static String dumpReversibilities(SMERGraph graph) {
        StringBuilder result = new StringBuilder();
        for (SMERNode node : graph.nodes) {
            result.append(String.format("(\"Node %s\", r=%d) ", node.getId().toString(), node.getReversibility()));
        }
        return result.toString();
    }

    void dumpConcurrency(int numberOfRuns) throws SMERException {
        double minConcurrency = Double.POSITIVE_INFINITY;
        double maxConcurrency = 0;
        double accConcurrency = 0;
        for (int i = 0; i < numberOfRuns; i++) {
            this.prepareToRun();
            double conc = this.dumpConcurrency_step();
            if (conc < minConcurrency) minConcurrency = conc;
            if (conc > maxConcurrency) maxConcurrency = conc;
            accConcurrency += conc;
        }
        ReportManager.log(String.format("Concorrência mínima observada: %.1f nós/estado", minConcurrency));
        ReportManager.log(String.format("Concorrência máxima observada: %.1f nós/estado", maxConcurrency));
        ReportManager.log(String.format("Concorrência média observada: %.1f nós/estado", accConcurrency / (double) numberOfRuns));
    }

    /**
     * Executa o grafo até encontrar o período e computa
     * a concorrência dentro do período, retornando valor
     * ao final.
     */
    private double dumpConcurrency_step() throws SMERException {
        enableHistoryRecording();
        while (!foundPeriod) {
            step();
        }
        disableHistoryRecording();

        HashMap<Object, Long> frequencies = new HashMap<>();
        for (SMERNode n : graph.nodes) {
            frequencies.put(n.getId(), 0L);
        }

        for (int i = 0; i < executionHistory.size(); i++) {
            SMERGraph g = executionHistory.get(i);

            // atualiza a frequencia de execução de cada nó.
            // cuidado para só contar as execuções que ocorreram dentro do período.
            if (i >= periodBegin && i <= periodEnd) {
                for (Object id : getSinkNodes(g)) {
                    frequencies.put(id, frequencies.get(id) + 1);
                }
            }
        }
        // calcula concorrencia do grafo:
        long concurrency = 0;
        for (Long value : frequencies.values()) {
            concurrency += value;
        }
        return concurrency / (double) (periodEnd - periodBegin + 1);
    }

    /**
     * A , B  |  B , C
     * 000. [3], 0  |  3 , 0
     * 001.  0 ,[3] | [3], 0
     * 002.  1 ,[2] | [2], 1  <-+
     * 003.  2 ,[1] | [1], 2    |
     * 004. [3], 0  |  0 ,[3]   |
     * 005.  0 , 3  |  1 ,[2]   |
     * 006.  0 , 3  |  2 ,[1] --+
     */
    public void dumpPeriod() throws SMERException {
        StringBuilder output = new StringBuilder("PERIODO DO GRAFO SMER\n");
        long periodSize;
        /*
         * Executa o grafo até que seu período seja encontrado.
         * Por mais complexo e grande que seja o grafo, em algum
         * momento ele vai repetir algum estado anteriormente já
         * visitado. Quando isso ocorrer, o período foi achado.
         */
        enableHistoryRecording();
        while (!foundPeriod) {
            step();
        }
        disableHistoryRecording();

        periodSize = periodEnd - periodBegin + 1;

        /*
         * Pega tamanho das maiores medidas para poder formatar
         * a saída corretamente de maneira que todas as linhas do
         * output fiquem alinhadas.
         */
        long maximumTokenCount = 0;
        long maximumNodeIdLength = 0;
        for (SMERGraph g : executionHistory) {
            for (SMERNode n : g.nodes) {
                if (n.getId().toString().length() > maximumNodeIdLength)
                    maximumNodeIdLength = n.getId().toString().length();
            }

            for (SMEREdge e : g.edges) {
                if (e.getTokenCountForNode(e.getNodeA()) > maximumTokenCount)
                    maximumTokenCount = e.getTokenCountForNode(e.getNodeA());
                if (e.getTokenCountForNode(e.getNodeB()) > maximumTokenCount)
                    maximumTokenCount = e.getTokenCountForNode(e.getNodeB());
            }
        }

        outputLineNumWidth = (long) (Math.floor(Math.log10(executionHistory.size())) + 1);
        outputFieldWidth = Math.max(maximumNodeIdLength, (long) (Math.floor(Math.log10(maximumTokenCount)) + 1));

        StringBuilder spaces = new StringBuilder();
        while (spaces.length() < outputLineNumWidth)
            spaces.insert(0, ' ');
        output.append(String.format("%s  %s", spaces.toString(), dumpHeader(executionHistory.get(0)))).append("\n");

        HashMap<Object, Long> frequencies = new HashMap<>();
        for (SMERNode n : graph.nodes) {
            frequencies.put(n.getId(), 0L);
        }

        for (int i = 0; i < executionHistory.size(); i++) {
            SMERGraph g = executionHistory.get(i);

            // line number:
            StringBuilder lineNum = new StringBuilder(String.valueOf(i + 1));
            while (lineNum.length() < outputLineNumWidth)
                lineNum.insert(0, ' ');

            // seta do periodo:
            String periodArrow;
            if (i == periodBegin)
                periodArrow = "<-+";
            else if (i == periodEnd)
                periodArrow = "--+";
            else if (i > periodBegin && i < periodEnd)
                periodArrow = "  |";
            else
                periodArrow = "   ";

            // atualiza a frequencia de execução de cada nó.
            // cuidado para só contar as execuções que ocorreram dentro do período.
            if (i >= periodBegin && i <= periodEnd) {
                for (Object id : getSinkNodes(g)) {
                    frequencies.put(id, frequencies.get(id) + 1);
                }
            }

            StringBuilder reversibilityChangeNote = new StringBuilder();
            if (this.reversibilityChangeHistory.get(i) != null) {
                reversibilityChangeNote = new StringBuilder(" ");
                for (ReversibilityChangeInfo info : this.reversibilityChangeHistory.get(i)) {
                    reversibilityChangeNote.append(String.format("[r%s: %d->%d] ", info.node.getName(),
                            info.oldReversibility, info.newReversibility));
                }
            }

            output.append(String.format("%s. %s %s  (%s) %s", lineNum.toString(), dumpGraph(g), periodArrow,
                    dumpListOfSinks(g), reversibilityChangeNote.toString())).append("\n");
        }

        output.append("O Grafo possui periodo de tamanho ").append(periodSize).append(".").append("\n");

        for (SMERNode n : graph.nodes) {
            /*
             * Quantas vezes o nó executou durante o período.
             */
            long freq = frequencies.get(n.getId());
            /*
             * Quantos % do tempo do período aquele nó passa executando.
             */
            double freqPerc = (freq / ((double) periodSize)) * 100;
            output.append(n).append(String.format(" Number of executions: %d, Percentual execution (relative to period size): %.1f%%", freq, freqPerc)).append("\n");
        }

        // calcula concorrencia do grafo:
        long concurrency = 0;
        for (Long value : frequencies.values()) {
            concurrency += value;
        }
        output.append(String.format("Concorrencia: %.1f nos/estado", concurrency / (double) (periodEnd - periodBegin + 1))).append("\n");
        ReportManager.log(output.toString());
    }

    private void enableHistoryRecording() {
        wantsToRecordHistory = true;
        executionHistory.clear();
    }

    private void disableHistoryRecording() {
        wantsToRecordHistory = false;
    }

    public ArrayList<Object> getSinkNodes() {
        return getSinkNodes(graph);
    }

    /**
     * @return lista de nós atualmente ativos (i.e., processando)
     */
    private ArrayList<Object> getSinkNodes(SMERGraph g) {
        ArrayList<Object> nodeIds = new ArrayList<>();

        for (SMERNode node : g.nodes) {
            if (node.isSink())
                nodeIds.add(node.getId());
        }
        return nodeIds;
    }

    /**
     * @return lista de nós que estarão ativos na próxima
     * iteração do grafo.
     */
    public ArrayList<Object> getSinkNodesForNextIteration() throws SMERException {
        return getSinkNodesForIteration(1);
    }

    /**
     * @return lista de nós que estarão ativos na n-ésima iteração do grafo, contando a iteração atual como n=0.
     */
    private ArrayList<Object> getSinkNodesForIteration(SMERGraph g, int iteration) throws SMERException {
        SMERGraph lookAheadGraph = lookAhead(g, iteration);

        return this.getSinkNodes(lookAheadGraph);
    }

    /**
     * @return lista de nós que estarão ativos na n-ésima iteração do grafo, contando a iteração atual como n=0.
     */
    public ArrayList<Object> getSinkNodesForIteration(int iteration) throws SMERException {
        return this.getSinkNodesForIteration(this.graph, iteration);
    }

    /**
     * Executa o AlgArestas para definir uma configuração
     * acíclica inicial.
     * <p>
     * 1. Para cada nó aberto, sorteia um número inteiro.
     * 2. Para cada aresta, orienta a aresta para o nó
     * com maior número sorteado e marca os nós como
     * fechados. Caso os valores sejam iguaism, não
     * orienta a aresta.
     * 3. Vá para 1 enquanto houver arestas não orientadas
     */
    @SuppressWarnings("unused")
    private void runAlgEdges() throws SMERException {
        ArrayList<SMEREdge> openEdges = new ArrayList<>(graph.edges);
        HashMap<SMERNode, Long> nodeValues = new HashMap<>();

        // forçando reversibilidade = 1 pois o AlgArestas só funciona em grafos SER:
        for (SMERNode n : graph.nodes) {
            n.setReversibility(1);
        }

        while (openEdges.size() > 0) {
            nodeValues.clear();
            for (SMERNode n : graph.nodes) {
                nodeValues.put(n, Rand.uniformLong());
            }

            ArrayList<SMEREdge> openEdgesCopy = new ArrayList<>(openEdges);
            for (SMEREdge e : openEdgesCopy) {
                if (nodeValues.get(e.getNodeA()) > nodeValues.get(e.getNodeB())) {
                    e.setOrientationTo(e.getNodeA());
                    openEdges.remove(e);
                } else if (nodeValues.get(e.getNodeA()) < nodeValues.get(e.getNodeB())) {
                    e.setOrientationTo(e.getNodeB());
                    openEdges.remove(e);
                }
            }
        }
    }

    /**
     * Executa o AlgCor para definir uma configuração
     * acíclica inicial.
     * <p>
     * 1. Para cada nó aberto, sorteia um número inteiro.
     * 2. Para cada aresta, orienta a aresta para o nó
     * com maior número sorteado e marca os nós como
     * fechados. Caso os valores sejam iguaism, não
     * orienta a aresta.
     * 3. Vá para 1 enquanto houver arestas não orientadas
     */
    private void runAlgColor() throws SMERException {
        HashMap<SMERNode, Long> nodeValues = new HashMap<>();
        HashMap<SMERNode, Long> nodeColors = new HashMap<>();

        // forçando reversibilidade = 1 pois o AlgArestas só funciona em grafos SER:
        for (SMERNode n : graph.nodes) {
            n.setReversibility(1);
        }

        ArrayList<SMERNode> probabilisticNodes = new ArrayList<>(graph.nodes);
        /*
         * Opera enquanto houver nós não coloridos.
         */
        while (probabilisticNodes.size() > 0) {
            nodeValues.clear();
            // cada nó probabilístico joga um dado:
            for (SMERNode n : probabilisticNodes) {
                nodeValues.put(n, Rand.uniformLong());
            }

            ArrayList<SMERNode> probabilisticNodesCopy = new ArrayList<>(probabilisticNodes);
            // agora achamos os nós que obtiveram valor maior que seus vizinhos probabilisticos e colorimos eles
            for (SMERNode n : probabilisticNodesCopy) {
                boolean wonTheToss = true;
                // para cada vizinho:
                for (SMERNode neighbor : n.getNeighbors()) {
                    // se o vizinho é probabilístico e o valor dele é maior que o meu, eu perdi essa rodada:
                    if (nodeValues.containsKey(neighbor) && nodeValues.get(neighbor) > nodeValues.get(n)) {
                        wonTheToss = false;
                        break;
                    }
                }

                // o nó ganhou de seus vizinhos e vai ganhar uma cor:
                if (wonTheToss) {
                    // a preferência é pela cor de menor índice possível:
                    long myColor = 0;
                    ArrayList<Long> neighborColors = new ArrayList<>();
                    // coleta as cores que meus vizinhos possuem:
                    for (SMERNode neighbor : n.getNeighbors()) {
                        // se meu vizinho já foi colorido:
                        if (nodeColors.containsKey(neighbor) && !neighborColors.contains(nodeColors.get(neighbor)))
                            // ...incrementa minha cor:
                            neighborColors.add(nodeColors.get(neighbor));
                    }

                    // encontra o menor índice possível que eu posso usar como cor:
                    while (neighborColors.contains(myColor))
                        myColor++;

                    nodeColors.put(n, myColor);
                    probabilisticNodes.remove(n);
                }
            }
        }

        /*
         * Uma vez que todos os nós estejam coloridos,
         * agora o algoritmo deve orientar as arestas da
         * maior cor para a menor.
         */
        for (SMEREdge e : graph.edges) {
            if (nodeColors.get(e.getNodeA()) > nodeColors.get(e.getNodeB()))
                e.setOrientationTo(e.getNodeB());
            else
                e.setOrientationTo(e.getNodeA());
        }
    }

    /**
     * Muda a taxa de execução dos nós do grafo. O HashMap passado como
     * parâmetro deve ter as taxas de execução de cada nó já normalizadas
     * com valores no intervalo [0,1], garantindo que a soma de todos os
     * pesos deve ser igual a 1.
     * <p>
     * A rotina transforma os pesos em valores inteiros no intervalo [0,100],
     * em seguida chamando uma rotina que calcula o MMC e enfim gerando
     * as reversibilidades e atribuindo a cada nó.
     */
    public void setNodeExecutionSlices(HashMap<Object, Double> rawPhaseWeights) {
        HashMap<Object, Long> normalizedPhaseWeights = new HashMap<>();

        if (DEBUG_INFO)
            ReportManager.log("Reavaliando pesos do controlador SMER.");

        // converte para inteiro:
        for (Object nodeIndex : rawPhaseWeights.keySet()) {
            /*
             * O peso está sendo mapeado para um valor dentro do intervalo [1,100].
             * Escolhi esta faixa para que a taxas de execução dos nós no grafo tenham
             * maior precisão em comparação com as taxas desejadas. Caso o intervalo
             * fosse na faixa [1,10] e por exemplo quiséssemos que um nó operasse na
             * taxa de 23% em relação ao outro nó existente no grafo (considerando um
             * exemplo com 2 nós para facilitar), o mais próximo que conseguiríamos
             * seria uma taxa de 20% para ele.
             */
            long intPhaseWeight = Math.round(rawPhaseWeights.get(nodeIndex) * 100d);

            /*
             * Como a engine não suporta hibernação de nó ainda, caso algum
             * peso seja zero eu seto ele para o menor valor válido: 1.
             */
            if (intPhaseWeight == 0)
                intPhaseWeight = 1;

            normalizedPhaseWeights.put(nodeIndex, intPhaseWeight);
        }

        // calcula o MMC dos pesos inteiros:
        long lcm = Utils.lcm(normalizedPhaseWeights.values().toArray(new Long[0]));

        // reconfigura as reversibilidades de todos os nós:
        for (Object nodeIndex : normalizedPhaseWeights.keySet()) {
            long newReversibility = lcm / normalizedPhaseWeights.get(nodeIndex);
            this.graph.getNodeById(nodeIndex).changeReversibility(newReversibility);
        }
    }

    public SMERGraph graph() {
        return this.graph;
    }
}
