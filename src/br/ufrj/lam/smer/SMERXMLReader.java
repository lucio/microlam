package br.ufrj.lam.smer;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Classe que lê um XML representando um grafo e instancia um objeto smer.Graph
 * que pode ser usado como entrada para uma smer.Engine para simular a execução
 * da reversão de múltiplas arestas.
 * <p>
 * Formato exemplo de entrada do XML:
 *
 * <smer>
 * <node id="nome_do_noh" reversibility="3" />
 * <node id="nome_do_outro" reversibility="1" />
 *
 * <edge nodeA="nome_do_noh" nodeB="nome_do_outro" nodeCountA="3" nodeCountB="0" />
 * </smer>
 *
 * @author Lucio Paiva
 * @since Feb-2012
 */
class SMERXMLReader {

    static SMERGraph load(String filename) {
        SMERGraph smerGraph = null;

        try {
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(filename);
            document.normalize();
            smerGraph = createGraph(document);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return smerGraph;
    }

    private static SMERGraph createGraph(Document document) throws SMERException {
        SMERGraphBuilder graphBuilder = new SMERGraphBuilder();

        // lê nós do grafo:
        NodeList xmlNodes = document.getElementsByTagName("node");
        for (int i = 0; i < xmlNodes.getLength(); i++) {
            org.w3c.dom.Element node = (Element) xmlNodes.item(i);

            String nid = node.getAttribute("id");
            String nrev = node.getAttribute("reversibility");

            if (nid == null || nid.length() == 0)
                throw new SMERException("Node attribute 'id' is missing.");

            /*
             * Caso a reversibilidade do nó não tenha sido especificada,
             * coloca 1 como reversibilidade padrão:
             */
            long lrev = (nrev == null || nrev.length() == 0) ? SMERNode.DEFAULT_REVERSIBILITY : Long.parseLong(nrev);
            graphBuilder.addNode(nid, lrev);
        }

        // lê arestas do grafo:
        NodeList xmlEdges = document.getElementsByTagName("edge");
        for (int i = 0; i < xmlEdges.getLength(); i++) {
            org.w3c.dom.Element node = (Element) xmlEdges.item(i);

            String eid = node.getAttribute("id");
            String nodeA = node.getAttribute("nodeA");
            String nodeB = node.getAttribute("nodeB");
            String nodeCountA = node.getAttribute("nodeCountA");
            String nodeCountB = node.getAttribute("nodeCountB");

            if (nodeA == null || nodeA.length() == 0)
                throw new SMERException("Edge attribute 'nodeA' is missing.");
            if (nodeB == null || nodeB.length() == 0)
                throw new SMERException("Edge attribute 'nodeB' is missing.");

            long lNodeCountA, lNodeCountB;
            /*
             * Se pelo menos um nodeCount da aresta não for fornecido, considera
             * que a aresta não foi inicializada. A engine vai tratar de
             * inicializá-la antes de começar a execução.
             */
            if ((nodeCountA == null || nodeCountA.length() == 0) ||
                    (nodeCountB == null || nodeCountB.length() == 0)) {
                lNodeCountA = SMEREdge.UNINITIALIZED_EDGE;
                lNodeCountB = SMEREdge.UNINITIALIZED_EDGE;
            } else {
                lNodeCountA = Long.parseLong(nodeCountA);
                lNodeCountB = Long.parseLong(nodeCountB);
            }

            if (eid == null || eid.length() == 0)
                graphBuilder.addEdge(nodeA,
                        lNodeCountA,
                        nodeB,
                        lNodeCountB);
            else
                graphBuilder.addEdge(Long.parseLong(eid),
                        nodeA,
                        lNodeCountA,
                        nodeB,
                        lNodeCountB);
        }

        return graphBuilder.graph();
    }

}
