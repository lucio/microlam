package br.ufrj.lam.smer;

import java.util.ArrayList;

import br.ufrj.lam.microlam.core.Utils;

/**
 * Representação de um nó no grafo SMER.
 *
 * @author Lucio Paiva
 * @since Jan-2012
 */
public class SMERNode {

    static final long DEFAULT_REVERSIBILITY = 1;

    private ArrayList<SMEREdge> edges; // conjunto de arestas incidentes a esse nó

    private Object id;
    private long reversibility; // reversibilidade do nó
    private long wantedReversibility; // reversibilidade desejada pelo nó, que entrará em vigor assim que ele for sink
    private boolean wantsToChangeReversibility;

    /**
     * Clona nó.
     * IMPORTANTE! AS ARESTAS NÃO SÃO COPIADAS. O NOVO NÓ NÃO POSSUI ARESTAS.
     */
    SMERNode(SMERNode source) {
        this.id = source.id;
        this.reversibility = source.reversibility;
        this.wantedReversibility = source.wantedReversibility;
        this.wantsToChangeReversibility = source.wantsToChangeReversibility;
        this.edges = new ArrayList<>();
    }

    SMERNode(Object id, long reversibility) {
        this.id = id;
        /*
         * Assumo que quando o nó é criado, ele sempre
         * começa com reversibilidade = 1. Ao longo da
         * execução do grafo SMER, todos os nós terão
         * oportunidade de elevar sua reversibilidade
         * para o valor passado como parâmetro para este
         * método.
         */
        this.reversibility = 1;

        /*
         * Prepara para mudar a reversibilidade para
         * o valor desejado assim que for sink pela
         * primeira vez.
         */
        if (reversibility != 1)
            this.changeReversibility(reversibility);

        edges = new ArrayList<>();
    }

    // adiciona aresta ao conjunto de arestas incidentes
    void addEdge(SMEREdge edge) {
        edges.add(edge);
    }

    void removeEdge(SMEREdge edge) {
        this.edges.remove(edge);
    }

    /**
     * Verifica se o nó passado como parâmetro tem o mesmo id e reversibilidade
     * que este nó (condição para serem iguais).
     * As arestas não são checadas.
     *
     * @return true se os nós são iguais
     */
    boolean isEqualTo(SMERNode otherNode) {
        return (this.id == otherNode.id) && (this.reversibility == otherNode.reversibility);
    }

    protected void step() throws SMERException {
        processIfIsSink();
    }

    // descobre se o nó pode executar (sink) ou não
    boolean isSink() {
        // para cada aresta incidente, descobre se eu possuo o número mínimo de tokens para executar
        for (SMEREdge edge : edges) {
            if (edge.getTokenCountForNode(this) < reversibility)
                return false;
        }
        return true;
    }

    /**
     * Verifica se existe uma chamada para mudar a reversibilidade.
     * Caso exista, recalcula o número de tokens e insere ou remove
     * a diferença de tokens.
     */
    private void changeReversibilityIfNeeded() {
        if (wantsToChangeReversibility) {
            for (SMEREdge edge : edges) {
                SMERNode otherNode = edge.getNodeA() == this ? edge.getNodeB() : edge.getNodeA();

                // r1 + r2 - gcd(r1, r2)
                long newNumberOfTokens = this.wantedReversibility + otherNode.getReversibility() -
                        Utils.gcd(this.wantedReversibility, otherNode.getReversibility());

                /*
                 * Inicialmente eu estava observando quantos tokens havia na aresta do lado do outro nó e modificando apenas
                 * o lado do nó que mudou sua reversibilidade, adicionando o número de tokens necessário para compensar a
                 * mudança. Porém, isso me gerou o seguinte problema, que leva o grafo para um deadlock:
                 *
                 * Node 1           Node 2
                 * [r1=61]<44>--<35>[r2=19]
                 *
                 * Nó 2 executa:
                 *
                 * Node 1           Node 2
                 * [r1=61]<63>--<16>[r2=19]
                 *
                 * Agora é a vez do nó 1 executar e ele recebeu uma instrução para que mude sua reversibilidade para 38.
                 * Recalculando o novo número de tokens:
                 *
                 * r1' + r2 - gcd(r1', r2) = 38 + 19 - 19 = 38
                 *
                 * Como eu preservo o número de tokens que já existem do lado do meu vizinho (16), devo mudar
                 * o número de tokens do meu lado para ficar com:
                 *
                 * 38 - 16 = 22 tokens
                 *
                 * Sendo assim, agora o grafo fica:
                 *
                 * Node 1           Node 2
                 * [r1=38]<22>--<16>[r2=19]
                 *
                 * E chega-se a um deadlock, já que nem 1 nem 2 podem executar.
                 */

                /*
                 * Nova solução:
                 * Dá ao nó que está mudando sua reversibilidade apenas o suficiente para executar uma vez.
                 * Isso garante que na próxima iteração seu nó vizinho poderá mudar de reversibilidade também,
                 * caso ele esteja aguardando oportunidade para tal. Dessa maneira, garanto que todos os nós
                 * do grafo mudarão suas reversibilidades em conjunto, já que no atual estado do algoritmo
                 * as reversibilidades todas mudam ao mesmo tempo.
                 */
                edge.setTokenCountForNode(this, this.wantedReversibility);
                edge.setTokenCountForNode(otherNode, newNumberOfTokens - this.wantedReversibility);
            }
            this.reversibility = this.wantedReversibility;
            wantsToChangeReversibility = false;
        }
    }

    // verifica se o nó é sink e, sendo, executa
    private void processIfIsSink() throws SMERException {
        if (isSink()) {
            changeReversibilityIfNeeded();

            for (SMEREdge edge : edges) {
                edge.revert(this);
            }
        }
    }

    /**
     * Avisa ao nó que na próxima execução ele deve mudar sua reversibilidade.
     * Difere do método setReversibility(), que muda a reversibilidade instan-
     * taneamente.
     */
    void changeReversibility(long reversibility) {
        this.wantedReversibility = reversibility;
        this.wantsToChangeReversibility = true;
    }

    /**
     * Muda instantaneamente a reversibilidade do nó, sem esperar que ele antes
     * seja sink. Difere da execução do método changeReversibility(), que muda
     * a reversibilidade só após o nó virar sink.
     */
    protected void setReversibility(long reversibility) {
        this.reversibility = reversibility;
    }

    protected long getReversibility() {
        return reversibility;
    }

    @Override
    public String toString() {
        return "Node " + id.toString() + ", Reversibility: " + reversibility;
    }

    /**
     * Versão simplificada do output do toString().
     */
    public String getName() {
        return id.toString();
    }

    protected Object getId() {
        return this.id;
    }

    ArrayList<SMEREdge> getEdges() {
        return edges;
    }

    public ArrayList<SMERNode> getNeighbors() {
        ArrayList<SMERNode> neighbors = new ArrayList<>();
        for (SMEREdge edge : getEdges()) {
            SMERNode neighbor = edge.getOtherNode(this);
            neighbors.add(neighbor);
        }
        return neighbors;
    }

    boolean doesWantToChangeReversibility() {
        return this.wantsToChangeReversibility;
    }
}
