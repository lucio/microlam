package br.ufrj.lam.smer;

import java.util.ArrayList;

/**
 * Representação de um grafo SMER.
 * Contém todos os nós e arestas que participam do grafo.
 * É apenas o modelo do grafo; a execução dele cabe à Engine.
 *
 * @author Lucio
 * @since Jun 06, 2011
 */
public class SMERGraph {

    ArrayList<SMERNode> nodes; // lista de nós do grafo
    ArrayList<SMEREdge> edges; // lista de arestas do grafo

    private long nextAvailableEdgeId = 1;

    protected SMERGraph() {
        // inicia vetores:
        nodes = new ArrayList<>();
        edges = new ArrayList<>();
    }

    public SMERNode getNodeById(Object id) {
        for (SMERNode node : nodes) {
            if (node.getId().equals(id))
                return node;
        }
        return null;
    }

    /**
     * Construtor de cópia.
     * Gera um novo grafo com os mesmo nós, mesmas arestas
     * e inclusive mantendo o sentido delas.
     */
    public SMERGraph(SMERGraph graph) {
        // copia nós
        nodes = new ArrayList<>();
        for (SMERNode node : graph.nodes) {
            SMERNode newNode = new SMERNode(node); // new SMERNode(node.getId(), node.getReversibility());
            nodes.add(newNode);
        }

        // copia arestas, já fazendo referência aos novos nodes criados acima
        edges = new ArrayList<>();
        for (SMEREdge edge : graph.edges) {
            SMERNode nodeA = this.getNodeById(edge.getNodeA().getId());
            SMERNode nodeB = this.getNodeById(edge.getNodeB().getId());
            long nodeCountA = edge.getTokenCountForNodeById(nodeA.getId());
            long nodeCountB = edge.getTokenCountForNodeById(nodeB.getId());

            SMEREdge newEdge = new SMEREdge(edge.getId(), nodeA, nodeCountA, nodeB, nodeCountB);
            edges.add(newEdge);

            // nós ficam sabendo da nova aresta que os liga
            nodeA.addEdge(newEdge);
            nodeB.addEdge(newEdge);
        }

        this.nextAvailableEdgeId = graph.nextAvailableEdgeId;
    }

    /**
     * Compara dois grafos checando se possuem mesmo conjunto de nós
     * e mesmo conjunto de arestas. Para cada aresta, verifica também
     * se possuem a mesma distribuição de tokens em cada ponta.
     *
     * @param graph Grafo que servirá de comparação.
     * @return true se os grafos são iguais.
     */
    boolean isEqualTo(SMERGraph graph) {
        // compara tamanho das coleções
        if (graph.nodes.size() != this.nodes.size())
            return false;
        if (graph.edges.size() != this.edges.size())
            return false;

        // compara conjunto de nós
        for (SMERNode theirNode : graph.nodes) {
            for (SMERNode myNode : this.nodes) {
                if (theirNode.getId().equals(myNode.getId())) {
                    if (!theirNode.isEqualTo(myNode))
                        return false;
                    break;
                }
            }
        }

        // compara conjunto de arestas e seus tokens
        for (SMEREdge theirEdge : graph.edges) {
            for (SMEREdge myEdge : this.edges) {
                if (theirEdge.getId() == myEdge.getId()) {
                    if (!theirEdge.isEqualTo(myEdge))
                        return false;
                    break;
                }
            }
        }

        // se chegou aqui, passou em todos os testes
        return true;
    }

    /**
     * Instancia um novo nó no grafo. Define seu nome e reversibilidade.
     *
     * @param id Pode ser qualquer Object que identifique unicamente aquele nó no grafo
     */
    void addNode(Object id, long reversibility) {
        SMERNode node = new SMERNode(id, reversibility);
        nodes.add(node);
    }

    void removeNode(Object id) {
        SMERNode node = this.getNodeById(id);
        if (node != null) {
            ArrayList<SMEREdge> copyOfMyEdges = new ArrayList<>(this.edges);
            for (SMEREdge edge : copyOfMyEdges) {
                if (edge.isConnectedTo(node)) {
                    this.removeEdge(edge);
                }
            }
            this.nodes.remove(node);
        }
    }

    private void removeEdge(SMEREdge edge) {
        edge.getNodeA().removeEdge(edge);
        edge.getNodeB().removeEdge(edge);
        this.edges.remove(edge);
    }

    private long getNextAvailableEdgeId() {
        return nextAvailableEdgeId++;
    }

    /**
     * Instancia uma nova aresta no grafo. Associa a nova aresta a dois nós que já devem ter sido previamente criados no grafo.
     * Além de criar a aresta, esta rotina também associa a aresta à lista de arestas dos nós.
     */
    SMEREdge addEdge(long edgeId, Object nodeIdA, long nodeCountA, Object nodeIdB, long nodeCountB) throws SMERException {
        if (getEdgeById(edgeId) != null)
            throw new SMERException("Edge <" + edgeId + "> already exists!");

        SMERNode nodeA = getNodeById(nodeIdA);
        SMERNode nodeB = getNodeById(nodeIdB);

        if (nodeA == null)
            throw new SMERException("Node <" + nodeIdA.toString() + "> could not be found.");
        if (nodeB == null)
            throw new SMERException("Node <" + nodeIdB.toString() + "> could not be found.");

        SMEREdge edge = new SMEREdge(edgeId, nodeA, nodeCountA, nodeB, nodeCountB);
        edges.add(edge);

        nodeA.addEdge(edge);
        nodeB.addEdge(edge);

        return edge;
    }

    private SMEREdge getEdgeById(long edgeId) {
        for (SMEREdge edge : edges)
            if (edge.getId() == edgeId)
                return edge;
        return null;
    }

    void addEdge(Object nodeIdA, long nodeCountA, Object nodeIdB, long nodeCountB) throws SMERException {
        addEdge(getNextAvailableEdgeId(), nodeIdA, nodeCountA, nodeIdB, nodeCountB);
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (SMERNode node : nodes)
            result.append(node.toString()).append("\n");
        result.append("\n");
        for (SMEREdge edge : edges)
            result.append(edge.toString()).append("\n");
        return result.toString();
    }
}
