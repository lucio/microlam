package br.ufrj.lam.smer;

public class SMERException extends Exception {

	private static final long serialVersionUID = 3096164650425052531L;

	public SMERException(String message) 
	{
		super(message);
	}
}
