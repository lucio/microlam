package br.ufrj.lam.smer;

import java.util.ArrayList;

import br.ufrj.lam.microlam.core.Rand;

/**
 * Classe de teste do mecanismo SMER.
 *
 * @author Lucio Paiva
 * @since Feb-2012
 */
@SuppressWarnings("unused")
public class TestSMER {

    private static ArrayList<Long> flows;
    private static ArrayList<Long> reversibilities;

    private static long gcd(long a, long b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    private static long lcm(long a, long b) {
        return (a / gcd(a, b)) * b;
    }

    private static void parseFlowArgs(String[] args) {
        flows = new ArrayList<>();

        for (String flow : args) {
            Long lflow = Long.parseLong(flow);

            if (lflow < 1) {
                System.out.println("Os valores de entrada precisam ser positivos.");
                return;
            }

            flows.add(lflow);
        }

        if (flows.size() < 2) {
            System.out.println("Você precisa fornecer ao menos dois inteiros positivos");
        }
    }

    private static void calculateLCMAndGCD() {
        // calcula MMC
        long lcm = flows.get(0);
        for (int i = 1; i < flows.size(); i++) {
            long b = flows.get(i);
            lcm = lcm(lcm, b);
        }

        // calcula MDC
        long gcd = flows.get(0);
        for (int i = 1; i < flows.size(); i++) {
            long b = flows.get(i);
            gcd = gcd(gcd, b);
        }

        System.out.println("Least common multiple: " + lcm);
        System.out.println("Greatest common divisor: " + gcd);
    }

    public static void main(String[] args) {

        Rand.setSeed(42L);

        SMERGraph graph = SMERXMLReader.load("smergraphs/smergraph3.xml");
        System.out.println(graph.toString());

        SMEREngine engine = new SMEREngine(graph);

        try {
            // faz um dump do periodo de 10 execucoes distintas:
            for (int i = 0; i < 10; i++) {
                engine.prepareToRun();
                engine.dumpPeriod();
            }

            // depois roda 1000 execucoes e mostra a concorrencia média:
            engine.dumpConcurrency(1000);
        } catch (SMERException e) {
            e.printStackTrace();
        }
    }
}
