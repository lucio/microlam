package br.ufrj.lam.microlam.coordination;

import java.util.ArrayList;

import br.ufrj.lam.microlam.core.InflowIntersection;
import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.SignalPhase;
import br.ufrj.lam.microlam.core.SignalizedIntersection;
import br.ufrj.lam.microlam.detector.VehiclePassageDetector;
import br.ufrj.lam.microlam.detector.VehiclePassageEvent;
import br.ufrj.lam.microlam.detector.VehiclePassageEventListener;
import br.ufrj.lam.microlam.report.ReportManager;
import br.ufrj.lam.microlam.signalcontrol.SMERCoordinatedSignalController;
import br.ufrj.lam.smer.SMERGraph;

/**
 * Representa um corredor num sistema gerenciado por um SMERCoordinator.
 *
 * @author lucio
 */
public class SMERCorridor implements VehiclePassageEventListener {

    /**
     * Posição do sensor do sistema de coordenação na via.
     * Valor em metros a partir do ponto downstream da via.
     */
    @SuppressWarnings("unused")
    private static final double COORDINATION_SENSOR_POSITION = 5;
    /**
     * Tags para diferenciar sensores upstream de downstream numa mesma via.
     */
    private static final long SENSOR_TAG_UPSTREAM = 1;
    @SuppressWarnings("unused")
    private static final long SENSOR_TAG_DOWNSTREAM = 2;

    private ArrayList<SMERCorridorNode> entranceNodes;
    private ArrayList<Road> entranceNodesIncomingRoads;
    private ArrayList<InflowIntersection> entranceNodesInflowIntersections;
    /**
     * ArrayList<InflowIntersection> entranceNodesInflowIntersections;
     * Nome do corredor. É usado apenas na geração de relatórios.
     */
    private String name;
    /**
     * Guarda a coleção de RoadConnections que participam deste
     * corredor. É usado pelo sistema para saber quais conexões
     * devem estar ativas quando o corredor tiver permissão para
     * operar.
     */
    private SignalPhase greenConnections;
    /**
     * Somatório das contagens de veículos que passaram por
     * qualquer um dos sensores deste corredor.
     */
    private long totalVehiclePassageCount = 0;

    /**
     * Construtor.
     *
     * @param name Nome do novo corredor.
     */
    public SMERCorridor(String name, SMERCorridorNode root, Road startingRoad) {
        this.name = name;
        this.entranceNodes = new ArrayList<>();
        this.entranceNodes.add(root);
        this.entranceNodesIncomingRoads = new ArrayList<>();
        this.entranceNodesIncomingRoads.add(startingRoad);
        this.entranceNodesInflowIntersections = new ArrayList<>();
        this.entranceNodesInflowIntersections.add((InflowIntersection) startingRoad.getUpstreamIntersection());
    }

    /**
     * Construtor de cópia.
     *
     * @param name   Nome do novo corredor.
     * @param source Corredor que vai ser copiado.
     */
    SMERCorridor(String name, SMERCorridor source) {
        this.name = (name != null) ? name : source.getName();

        this.entranceNodesIncomingRoads = new ArrayList<>();
        this.entranceNodes = new ArrayList<>();

        for (int i = 0; i < source.entranceNodesIncomingRoads.size(); i++) {
            // clona o nó e automaticamente clona toda a árvore abaixo dele:
            SMERCorridorNode root = new SMERCorridorNode(source.entranceNodes.get(i));
            Road incomingRoad = source.entranceNodesIncomingRoads.get(i);
            // cria um SMERCorridor temporário:
            SMERCorridor tempCorridor = new SMERCorridor("", root, incomingRoad);
            // integra o SMERCorridor temporário ao grafo atual. Esse foi um
            // macete que encontrei para evitar duplicidades de nós ao copiar
            // os entranceNodes (2 entrance nodes podem possuir em sua
            // árvore um nó em comum e esse nó seria duplicado ao fazer
            // o new SMERCorridorNode(entranceNode). Fazendo merge eu
            // garanto que vou remover as duplicatas.
            this.mergeCorridor(tempCorridor);
        }
    }

    /**
     * Verifica se este SMERCorridor possui o SMERCorridorNode em questão.
     * O SMERCorridorNode deve ser identificado unicamente pela sua Intersection!
     */
    private boolean contains(SMERCorridorNode node) {
        return this.getNodeList().contains(node);
    }

    private void addUpstreamNode(SMERCorridorNode node, SMERCorridorNode upstreamNode) {
        if (this.contains(node)) {
            if (!this.contains(upstreamNode)) {
                node.addUpstreamNode(upstreamNode);
                upstreamNode.addDownstreamNode(node);
            } else // noinspection StatementWithEmptyBody
                if (node.hasUpstreamNode(upstreamNode)) {
                /*
                 * A conexão já existe, então não faz nada.
                 */
            } else {
                /*
                 * Aqui pode ser que apareça um ciclo. Para evitar, temos
                 * que assegurar que node não possui upstreamNode como seu
                 * descendente. Se ele for um descendente, um ciclo seria
                 * formado e devemos proibir a inserção da aresta. Caso con-
                 * trário a aresta será inserida.
                 */
                if (node.hasDescendant(upstreamNode)) {
                    ReportManager.err("A criação da aresta com '" + node + "' no downstream e '" + upstreamNode + "' no upstream não pode ser criada pois fecharia um ciclo no grafo.");
                    Thread.dumpStack();
                    System.exit(1);
                } else {
                    SMERCorridorNode myUpstreamNode = this.getNodeByIntersection(upstreamNode.getIntersection());
                    if (myUpstreamNode != null) {
                        node.addUpstreamNode(myUpstreamNode);
                        myUpstreamNode.addDownstreamNode(node);
                    }
                }
            }
        } else {
            ReportManager.err("O nó '" + node + "' não existe neste SMERCorridor.");
            Thread.dumpStack();
            System.exit(1);
        }
    }

    /**
     * Adiciona um nó downstreamNode downstream em relação a parentNode.
     */
    public void addDownstreamNode(SMERCorridorNode parentNode, SMERCorridorNode downstreamNode) {

        if (this.contains(parentNode)) {
            if (!this.contains(downstreamNode)) {
                // adiciona downstreamNode como downstream de parentNode
                parentNode.addDownstreamNode(downstreamNode);
                // adiciona parentNode como upstream de downstreamNode
                downstreamNode.addUpstreamNode(parentNode);
            } else //noinspection StatementWithEmptyBody
                if (parentNode.hasDownstreamNode(downstreamNode)) {
                /*
                 * Este é um caso que parentNode já existe, downstreamNode também
                 * e ainda por cima downstreamNode já é filho de parentNode.
                 * Aqui não precisaríamos fazer nada, pois a aresta já existe.
                 */
            } else {
                /*
                 * Se chegou aqui é porque downstreamNode já existe no grafo
                 * porém não é filho de parentNode. Duas situações são pos-
                 * síveis:
                 * 1. se o corredor sendo montado não é um merge de 2 ou mais
                 * corredores, significa que encontramos um loop, já que neces-
                 * sariamente downstreamNode é um ascendente de parentNode pois
                 * o grafo é uma árvore;
                 * 2. se está sendo feito um merge, é possível que downstreamNode
                 * exista como filho de um nó de um corredor concorrente, o que
                 * não necessariamente implica num loop; pode ser apenas um
                 * cruzamento entre os dois corredores.
                 * Para eliminar a dúvida, o algoritmo deve subir por todos os
                 * ascendentes de parentNode procurando por downstreamNode. Caso
                 * encontre, temos um loop. Se não encontrar, significa que
                 * encontramos o cruzamento entre dois corredores e o que deve
                 * ser feito é apenas ligar uma aresta de parentNode para o
                 * downstreamNode que já existe no grafo.
                 */
                if (parentNode.hasAscendant(downstreamNode)) {
                    ReportManager.err("A criação da aresta com '" + parentNode + "' no upstream e '" + downstreamNode +
                            "' no downstream não pode ser criada pois fecharia um ciclo no grafo.");
                    Thread.dumpStack();
                    System.exit(1);
                } else {
                    SMERCorridorNode myDownstreamNode = this.getNodeByIntersection(downstreamNode.getIntersection());
                    if (myDownstreamNode != null) {
                        // adiciona downstreamNode como downstream de parentNode
                        parentNode.addDownstreamNode(myDownstreamNode);
                        // adiciona parentNode como upstream de downstreamNode
                        myDownstreamNode.addUpstreamNode(parentNode);
                    }
                }
            }
        } else {
            ReportManager.err("Parent node '" + parentNode + "' não existe neste SMERCorridor.");
            Thread.dumpStack();
            System.exit(1);
        }

    }

    /**
     * Funde o grafo deste SMERCorridor com o de outro SMERCorridor.
     * O corredor corridorToMerge não é clonado antes da cópia.
     * Caso seja necessário, espera-se que o parâmetro já seja uma
     * cópia clonada do corredor original.
     * <p>
     * O trabalho de merge consiste basicamente em detectar quais nós já
     * existiam anteriormente para evitar duplicidades. Ao encontrar
     * nós que já existiam, arestas novas são acrescentadas nele se-
     * gundo a necessidade do grafo de corridorToMerge. Ao final da
     * execução, os entranceNodes de corridorToMerge são adicionados
     * à coleção de entranceNodes deste SMERCorridor.
     */
    void mergeCorridor(SMERCorridor corridorToMerge) {
        /*
         * visitedNodes tem que ficar aqui fora, pois se o corridorToMerge tiver
         * mais de um entranceNode, pode ser que acabemos visitando o mesmo nó
         * duas vezes, um por cada entranceNode. Para evitar isso, checamos se já
         * visitamos aquele nó antes.
         */
        ArrayList<SMERCorridorNode> visitedNodes = new ArrayList<>();

        // para cada nó de entrada do corredor a ser fundido:
        for (int i = 0; i < corridorToMerge.entranceNodes.size(); i++) {
            SMERCorridorNode entranceNode = corridorToMerge.entranceNodes.get(i);
            // verifica antes se aquela entrada já não existe:
            if (this.entranceNodes.contains(entranceNode)) {
                Road entranceNodeIncomingRoad = corridorToMerge.entranceNodesIncomingRoads.get(i);

                for (int j = 0; j < this.entranceNodes.size(); j++) {
                    SMERCorridorNode node = this.entranceNodes.get(j);
                    if (node.equals(entranceNode))
                        if (this.entranceNodesIncomingRoads.get(j).getId() == entranceNodeIncomingRoad.getId()) {
                            ReportManager.err("Detectados dois corredores começando no mesmo nó de entrada e com a mesma incoming road!");
                            Thread.dumpStack();
                            System.exit(1);
                        }
                }
            }

            ArrayList<SMERCorridorNode> openNodes = new ArrayList<>();
            openNodes.add(entranceNode);

            while (openNodes.size() > 0) {
                SMERCorridorNode node = openNodes.remove(0);

                if (this.contains(node)) {
                    /*
                     * Se o nó procurado já existia, significa que
                     * encontramos uma interseção entre as árvores.
                     * Faz o merge ligando os nós downstream e
                     * upstream no ponto de interseção.
                     * É importante recuperar o nó equivalente no
                     * grafo já existente, caso contrário de nada
                     * vai adiantar já que não estaremos reutilizan-
                     * do os nó que já existiam. A recuperação aqui
                     * é feita na variável myNode.
                     */
                    SMERCorridorNode myNode = this.getNodeByIntersection(node.getIntersection());

                    /*
                     * Copia as ligações que o nó antigo tinha (tanto
                     * upstream quanto downstream) para o nó que já
                     * existia, fundindo a camada anterior com esta que
                     * está iterando.
                     */
                    for (SMERCorridorNode downstreamNode : node.getDownstreamNodes())
                        this.addDownstreamNode(myNode, downstreamNode);
                    for (SMERCorridorNode upstreamNode : node.getUpstreamNodes())
                        this.addUpstreamNode(myNode, upstreamNode);

                    /*
                     * Agora falta a recíproca, que é atualizar o grafo
                     * de corridorToMerge para que, quando descermos a
                     * partir do entranceNode dele, também encontremos o
                     * caminho fundido.
                     */
                    node.replaceMe(myNode);
                    /*
                     * Se o nó que está sendo substituído era um nó de
                     * entrada, temos que atualizar também o array entranceNodes
                     * do corridorToMerge.
                     */
                    if (corridorToMerge.entranceNodes.contains(node)) {
                        int pos = corridorToMerge.entranceNodes.indexOf(node);
                        corridorToMerge.entranceNodes.remove(pos);
                        corridorToMerge.entranceNodes.add(pos, myNode);
                    }
                }

                visitedNodes.add(node);

                for (SMERCorridorNode downstreamNode : node.getDownstreamNodes()) {
                    if (!visitedNodes.contains(downstreamNode) && !openNodes.contains(downstreamNode))
                        openNodes.add(downstreamNode);
                }
            }

            /*
             * Adiciona o entrance node somente ao fim do merge.
             * Se adicionar antes, o this.contains() vai falsamente avisar que já contém
             * os nós que estão sendo inseridos!
             */
            /*
             * Verifica se entranceNodes já contém aquele nó. Pode ocorrer
             * quando dois corredores começam numa mesma interseção. Nesse caso
             * não queremos manter duas cópias da interseção no entranceNodes, caso
             * contrário o cálculo do offset será gerado duas vezes para ela e
             * acarretará uma assincronia.
             */
            if (!this.entranceNodes.contains(entranceNode)) {
                /*
                 * Pode ser que o entranceNode do corredor sendo fundido tenha sido substituido
                 * por uma instancia que já existia anteriormente no grafo. Por exemplo:
                 *
                 * grafo original
                 *
                 *       i1    i2    i3
                 *       o-----o-----o
                 *
                 * grafo sendo fundido
                 *
                 *             i2
                 *             o
                 *             |
                 *             o i4
                 *
                 * Se isso acontecer, temos que garantir que é o i2 original que adicionaremos
                 * como entranceNode, pois caso contrário a referência apontará para uma instância
                 * diferente da que usamos efetivamente no grafo.
                 */
                SMERCorridorNode originalNode = this.getNodeByIntersection(entranceNode.getIntersection());
                this.entranceNodes.add(originalNode == null ? entranceNode : originalNode);
                this.entranceNodesIncomingRoads.add(corridorToMerge.entranceNodesIncomingRoads.get(i));
            }
        }
    }

    private class NodeDistanceTuple {
        private SMERCorridorNode node;
        private double distance;

        NodeDistanceTuple(SMERCorridorNode node, double distance) {
            this.node = node;
            this.distance = distance;
        }

        @Override
        public String toString() {
            return node + " (" + distance + "m)";
        }
    }

    private NodeDistanceTuple findDeepestIntersection_recursive(SMERCorridorNode intersectionNode, double distanceSoFar) {
        NodeDistanceTuple deepestKnown = new NodeDistanceTuple(intersectionNode, distanceSoFar);

        SignalizedIntersection intersection = intersectionNode.getIntersection();

        for (SMERCorridorNode downstreamNode : intersectionNode.getDownstreamNodes()) {
            Road outgoingRoad = intersection.getRoadDownstreamTo(downstreamNode.getIntersection());
            /*
             * O comprimento de travessia da interseção foi marretado em 12 metros.
             * O problema é que nem sempre eu vou ter uma RoadConnection ligando incomingRoad a outgoingRoad
             * O grafo não descreve conexões de legal movement, mas sim conexões de sincronia, portanto é
             * perfeitamente normal se não encontrarmos um RoadConnection incomingRoad->outgoingRoad.
             * 12m é o comprimento padrão de uma interseção com 2 vias cruzando perpendiculares entre si.
             */
            double currentIntersectionLength = 12; // intersection.getLength(incomingRoad, outgoingRoad);
            double distanceFromCurrentIntersectionToDownstreamIntersection = currentIntersectionLength + outgoingRoad.getLength();
            double iterDistance = distanceSoFar + distanceFromCurrentIntersectionToDownstreamIntersection;
            NodeDistanceTuple iterDeepest = findDeepestIntersection_recursive(downstreamNode, iterDistance);

            if (iterDeepest.distance > deepestKnown.distance) {
                deepestKnown = iterDeepest;
            }
        }

        ReportManager.log("\t" + deepestKnown);

        return deepestKnown;
    }

    /**
     * Este método estava sendo usado para encontrar o nó mais
     * profundo. Achando o nó, eu setava offset = 0 para ele e
     * vinha percorrendo o caminho upstream dele, incrementando
     * o offset proporcionalmente à distância percorrida.
     * <p>
     * Entretanto, descobri que o algoritmo é falho. Um contra-
     * exemplo abaixo:
     * <p>
     * Grade Manhattan (interseções):
     * <p>
     * 17 18 19 20
     * 13 14 15 16
     * 9 10 11 12
     * 5  6  7  8
     * 1  2  3  4
     * <p>
     * Corredores (interseções em ordem, começando pela interseção de entrada):
     * Corredor A : 5, 6, 7, 8
     * Corredor B : 15, 11, 7, 3
     * Corredor C : 13, 9, 5, 1
     * Corredor D : 12, 11, 10, 14, 18
     * <p>
     * O algoritmo vai encontrar a interseção 8 como mais profunda, porém
     * ao gerar os offsets vamos encontrar os seguintes valores:
     * <p>
     * Corredor A : 3, 2, 1, 0
     * Corredor B : 3, 2, 1, 0
     * Corredor C : 5, 4, 3, 2
     * Corredor D : 3, 2, 1, 0, -1
     * <p>
     * Veja que a interseção 18 ganhou offset negativo, o que não é permitido
     * pois seria complicado voltar o grafo SMER no tempo.
     * <p>
     * Eu acabei abandonando esse algoritmo pois percebi que não preciso de
     * tanta complicação... posso começar em qualquer nó, setar os offsets
     * gerando inclusive offsets negativos, depois descobrir o valor mais
     * baixo e normalizar por ele, garantindo que no final todos terão offset
     * positivo.
     */
    @SuppressWarnings("unused")
    private NodeDistanceTuple findDeepestIntersection() {
        NodeDistanceTuple deepestIntersection = null;
        for (SMERCorridorNode entranceNode : this.entranceNodes) {
            ReportManager.log("Entrance node: " + entranceNode);

            NodeDistanceTuple iterDeepest = findDeepestIntersection_recursive(entranceNode, 0d);

            if (deepestIntersection == null ||
                    iterDeepest.distance > deepestIntersection.distance)
                deepestIntersection = iterDeepest;

            ReportManager.log("Mais profundo desta entrada: " + iterDeepest);
        }

        ReportManager.log("Mais profundo de todos: " + deepestIntersection);
        return deepestIntersection;
    }

    /**
     * Método recursivo para setar offsets iniciais que num passo seguinte
     * serão normalizados e devidamente concretizados nos signal controllers.
     * Neste passo os offsets são apenas registrados, sem serem aplicados
     * às interseções pois ainda precisam ser normalizados. Durante a recursão
     * alguns offsets negativos serão gerados e por isso a posterior norma-
     * lização é necessária.
     *
     * @param startupLostTime      Tempo necessário para que o pelotão ganhe velocidade
     *                             e alcance a velocidade da onda.
     * @param greenWaveSpeedFactor Este parâmetro regula a velocidade da onda verde em
     *                             relação ao limite de velocidade da via. Se for 1,
     *                             a onda verde vai se deslocar no limite da velocidade.
     *                             Use valores inferiores para reduzir a velocidade da
     *                             onda proporcionalmente em relação ao limite da via.
     */
    private void presetOffsets(SMERCorridorNode node, double currentOffset, double startupLostTime, double greenWaveSpeedFactor) {
        /*
         * Só seta o offset caso ainda não tenha sido setado.
         * Essa verificação é o equivalente não recursivo de guardar
         * uma lista de nós visitados. Isso impede que o algoritmo
         * entre em loop infinito (o pai chama o filho, o filho chama o pai, ...)
         */
        if (!node.alreadyHasOffset()) {
            node.presetOffset(currentOffset);

            final double intersectionLength = 12; // marretado em 12m. Ver explicação em findDeepestIntersection_recursive(SMERCorridorNode, double)

            for (SMERCorridorNode downstreamNode : node.getDownstreamNodes()) {
                Road outgoingRoad = node.getIntersection().getRoadDownstreamTo(downstreamNode.getIntersection());
                double distance = intersectionLength + outgoingRoad.getLength();
                /*
                 * Calcula quanto tempo leva para sair da interseção atual e chegar na
                 * interseção downstream, usando como velocidade base a velocidade
                 * máxima da via.
                 * Leva em conta o startup lost time, que é o tempo necessário para o
                 * pelotão, que estava parado no sinal, desenvolva velocidade e seu
                 * headway se torne constante. Esse tempo é levado em conta adicionando
                 * um tempo extra necessário para chegar na interseção downstream. Esse
                 * tempo é conhecidamente 2s, segundo a litetura.
                 */
                double deltaTime = distance / (greenWaveSpeedFactor * outgoingRoad.getSpeedLimit()) + startupLostTime;
                /*
                 * Uma interseção downstream deve estar sempre atrasada em relação
                 * à interseção upstream (assim, quando abrir o sinal no upstream,
                 * vai dar tempo dos veículos chegarem no downstream sincronizados
                 * com aquela mesma onda verde.
                 */
                presetOffsets(downstreamNode, currentOffset - deltaTime, startupLostTime, greenWaveSpeedFactor);
            }

            for (SMERCorridorNode upstreamNode : node.getUpstreamNodes()) {
                Road incomingRoad = upstreamNode.getIntersection().getRoadDownstreamTo(node.getIntersection());
                double distance = intersectionLength + incomingRoad.getLength();
                /*
                 * Calcula quanto tempo leva para sair da interseção atual e chegar na
                 * interseção upstream, usando como velocidade base a velocidade
                 * máxima da via upstream.
                 * Leva em conta o startup lost time, que é o tempo necessário para o
                 * pelotão, que estava parado no sinal, desenvolva velocidade e seu
                 * headway se torne constante. Esse tempo é levado em conta adicionando
                 * um tempo extra necessário para chegar na interseção downstream. Esse
                 * tempo é conhecidamente 2s, segundo a litetura.
                 */
                double deltaTime = distance / (greenWaveSpeedFactor * incomingRoad.getSpeedLimit()) + startupLostTime;
                /*
                 * Uma interseção upstream deve estar sempre adiantada em relação
                 * à interseção downstream (assim, quando abrir o sinal no upstream,
                 * vai dar tempo dos veículos chegarem no downstream sincronizados
                 * com aquela mesma onda verde.
                 */
                presetOffsets(upstreamNode, currentOffset + deltaTime, startupLostTime, greenWaveSpeedFactor);
            }
        }
    }

    /**
     * @return maior offset encontrado.
     */
    private double setOffsets(double startupLostTime, double greenWaveSpeedFactor) {
        ReportManager.log("Iniciando calculo dos offsets das intersecoes coordenadas...");

        /*
         * Primeiro calcula um offset para cada interseção em
         * relação ao offset zero da interseção de entrada. Nesse
         * momento podem ser gerados offsets negativos (para inter-
         * seções que estejam upstream em relação ao nó de entrada),
         * porém eles serão normalizados no próximo passo.
         */
        presetOffsets(this.entranceNodes.get(0), 0d, startupLostTime, greenWaveSpeedFactor);

        return normalizeOffsets();
    }

    /**
     * @return maior offset encontrado.
     */
    private double normalizeOffsets() {
        double lowestOffset = Double.POSITIVE_INFINITY;

        for (SMERCorridorNode node : this.getNodeList()) {
            if (node.getOffset() < lowestOffset)
                lowestOffset = node.getOffset();
        }

        double baseOffset = Math.abs(lowestOffset);
        ReportManager.log(String.format("Normalizando offsets usando como base o offset %.1f...", baseOffset));

        double maxOffset = Double.NEGATIVE_INFINITY;

        for (SMERCorridorNode node : this.getNodeList()) {
            double offset = node.getOffset() + baseOffset;
            node.setOffset(offset);
            if (offset > maxOffset)
                maxOffset = offset;
        }

        return maxOffset;
    }

    private void initializeSignalControllers() {
        for (SMERCorridorNode node : this.getNodeList()) {
            node.initializeSignalController();
        }
    }

    double initializeIntersections(SMERGraph graph, int cycleLength, double startupLostTime, double greenWaveSpeedFactor) {
        // distribui o grafo SMER:
        for (SMERCorridorNode node : this.getNodeList()) {
            /*
             * Dá uma cópia do grafo e não o original, pois cada
             * interseção vai ter que adiantar sua cópia de acordo
             * com o offset em relação ao corredor.
             */
            node.setCoordinationGraph(new SMERGraph(graph), cycleLength);
        }

        // instrui cada interseção a setar o offset do grafo SMER de acordo com sua posição no corredor:
        double maxOffset = setOffsets(startupLostTime, greenWaveSpeedFactor);

        // finalmente, permite que os signal controllers executem seu método de inicialização:
        initializeSignalControllers();

        return maxOffset;
    }

    void setReversibilityChangePeriod(int cycleLength) {
        for (SMERCorridorNode node : this.getNodeList()) {
            node.setCoordinationCycleLength(cycleLength);
        }
    }

    private void placeCoordinationSensor(Road road) {
        double sensorPosition;

        /*
         * Sensor upstream.
         */
        sensorPosition = 5;
        VehiclePassageDetector upstreamSensor = new VehiclePassageDetector(road, sensorPosition);
        upstreamSensor.setTag(SENSOR_TAG_UPSTREAM);
        upstreamSensor.addListener(this);
        road.addVehicleDetector(upstreamSensor);
    }

    private void prepareCorridorConnections_recursive(Road incomingRoad, SMERCorridorNode node) {
        /*
         * Adiciona todas as RoadConnections que possuem incomingRoad no upstream.
         * A princípio só as conexões específicas com os nós downstream que participam
         * do corredor é que seriam adicionadas, porém há um problema com isso. Imagine
         * o seguinte cenário:
         * A via R1 desemboca na interseção I1. A partir de I1, R1 pode seguir para R2 ou para
         * R3 (ou seja, existem as RoadConnections (R1,R2) e (R1,R3)), porém o corredor
         * só compreende a ligação (R1,R2). O que aconteceria se não adicionássemos (R1,R3)
         * também nas conexões verdes do corredor? No momento em que o sinal ficasse verde
         * para R1, se houvesse um carro na fila querendo entrar em R3 ele ficaria retido
         * porque o sinal para ele está vermelho. Isso implica que qualquer veículo querendo
         * ir para R2 que estivesse atrás desse veículo também ficaria retido. Conclusão:
         * temos que adicionar todas as RoadConnections que tenham R1 como upstream, de modo
         * a não obstruir a passagem do corredor. Futuramente, quando forem implementadas
         * múltiplas faixas no simulador, pode haver uma faixa exclusiva para quem vai
         * virar a direita, à esquerda, etc... e aí faria sentido uma outra aproximação
         * mais seletiva.
         */
        SMERCoordinatedSignalController smerSignalController = (SMERCoordinatedSignalController) node.getIntersection().getSignalController();
        smerSignalController.promoteRoadConnectionsToCoordinated(incomingRoad, this.greenConnections);

        ReportManager.log("> RoadConnections de " + node + " mapeadas.");

        // para cada interseção downstream
        for (SMERCorridorNode downstreamNode : node.getDownstreamNodes()) {
            // recupera a via que liga esta interseção à interseção downstream:
            Road outgoingRoad = node.getIntersection().getRoadDownstreamTo(downstreamNode.getIntersection());

            // recursão para o nó downstream:
            prepareCorridorConnections_recursive(outgoingRoad, downstreamNode);
        }
    }

    /**
     * Monta a lista de RoadConnections que estarão ativas quando este
     * corredor tiver a vez de executar.
     */
    void prepareCorridorConnections() {
        this.greenConnections = new SignalPhase();

        /*
         * Coloca sensor na entrada do corredor, que vai servir para
         * observar a variação do fluxo que chega.
         */
        placeCoordinationSensor(this.entranceNodesIncomingRoads.get(0));

        ReportManager.log("Comecando mapeamento das RoadConnections do corredor " + this);
        prepareCorridorConnections_recursive(this.entranceNodesIncomingRoads.get(0), this.entranceNodes.get(0));
    }

    String dumpEntranceNodes() {
        StringBuilder result = new StringBuilder();

        for (SMERCorridorNode node : this.entranceNodes) {
            result.append(node.toString()).append(" ");
        }

        return result.toString();
    }

    private SMERCorridorNode getNodeByIntersection(SignalizedIntersection intersection) {
        for (SMERCorridorNode node : this.getNodeList()) {
            if (node.getIntersection().getId() == intersection.getId())
                return node;
        }

        return null;
    }

    /**
     * Gera lista com todos os nós deste SMERCorridor.
     *
     * @return lista gerada
     */
    ArrayList<SMERCorridorNode> getNodeList() {
        ArrayList<SMERCorridorNode> visitedNodes = new ArrayList<>();
        ArrayList<SMERCorridorNode> openNodes = new ArrayList<>(this.entranceNodes);

        while (openNodes.size() > 0) {
            SMERCorridorNode node = openNodes.remove(0);

            for (SMERCorridorNode downstreamNode : node.getDownstreamNodes()) {
                if (!visitedNodes.contains(downstreamNode) && !openNodes.contains(downstreamNode))
                    openNodes.add(downstreamNode);
            }
            visitedNodes.add(node);
        }

        return visitedNodes;
    }

    /**
     * Gera lista de todas as interseções participantes deste SMERCorridor.
     *
     * @return lista gerada
     */
    public ArrayList<SignalizedIntersection> getIntersections() {
        ArrayList<SignalizedIntersection> intersections = new ArrayList<>();
        for (SMERCorridorNode node : this.getNodeList()) {
            intersections.add(node.getIntersection());
        }
        return intersections;
    }

    String dumpNodes() {
        StringBuilder result = new StringBuilder();

        for (SMERCorridorNode node : this.getNodeList()) {
            result.append(node.toString()).append(" ");
        }

        return result.toString();
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    /**
     * Este método assume que quem chama está supondo
     * que este corredor só tem uma raiz, que é o mais
     * habitual.
     *
     * @return o nó de entrada com índice 0
     */
    public SMERCorridorNode getRoot() {
        return this.entranceNodes.get(0);
    }

    public SignalPhase getSignalPhase() {
        return this.greenConnections;
    }

    /**
     * Método chamado pelos sensores do corredor coordenado quando
     * ocorre um evento de interesse do SMERCorridor. Atualmente
     * esse evento é a passagem de um veículo.
     */
    @Override
    public void vehiclePassageEvent(VehiclePassageEvent event) {
        totalVehiclePassageCount++;
    }

    public long getVehiclePassageCount() {
        return this.totalVehiclePassageCount;
    }

    void resetVehiclePassageCount() {
        this.totalVehiclePassageCount = 0;
    }

    double getCurrentArrivalRate() {
        double totalRate = 0;
        for (InflowIntersection inflowIntersection : this.entranceNodesInflowIntersections) {
            totalRate += inflowIntersection.getCurrentArrivalRateInVehiclesPerHour();
        }
        return totalRate;
    }
}
