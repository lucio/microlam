package br.ufrj.lam.microlam.coordination;

import java.util.ArrayList;

import br.ufrj.lam.microlam.core.SignalizedIntersection;
import br.ufrj.lam.microlam.report.ReportManager;
import br.ufrj.lam.microlam.signalcontrol.SMERCoordinatedSignalController;
import br.ufrj.lam.smer.SMERGraph;

public class SMERCorridorNode {

    /**
     * Flag que indica se o offset desta interseção já foi
     * programado. Usado para evitar loops infinitos durante
     * a execução do algoritmo que seta os offsets no sistema
     * coordenado.
     */
    private boolean hasOffset;
    private double offset;
    private ArrayList<SMERCorridorNode> upstream, downstream;
    /**
     * Qual interseção este nó representa.
     */
    private SignalizedIntersection intersection;

    /**
     * Construtor.
     *
     * @param intersection interseção sendo representada por este nó
     */
    public SMERCorridorNode(SignalizedIntersection intersection) {
        this.upstream = new ArrayList<>();
        this.downstream = new ArrayList<>();
        this.intersection = intersection;
        this.hasOffset = false;
    }

    private void copyUpstream(SMERCorridorNode source) {
        for (SMERCorridorNode parent : source.upstream) {
            SMERCorridorNode newUpstream = new SMERCorridorNode(parent.getIntersection());
            this.upstream.add(newUpstream);
            newUpstream.addDownstreamNode(this);

            newUpstream.copyUpstream(parent);
        }
    }

    private void copyDownstream(SMERCorridorNode source) {
        for (SMERCorridorNode child : source.downstream) {
            SMERCorridorNode newDownstream = new SMERCorridorNode(child.getIntersection());
            this.downstream.add(newDownstream);
            newDownstream.upstream.add(this);

            newDownstream.copyDownstream(child);
        }
    }

    /**
     * Construtor de cópia.
     *
     * @param source Nó a ser copiado.
     */
    @SuppressWarnings("CopyConstructorMissesField")
    SMERCorridorNode(SMERCorridorNode source) {
        this.intersection = source.intersection;
        this.upstream = new ArrayList<>();
        this.downstream = new ArrayList<>();
        this.hasOffset = source.hasOffset;

        // a cópia dos ascendentes e descentes tem que ser feita em duas
        // partes. Inicialmente tinha feito um método só para tudo, porém
        // isso gera uma recursão infinita (nó n1 cria n2 como seu pai, porém
        // n2, ao ser criado, descobre que tem que criar n1 como seu filho,
        // e aí entra num loop infinito.
        // Separando em duas etapas funciona:
        this.copyUpstream(source);
        this.copyDownstream(source);
    }

    void addUpstreamNode(SMERCorridorNode upstreamNode) {
        if (!this.hasUpstreamNode(upstreamNode))
            this.upstream.add(upstreamNode);
    }

    void addDownstreamNode(SMERCorridorNode downstreamNode) {
        if (!this.hasDownstreamNode(downstreamNode))
            this.downstream.add(downstreamNode);
    }

    private void removeUpstreamNode(SMERCorridorNode node) {
        this.upstream.remove(node);
    }

    private void removeDownstreamNode(SMERCorridorNode node) {
        this.downstream.remove(node);
    }

    void replaceMe(SMERCorridorNode substitute) {
        for (SMERCorridorNode upstreamNode : this.upstream) {
            upstreamNode.removeDownstreamNode(this);
            upstreamNode.addDownstreamNode(substitute);
        }
        for (SMERCorridorNode downstreamNode : this.downstream) {
            downstreamNode.removeUpstreamNode(this);
            downstreamNode.addUpstreamNode(substitute);
        }
    }

    @Override
    public boolean equals(Object otherNode) {
        if (otherNode instanceof SMERCorridorNode) {
            return this.getIntersection() == ((SMERCorridorNode) otherNode).getIntersection();
        } else {
            return super.equals(otherNode);
        }
    }

    @Override
    public int hashCode() {
        return this.intersection.hashCode();
    }

    /**
     * Verifica se {@code node}  é um ascedente deste nó.
     *
     * @return {@code true}  se {@code node} é um ascendente.
     */
    boolean hasAscendant(SMERCorridorNode node) {
        ArrayList<SMERCorridorNode> visitedNodes = new ArrayList<>();
        ArrayList<SMERCorridorNode> openNodes = new ArrayList<>(this.upstream);
        while (openNodes.size() > 0) {
            SMERCorridorNode candidateNode = openNodes.remove(0);

            if (candidateNode.equals(node))
                return true;

            for (SMERCorridorNode upstreamNode : candidateNode.upstream) {
                if (!visitedNodes.contains(upstreamNode) && !openNodes.contains(upstreamNode))
                    openNodes.add(upstreamNode);
            }
            visitedNodes.add(candidateNode);
        }
        return false;
    }

    /**
     * Verifica se {@code node}  é um descedente deste nó.
     *
     * @return {@code true}  se {@code node} é um descendente.
     */
    boolean hasDescendant(SMERCorridorNode node) {
        ArrayList<SMERCorridorNode> visitedNodes = new ArrayList<>();
        ArrayList<SMERCorridorNode> openNodes = new ArrayList<>(this.downstream);
        while (openNodes.size() > 0) {
            SMERCorridorNode candidateNode = openNodes.remove(0);

            if (candidateNode.equals(node))
                return true;

            for (SMERCorridorNode downstreamNode : candidateNode.downstream) {
                if (!visitedNodes.contains(downstreamNode) && !openNodes.contains(downstreamNode))
                    openNodes.add(downstreamNode);
            }
            visitedNodes.add(candidateNode);
        }
        return false;
    }

    /**
     * Verifica se {@code node} é um descendente direto de mim.
     * Difere do hasDescendant() pois só olha um nível abaixo.
     */
    boolean hasDownstreamNode(SMERCorridorNode node) {
        return this.downstream.contains(node);
    }

    /**
     * Verifica se {@code node} é um ascendente direto de mim.
     * Difere do hasAscendant() pois só olha um nível acima.
     */
    boolean hasUpstreamNode(SMERCorridorNode node) {
        return this.upstream.contains(node);
    }

    ArrayList<SMERCorridorNode> getDownstreamNodes() {
        return new ArrayList<>(this.downstream);
    }

    ArrayList<SMERCorridorNode> getUpstreamNodes() {
        return new ArrayList<>(this.upstream);
    }

    /**
     * É considerado nó raiz se não possui nós upstream.
     */
    public boolean isRoot() {
        return this.upstream.size() == 0;
    }

    /**
     * Recupera a interseção sinalizada acima do nó atual.
     * Para isso precisamos buscar o nó pai e pegar a interseção dele.
     * Não podemos simplesmente chamar this.road.getUpstreamIntersection()
     * pois ela pode não fazer parte do sistema coordenado. Para ter
     * certeza, só perguntando ao nó pai (que pode ser que nem exista,
     * então nesse caso o nó atual é raiz e é um ponto de entrada
     * do sistema coordenado).
     * Obs: quem chama esta rotina assume que este nó só possui um
     * pai. Apesar do SMERCorridorNode permitir mais de um pai,
     * esse caso só faz sentido quando se faz um merge de vários cor-
     * redores. Normalmente o grafo é uma árvore e, assim, um nó
     * só pode ter um pai.
     */
    public SignalizedIntersection getUpstreamIntersection() {
        if (this.upstream.size() > 0) {
            return this.upstream.get(0).getIntersection();
        } else {
            return null;
        }
    }

    public SignalizedIntersection getIntersection() {
        return this.intersection;
    }

    public double getOffset() {
        return this.offset;
    }

    void presetOffset(double offsetInSeconds) {
        this.offset = offsetInSeconds;

        this.hasOffset = true;

        ReportManager.log("> Offset de " + this + " iniciado em " + offsetInSeconds + "s");
    }

    public void setOffset(double offsetInSeconds) {
        this.offset = offsetInSeconds;

        this.hasOffset = true;

        SMERCoordinatedSignalController signalController = (SMERCoordinatedSignalController) this.intersection.getSignalController();

        signalController.setOffset(offsetInSeconds);

        ReportManager.log("> Offset de " + this + " normalizado para " + offsetInSeconds + "s");
    }

    void setCoordinationGraph(SMERGraph graph, int cycleLength) {
        SMERCoordinatedSignalController signalController = (SMERCoordinatedSignalController) this.intersection.getSignalController();

        signalController.setCoordinationGraph(graph, 0, cycleLength);
    }

    void setCoordinationCycleLength(int cycleLength) {
        SMERCoordinatedSignalController signalController = (SMERCoordinatedSignalController) this.intersection.getSignalController();

        signalController.setCoordinationCycleLength(cycleLength);
    }

    @Override
    public String toString() {
        return this.intersection.toString();
    }

    boolean alreadyHasOffset() {
        return this.hasOffset;
    }

    void initializeSignalController() {
        SMERCoordinatedSignalController signalController = (SMERCoordinatedSignalController) this.intersection.getSignalController();
        signalController.initialize();
    }
}
