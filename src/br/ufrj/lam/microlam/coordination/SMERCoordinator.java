package br.ufrj.lam.microlam.coordination;

import java.util.ArrayList;
import java.util.HashMap;

import br.ufrj.lam.microlam.core.Rand;
import br.ufrj.lam.microlam.core.SignalizedIntersection;
import br.ufrj.lam.microlam.report.ReportManager;
import br.ufrj.lam.microlam.signalcontrol.SMERCoordinatedSignalController;
import br.ufrj.lam.microlam.signalcontrol.SignalControllerEvent;
import br.ufrj.lam.microlam.signalcontrol.SignalControllerListener;
import br.ufrj.lam.smer.SMEREngine;
import br.ufrj.lam.smer.SMERException;
import br.ufrj.lam.smer.SMERGraph;
import br.ufrj.lam.smer.SMERGraphBuilder;
import br.ufrj.lam.smer.SMERNode;

/**
 * Implementa um sistema de corredores coordenados de sinais SMER.
 * <p>
 * Restrições de uso:
 * - o sistema precisa necessariamente ter pelo menos 2 corredores e estes devem se cruzar em alguma interseção
 * Não é possível atualmente gerenciar corredores que não se cruzam, pois neste caso não há como formar
 * um grafo SMER - o grafo teria apenas um nó e executaria eternamente, porém temos que levar em conta que
 * existem vias transversais que precisam de algum tempo de verde
 * - cada corredor deve formar uma árvore cuja raiz é a interseção de início do corredor. Isso implica que não
 * deve haver ciclos!
 *
 * @author luciopaiva
 */
public class SMERCoordinator implements SignalControllerListener {
    /**
     * Tempo de duração de um ciclo de coordenação. Um ciclo de coordenação
     * é o tempo durante o qual um signal controller se mantém utilizando o
     * mesmo grafo SMER coordenado para operar. Ao fim do ciclo, o SMERCoordinator
     * deve computar um novo grafo e passar ao signal controller para início
     * de um novo ciclo.
     * O valor do tempo de duração é medido em número de fases do controlador.
     */
    private static final int COORDINATED_CYCLE_LENGTH = 20;  // em número de fases do controlador
    private int coordinatedCycleLength = COORDINATED_CYCLE_LENGTH;

    /**
     * tempo em segundos que demora para que o pelotão, saindo do
     * sinal vermelho, leva para restabelecer o headway.
     */
    private static final double STARTUP_LOST_TIME = 2;
    private double startupLostTime = STARTUP_LOST_TIME;
    /**
     * fator de calculo da velocidade das ondas verdes dos
     * corredores coordenados. se for 1, a onda verde viaja
     * na velocidade limite da via; abaixo disso, viaja num
     * percentual da velocidade limite da via.
     */
    private static final double GREEN_WAVE_SPEED_FACTOR = 1;
    private double greenWaveSpeedFactor = GREEN_WAVE_SPEED_FACTOR;

    /**
     * Usado para construir o grafo SMER descrevendo a interação entre os corredores concorrentes.
     */
    private SMERGraphBuilder graphBuilder;
    /**
     * Engine que vai rodar o grafo de sincronização dos corredores.
     */
    private SMEREngine engine;
    /**
     * Guarda um histórico de todos os grafos criados para cada ciclo
     * de coordenação até então. Um ciclo de coordenação é um determinado
     * tempo em que o sistema está avaliando os fluxos de entrada para
     * então (ao final do ciclo) promover as mudanças de reversibilidade
     * de acordo.
     */
    private HashMap<Integer, SMERGraph> graphs;

    private ArrayList<SMERCorridor> corridors;
    /**
     * Este é um SMERCorridor especial que guarda
     * um grafo com todos os corredores mapeados.
     * Ele foi criado para descobrir qual é a inter-
     * seção mais profunda do sistema e poder assim
     * usá-la como referência para os offsets de
     * cada interseção.
     */
    private SMERCorridor mergedCorridors;

    /**
     * Guarda um mapa de cor -> peso.
     * Usado para cálculo das novas reversibilidades de cada grupo de corredor.
     */
    private HashMap<Integer, Double> colorWeights;
    /**
     * Guarda um mapa de corredor -> cor.
     * Usado para identificar a cor de cada corredor ao agregar as demandas dos grupos.
     */
    private HashMap<SMERCorridor, Integer> corridorColors;

    public SMERCoordinator() {
        graphBuilder = new SMERGraphBuilder();
        corridors = new ArrayList<>();

        colorWeights = new HashMap<>();
        corridorColors = new HashMap<>();

        this.graphs = new HashMap<>();
    }

    public void addCorridor(SMERCorridor corridor) {
        this.corridors.add(corridor);
        this.graphBuilder.addNode(corridor);
    }

    /**
     * @return maior offset encontrado entre as interseções coordenadas
     */
    public double initialize() {
        ReportManager.logTitle("SMERCOORDINATOR STARTUP");

        if (corridors.size() < 2) {
            ReportManager.err("Precisa haver pelo menos 2 corredores no sistema SMERCoordinator para que o mecanismo possa operar.");
            System.exit(1);
        }

        // prepara o objeto SignalPhase para atender às requisições dos signal controllers:
        prepareCorridorsConnections();

        // cria arestas do grafo SMER:
        buildSMERGraphEdges();

        // roda o algarestas e deixa grafo pronto para executar:
        prepareSMERGraph();

        groupSMERGraphByColors();

        // une todos os corredores num grafo só:
        mergeCorridors();

        // seta o offset e dá a cada interseção uma cópia do grafo SMER de coordenação dos corredores:
        return initializeIntersections();
    }

    private void prepareCorridorsConnections() {
        for (SMERCorridor corridor : this.corridors) {
            corridor.prepareCorridorConnections();
        }
    }

    /**
     * Funde todos os corredores num grande digrafo.
     * O digrafo resultante servirá para fazer a busca do nó mais profundo, isto é,
     * o nó nfarthest cujo caminho para chegar a ele é maior que o caminho do nó n,
     * com n pertencente ao conjunto dos nós do digrafo com exceção do próprio nfarthest.
     */
    private void mergeCorridors() {
        // copia o primeiro corredor. ele será a base para o merge:
        mergedCorridors = new SMERCorridor("merged corridors", corridors.get(0));

        // para cada outro corredor:
        for (int i = 1; i < corridors.size(); i++) {
            mergedCorridors.mergeCorridor(new SMERCorridor(null, corridors.get(i)));
        }

        /*
         * Lista a coleção final de nós após o merge de todos os corredores num só mapeamento.
         */
        ReportManager.log("Merged nodes: " + mergedCorridors.dumpNodes());
        /*
         * Lista a coleção de nós de entrada. Vai haver um para cada corredor que existia originalmente.
         */
        ReportManager.log("Entrance nodes: " + mergedCorridors.dumpEntranceNodes());
    }

    private double initializeIntersections() {
        double maxOffset = mergedCorridors.initializeIntersections(
                this.engine.graph(), coordinatedCycleLength, this.startupLostTime, this.greenWaveSpeedFactor);

        /*
         * Adiciona este SMERCoordinator como listener de todos os signal controllers
         * envolvidos nos corredores, assim eles poderão se comunicar quando for ne-
         * cessário promover uma mudança de reversibilidade.
         */
        for (SMERCorridorNode node : mergedCorridors.getNodeList()) {
            SignalizedIntersection intersection = node.getIntersection();
            SMERCoordinatedSignalController signalController = (SMERCoordinatedSignalController) intersection.getSignalController();
            /*
             * Adiciona SMERCoordinator como listener do controller para ser
             * informado toda vez que o controller requisitar novo ciclo
             * coordenado.
             */
            signalController.addEventListener(this);
        }

        return maxOffset;
    }

    public void setReversibilityChangePeriod(int cycleLength) {
        this.coordinatedCycleLength = cycleLength;
        if (mergedCorridors != null)
            mergedCorridors.setReversibilityChangePeriod(this.coordinatedCycleLength);
        else {
            ReportManager.err("mergedCorridors não inicializado!");
            System.exit(1);
        }
    }

    private void prepareSMERGraph() {
        SMERGraph graph = graphBuilder.graph();
        /*
         * Guarda o grafo inicial do primeiro ciclo do sistema.
         */
        this.engine = new SMEREngine(graph);
        try {
            this.engine.prepareToRun();
            this.graphs.put(0, engine.graph());
        } catch (SMERException e) {
            e.printStackTrace();
        }
    }

    /**
     * Executa o AlgCor para definir uma configuração
     * acíclica inicial.
     * <p>
     * 1. Para cada nó aberto, sorteia um número inteiro.
     * 2. Para cada aresta, orienta a aresta para o nó
     * com maior número sorteado e marca os nós como
     * fechados. Caso os valores sejam iguaism, não
     * orienta a aresta.
     * 3. Vá para 1 enquanto houver arestas não orientadas
     */
    private void groupSMERGraphByColors() {
        HashMap<SMERNode, Long> nodeValues = new HashMap<>();
        HashMap<SMERNode, SMERCorridor> nodeCorridors = new HashMap<>();

        ArrayList<SMERNode> probabilisticNodes = new ArrayList<>();

        /*
         * Inicia array de nós probabilísticos.
         */
        for (SMERCorridor corridor : this.corridors) {
            SMERNode node = this.graphBuilder.graph().getNodeById(corridor);
            nodeCorridors.put(node, corridor);
            probabilisticNodes.add(node);
        }

        /*
         * Opera enquanto houver nós não coloridos.
         */
        while (probabilisticNodes.size() > 0) {
            nodeValues.clear();
            // cada nó probabilístico joga um dado:
            for (SMERNode n : probabilisticNodes) {
                nodeValues.put(n, Rand.uniformLong());
            }

            ArrayList<SMERNode> probabilisticNodesCopy = new ArrayList<>(probabilisticNodes);
            // agora achamos os nós que obtiveram valor maior que seus vizinhos probabilisticos e colorimos eles
            for (SMERNode n : probabilisticNodesCopy) {
                boolean wonTheToss = true;
                // para cada vizinho:
                for (SMERNode neighbor : n.getNeighbors()) {
                    // se o vizinho é probabilístico e o valor dele é maior que o meu, eu perdi essa rodada:
                    if (nodeValues.containsKey(neighbor) && nodeValues.get(neighbor) > nodeValues.get(n)) {
                        wonTheToss = false;
                        break;
                    }
                }

                // o nó ganhou de seus vizinhos e vai escolher uma cor:
                if (wonTheToss) {
                    // a preferência é pela cor de menor índice possível:
                    int myColor = 0;
                    ArrayList<Integer> neighborColors = new ArrayList<>();
                    // coleta as cores que meus vizinhos possuem:
                    for (SMERNode neighbor : n.getNeighbors()) {
                        // se meu vizinho já foi colorido:
                        SMERCorridor neighborCorridor = nodeCorridors.get(neighbor);
                        if (corridorColors.containsKey(neighborCorridor) && !neighborColors.contains(corridorColors.get(neighborCorridor)))
                            // ...incrementa minha cor:
                            neighborColors.add(corridorColors.get(neighborCorridor));
                    }

                    // encontra o menor índice possível que eu posso usar como cor:
                    while (neighborColors.contains(myColor))
                        myColor++;

                    corridorColors.put(nodeCorridors.get(n), myColor);
                    probabilisticNodes.remove(n);
                }
            }
        }

        for (SMERCorridor corridor : this.corridors) {
            ReportManager.log(String.format("Corridor %s color: %d", corridor, corridorColors.get(corridor)));
        }
    }

    @SuppressWarnings("unused")
    private void dumpSMERGraph() {
        ReportManager.log(graphBuilder.graph().toString());
        SMEREngine engine = new SMEREngine(graphBuilder.graph());
        try {
            for (int i = 0; i < 10; i++) {
                engine.prepareToRun();
                engine.dumpPeriod();
            }
        } catch (SMERException e) {
            e.printStackTrace();
        }
    }

    /**
     * Constroi arestas do grafo SMER mapeando as exclusões mútuas que devem
     * existir entre os corredores do sistema.
     */
    private void buildSMERGraphEdges() {
        ReportManager.log("Mapeamento das exclusões mútuas entre os corredores:");
        /*
         * Esses dois loops mais externos fazem combinação 2 a 2
         * de cada corredor existente no sistema coordenador:
         */
        for (int i = 0; i < corridors.size() - 1; i++) {
            for (int j = i + 1; j < corridors.size(); j++) {
                SMERCorridor corridorA = corridors.get(i);
                SMERCorridor corridorB = corridors.get(j);

                /*
                 * Combina cada interseção do corredor A com cada interseção do
                 * corredor B e verifica se eles possuem alguma em comum.
                 */
                for (SMERCorridorNode nodeA : corridorA.getNodeList()) {
                    for (SMERCorridorNode nodeB : corridorB.getNodeList()) {
                        // se encontrou uma interseção que está presente nos dois corredores:
                        if (nodeA.equals(nodeB)) {
                            ReportManager.log("> A interseção " + nodeA + " está presente tanto no corredor " +
                                    corridorA.getName() + " quanto no corredor " + corridorB.getName());

                            try {
                                graphBuilder.addEdge(corridorA, corridorB);
                            } catch (SMERException e) {
                                ReportManager.err("Aresta não pôde ser inicializada devidamente.");
                                e.printStackTrace();
                                System.exit(1);
                            }
                        }
                    }
                }
            }
        }
    }

    private void processSensorDataAndComputeNewGraphGroupedByColors(int cycleId) {
        ReportManager.log("Criando reversibilidades para o ciclo #" + cycleId + " do SMERCoordinator:");

        HashMap<Object, Double> corridorWeights = new HashMap<>();

        /*
         * Para cada grupo de corredores de uma mesma cor,
         * pega o maior peso dentre os corredores daquele grupo.
         */
        double weightSum = 0d;
        // é importante zerar os pesos para que os pesos calculados na mudanca de reversibilidade anterior nao atrapalhem
        colorWeights = new HashMap<>();
        for (SMERCorridor corridor : this.corridors) {
            /*
             * Lucio, 17 out 2012
             * Fiz uma modificação aqui para passar a pegar o
             * valor real da taxa de chegada no corredor, em
             * vez de pegar o valor aferido; fiz isso para
             * concentrar em validar o modelo dos corredores,
             * pois não estou interessado necessariamente em
             * validá-lo com um sistema de aferição específico.
             */
            double weight = corridor.getCurrentArrivalRate();
            System.out.println(String.format("PESO: %.1f", weight));
            corridor.resetVehiclePassageCount();

            weightSum += weight;
            double groupWeight = colorWeights.get(corridorColors.get(corridor)) != null ?
                    colorWeights.get(corridorColors.get(corridor)) : 0;

            colorWeights.put(corridorColors.get(corridor), Math.max(groupWeight, weight));

            ReportManager.log("> Peso não normalizado para o grupo " + corridorColors.get(corridor) + ": " +
                    colorWeights.get(corridorColors.get(corridor)));
        }

        /*
         * O peso de um corredor passa a ser então o
         * maior dos pesos dos corredores de seu grupo.
         */
        for (SMERCorridor corridor : this.corridors) {
            Double weight = colorWeights.get(corridorColors.get(corridor));
            double normalizedWeight = weight / weightSum;

            corridorWeights.put(corridor, normalizedWeight);

            ReportManager.log(String.format("> Peso normalizado para o corredor %s: %.2f", corridor, normalizedWeight));
        }

        this.engine.setNodeExecutionSlices(corridorWeights);

        /*
         * Tira uma cópia do grafo após a mudança de reversibilidade e
         * guarda como original a ser usado para gerar as cópias para
         * cada signal controller.
         */
        this.graphs.put(cycleId, new SMERGraph(this.engine.graph()));

        try {
            SMEREngine tempEngine = new SMEREngine(new SMERGraph(this.engine.graph()));
            tempEngine.dumpPeriod();
        } catch (SMERException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método chamado por um SMERCoordinatedSignalController quando ele
     * completa um ciclo de coordenação. Nesse momento o SMERCoordinator
     * precisa passar ao signal controller um novo grafo que vai ditar o
     * funcionamento das fases durante o novo ciclo.
     */
    @Override
    public void signalControllerEvent(SignalControllerEvent signalControllerEvent) {
        if (signalControllerEvent.getEventKind() == SignalControllerEvent.EventKind.signalControllerCoordinatedCycleCompleted) {
            SMERCoordinatedSignalController signalController = (SMERCoordinatedSignalController) signalControllerEvent.getSignalController();
            int cycleId = signalController.getCurrentCoordinatedCycleId();

            ReportManager.log("SignalController " + signalControllerEvent.getSignalController() + " pedindo novo ciclo ao SMERCoordinator.");

            /*
             * Incrementa para o próximo ciclo.
             */
            cycleId++;

            /*
             * Verifica se já exista um ciclo coordenado computado com o índice requisitado.
             */
            if (this.graphs.get(cycleId) == null) {
                /*
                 * Caso não exista ainda (lazy), computa novo ciclo coordenado.
                 * A computação envolve coletar todos os dados gerados pelos
                 * sensores nas vias e calcular as novas reversibilidades dos nós.
                 */
                processSensorDataAndComputeNewGraphGroupedByColors(cycleId);
            }

            signalController.setCoordinationGraph(new SMERGraph(this.graphs.get(cycleId)), cycleId, coordinatedCycleLength);
        }
    }

    /**
     * @param startupLostTime tempo em segundos que demora para que o pelotão, saindo do
     *                        sinal vermelho, leva para restabelecer o headway.
     */
    public void setCoordinatedStartupLostTime(double startupLostTime) {
        this.startupLostTime = startupLostTime;
    }

    public void setCoordinatedGreenWaveSpeedFactor(double speedFactor) {
        this.greenWaveSpeedFactor = speedFactor;
    }
}
