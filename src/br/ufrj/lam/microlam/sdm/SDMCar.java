package br.ufrj.lam.microlam.sdm;

public class SDMCar extends SDMVehicle {
    private static final double TOP_ACCELERATION = 0.5;
    private static final double TOP_BREAKING = -3.0;
    private static final double TOP_VELOCITY = 120.0 / 3.6; // 120km/h para m/s

    // medidas retiradas de http://en.wikipedia.org/wiki/Fiat_Uno
    private static final double LENGTH = 3.7d;
    private static final double WIDTH = 1.6d;

    public SDMCar(long id) {
        super(id, TOP_ACCELERATION, TOP_BREAKING, TOP_VELOCITY, LENGTH, WIDTH);
    }

    public SDMCar(SDMCar source) {
        super(source);
    }
}
