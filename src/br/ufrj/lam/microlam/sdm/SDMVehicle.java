package br.ufrj.lam.microlam.sdm;

import java.awt.Color;

import br.ufrj.lam.microlam.core.Vehicle;

public abstract class SDMVehicle extends Vehicle {
    private final double topAcceleration;
    private final double topBreaking;
    private final double topVelocity;
    private final double reducedBreaking;    // A frenagem reduzida é equivalente a aceleração máxima

    private final double length;
    private final double width;

    private final double minimumDistance = 2.0;

    SDMVehicle(long id, double topAcceleration, double topBreaking, double topVelocity, double length, double width) {
        super(id);

        this.topAcceleration = topAcceleration;
        this.topBreaking = topBreaking;
        this.topVelocity = topVelocity;
        this.reducedBreaking = -topAcceleration;

        this.length = length;
        this.width = width;
    }

    SDMVehicle(SDMVehicle source) {
        super(source);

        this.topAcceleration = source.topAcceleration;
        this.topBreaking = source.topBreaking;
        this.topVelocity = source.topVelocity;
        this.reducedBreaking = -topAcceleration;

        this.length = source.length;
        this.width = source.width;
    }

    @Override
    public void calculateAccelerationAndVelocity(double dt) {
        // Distância segura para poder frear até o carro da frente com frenagem máxima
        double safeBreakingDistance = (-1.0 * getCurrentVelocity() * getCurrentVelocity()) / (2.0 * topBreaking);

        // Distância segura para poder frear até o carro da frente com frenagem reduzida
        double safeBreakingDistanceWithReducedBreaking = (-1.0 * getCurrentVelocity() * getCurrentVelocity()) / (2.0 * reducedBreaking);

        double newAcceleration;
        double newVelocity;

        if (getDistanceToFrontVehicle() > safeBreakingDistanceWithReducedBreaking) {
            newAcceleration = topAcceleration;
            setCurrentAcceleration(newAcceleration);
        } else if (getDistanceToFrontVehicle() > safeBreakingDistance) {
            newAcceleration = reducedBreaking;
            setCurrentAcceleration(newAcceleration);
        } else {
            newAcceleration = topBreaking;
            setCurrentAcceleration(newAcceleration);
        }

        // Se o veículo não vai conseguir parar antes de bater, então pará-lo instantaneamente
        if (getDistanceToFrontVehicle() < (minimumDistance + (getCurrentVelocity() * (dt / 1000.0) + newAcceleration * (dt / 1000.0) * (dt / 1000.0) / 2.0))) {
            newAcceleration = 0;
            setCurrentAcceleration(newAcceleration);

            newVelocity = 0;
            setCurrentVelocity(newVelocity);
        } else {
            newVelocity = getCurrentVelocity() + newAcceleration * (dt / 1000.0);
            setCurrentVelocity(newVelocity);
            if (newVelocity > topVelocity) {
                newVelocity = topVelocity;
                setCurrentVelocity(newVelocity);
            }
            if (getCurrentVelocity() < 0) {
                newVelocity = 0;
                setCurrentVelocity(newVelocity);
            }
        }
    }

    @Override
    public void calculateStartingVelocity() {
        // FIXME:Daniel Velocidade inicial está marratada
        setCurrentVelocity(30 / 3.6d); // 30km/h
    }

    @Override
    public double getMinimumDistanceToFrontVehicle() {
        return minimumDistance;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public Color getColor() {
        return Color.ORANGE;
    }
}
