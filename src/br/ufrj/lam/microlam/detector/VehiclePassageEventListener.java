package br.ufrj.lam.microlam.detector;

public interface VehiclePassageEventListener {

    void vehiclePassageEvent(VehiclePassageEvent event);
}
