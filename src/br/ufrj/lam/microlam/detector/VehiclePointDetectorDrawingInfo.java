package br.ufrj.lam.microlam.detector;

import java.awt.geom.Point2D;

public interface VehiclePointDetectorDrawingInfo {

    Point2D.Double getUpstreamPosition();

    Point2D.Double getDownstreamPosition();

    double getRoadWidth();

    double getAngle();
}
