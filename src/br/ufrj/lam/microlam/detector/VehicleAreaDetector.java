package br.ufrj.lam.microlam.detector;

import java.util.HashMap;

import br.ufrj.lam.microlam.core.RoadConnection;
import br.ufrj.lam.microlam.core.SignalPhase;
import br.ufrj.lam.microlam.signalcontrol.VehicleDetector;

public class VehicleAreaDetector implements VehicleDetector {
    private static final long PERIOD_IN_MINUTES = 15 * 60; // 15 min
    private static final long DETECTOR_IS_DEACTIVATED = Long.MAX_VALUE;
    private long cycleLength = PERIOD_IN_MINUTES;
    private HashMap<SignalPhase, Double> phaseWeights;
    private long timeToNextPeriod = PERIOD_IN_MINUTES;
    private double lastDetectorQuery;

    public VehicleAreaDetector() {
        phaseWeights = new HashMap<>();
    }

    /**
     * @param cycleLength tamanho do ciclo, em minutos.
     */
    public void setReversibilityChangePeriod(int cycleLength) {
        if (cycleLength == 0) {
            this.cycleLength = DETECTOR_IS_DEACTIVATED;
        } else {
            this.cycleLength = cycleLength * 60; // converte para segundos
        }

        this.restart();
    }

    @Override
    public void addPhase(SignalPhase signalPhase) {
        phaseWeights.put(signalPhase, 0d); // inicia
    }

    public void removePhase(SignalPhase signalPhase) {
        this.phaseWeights.remove(signalPhase);
    }

    @Override
    public void step(double executionTime) {
        /*
         * Caso o período seja zero, trata como se o detetor tivesse
         * sido desativado. A mudança de reversibilidade nunca vai
         * ocorrer neste caso.
         */
        if (this.cycleLength == DETECTOR_IS_DEACTIVATED)
            return;

        // invoca detetor de veículos 1 vez por segundo:
        if ((lastDetectorQuery < Math.floor(executionTime)) &&
                (timeToNextPeriod > 0)) {
            lastDetectorQuery = (long) Math.floor(executionTime);

            /*
             * A ideia aqui é contar a quantidade de carros em cada via
             * de chegada a esta interseção e calcular a porcentagem de
             * cada uma em relação ao somatório de todas. Isso dá uma
             * ideia de peso de tráfego em cada via de chegada. Isso é
             * calculado 1 vez por segundo.
             */
            for (SignalPhase signalPhase : this.phaseWeights.keySet()) {  // para cada fase
                long phaseVehicleCount = 0;
                double phaseRoadLengthSum = 0;

                for (RoadConnection roadConnection : signalPhase.getConnections()) // para cada legal movement da fase
                {
                    phaseVehicleCount += roadConnection.getIncomingRoad().getVehicleCount();
                    phaseRoadLengthSum += roadConnection.getIncomingRoad().getLength();
                }

                double phaseVehiclePerMeter = phaseVehicleCount / phaseRoadLengthSum;

                phaseWeights.put(signalPhase, phaseWeights.get(signalPhase) + phaseVehiclePerMeter);
            }

            --timeToNextPeriod;
        }

    }

    public boolean hasEnoughData() {
        return timeToNextPeriod == 0;
    }

    public HashMap<Object, Double> getPhaseWeights() {
        HashMap<Object, Double> normalizedWeights = new HashMap<>();

        /*
         * Após X minutos de detecções, pegamos todas as amostras
         * observadas e fazemos uma média, para então determinar
         * quanto cada fase deve ganhar de verde percentualmente
         * em relação ao tempo total de verde disponível para ca-
         * da ciclo do semáforo.
         * O cálculo da média é importante para evitar variações
         * bruscas. Outros métodos podem ser usados, como um que
         * envolva uma histerese.
         */
        // TODO implementar método de teste com histerese para mudança das reversibilidades
        if (hasEnoughData()) {
            double totalWeightSum = 0d;
            // computa total de pesos resultantes dos veículos por metro contados em todas as aproximações:
            for (double phaseWeight : phaseWeights.values()) {
                totalWeightSum += phaseWeight;
            }

            // normaliza os pesos de cada fase:
            for (SignalPhase signalPhase : this.phaseWeights.keySet()) {
                normalizedWeights.put(signalPhase, phaseWeights.get(signalPhase) / totalWeightSum);
            }

            return normalizedWeights;
        }
        return null;
    }

    public void restart() {
        // reinicia pesos de cada fase:
        for (SignalPhase signalPhase : phaseWeights.keySet()) {
            phaseWeights.put(signalPhase, 0d);
        }

        timeToNextPeriod = this.cycleLength;
    }

    @Override
    public java.awt.geom.Point2D.Double getUpstreamPosition() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public java.awt.geom.Point2D.Double getDownstreamPosition() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getRoadWidth() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public double getAngle() {
        // TODO Auto-generated method stub
        return 0;
    }
}
