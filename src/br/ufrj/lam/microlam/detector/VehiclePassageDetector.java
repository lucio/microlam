package br.ufrj.lam.microlam.detector;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;

import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.SignalPhase;
import br.ufrj.lam.microlam.core.Vehicle;
import br.ufrj.lam.microlam.signalcontrol.VehicleDetector;

public class VehiclePassageDetector implements VehicleDetector {

    private ArrayList<VehiclePassageEventListener> listeners;
    /**
     * Via na qual o sensor está instalado.
     */
    private Road road;
    /**
     * Posição da via em que o sensor foi colocado (medida em metros
     * em relação ao ponto upstream da via).
     */
    private double position;
    private ArrayList<Vehicle> previousVehiclesDetected;
    /**
     * Variável auxiliar para possibilitar ao gerenciador
     * deste detector diferenciá-lo de outros detectores
     * que ele esteja gerenciando na mesma via.
     */
    private long tag;

    /**
     * Construtor.
     *
     * @param road     via onde o sensor está sendo instalado.
     * @param position posição em que ele vai ficar na via, em metros e em relação ao ponto upstream.
     */
    public VehiclePassageDetector(Road road, double position) {
        this.road = road;
        this.position = position;

        this.listeners = new ArrayList<>();
        this.previousVehiclesDetected = new ArrayList<>();
    }

    public void setTag(long tag) {
        this.tag = tag;
    }

    public long getTag() {
        return this.tag;
    }

    /**
     * Cada execução de step() verifica se algum veículo novo acabou de
     * cruzar o sensor e dispara um evento para cada novo veículo.
     *
     * @param executionTime tempo em milissegundos desde o último step.
     */
    @Override
    public void step(double executionTime) {
        ArrayList<Vehicle> currentVehiclesDetected = new ArrayList<>();

        /*
         * Calcula o espaço percorrido por um veículo que viaja a 300km/h
         * durante o tempo de execução de um step.
         */
        double positionDelta = (300 / 3.6d) * (executionTime / 1000d);

        for (Vehicle vehicle : this.road.getVehicles()) {
            // ignora veículos que ainda não passaram pelo sensor:
            if (vehicle.getCurrentPosition() < this.position)
                continue;

            // ignora veículos que já passaram pelo sensor:
            if (vehicle.getCurrentPosition() > this.position + positionDelta)
                continue;

            currentVehiclesDetected.add(vehicle);

            // ignora os veículos que ainda estão na zona do sensor mas já
            // foram detectados num step anterior:
            if (previousVehiclesDetected.contains(vehicle))
                continue;

            // os veículos que sobram estão na zona de detecção do sensor
            // e sendo detectados pela primeira vez:
            broadcastEvent(new VehiclePassageEvent(vehicle, this.road));
        }

        this.previousVehiclesDetected = currentVehiclesDetected;
    }

    public void addListener(VehiclePassageEventListener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    private void broadcastEvent(VehiclePassageEvent event) {
        for (VehiclePassageEventListener listener : listeners)
            listener.vehiclePassageEvent(event);
    }

    @Override
    public void addPhase(SignalPhase signalPhase) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removePhase(SignalPhase signalPhase) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean hasEnoughData() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public HashMap<Object, Double> getPhaseWeights() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void restart() {
        // TODO Auto-generated method stub
    }

    /**
     * Usado para desenhar o sensor na UI na posição correta da via.
     */
    @Override
    public Point2D.Double getUpstreamPosition() {
        return this.road.convertRoadPositionToCartesianPosition(this.position);
    }

    /**
     * Usado para desenhar o sensor na UI com o comprimento devido.
     */
    @Override
    public Point2D.Double getDownstreamPosition() {
        return this.road.convertRoadPositionToCartesianPosition(this.position - 1); // marretado em 1 metro apenas para visualização - não modifica em nada a simulação
    }

    @Override
    public double getRoadWidth() {
        return this.road.getRoadWidth();
    }

    @Override
    public double getAngle() {
        return this.road.getAngle();
    }
}
