package br.ufrj.lam.microlam.detector;

import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.Vehicle;

public class VehiclePassageEvent {

    private Vehicle vehicle;
    private Road road;

    VehiclePassageEvent(Vehicle vehicle, Road road) {
        this.vehicle = vehicle;
        this.road = road;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public Road getRoad() {
        return road;
    }
}
