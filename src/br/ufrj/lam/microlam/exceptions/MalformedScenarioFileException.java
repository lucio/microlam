package br.ufrj.lam.microlam.exceptions;

import br.ufrj.lam.microlam.core.Scenario;

/**
 * Exceção que é lançada ao ler um arquivo de cenário inválido.
 *
 * @author Flávio Faria
 * @see Scenario
 * @since Feb-12-2012
 */
public class MalformedScenarioFileException extends Exception {

    public MalformedScenarioFileException(String message) {
        super(message);
    }

    public MalformedScenarioFileException(String message, Throwable cause) {
        super(message, cause);
    }
}
