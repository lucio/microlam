package br.ufrj.lam.microlam.exceptions;

public class UnknownVehicleContainerException extends RuntimeException {

    public UnknownVehicleContainerException(String message) {
        super(message);
    }
}
