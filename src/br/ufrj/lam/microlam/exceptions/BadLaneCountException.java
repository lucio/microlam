package br.ufrj.lam.microlam.exceptions;

public class BadLaneCountException extends RuntimeException {

    public BadLaneCountException(String message) {
        super(message);
    }
}
