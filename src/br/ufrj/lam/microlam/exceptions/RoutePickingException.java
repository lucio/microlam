package br.ufrj.lam.microlam.exceptions;

public class RoutePickingException extends Exception {

	public RoutePickingException(String message) {
		super(message);
	}
}
