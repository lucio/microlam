package br.ufrj.lam.microlam.exceptions;

public class StepTooBigException extends Exception {

    public StepTooBigException(String message) {
        super(message);
    }
}
