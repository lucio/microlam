package br.ufrj.lam.microlam.exceptions;

public class InvalidRoadIdException extends Exception {

    public InvalidRoadIdException(String message) {
        super(message);
    }
}
