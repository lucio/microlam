package br.ufrj.lam.microlam.exceptions;

public class MicroLAMUnkownException extends Exception {

    public MicroLAMUnkownException(String message) {
        super(message);
    }
}
