package br.ufrj.lam.microlam.exceptions;

import br.ufrj.lam.microlam.core.Scenario;

/**
 * Exceção que é lançada ao carregar um arquivo de cenário.
 *
 * @author Flávio Faria
 * @see Scenario
 * @since Feb-12-2012
 */
public class ScenarioLoadException extends Exception {

    public ScenarioLoadException(String message) {
        super(message);
    }

    public ScenarioLoadException(String message, Throwable cause) {
        super(message, cause);
    }
}
