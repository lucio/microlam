package br.ufrj.lam.microlam.exceptions;

public class InvalidDriverModelException extends RuntimeException {
    public InvalidDriverModelException(String message) {
        super(message);
    }

    public InvalidDriverModelException(Throwable cause) {
        super(cause);
    }
}
