package br.ufrj.lam.microlam.exceptions;

/**
 * Exception lançada toda vez que o método Engine.step() tenta construir um veículo que não possui copy constructor
 *
 * @author Lucio Paiva
 * @since 13-Mar-2011
 */
public class VehicleCopyConstructorMissingException extends Exception {

    public VehicleCopyConstructorMissingException(String message) {
        super(message);
    }

}
