package br.ufrj.lam.microlam.signalcontrol;

import java.util.ArrayList;

import br.ufrj.lam.microlam.core.GreenWaveParameters;
import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.RoadConnection;
import br.ufrj.lam.microlam.core.SignalPhase;
import br.ufrj.lam.microlam.core.SignalizedIntersection.Light;

public class PretimedSignalController extends SignalController {

    private class PretimedPhaseTiming {
        private double greenTime, amberTime, allRedTime, duration;

        private PretimedPhaseTiming(double greenTime, double amberTime, double allRedTime) {
            this.greenTime = greenTime;
            this.amberTime = amberTime;
            this.allRedTime = allRedTime;

            this.duration = this.greenTime + this.amberTime + this.allRedTime;
        }

        private void setPhaseTiming(double greenTime, double amberTime, double allRedTime) {
            this.greenTime = greenTime;
            this.amberTime = amberTime;
            this.allRedTime = allRedTime;

            this.duration = this.greenTime + this.amberTime + this.allRedTime;
        }

        private double getDuration() {
            return duration;
        }

        private boolean isGreen(double phasePosition) {
            return (phasePosition < greenTime + amberTime);
        }

        private Light getLightColor(double phasePosition) {
            if (phasePosition < greenTime)
                // está na etapa de verde
                return Light.GREEN;
            else if (phasePosition < greenTime + amberTime)
                // está na etapa de amarelo
                return Light.AMBER;
            else
                // está na etapa de "all red" em que todas as conexões estão em sinal vermelho
                return Light.RED;
        }
    }

    private ArrayList<SignalPhase> phases;
    private ArrayList<PretimedPhaseTiming> timings;

    private int currentPhase;
    private double phasePosition;

    private double offset;

    private boolean hasStarted = false;

    public PretimedSignalController(int id, double offset) {
        super(id);

        this.currentPhase = 0;
        this.phasePosition = 0;
        this.offset = offset;
        this.phases = new ArrayList<>();
        this.timings = new ArrayList<>();
    }

    @Override
    public void setPhaseTiming(long greenTime, long amberTime, long allRedTime) {
        for (PretimedPhaseTiming timing : this.timings) {
            timing.setPhaseTiming(greenTime, amberTime, allRedTime);
        }
    }

    /**
     * Adiciona uma nova fase ao ciclo deste controlador de semáforo.
     *
     * @param greenTime      Tempo de verde.
     * @param amberTime      Tempo de amarelo.
     * @param allRedTime     Tempo de "todos vermelhos" (tempo doado para ajudar a clarear o cruzamento antes do início da próxima fase).
     * @param legalMovements Lista de {@link RoadConnection}s com passagem permitida nesta fase.
     */
    public void addPhase(double greenTime, double amberTime, double allRedTime, RoadConnection... legalMovements) {
        if (legalMovements.length > 0) {
            SignalPhase signalPhase = new SignalPhase(legalMovements);
            this.phases.add(signalPhase);
            PretimedPhaseTiming pt = new PretimedPhaseTiming(greenTime, amberTime, allRedTime);
            this.timings.add(pt);
        }

        resetPhasePosition();
        adjustPhasePosition(offset);
    }

    private int getNextPhaseIndex(int currentPhaseIndex) {
        return (currentPhaseIndex + 1) % this.phases.size();
    }

    /**
     * Zera informações sobre a fase atual do semáforo.
     */
    private void resetPhasePosition() {
        this.currentPhase = 0;
        this.phasePosition = 0;
    }

    /**
     * Aplica a variação de tempo passada como parâmetro
     * e incrementa o temporizador do semáforo, mudando
     * de fase quando necessário.
     */
    private void adjustPhasePosition(double dt) {
        double newPhasePosition = this.phasePosition + dt;

        // avança a fase do sinal de acordo com o tempo passado segundo o parâmetro dt:
        while (newPhasePosition > this.getCurrentPhaseDuration()) {
            newPhasePosition -= this.getCurrentPhaseDuration();
            this.currentPhase = getNextPhaseIndex(this.currentPhase);
        }
        this.phasePosition = newPhasePosition;
    }

    /**
     * Atualiza executionTime e a partir dele atualiza também qual é a fase atualmente ativa
     * e em que momento dela nós estamos.
     */
    @Override
    public void step(double dtInSeconds) {
        int originalPhase = this.currentPhase;

        // avança a fase do sinal de acordo com o tempo passado segundo dtInSeconds:
        adjustPhasePosition(dtInSeconds);

        // se acabou de mudar de fase ou se step() está sendo chamado pela primeira vez para este signal controller:
        if ((this.currentPhase != originalPhase) ||
                !this.hasStarted) { // no início da simulação não há mudança de fase, porém queremos fingir que houve uma para disparar ondas verdes
            this.hasStarted = true;
            this.broadcastEvent(SignalControllerEvent.createPhaseChangeEvent(
                    new ArrayList<>(this.getLocalPhases().get(this.currentPhase).getConnections()),
                    this.getCurrentPhaseDuration()));
        }
    }

    /**
     * Usado por uma interseção upstream que está para começar uma nova
     * janela verde para calcular qual deve ser a velocidade constante
     * do deslocamento da janela.
     *
     * @param incoming             via de chegada da onda.
     * @param minimumReachableTime tempo mínimo (em segundos) em que a onda consegue chegar na interseção. É pautado pela velocidade máxima da via.
     * @return GreenWaveParameters indicando quanto tempo falta para a próxima fase verde alcançável pela onda a ser criada e qual será a duração da janela.
     */
    @Override
    public GreenWaveParameters getTimeToNextReachableGreenPhase(Road incoming, double minimumReachableTime) {
        GreenWaveParameters gwp = new GreenWaveParameters();
        double timeToNextGreen = 0d;

        // caso esteja verde na fase atual E a onda tenha tempo de chegar com o sinal ainda verde, termina aqui:
        if (isGreenInPhase(incoming, this.currentPhase)) {
            double timeLeft = this.getCurrentPhaseDuration() - this.phasePosition;
            // TODO marretado 1 segundo reservado para que a onda verde tenha alguma largura de banda - esse valor deve ser melhor especificado
            if (minimumReachableTime < timeLeft - 1) {
                gwp.setTimeToNextReachableGreenPhase(minimumReachableTime);
                gwp.setAvailableTimespan(timeLeft - minimumReachableTime);
                return gwp;
            }
        }

        // conta quanto tempo falta para a fase em andamento acabar:
        timeToNextGreen += this.getCurrentPhaseDuration() - this.phasePosition;
        int phaseIndex = getNextPhaseIndex(this.currentPhase);

        /*
         * continua acumulando os tempos das fases na variável {@code timeToNextGreen} enquanto
         * não encontrar uma fase com a via em questão ativa OU enquanto o tempo acumulado for
         * menor que o necessário para que a onda consiga chegar na interseção downstream res-
         * peitando o limite de velocidade:
         */
        while (true) {
            if (isGreenInPhase(incoming, phaseIndex)) {
                // checa se dá tempo de chegar
                // TODO marretado 1 segundo reservado para que a onda verde tenha alguma largura de banda - esse valor deve ser melhor especificado
                if (minimumReachableTime < timeToNextGreen + this.timings.get(phaseIndex).getDuration() - 1) {
                    double max = Math.max(minimumReachableTime, timeToNextGreen);
                    gwp.setTimeToNextReachableGreenPhase(max);
                    gwp.setAvailableTimespan(timeToNextGreen + this.timings.get(phaseIndex).getDuration() - max);
                    return gwp;
                }
            }

            timeToNextGreen += this.timings.get(phaseIndex).getDuration();
            phaseIndex = getNextPhaseIndex(phaseIndex);
        }
    }

    /**
     * @return tempo em segundos
     */
    private double getCurrentPhaseDuration() {
        return this.timings.get(this.currentPhase).getDuration();
    }

    /**
     * Este método é similar ao {@link PretimedSignalController#isGreen(Road, Road)},
     * porém só está interessado em saber se a fase cujo índice é passado como
     * parâmetro possui alguma RoadConnection que tenha {@code incoming} como
     * via de entrada, sem se interessar pela via de saída.
     *
     * @param incoming   via que liga o sinal deste SignalController ao sinal upstream
     * @param phaseIndex fase deste SignalController que estamos querendo saber se dá passagem para a via {@code incoming}.
     * @return {@code true} se {@code incoming} tive sinal verde na fase cujo índice é {@code phaseIndex}.
     */
    private boolean isGreenInPhase(Road incoming, int phaseIndex) {
        return this.phases.get(phaseIndex).hasConnection(incoming, null);
    }

    @Override
    public boolean isGreen(Road incoming) {
        return this.isGreen(incoming, null);
    }

    /**
     * Uma SignalizedIntersection delega ao seu SignalController a tarefa
     * de saber se uma conexão está com sinal verde num dado momento.
     * É este método que ela chama quando deseja saber isso.
     *
     * @param incoming via de entrada...
     * @param outgoing ...e via de saída da conexão sendo investigada
     * @return true se conexão estiver ativa
     */
    @Override
    public boolean isGreen(Road incoming, Road outgoing) {
        return
                // se ligação incoming->outgoing está ativa na fase atual
                this.phases.get(this.currentPhase).hasConnection(incoming, outgoing) &&
                        // e a fase atual está na etapa de verde
                        this.timings.get(this.currentPhase).isGreen(this.phasePosition);
    }

    /**
     * Chamado pela SignalizedIntersection quando ela deseja saber
     * a cor do sinal de determinada RoadConnection.
     */
    public Light getLightColor(RoadConnection rc) {
        // se RoadConnection requisitada não estiver na fase atualmente ativa, o sinal está vermelho:
        if (!this.phases.get(this.currentPhase).hasConnection(rc.getIncomingRoad(), rc.getOutgoingRoad()))
            return Light.RED;
        else
            // caso esteja na fase ativa, verifica em qual momento da fase estamos:
            return this.timings.get(this.currentPhase).getLightColor(this.phasePosition);
    }

    @Override
    public ArrayList<SignalPhase> getLocalPhases() {
        return this.phases;
    }
}
