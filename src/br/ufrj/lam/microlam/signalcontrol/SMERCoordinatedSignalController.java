package br.ufrj.lam.microlam.signalcontrol;

import java.util.ArrayList;
import java.util.HashMap;

import br.ufrj.lam.microlam.coordination.SMERCorridor;
import br.ufrj.lam.microlam.core.GreenWaveParameters;
import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.RoadConnection;
import br.ufrj.lam.microlam.core.SignalPhase;
import br.ufrj.lam.microlam.core.SignalizedIntersection;
import br.ufrj.lam.microlam.core.SignalizedIntersection.Light;
import br.ufrj.lam.microlam.detector.VehicleAreaDetector;
import br.ufrj.lam.microlam.report.ReportManager;
import br.ufrj.lam.smer.SMEREngine;
import br.ufrj.lam.smer.SMERException;
import br.ufrj.lam.smer.SMERGraph;
import br.ufrj.lam.smer.SMERGraphBuilder;

/**
 * Controlador de sinais que opera segundo o método SMER:
 * Scheduling by Multiple Edge Reversal.
 * <p>
 * Utiliza o mecanismo SMER encontrado em br.ufrj.lam.smer.
 *
 * @author Lucio Paiva
 * @see br.ufrj.lam.microlam.signalcontrol.SignalController
 * @since Mar-08-2011
 */
public class SMERCoordinatedSignalController extends SignalController {

    private static final double GREEN_DURATION = 27;
    private static final double AMBER_DURATION = 2;
    private static final double ALLRED_DURATION = 1;

    /**
     * Grafo SMER de controle dos fluxos locais.
     */
    private SMEREngine localEngine;
    /**
     * construtor do grafo de fluxos locais
     **/
    private SMERGraphBuilder graphBuilder;
    /**
     * Grafo SMER de coordenação entre interseções.
     * Este grafo tem prioridade de execução sobre
     * o dos fluxos locais.
     */
    private SMEREngine coordinationEngine;

    /**
     * Coleção que guarda as fases deste controlador
     * que não estão coordenadas em nenhum corredor.
     */
    private ArrayList<SignalPhase> localPhases;
    /**
     * Coleção que guarda as fases deste controlador
     * que estão sob responsabilidade do coordenador.
     */
    private ArrayList<SignalPhase> coordinatedPhases;
    /**
     * Flag que diz se a fase atual é uma fase que faz
     * parte de um corredor coordenado ou se é uma fase
     * local.
     */
    private boolean isInCoordinatedPhase;
    /**
     * Quanto tempo já se passou desde o início
     * da simulação (em segundos).
     */
    private double executionTime;
    /**
     * Quanto tempo se passou desde o início da fase atual.
     */
    private double phasePosition;

    private VehicleDetector vehicleDetector;

    /**
     * Quantas fases já executou desde o início do ciclo coordenado atual.
     */
    private int phasesExecutionCount;
    /**
     * Duração do ciclo coordenado atual, em número de fases.
     * Quando phasesExecutionCount == coordinatedCycleLength, o controller
     * completou um ciclo coordenado e deve requisitar novo grafo ao
     * SMERCoordinator.
     */
    private int coordinatedCycleLength;
    /**
     * Usado quando se quer desativar a mudança de reversibilidade.
     * Se coordinatedCycleLength possuir este valor, a mudança nunca
     * será feita.
     */
    private int COORDINATION_CYCLE_INFINITE = Integer.MAX_VALUE;
    /**
     * Índice do ciclo coordenado em operação atualmente.
     * É usado para informar ao SMERCoordinator qual é
     * o ciclo atual deste controlador para que ele possa
     * saber qual deve ser o próximo grafo SMER a mandar.
     */
    private int currentCoordinatedCycleId;

    /**
     * Estados possíveis de uma fase do controlador.
     */
    private enum PhaseState {
        /**
         * Tempo de verde da fase, em que os carros dos movimentos permitidos
         * têm a chance de atravessar o cruzamento.
         */
        greenTime,
        /**
         * Tempo de amarelo. Atualmente só é usado no MicroLAM para ilustrar
         * no applet que a mudança para vermelho está prestes a acontecer.
         * Os veículos estão tratando o tempo de amarelo como continuação do
         * tempo de verde.
         */
        amberTime,
        /**
         * Na literatura de engenharia de tráfego, "clearance time" é o tempo
         * de segurança dado para que os veículos que deixaram a via nos últi-
         * mos instantes do tempo de verde/amarelo tenham tempo de cruzar a
         * interseção por completo. Durante este tempo é garantido que os fluxos
         * concorrentes ficarão vermelhos também.
         */
        clearanceTime,
        /**
         * Usado quando a mesma fase vai continuar operacional na iteração
         * seguinte. Quando isso acontece, normalmente eu estava pulando o tempo
         * de clearance time e passando direto para o próximo verde, porém isso
         * estava colocando a interseção fora de sincronia com o resto do cor-
         * redor. Para consertar essa falha sem ter que mandar sinal vermelho
         * para os carros, eu criei esta extensão do tempo de amarelo como um
         * patch para manter a interseção coordenada.
         */
        extendedAmberTime
    }

    /**
     * Guarda o estado atual da fase.
     */
    private PhaseState phaseState = PhaseState.greenTime;
    /**
     * Quanto tempo falta para o estado atual da fase acabar
     * e dar vez ao próximo estado (em segundos).
     */
    private double stateTimeLeft = GREEN_DURATION;
    /**
     * Flag usada enquanto estiver setando o offset do
     * controlador para que ele não gere ondas verdes
     * indevidas.
     */
    private boolean wantsToProduceGreenWaves = true;

    /**
     * Classe para encapsular um HashMap constante com os tempos de
     * cada etapa de uma fase do controlador.
     *
     * @author luciopaiva
     */
    private static class PhaseDurationHashMap extends HashMap<PhaseState, Double> {

        private double totalPhaseDuration;

        {
            setPhaseTiming(GREEN_DURATION, AMBER_DURATION, ALLRED_DURATION);
        }

        private void setPhaseTiming(double greenTime, double amberTime, double clearanceTime) {
            this.put(PhaseState.greenTime, greenTime);
            this.put(PhaseState.amberTime, amberTime);
            this.put(PhaseState.clearanceTime, clearanceTime);

            totalPhaseDuration = 0d;
            for (double time : this.values()) {
                this.totalPhaseDuration += time;
            }
        }

        /**
         * @return tempo total de duração de uma fase do controlador, considerando a soma da duração de todos os estados do semáforo.
         */
        private double getTotalPhaseDuration() {
            return this.totalPhaseDuration;
        }

        /**
         * @return tempo de verde + amarelo da fase do controlador.
         */
        private double getPhaseGreenAmberDuration() {
            return this.totalPhaseDuration - this.get(PhaseState.clearanceTime);
        }
    }

    private static final PhaseDurationHashMap phaseDuration = new PhaseDurationHashMap();

    /**
     * Construtor.
     *
     * @param id id do controlador no cenário.
     */
    public SMERCoordinatedSignalController(int id) {
        super(id);

        graphBuilder = new SMERGraphBuilder();

        localPhases = new ArrayList<>();
        coordinatedPhases = new ArrayList<>();
        executionTime = 0;
        this.setPhasePosition(0);

        vehicleDetector = new VehicleAreaDetector();

        phasesExecutionCount = 0;
    }

    @Override
    public void setPhaseTiming(long greenTime, long amberTime, long clearanceTime) {
        SMERCoordinatedSignalController.phaseDuration.setPhaseTiming(greenTime, amberTime, clearanceTime);
    }

    /**
     * Método chamado após os offsets terem sido setados.
     * O propósito atual do método é apenas de criar as
     * ondas verdes iniciais, caso contrário só veremos
     * as ondas verdes funcionando na segunda iteração
     * do grafo SMER, quando então o código executaria
     * uma reversão de aresta de fato e a onda seria criada.
     */
    public void initialize() {
        this.broadcastPhaseChangeEvent();
    }

    /**
     * Dispara uma mensagem para os listeners avisando que começou uma
     * nova fase. Atualmente as únicas interessadas são as SignalizedIntersections.
     * Elas usam essa informação para disparar a criação de ondas verdes.
     */
    private void broadcastPhaseChangeEvent() {
        ArrayList<RoadConnection> filteredRoadConnections;

        filteredRoadConnections = this.getActivePhasesConnections(getCurrentEngine());

        this.broadcastEvent(SignalControllerEvent.createPhaseChangeEvent(
                filteredRoadConnections,
                /*
                 * Normalmente este evento só é disparado no exato início de uma fase, então getPhasePosition()
                 * seria sempre 0. Porém, assim que a simulação começa esse método também é chamado e temos
                 * que considerar que o signalcontroller pode estar offsetado, o que significa que a onda pode
                 * não estar começando sincronizada com o início da fase neste caso.
                 */
                this.getPhaseDuration() - this.getPhasePosition()));
    }

    /**
     * Adiciona mais uma fase ao controlador. Cada fase no grafo SMER será representada por um nó.
     * Do jeito que está implementado hoje, o grafo SMER será um grafo completo, o que implica que
     * apenas um nó estará executando num determinado instante.
     *  @param reversibility  Reversibilidade do nó que vai representar esta fase no grafo SMER.
     * @param legalMovements Lista de movimentos permitidos para esta fase, sob a forma de RoadConnections.
     */
    public void addPhase(int reversibility, RoadConnection... legalMovements) throws SMERSignalControllerException {
        SignalPhase result;

        if (legalMovements.length > 0) {
            result = new SignalPhase(legalMovements);
            this.localPhases.add(result);

            this.graphBuilder.addNode(result, reversibility);
            try {
                this.graphBuilder.makeCompleteGraph();
                this.localEngine = new SMEREngine(graphBuilder.graph());
                this.localEngine.prepareToRun();
            } catch (SMERException e) {
                ReportManager.log(e.toString());
                throw new SMERSignalControllerException("smer.GraphBuilder terminated with error in SMERSignalController");
            }

            // atualiza percentagens de verde para cada fase, deixando
            // por default todas com uma fatia igual do tempo:
            this.vehicleDetector.addPhase(result);
        }

    }

    // TODO retirar esse método daqui e tornar o AreaDetector desacoplado e baseado em trigger de eventos como o PointDetector.
    protected void stepDetector() {
        vehicleDetector.step(this.executionTime);

        if (vehicleDetector.hasEnoughData()) {
            this.getCurrentEngine().setNodeExecutionSlices(vehicleDetector.getPhaseWeights());

            vehicleDetector.restart();
        }
    }

    /**
     * Adianta a execução deste controlador.
     * Este método é chamado pelo sistema do SMERCoordinator para
     * sincronizar este sinal com o corredor verde.
     */
    public void setOffset(double dtInSeconds) {
        this.wantsToProduceGreenWaves = false;
        try {
            step(dtInSeconds);
        } catch (SignalControllerException e) {
            ReportManager.err("Estourou uma exception não tratada quando tentei setar o offset de um SMERCoordinatedSignalController");
            e.printStackTrace();
        }
        this.wantsToProduceGreenWaves = true;
    }

    /**
     * Passos executados:
     * - atualiza variavel do phase position
     * - dá um step no detector de area
     * - atualiza cor do sinal
     * - se chegou ao fim de uma fase, dá step na SMEREngine
     */
    @Override
    public void step(double dtInSeconds) throws SignalControllerException {
        // não é possível retroceder o controlador no tempo:
        if (dtInSeconds <= 0) return;

        /*
         * Executa o step em passos homeopáticos. Dar um passo maior que
         * stateTimeLeft vai bugar o código pois as transições de estado
         * da fase não vão acontecer corretamente. Assim, garanto que cada
         * passo enviado não seja maior que o stateTimeLeft, que é o tempo
         * que falta para o estado atual da fase acabar.
         */
        double dtLeft = dtInSeconds;
        while (dtLeft > 0) {
            /*
             * Se o tempo que falta para processar for maior que stateTimeLeft,
             * processa só até stateTimeLeft e guarda o resto para a próxima
             * iteração. Se for menor que stateTimeLeft, processa tudo que resta
             * de uma vez.
             */
            double curDt = (dtLeft > stateTimeLeft) ? stateTimeLeft : dtLeft;
            dtLeft -= curDt;
            step_iter(curDt);
        }
    }

    private void step_iter(double dtInSeconds) throws SignalControllerException {
        this.updateExecutionTime(dtInSeconds);
        stateTimeLeft -= dtInSeconds;
        this.setPhasePosition(this.getPhasePosition() + dtInSeconds);

        /*
         * Verifica em qual estado da fase estamos (verde, amarelo ou clearance time?)
         */
        switch (this.phaseState) {
            case greenTime:
                if (stateTimeLeft <= 0) {
                    /*
                     * trasiciona para o amarelo
                     */
                    this.phaseState = PhaseState.amberTime;
                    this.stateTimeLeft = phaseDuration.get(PhaseState.amberTime);
                }
                break;
            case amberTime:
                if (stateTimeLeft <= 0) {
                    if (this.isNextPhaseSameAsCurrentOne()) {
                        /*
                         * É importante que, caso vá continuar verde na próxima fase,
                         * não pule direto do amarelo para o próximo verde sem contar
                         * o tempo que normalmente seria alocado para o clearance time.
                         * Caso ele seja pulado, vai haver uma falha de sincronia no
                         * corredor coordenado, pois o tamanho desta fase vai encolher!
                         */
                        this.phaseState = PhaseState.extendedAmberTime;
                        this.stateTimeLeft = phaseDuration.get(PhaseState.clearanceTime);
                    } else {
                        /*
                         * transiciona para o vermelho
                         */
                        this.phaseState = PhaseState.clearanceTime;
                        this.stateTimeLeft = phaseDuration.get(PhaseState.clearanceTime);
                    }
                }
                break;
            case extendedAmberTime:
                if (stateTimeLeft <= 0) {
                    /*
                     * trasiciona para a próxima fase
                     */
                    this.processEndOfPhase();
                }
                break;
            case clearanceTime:
                if (stateTimeLeft <= 0) {
                    this.processEndOfPhase();
                }
                break;
        }
    }

    public int getCurrentCoordinatedCycleId() {
        return this.currentCoordinatedCycleId;
    }

    private void processEndOfPhase() throws SMERSignalControllerException {
        /*
         * Reseta variáveis sobre o estado da fase atual.
         */
        this.setPhasePosition(0);
        this.phaseState = PhaseState.greenTime;
        this.stateTimeLeft = phaseDuration.get(PhaseState.greenTime);

        /*
         * Atualiza o contador de fases que já foram executadas.
         */
        this.phasesExecutionCount++;

        /*
         * Se completou um ciclo de coordenação, avisa ao coordenador para
         * que ele mande o novo grafo a ser executado.
         */
        if ((this.coordinatedCycleLength != COORDINATION_CYCLE_INFINITE) && (this.phasesExecutionCount == this.coordinatedCycleLength)) {
            this.phasesExecutionCount = 0;
            broadcastEvent(SignalControllerEvent.createCoordinatedCycleCompletedEvent(this));
        } else {
            /*
             * Se mudou para a próxima fase, é preciso atualizar
             * as engines para que elas revertam as arestas dos nós
             * que estavam executando.
             */
            try {
                /*
                 * Se a engine local estava executando nesta
                 * fase, dá um step nela para que o grafo
                 * reverta suas arestas e fique preparado para
                 * a próxima oportunidade de execução.
                 */
                if (!this.isInCoordinatedPhase) {
                    this.localEngine.step();
                }

                /*
                 * A engine do coordenador é sempre iterada para
                 * sempre estar coordenada com o resto do sistema.
                 */
                this.coordinationEngine.step();

                /*
                 * Após o step() na engine de coordenação, agora
                 * checamos para ver se o coordenador vai ter vez
                 * nessa fase ou se podemos dar um tempo de execução
                 * para as fases locais. O resultado ficará guardado
                 * na variável this.isInCoordinatedPhase.
                 */
                this.checkIfIsInCoordinationPhase();

            } catch (SMERException e) {
                throw new SMERSignalControllerException("SMERSignalControllerException: " + e.getMessage());
            }

            /*
             * Dispara uma mensagem para os listeners avisando que começou uma
             * nova fase. Atualmente as únicas interessadas são as SignalizedIntersections.
             * Elas usam essa informação para disparar a criação de ondas verdes.
             */
            if (this.wantsToProduceGreenWaves) // essa flag serve para nao gerar ondas verdes enquanto estiver setando o offset do controlador
                broadcastPhaseChangeEvent();
        }
    }

    protected double getExecutionTime() {
        return this.executionTime;
    }

    private void updateExecutionTime(double dtInSeconds) {
        this.executionTime += dtInSeconds;
    }

    private double getPhaseDuration() {
        return SMERCoordinatedSignalController.phaseDuration.getTotalPhaseDuration();
    }

    private double getPhaseGreenAmberDuration() {
        return SMERCoordinatedSignalController.phaseDuration.getPhaseGreenAmberDuration();
    }

    /**
     * Chamado por um controlador de semáforo upstream que deseja saber
     * qual é a próxima janela de verde deste controlador.
     */
    @Override
    public GreenWaveParameters getTimeToNextReachableGreenPhase(Road incoming, double minimumReachableTime) {
        GreenWaveParameters gwp;
        double timeToNextGreen = 0d;

        /*
         * Se a fase atualmente ativa possui a via incoming na sua coleção
         * de RoadConnections:
         */
        if (isGreen(incoming, null)) {
            // calcula quanto tempo falta para o fim do tempo de verde da fase atual:
            double timeLeft = this.getPhaseGreenAmberDuration() - this.getPhasePosition();

            // caso vá ficar verde na próxima fase, adiciona esse tempo ao timeLeft:
            if (this.willBeGreenInNextPhase(incoming, null))
                timeLeft += this.getPhaseGreenAmberDuration();

            /*
             * Descobre se a onda vindo de upstream vai conseguir chegar a
             * tempo de pegar a fase atual ainda ativa, com folga para ter
             * alguma banda para trazer veículos consigo.
             */
            // TODO marretados 5 segundos reservados para que a onda verde tenha alguma largura de banda - esse valor deve ser melhor especificado
            if (minimumReachableTime < timeLeft - 5) {
                gwp = new GreenWaveParameters();
                gwp.setTimeToNextReachableGreenPhase(minimumReachableTime);
                gwp.setAvailableTimespan(timeLeft - minimumReachableTime);
                return gwp;
            }
        }

        timeToNextGreen += this.getPhaseDuration() - this.getPhasePosition();

        /*
         * Antigamente aqui eu executava o grafo por n iterações até encontrar
         * uma em que a via incoming tenha verde, porém, convenhamos que se na
         * próxima iteração ela não ganhar verde, a velocidade da onda vai ser
         * muito baixa para chamar de "onda verde". Outro motivo (o principal)
         * para eu ter abandonado esse algoritmo é que agora que existem dois
         * grafos SMER concorrentes (o local e o coordenado), fica muito difícil
         * fazer projeções no futuro, já que eu teria que intercalar as execuções
         * dos dois. Para simplificar então, agora eu apenas verifico se na pró-
         * xima fase a via terá verde. Se não tiver, não crio a onda.
         */
        if (willBeGreenInNextPhase(incoming, null)) {
            gwp = new GreenWaveParameters();
            gwp.setTimeToNextReachableGreenPhase(timeToNextGreen);
            gwp.setAvailableTimespan(this.getPhaseGreenAmberDuration());
            return gwp;
        } else {
            return null;
        }
    }

    /**
     * Roda o grafo por N iterações à frente e verifica se fase estará
     * verde. Usado para determinar quando uma fase ficará verde pela
     * próxima vez, para que a onda verde possa calcular sua velocidade.
     * <p>
     * Vale lembrar que esta previsão de quando a fase ganhará verde
     * não é garantida e pode ocorrer antes por conta de eventuais mu-
     * danças de reversibilidade no grafo SMER.
     *
     * @return {@code true} se a via terá sinal verde na n-ésima iteração do grafo contando a iteração atual como n=0
     */
    @SuppressWarnings("unused")
    private boolean isGreenAfterNGraphIterations(Road incoming, int iteration) {
        try {
            for (Object nodeIndex : this.getCurrentEngine().getSinkNodesForIteration(iteration)) {
                SignalPhase sp = (SignalPhase) nodeIndex;

                if (sp.hasConnection(incoming, null)) {
                    return true;
                }
            }
        } catch (SMERException e) {
            // Falha silenciosamente em caso de erro. Não era para acontecer,
            // porém, caso aconteça, vai falhar de qualquer jeito na próxima
            // iteração real do grafo.
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Equivalente ao método {@code willBeGreenInNextPhase(Road incoming, Road outgoing)},
     * porém em vez de perguntar se determinada RoadConnection estará ativa, faz uma per-
     * gunta mais genérica, procurando por qualquer RoadConnection que possua Road incoming
     * como via de chegada e retornando true se qualquer uma dessas RoadConnections estiver
     * ativa na próxima etapa.
     */
    protected boolean willBeGreenInNextPhase(Road incoming) {
        return this.willBeGreenInNextPhase(incoming, null);
    }

    /**
     * Faz um lookahead e verifica se a RoadConnection compreendida
     * pelas Roads incoming e outgoing estará ativa na próxima fase
     * do semáforo.
     * Este método é usado para não acionar a etapa de "all red" quando
     * a RoadConnection for continuar ativa na próxima fase.
     */
    private boolean willBeGreenInNextPhase(Road incoming, Road outgoing) {
        try {
            /*
             * Para saber se a RoadConnection vai estar ativa na próxima fase,
             * primeiro preciso saber qual é a engine que vai estar rodando.
             */
            if (this.checkIfIsInCoordinationPhase(true)) {
                for (Object nodeIndex : this.coordinationEngine.getSinkNodesForNextIteration()) {
                    SignalPhase sp = ((SMERCorridor) nodeIndex).getSignalPhase();

                    if (sp.hasConnection(incoming, outgoing)) {
                        return true;
                    }
                }
            } else {
                for (Object nodeIndex : this.localEngine.getSinkNodesForNextIteration()) {
                    SignalPhase sp = (SignalPhase) nodeIndex;

                    if (sp.hasConnection(incoming, outgoing)) {
                        // o coordenador não vai operar, logo a fase local está liberada para executar:
                        return true;
                    }
                }
            }
        } catch (SMERException e) {
            // Falha silenciosamente em caso de erro. Não era para acontecer,
            // porém, caso aconteça, vai falhar de qualquer jeito na próxima
            // iteração real do grafo.
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Obtém quais fases estão ativas atualmente a partir
     * da observação de quais nós estão ativos no grafo
     * SMER.
     * <p>
     * Observe que, no caso do grafo SMER estar representando
     * o corredor do qual este signal controller faz parte,
     * serão retornadas também as SignalPhases de outros cor-
     * redores que estão concomitantemente ativos no momento.
     * <p>
     * Além disso, a própria SignalPhase deste signal controller
     * também vai trazer RoadConnections que não dizem respeito
     * a ele propriamente, pois nesta SignalPhase também estão
     * as RoadConnections das outras interseções que fazem parte
     * do mesmo corredor.
     *
     * @return SignalPhase ativo.
     */
    private ArrayList<SignalPhase> getActivePhases(SMEREngine whichEngine) {
        ArrayList<SignalPhase> result = new ArrayList<>();
        ArrayList<Object> sinkNodes = whichEngine.getSinkNodes();
        for (Object obj : sinkNodes) {
            if (obj instanceof SignalPhase)
                result.add((SignalPhase) obj);
            else if (obj instanceof SMERCorridor)
                result.add((((SMERCorridor) obj).getSignalPhase()));
            else
                ReportManager.err("Objeto desconhecido (" + obj + ") no nó do grafo.");
        }
        return result;
    }

    /**
     * Monta uma coleção de RoadConnections baseada nas SignalPhases
     * retornadas a partir de getActivePhases(). Leia a descrição de
     * geActivePhases() para mais detalhes.
     */
    private ArrayList<RoadConnection> getActivePhasesConnections(SMEREngine whichEngine) {
        ArrayList<RoadConnection> connections = new ArrayList<>();
        for (SignalPhase sp : this.getActivePhases(whichEngine))
            connections.addAll(sp.getConnections());
        return connections;
    }

    @Override
    public boolean isGreen(Road incoming) {
        return isGreen(incoming, null);
    }

    @Override
    public boolean isGreen(Road incoming, Road outgoing) {
        for (SignalPhase sp : this.getActivePhases(this.getCurrentEngine())) {
            if (sp.hasConnection(incoming, outgoing)) {
                /*
                 * Descubro se devo ou não entrar na etapa "all red". Caso
                 * a mesma fase venha a ficar ativa na próxima iteração do
                 * grafo SMER, não deixo que o sinal entre no "all red", pois
                 * isso vai causar uma interrupção desnecessária do fluxo que
                 * continuaria logo em seguida.
                 */
                if (willBeGreenInNextPhase(incoming, outgoing)) {
                    return true; // ignora etapa de "all red" pois continuará com a mesma fase ativa na próxima etapa
                } else {
                    return this.phaseState != PhaseState.clearanceTime;
                }
            }
        }

        return false;
    }

    /**
     * Usado para a visualização no applet.
     */
    @Override
    public Light getLightColor(RoadConnection rc) {
        if (isGreen(rc.getIncomingRoad(), rc.getOutgoingRoad())) {
            // green na verdade pode signifcar verde ou amarelo. Descubro exatamente
            // qual cor que deve ser retornada:
            if (this.phaseState == PhaseState.greenTime) {
                return Light.GREEN;
            } else if (this.phaseState == PhaseState.amberTime || this.phaseState == PhaseState.extendedAmberTime) {
                return Light.AMBER;
            }
        }

        return Light.RED;
    }

    /**
     * Usado pelo SMERCorridor quando ele está montando a coleção
     * de RoadConnections do seu corredor.
     */
    public ArrayList<SignalPhase> getLocalPhases() {
        return this.localPhases;
    }

    private SMEREngine getCurrentEngine() {
        if (this.isInCoordinatedPhase)
            return this.coordinationEngine;
        else
            return localEngine;
    }

    private double getPhasePosition() {
        return phasePosition;
    }

    private void setPhasePosition(double phasePosition) {
        this.phasePosition = phasePosition;
    }

    /**
     * Executa um lookahead no grafo SMER para ver
     * se no próximo estado a fase atualmente ativa
     * continuará ativa.
     * <p>
     * Usado para saber se deve entrar na fase de "all red", já
     * que se as mesmas road connections atuais forem continuar
     * ativas na próxima fase, não há porque interromper o fluxo
     * com clerance time.
     *
     * @return {@code true} se próximo estado do grafo SMER repetirá a mesma fase atual.
     */
    private boolean isNextPhaseSameAsCurrentOne() {
        try {
            /*
             * Se a fase atual é coordenada, basta checar se ela vai operar na próxima iteração:
             */
            if (this.isInCoordinatedPhase) {
                for (Object nodeIndex : this.coordinationEngine.getSinkNodesForNextIteration()) {
                    SignalPhase sp = ((SMERCorridor) nodeIndex).getSignalPhase();

                    if (this.getActivePhases(getCurrentEngine()).contains(sp)) {
                        return true;
                    }
                }
            }
            /*
             * Se a fase atual é local, além de verificar se ela vai estar ativa na próxima
             * execução do grafo local, também temos que verificar se o grafo local vai ter
             * vez na próxima iteração ou se o grafo coordenado vai ter prioridade.
             */
            else {
                for (Object nodeIndex : this.localEngine.getSinkNodesForNextIteration()) {
                    SignalPhase sp = (SignalPhase) nodeIndex;

                    if (this.getActivePhases(this.localEngine).contains(sp)) {
                        /*
                         * Ao chegar aqui, sabemos que se a localEngine executar na
                         * próxima iteração, a fase atual ainda estará ativa. Porém, resta
                         * saber se na próxima iteração a engine coordenadora estará
                         * ativa, pois se estiver, vai impedir que a engine local
                         * possa executar em seguida e assim a fase não estará verde.
                         */
                        return !this.checkIfIsInCoordinationPhase(true);
                    }
                }
            }
        } catch (SMERException e) {
            // Falha silenciosamente em caso de erro. Não era para acontecer,
            // porém, caso aconteça, vai falhar de qualquer jeito na próxima
            // iteração real do grafo.
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Inicia um novo ciclo coordenado. Um ciclo é ditado
     * por um grafo SMER representando a concorrência entre
     * os corredores em que cada nó representa um corredor
     * e as reversibilidades são configuradas de acordo com
     * a observação da variação de fluxo recente.
     * <p>
     * A cada novo ciclo, um novo grafo é gerado, porém os
     * nós e arestas são sempre os mesmos; o que varia é
     * apenas a reversibilidade de cada nó e o estado inicial
     * do grafo.
     *
     * @param graph       Novo grafo a ser executado para as fases coordenadas.
     * @param cycleId     Índice do ciclo coordenado que vai começar a ser executado.
     * @param cycleLength Comprimento (em número de fases) do ciclo que vai começar a ser executado.
     */
    public void setCoordinationGraph(SMERGraph graph, int cycleId, int cycleLength) {
        this.coordinationEngine = new SMEREngine(graph);
        this.currentCoordinatedCycleId = cycleId;
        this.phasesExecutionCount = 0;
        this.setCoordinationCycleLength(cycleLength);

        this.checkIfIsInCoordinationPhase();
    }

    public void setCoordinationCycleLength(int cycleLength) {
        if (cycleLength == 0)
            this.coordinatedCycleLength = COORDINATION_CYCLE_INFINITE;
        else
            this.coordinatedCycleLength = cycleLength;
    }

    private boolean checkIfIsInCoordinationPhase() {
        return this.checkIfIsInCoordinationPhase(false);
    }

    private boolean checkIfIsInCoordinationPhase(boolean checkForNextIteration) {
        try {
            // para cada nó sink do grafo do coordenador:
            for (Object node : checkForNextIteration ?
                    this.coordinationEngine.getSinkNodesForNextIteration() : this.coordinationEngine.getSinkNodes()) {
                SMERCorridor corridor = (SMERCorridor) node;

                for (SignalizedIntersection intersection : corridor.getIntersections())
                    if (intersection.getSignalController().getId() == this.getId())
                        if (checkForNextIteration) {
                            return true;
                        } else {
                            this.isInCoordinatedPhase = true;
                            return true;
                        }
            }
        } catch (SMERException e) {
            e.printStackTrace();
        }

        // se chegou aqui é porque não encontrou nenhuma conexão ativa no grafo de coordenação
        if (checkForNextIteration) {
            return false;
        } else {
            this.isInCoordinatedPhase = false;
            return false;
        }

    }

    /**
     * Usado pelo SMERCorridor para promover determinados RoadConnections
     * à jurisdição do coordenador. A RoadConnection é removida do grafo
     * local e passa a operar segundo o regime do grafo global.
     */
    public void promoteRoadConnectionsToCoordinated(Road incomingRoad, SignalPhase coordinatorPhase) {
        ArrayList<SignalPhase> copyOfMyPhases = new ArrayList<>(this.localPhases);
        for (SignalPhase sp : copyOfMyPhases) {
            coordinatorPhase.addConnections(sp.removeConnections(incomingRoad));
            if (sp.getConnections().size() == 0) {
                this.graphBuilder.removeNode(sp);
                this.localEngine = new SMEREngine(graphBuilder.graph());
                try {
                    this.localEngine.prepareToRun();
                } catch (SMERException e) {
                    ReportManager.err(e.toString());
                    ReportManager.err("Erro ao tentar recriar grafo local após remoção de fases globais.");
                    System.exit(1);
                }

                this.localPhases.remove(sp);
                this.vehicleDetector.removePhase(sp);
            }
        }

        if (!this.coordinatedPhases.contains(coordinatorPhase)) {
            this.coordinatedPhases.add(coordinatorPhase);
        }
    }
}
