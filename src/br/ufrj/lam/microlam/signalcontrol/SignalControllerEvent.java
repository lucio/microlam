package br.ufrj.lam.microlam.signalcontrol;

import java.util.ArrayList;

import br.ufrj.lam.microlam.core.RoadConnection;

public class SignalControllerEvent {

    private EventKind eventKind;

    private ArrayList<RoadConnection> greenConnections;
    private double phaseDuration;

    private SignalController signalController;

    public enum EventKind {
        /**
         * Evento disparado quando ocorre uma mudança de fase no controlador.
         */
        signalControllerPhaseChange,
        /**
         * Evento disparado quando o signal controller completa um ciclo de
         * coordenação.
         */
        signalControllerCoordinatedCycleCompleted
    }

    /**
     * @param greenConnections lista de RoadConnections que entraram em atividade com a mudança de fase
     * @param phaseDuration    duração da fase que está começando
     */
    static SignalControllerEvent createPhaseChangeEvent(ArrayList<RoadConnection> greenConnections, double phaseDuration) {
        SignalControllerEvent sce = new SignalControllerEvent(EventKind.signalControllerPhaseChange);
        sce.greenConnections = greenConnections;
        sce.phaseDuration = phaseDuration;
        return sce;
    }

    static SignalControllerEvent createCoordinatedCycleCompletedEvent(SignalController signalController) {
        SignalControllerEvent sce = new SignalControllerEvent(EventKind.signalControllerCoordinatedCycleCompleted);
        sce.signalController = signalController;
        return sce;
    }

    public SignalController getSignalController() {
        return this.signalController;
    }

    /**
     * Construtor private. SignalControlerEvent não pode ser criado diretamente.
     */
    private SignalControllerEvent(EventKind eventKind) {
        this.eventKind = eventKind;
    }

    public EventKind getEventKind() {
        return this.eventKind;
    }

    public ArrayList<RoadConnection> getGreenConnections() {
        return greenConnections;
    }

    public double getPhaseDuration() {
        return phaseDuration;
    }

}
