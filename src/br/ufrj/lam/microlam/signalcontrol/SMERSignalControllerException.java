package br.ufrj.lam.microlam.signalcontrol;

public class SMERSignalControllerException extends SignalControllerException {

    SMERSignalControllerException(String message) {
        super(message);
    }
}
