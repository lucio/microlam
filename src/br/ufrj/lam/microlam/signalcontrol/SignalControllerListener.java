package br.ufrj.lam.microlam.signalcontrol;

public interface SignalControllerListener {

	void signalControllerEvent(SignalControllerEvent signalControllerEvent);
}
