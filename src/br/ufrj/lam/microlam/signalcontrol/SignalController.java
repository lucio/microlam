package br.ufrj.lam.microlam.signalcontrol;

import java.util.ArrayList;

import br.ufrj.lam.microlam.core.GreenWaveParameters;
import br.ufrj.lam.microlam.core.MapEntity;
import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.RoadConnection;
import br.ufrj.lam.microlam.core.SignalPhase;
import br.ufrj.lam.microlam.core.SignalizedIntersection.Light;

/**
 * Classe abstrata que define um controlador de sinais.
 * O controlador de sinais faz o papel de avisar quando os semáforos
 * vão abrir ou fechar, tanto num único cruzamento quanto em um conjunto.
 * Nesse último caso, o controlador de sinais faz o papel de coordenação
 * das interseções.
 *
 * @author Lucio Paiva
 * @since Mar-08-2011
 */
public abstract class SignalController implements MapEntity {
    /**
     * Identificador do signal controller.
     */
    private int id;

    private ArrayList<SignalControllerListener> listeners;

    public abstract void step(double dt) throws SignalControllerException;

    /* deve retornar true se o sinal estiver aberto para o respectivo fluxo */
    public abstract boolean isGreen(Road incoming, Road outgoing);

    public abstract boolean isGreen(Road incoming);

    public abstract Light getLightColor(RoadConnection rc);

    public abstract ArrayList<SignalPhase> getLocalPhases();

    public SignalController(int id) {
        this.id = id;
        listeners = new ArrayList<>();
    }

    /**
     * Usado por uma interseção upstream que está para começar uma nova
     * janela verde para calcular qual deve ser a velocidade constante
     * do deslocamento da janela.
     *
     * @param incoming             via de chegada da onda.
     * @param minimumReachableTime tempo mínimo (em segundos) em que a onda consegue chegar na interseção. É pautado pela velocidade máxima da via.
     * @return GreenWaveParameters indicando quanto tempo falta para a próxima fase verde alcançável pela onda a ser criada e qual será a duração da janela.
     */
    public abstract GreenWaveParameters getTimeToNextReachableGreenPhase(Road incoming, double minimumReachableTime);

    public void addEventListener(SignalControllerListener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    void broadcastEvent(SignalControllerEvent event) {
        for (SignalControllerListener listener : listeners)
            listener.signalControllerEvent(event);
    }

    protected int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "SC" + this.getId();
    }

    public abstract void setPhaseTiming(long greenTime, long amberTime, long clearanceTime);
}
