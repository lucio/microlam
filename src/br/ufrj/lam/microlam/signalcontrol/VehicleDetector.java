package br.ufrj.lam.microlam.signalcontrol;

import java.util.HashMap;

import br.ufrj.lam.microlam.core.SignalPhase;
import br.ufrj.lam.microlam.detector.VehiclePointDetectorDrawingInfo;

public interface VehicleDetector extends VehiclePointDetectorDrawingInfo {
    /**
     * Adiciona cada fase que precisa ter suas RoadConnections sensoreadas.
     */
    void addPhase(SignalPhase signalPhase);

    void removePhase(SignalPhase signalPhase);

    /**
     * Executa uma iteração de coleta do detetor.
     */
    void step(double executionTime);

    /**
     * Retorna true se o detetor já tem dados prontos para serem coletados.
     */
    boolean hasEnoughData();

    /**
     * Coleta dados do detetor.
     */
    HashMap<Object, Double> getPhaseWeights();

    /**
     * Reinicia coleta de dados do detetor.
     */
    void restart();
}
