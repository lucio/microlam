package br.ufrj.lam.microlam.signalcontrol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.ufrj.lam.microlam.core.GreenWaveParameters;
import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.RoadConnection;
import br.ufrj.lam.microlam.core.SignalPhase;
import br.ufrj.lam.microlam.core.SignalizedIntersection.Light;
import br.ufrj.lam.microlam.detector.VehicleAreaDetector;
import br.ufrj.lam.microlam.report.ReportManager;
import br.ufrj.lam.smer.SMEREngine;
import br.ufrj.lam.smer.SMERException;
import br.ufrj.lam.smer.SMERGraph;
import br.ufrj.lam.smer.SMERGraphBuilder;

/**
 * Controlador de sinais que opera segundo o método SMER:
 * Scheduling by Multiple Edge Reversal.
 * <p>
 * Utiliza o mecanismo SMER encontrado em br.ufrj.lam.smer.
 *
 * @author Lucio Paiva
 * @see br.ufrj.lam.microlam.signalcontrol.SignalController
 * @since Mar-08-2011
 */
public class SMERSignalController extends SignalController {
    /*
     * Valores padrão que podem ser sobrescritos pela engine.
     */
    private static final double GREEN_DURATION = 27;
    private static final double AMBER_DURATION = 2;
    private static final double ALLRED_DURATION = 1;

    private SMEREngine engine;
    private SMERGraphBuilder graphBuilder;

    private ArrayList<SignalPhase> phases;
    private double executionTime;
    private double phasePosition;

    VehicleDetector vehicleAreaDetector;

    /**
     * Registra quantas vezes determinada fase operou.
     * Valor usado para ponderar os pesos recebidos do
     * sensor de área durante o cálculo da mudança de
     * reversibilidade.
     */
    private Map<SignalPhase, Long> phaseExecutionCount;

    private enum PhaseState {
        greenTime,
        amberTime,
        clearanceTime
    }

    private PhaseState phaseState = PhaseState.greenTime;
    private double stateTimeLeft = GREEN_DURATION;

    /**
     * Classe para encapsular um HashMap constante com os tempos de
     * cada etapa de uma fase do controlador.
     *
     * @author luciopaiva
     */
    private static class PhaseDurationHashMap extends HashMap<PhaseState, Double> {

        private double totalPhaseDuration;

        {
            setPhaseTiming(GREEN_DURATION, AMBER_DURATION, ALLRED_DURATION);
        }

        private void setPhaseTiming(double greenTime, double amberTime, double clearanceTime) {
            this.put(PhaseState.greenTime, greenTime);
            this.put(PhaseState.amberTime, amberTime);
            this.put(PhaseState.clearanceTime, clearanceTime);

            totalPhaseDuration = 0d;
            for (double time : this.values()) {
                this.totalPhaseDuration += time;
            }
        }

        /**
         * @return tempo total de duração de uma fase do controlador, considerando a soma da duração de todos os estados do semáforo.
         */
        private double getTotalPhaseDuration() {
            return this.totalPhaseDuration;
        }

        /**
         * @return tempo de verde + amarelo da fase do controlador.
         */
        private double getPhaseGreenAmberDuration() {
            return this.totalPhaseDuration - this.get(PhaseState.clearanceTime);
        }
    }

    private static final PhaseDurationHashMap phaseDuration = new PhaseDurationHashMap();

    public SMERSignalController(int id) {
        super(id);

        graphBuilder = new SMERGraphBuilder();

        phases = new ArrayList<>();
        executionTime = 0;
        this.setPhasePosition(0);

        vehicleAreaDetector = new VehicleAreaDetector();
        this.phaseExecutionCount = new HashMap<>();
    }

    public void setReversibilityChangePeriod(int cycleLength) {
        // TODO o SMERSignalController não deveria acreditar que está usando um VehicleAreaDetector. Fiz às pressas por causa do deadline do mestrado!
        ((VehicleAreaDetector) this.vehicleAreaDetector).setReversibilityChangePeriod(cycleLength);
    }

    @Override
    public void setPhaseTiming(long greenTime, long amberTime, long clearanceTime) {
        SMERSignalController.phaseDuration.setPhaseTiming(greenTime, amberTime, clearanceTime);
    }

    /**
     * Adiciona mais uma fase ao controlador. Cada fase no grafo SMER será representada por um nó.
     * Do jeito que está implementado hoje, o grafo SMER será um grafo completo, o que implica que
     * apenas um nó estará executando num determinado instante.
     *
     * @param reversibility  Reversibilidade do nó que vai representar esta fase no grafo SMER.
     * @param legalMovements Lista de movimentos permitidos para esta fase, sob a forma de RoadConnections.
     */
    public SignalPhase addPhase(int reversibility, RoadConnection... legalMovements) throws SMERSignalControllerException {
        SignalPhase result = null;

        if (legalMovements.length > 0) {
            result = new SignalPhase(legalMovements);
            this.phases.add(result);

            this.graphBuilder.addNode(result, reversibility);
            try {
                this.graphBuilder.makeCompleteGraph();
                this.engine = new SMEREngine(graphBuilder.graph());
                this.engine.prepareToRun();
            } catch (SMERException e) {
                ReportManager.log(e.toString());
                throw new SMERSignalControllerException("smer.GraphBuilder terminated with error in SMERSignalController");
            }

            // atualiza percentagens de verde para cada fase, deixando
            // por default todas com uma fatia igual do tempo:
            this.vehicleAreaDetector.addPhase(result);
            this.phaseExecutionCount.put(result, 0L);
        }

        return result;
    }

    // TODO retirar esse método daqui e tornar o AreaDetector desacoplado e baseado em trigger de eventos como o PointDetector.
    protected void stepDetector() {
        vehicleAreaDetector.step(this.executionTime);

        if (vehicleAreaDetector.hasEnoughData()) {
            HashMap<Object, Double> phaseWeights = vehicleAreaDetector.getPhaseWeights();

            /*
             * Cada peso calculado pelo sensor de área tem que ser ainda ponderado.
             * A ponderação é feita levando em conta o tempo de verde que aquela
             * fase teve ao longo do último ciclo. Entre duas fases que ganharam um
             * mesmo peso, se uma delas tiver tido mais tempo de verde, significa
             * que, apesar do sensor ter medido o mesmo valor para as duas, a que
             * teve mais verde está tendo maior volume, que só está igualado atualmente
             * por conta da maior vazão, mas que se cortarmos essa vazão maior, vamos
             * desestabilizar o mecanismo SMER.
             */
            double sum = 0;
            for (Object obj : phaseWeights.keySet()) {
                SignalPhase phase = (SignalPhase) obj;
                double weightedPhaseValue = phaseWeights.get(phase) * (double) this.phaseExecutionCount.get(phase);
                sum += weightedPhaseValue;
                phaseWeights.put(phase, weightedPhaseValue);
            }

            // normaliza na faixa [0..1]:
            for (Object phase : phaseWeights.keySet()) {
                phaseWeights.put(phase, phaseWeights.get(phase) / sum);
            }

            engine.setNodeExecutionSlices(phaseWeights);

            /*
             * Faz um dump do periodo do grafo no log para registro.
             */
            try {
                SMEREngine tempEngine = new SMEREngine(new SMERGraph(this.engine.graph()));
                tempEngine.dumpPeriod();
            } catch (SMERException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            /*
             * Zera o contador de execuções para prepará-lo para o próximo ciclo:
             */
            for (SignalPhase phase : this.phaseExecutionCount.keySet()) {
                this.phaseExecutionCount.put(phase, 0L);
            }

            vehicleAreaDetector.restart();
        }
    }

    /**
     * Passos executados:
     * - atualiza variavel do phase position
     * - dá um step no detector de area
     * - atualiza cor do sinal
     * - se chegou ao fim de uma fase, dá step na SMEREngine
     */
    @Override
    public void step(double dtInSeconds) throws SignalControllerException {
        this.updateExecutionTime(dtInSeconds);
        stateTimeLeft -= dtInSeconds;
        this.setPhasePosition(this.getPhasePosition() + dtInSeconds);

        stepDetector();

        switch (this.phaseState) {
            /*
             * estado atual é verde
             */
            case greenTime:
                if (stateTimeLeft <= 0) {
                    /*
                     * trasiciona para o amarelo
                     */
                    this.phaseState = PhaseState.amberTime;
                    this.stateTimeLeft = phaseDuration.get(PhaseState.amberTime);
                }
                break;
            /*
             * estado atual é amarelo
             */
            case amberTime:
                if (stateTimeLeft <= 0) {
                    if (this.isNextPhaseSameAsCurrentOne()) {
                        this.processEndOfPhase();
                    } else {
                        /*
                         * transiciona para o vermelho
                         */
                        this.phaseState = PhaseState.clearanceTime;
                        this.stateTimeLeft = phaseDuration.get(PhaseState.clearanceTime);
                    }
                }
                break;
            /*
             * está no vermelho reservado para dar tempo de limpar o cruzamento
             * que pode ter sido sujado pela fase atual
             */
            case clearanceTime:
                if (stateTimeLeft <= 0) {
                    this.processEndOfPhase();
                }
                break;
        }
    }

    private void processEndOfPhase() throws SMERSignalControllerException {
        /*
         * Incrementa o contador de quantas vezes a fase atual executou.
         */
        SignalPhase currentPhase = this.getCurrentPhase();
        this.phaseExecutionCount.put(currentPhase, this.phaseExecutionCount.get(currentPhase) + 1);

        /*
         * Se mudou para a próxima fase, é preciso atualizar
         * a engine para que ela reverta as arestas dos nós
         * que estavam executando.
         */
        try {
            engine.step();
        } catch (SMERException e) {
            throw new SMERSignalControllerException("SMERSignalControllerException: " + e.getMessage());
        }

        /*
         * Observar que o SMERSignalController não guarda informação
         * sobre qual é a fase atual. Ele não sabe em nenhum momento
         * quais são os legal movements que estão ativos. Ele apenas
         * guarda a phase position para saber quando acaba a fase e
         * quando ele deve dar um step na SMEREngine. Sempre que um
         * veículo pergunta se o sinal está aberto, o SMERSignalController
         * pergunta pra SMEREngine.
         */
        this.setPhasePosition(0);
        this.phaseState = PhaseState.greenTime;
        this.stateTimeLeft = phaseDuration.get(PhaseState.greenTime);

        this.broadcastEvent(SignalControllerEvent.createPhaseChangeEvent(
                new ArrayList<>(this.getCurrentPhase().getConnections()),
                this.getPhaseDuration()));
    }

    protected double getExecutionTime() {
        return this.executionTime;
    }

    void updateExecutionTime(double dtInSeconds) {
        this.executionTime += dtInSeconds;
    }

    private double getPhaseDuration() {
        return SMERSignalController.phaseDuration.getTotalPhaseDuration();
    }

    private double getPhaseGreenAmberDuration() {
        return SMERSignalController.phaseDuration.getPhaseGreenAmberDuration();
    }

    /**
     * Chamado por um controlador de semáforo upstream que deseja saber
     * qual é a próxima janela de verde deste controlador.
     */
    @Override
    public GreenWaveParameters getTimeToNextReachableGreenPhase(Road incoming, double minimumReachableTime) {
        GreenWaveParameters gwp = new GreenWaveParameters();
        double timeToNextGreen = 0d;

        /*
         * Se a fase atualmente ativa possui a via incoming na sua coleção
         * de RoadConnections:
         */
        if (isGreen(incoming)) {
            // calcula quanto tempo falta para o fim do tempo de verde da fase atual:
            double timeLeft = this.getPhaseGreenAmberDuration() - this.getPhasePosition();

            // caso vá ficar verde na próxima fase, adiciona esse tempo ao timeLeft:
            if (this.willBeGreenInNextPhase(incoming))
                timeLeft += this.getPhaseGreenAmberDuration();

            /*
             * Descobre se a onda vindo de upstream vai conseguir chegar a
             * tempo de pegar a fase atual ainda ativa, com folga para ter
             * alguma banda para trazer veículos consigo.
             */
            // TODO marretados 5 segundos reservados para que a onda verde tenha alguma largura de banda - esse valor deve ser melhor especificado
            if (minimumReachableTime < timeLeft - 5) {
                gwp.setTimeToNextReachableGreenPhase(minimumReachableTime);
                gwp.setAvailableTimespan(timeLeft - minimumReachableTime);
                return gwp;
            }
        }

        timeToNextGreen += this.getPhaseDuration() - this.getPhasePosition();
        int n = 1;

        /*
         * continua acumulando os tempos das fases na variável {@code timeToNextGreen} enquanto
         * não encontrar uma fase com a via em questão ativa OU enquanto o tempo acumulado for
         * menor que o necessário para que a onda consiga chegar na interseção downstream res-
         * peitando o limite de velocidade:
         */
        while (!isGreenAfterNGraphIterations(incoming, n) || (timeToNextGreen < minimumReachableTime)) {
            timeToNextGreen += this.getPhaseDuration();
            n++;

            // artifício para evitar que o while entre num loop infinito
            // caso a via passada não pertença a nenhuma fase deste
            // SignalController ou que por outro motivo ela não ganhe
            // verde nunca:
            if (n == 100) {
                gwp.setTimeToNextReachableGreenPhase(Double.POSITIVE_INFINITY);
                gwp.setAvailableTimespan(0);
                return gwp;
            }
        }

        gwp.setTimeToNextReachableGreenPhase(timeToNextGreen);
        gwp.setAvailableTimespan(this.getPhaseGreenAmberDuration());
        return gwp;
    }

    /**
     * Roda o grafo por N iterações à frente e verifica se fase estará
     * verde. Usado para determinar quando uma fase ficará verde pela
     * próxima vez, para que a onda verde possa calcular sua velocidade.
     * <p>
     * Vale lembrar que esta previsão de quando a fase ganhará verde
     * não é garantida e pode ocorrer antes por conta de eventuais mu-
     * danças de reversibilidade no grafo SMER.
     *
     * @return {@code true} se a via terá sinal verde na n-ésima iteração do grafo contando a iteração atual como n=0
     */
    private boolean isGreenAfterNGraphIterations(Road incoming, int iteration) {
        try {
            for (Object nodeIndex : engine.getSinkNodesForIteration(iteration)) {
                SignalPhase sp = (SignalPhase) nodeIndex;

                if (sp.hasConnection(incoming, null)) {
                    return true;
                }
            }
        } catch (SMERException e) {
            // Falha silenciosamente em caso de erro. Não era para acontecer,
            // porém, caso aconteça, vai falhar de qualquer jeito na próxima
            // iteração real do grafo.
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Equivalente ao método {@code willBeGreenInNextPhase(Road incoming, Road outgoing)},
     * porém em vez de perguntar se determinada RoadConnection estará ativa, faz uma per-
     * gunta mais genérica, procurando por qualquer RoadConnection que possua Road incoming
     * como via de chegada e retornando true se qualquer uma dessas RoadConnections estiver
     * ativa na próxima etapa.
     */
    boolean willBeGreenInNextPhase(Road incoming) {
        return this.willBeGreenInNextPhase(incoming, null);
    }

    /**
     * Faz um lookahead e verifica se a RoadConnection compreendida
     * pelas Roads incoming e outgoing estará ativa na próxima fase
     * do semáforo.
     * Este método é usado para não acionar a etapa de "all red" quando
     * a RoadConnection for continuar ativa na próxima fase.
     */
    boolean willBeGreenInNextPhase(Road incoming, Road outgoing) {
        try {
            for (Object nodeIndex : engine.getSinkNodesForNextIteration()) {
                SignalPhase sp = (SignalPhase) nodeIndex;

                if (sp.hasConnection(incoming, outgoing)) {
                    return true;
                }
            }
        } catch (SMERException e) {
            // Falha silenciosamente em caso de erro. Não era para acontecer,
            // porém, caso aconteça, vai falhar de qualquer jeito na próxima
            // iteração real do grafo.
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean isGreen(Road incoming) {
        return this.isGreen(incoming, null);
    }

    /**
     * Obtém qual fase está ativa atualmente a partir
     * da observação de qual nó está ativo no grafo
     * SMER.
     *
     * @return SignalPhase ativo.
     */
    SignalPhase getCurrentPhase() {
        ArrayList<Object> sinkNodes = engine.getSinkNodes();
        if (sinkNodes.size() != 1) {
            // TODO lançar uma exception pra melhorar essa forma de avisar que deu erro
            Thread.dumpStack();
            ReportManager.err("Controlador de semáforo com mais de uma fase ativa!");
        }
        return (SignalPhase) sinkNodes.get(0);
    }

    @Override
    public boolean isGreen(Road incoming, Road outgoing) {
        SignalPhase sp = this.getCurrentPhase();
        if (sp.hasConnection(incoming, outgoing)) {
            /*
             * Descubro se devo ou não entrar na etapa "all red". Caso
             * a mesma fase venha a ficar ativa na próxima iteração do
             * grafo SMER, não deixo que o sinal entre no "all red", pois
             * isso vai causar uma interrupção desnecessária do fluxo que
             * continuaria logo em seguida.
             */
            if (willBeGreenInNextPhase(incoming, outgoing)) {
                return true; // ignora etapa de "all red" pois continuará com a mesma fase ativa na próxima etapa
            } else {
                return this.phaseState != PhaseState.clearanceTime; // this.getPhasePosition() < PREDEFINED_GREEN_AMBER_TIME; // retorna false se estiver na etapa de "all red"
            }
        }
        return false;
    }

    @Override
    public Light getLightColor(RoadConnection rc) {
        if (isGreen(rc.getIncomingRoad(), rc.getOutgoingRoad())) {
            // green na verdade pode signifcar verde ou amarelo. Descubro exatamente
            // qual cor que deve ser retornada:
            if (this.phaseState == PhaseState.greenTime) {
                return Light.GREEN;
            } else if (this.phaseState == PhaseState.amberTime) {
                return Light.AMBER;
            }
        }

        return Light.RED;
    }

    public ArrayList<SignalPhase> getLocalPhases() {
        return this.phases;
    }

    protected SMEREngine getEngine() {
        return engine;
    }

    double getPhasePosition() {
        return phasePosition;
    }

    void setPhasePosition(double phasePosition) {
        this.phasePosition = phasePosition;
    }

    /**
     * Executa um lookahead no grafo SMER para ver
     * se no próximo estado a fase atualmente ativa
     * continuará ativa.
     * <p>
     * Usado para saber se deve entrar na fase de "all red", já
     * que se as mesmas road connections atuais forem continuar
     * ativas na próxima fase, não há porque interromper o fluxo
     * com clerance time.
     *
     * @return {@code true} se próximo estado do grafo SMER repetirá a mesma fase atual.
     */
    boolean isNextPhaseSameAsCurrentOne() {
        try {
            for (Object nodeIndex : this.getEngine().getSinkNodesForNextIteration()) {
                SignalPhase sp = (SignalPhase) nodeIndex;

                if (sp == this.getCurrentPhase()) {
                    return true;
                }
            }
        } catch (SMERException e) {
            // Falha silenciosamente em caso de erro. Não era para acontecer,
            // porém, caso aconteça, vai falhar de qualquer jeito na próxima
            // iteração real do grafo.
            e.printStackTrace();
        }
        return false;
    }
}
