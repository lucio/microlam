package br.ufrj.lam.microlam.signalcontrol;

import java.util.ArrayList;

import br.ufrj.lam.microlam.core.GreenWaveParameters;
import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.RoadConnection;
import br.ufrj.lam.microlam.core.SignalPhase;
import br.ufrj.lam.microlam.core.SignalizedIntersection.Light;

/**
 * Controlador de sinais com tempos aleatórios.
 * Sem nenhum propósito específico... só por diversão.
 *
 * @author Lucio Paiva
 * @see br.ufrj.lam.microlam.signalcontrol.SignalController
 * @since Mar-08-2011
 */
public class RandomSignalController extends SignalController {

    public RandomSignalController(int id) {
        super(id);
    }

    @Override
    public void step(double dt) {
        // TODO RandomSignalController Auto-generated method stub

    }

    @Override
    public boolean isGreen(Road incoming, Road outgoing) {
        // TODO RandomSignalController Auto-generated method stub
        return false;
    }

    @Override
    public Light getLightColor(RoadConnection rc) {
        // TODO RandomSignalController Auto-generated method stub
        return null;
    }

    @Override
    public ArrayList<SignalPhase> getLocalPhases() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GreenWaveParameters getTimeToNextReachableGreenPhase(Road incoming, double minimumReachableTime) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setPhaseTiming(long greenTime, long amberTime,
                               long clearanceTime) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isGreen(Road incoming) {
        // TODO Auto-generated method stub
        return false;
    }
}
