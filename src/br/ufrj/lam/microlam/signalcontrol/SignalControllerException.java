package br.ufrj.lam.microlam.signalcontrol;

public class SignalControllerException extends Exception {

	SignalControllerException(String message) {
		super(message);
	}
}
