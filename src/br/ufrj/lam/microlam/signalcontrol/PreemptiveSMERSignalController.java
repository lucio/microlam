package br.ufrj.lam.microlam.signalcontrol;

import java.util.ArrayList;
import java.util.HashMap;

import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.RoadConnection;
import br.ufrj.lam.microlam.core.SignalPhase;
import br.ufrj.lam.microlam.core.SignalizedIntersection.Light;
import br.ufrj.lam.microlam.detector.VehiclePassageDetector;
import br.ufrj.lam.microlam.detector.VehiclePassageEvent;
import br.ufrj.lam.microlam.detector.VehiclePassageEventListener;
import br.ufrj.lam.microlam.report.ReportManager;
import br.ufrj.lam.smer.SMERException;

public class PreemptiveSMERSignalController extends SMERSignalController implements VehiclePassageEventListener {

    /**
     * Posição dos sensores de passagem em relação ao ponto
     * DOWNSTREAM da via (colocado próximo da interseção de
     * saída propositalmente).
     * <p>
     * Valor em METROS.
     */
    private static final double SENSOR_POSITION = 40;

    /**
     * Tempo mínimo de duração de uma fase. O modelo garante
     * que durante este tempo mínimo a fase terá verde mesmo
     * que nenhum veículo acione o sensor.
     * <p>
     * Tempo em SEGUNDOS.
     */
    private static final double MINIMUM_PHASE_DURATION = 10;
    /**
     * Uma vez que a duração da fase tenha ultrapassado o limite
     * mínimo, começa uma contagem regressiva com o valor
     * de GAP_TIMEOUT. Se durante este tempo algum veículo
     * acionar o sensor, o controlador reinicia o contador
     * e mantém este procedimento até que nenhum outro veículo
     * chegue, quando então o contador vai a zero.
     * <p>
     * Caso o tempo se esgote, o controlador verifica se o
     * sensor da via concorrente pediu recall, ou seja, se
     * algum veículo acionou o sensor daquela via. Em caso
     * positivo, o controlador entrega o verde para a próxima
     * fase. Caso contrário, a fase atual continua com verde,
     * porém qualquer acionamento do sensor da via concorrente
     * faz com que o controlador passe para a próxima fase.
     * <p>
     * Tempo em SEGUNDOS.
     */
    private static final double GAP_TIMEOUT = 5;
    private double gapTimeout = GAP_TIMEOUT;
    /**
     * Caso haja um pelotão muito grande chegando pela via,
     * o contador de GAP_TIMEOUT vai se prolongar por muito
     * tempo. Neste caso, é desejável que a fase possua um
     * teto máximo de modo a evitar starvation por parte da
     * via concorrente. Este teto máximo é definido pela
     * constante MAXIMUM_PHASE_DURATION (segundos).
     * <p>
     * Tempo em SEGUNDOS.
     */
    private static final double MAXIMUM_PHASE_DURATION = 40;

    private static final double AMBER_DURATION = 1;
    private static final double CLEARANCE_DURATION = 1;
    private double amberTimeLeft;
    private double clearanceTimeLeft;

    private HashMap<SignalPhase, Double> phaseTimes = new HashMap<>();

    private enum PhaseState {
        /**
         * Fase está dentro do período mínimo de execução.
         */
        minimumPeriod,
        /**
         * Fase está na etapa de extensão de seu tempo pois já passou do tempo mínimo.
         */
        extensionPeriod,
        /**
         * O último tempo de extensão expirou e agora a fase está com verde porém
         * aguardando a chamada de algum detector presente em outras vias.
         */
        timedOut,
        amberTime,
        clearanceTime
    }

    private PhaseState phaseState = PhaseState.minimumPeriod;
    /**
     * Flag usada para avisar ao controlador que uma chamada de
     * extensão foi colocada para a fase atual.
     */
    private boolean isExtensionCallPlaced = false;
    /**
     * Flag usada para avisar ao controlador que foi colocada
     * uma chamada de uma via que não possui movimento legal
     * na fase atual.
     */
    private boolean isRecallPlaced = false;

    public PreemptiveSMERSignalController(int id) {
        super(id);
    }

    @Override
    public SignalPhase addPhase(int reversibility, RoadConnection... legalMovements) throws SMERSignalControllerException {
        SignalPhase result = super.addPhase(reversibility, legalMovements);
        phaseTimes.put(result, 0d);

        /*
         * Evita inserir sensores duplicados na mesma incoming road,
         * já que a mesma signal phase pode ter mais de um legal
         * movement com a mesma incoming road.
         */
        ArrayList<Road> incomingRoads = new ArrayList<>();
        for (RoadConnection roadConnection : legalMovements) {
            if (!incomingRoads.contains(roadConnection.getIncomingRoad()))
                incomingRoads.add(roadConnection.getIncomingRoad());
        }

        /*
         * Adiciona em cada incoming road um sensor de passagem.
         */
        for (Road incomingRoad : incomingRoads) {
            /*
             * Se o tamanho da via é maior que a posição em que o sensor deveria
             * ser colocado, coloca ele na posição ideal. Porém, pode ser que a
             * via seja menor. Nesse caso, coloca o sensor no início da via (em
             * relação à interseção upstream). Ele não pode ser colocado no final
             * da via pois o código do sensor varre uma região que se estende para
             * adiante do ponto de instalação dele, logo ele precisa ter essa folga
             * depois dele para poder verificar os veículos que passaram.
             */
            double sensorPosition = incomingRoad.getLength() > SENSOR_POSITION ? incomingRoad.getLength() - SENSOR_POSITION : 0;
            VehiclePassageDetector sensor = new VehiclePassageDetector(incomingRoad, sensorPosition);
            sensor.addListener(this);
            incomingRoad.addVehicleDetector(sensor);
        }

        return result;
    }

    /**
     * Atualiza quanto tempo a fase já executou (para geração de estatísticas).
     */
    private void updatePhaseTime(SignalPhase signalPhase, double dtInSeconds) {
        this.phaseTimes.put(signalPhase, dtInSeconds + this.phaseTimes.get(signalPhase));
    }

    private void dumpPhaseTimes() {
        double totalTime = 0;
        for (SignalPhase signalPhase : this.phaseTimes.keySet()) {
            totalTime += this.phaseTimes.get(signalPhase);
        }

        Thread.dumpStack();
        ReportManager.log(String.format("=== Fim de período do controlador #%d ===", this.getId()));

        HashMap<Object, Double> phaseWeights = this.vehicleAreaDetector.getPhaseWeights();
        int phaseId = 1;
        for (Object obj : phaseWeights.keySet()) {
            SignalPhase signalPhase = (SignalPhase) obj;

            double measured = this.phaseTimes.get(signalPhase) / totalTime;

            ReportManager.log(String.format("Phase#%d\tCalculada: %.2f\tAferida: %.2f", phaseId, phaseWeights.get(obj), measured));

            phaseId++;
        }

        this.clearPhaseTimes();
    }

    private void clearPhaseTimes() {
        for (SignalPhase signalPhase : this.phaseTimes.keySet())
            this.phaseTimes.put(signalPhase, 0d);
    }

    @Override
    protected void stepDetector() {

        vehicleAreaDetector.step(this.getExecutionTime());

        if (vehicleAreaDetector.hasEnoughData()) {
            this.dumpPhaseTimes();

            this.getEngine().setNodeExecutionSlices(vehicleAreaDetector.getPhaseWeights());

            vehicleAreaDetector.restart();
        }
    }

    @Override
    public void step(double dtInSeconds) throws SignalControllerException {
        this.updateExecutionTime(dtInSeconds);
        this.updatePhaseTime(this.getCurrentPhase(), dtInSeconds);

        stepDetector();

        this.setPhasePosition(this.getPhasePosition() + dtInSeconds);

        /*
         * Caso tenha ultrapassado o tempo máximo que pode ficar verde,
         * preempta para que a outra fase possa executar pelo menos
         * um período mínimo. O verde de uma fase poderia se estender
         * indefinidamente se houvesse um pelotão muito grande atraves-
         * sando, pois haveria um disparo contínuo do período de
         * extensão.
         */
        if ((this.getPhasePosition() > MAXIMUM_PHASE_DURATION) && (this.phaseState != PhaseState.amberTime) &&
                (this.phaseState != PhaseState.clearanceTime)) {
            this.phaseState = PhaseState.timedOut;
            this.isRecallPlaced = true;
            ReportManager.log("Phase MAX");
        }

        switch (this.phaseState) {
            case minimumPeriod:
                if (isExtensionCallPlaced) {
                    isExtensionCallPlaced = false;

                    /*
                     * Se o gap da extensão somado ao tempo total de execução até
                     * agora for maior que o tempo mínimo de execução, saímos do
                     * tempo de período mínimo e entramos em regime de extensão.
                     */
                    if (this.getPhasePosition() + GAP_TIMEOUT > MINIMUM_PHASE_DURATION) {
                        this.gapTimeout = GAP_TIMEOUT;
                        this.phaseState = PhaseState.extensionPeriod;
                        ReportManager.log("entered extension period");
                    }
                    /*
                     * Se o pedido de extensão foi feito ainda dentro do regime
                     * de período mínimo e a extensão não aumentaria o tempo de
                     * execução (o fim do período de extensão aconteceria mais cedo
                     * que o fim do período mínimo), ignora e continua no regime
                     * do período mínimo.
                     */

                } else if (this.getPhasePosition() > MINIMUM_PHASE_DURATION) {
                    this.phaseState = PhaseState.timedOut;
                    ReportManager.log("entered timeout period");
                }
                break;
            case extensionPeriod:
                this.gapTimeout -= dtInSeconds;
                if (isExtensionCallPlaced) {
                    isExtensionCallPlaced = false;
                    this.gapTimeout = GAP_TIMEOUT;
                    ReportManager.log("renewed extension period");
                } else if (this.gapTimeout <= 0) {
                    this.phaseState = PhaseState.timedOut;
                    ReportManager.log("entered timeout period");
                }
                break;
            case timedOut:
                if (isRecallPlaced) {
                    isRecallPlaced = false;
                    amberTimeLeft = AMBER_DURATION;
                    this.phaseState = PhaseState.amberTime;
                    ReportManager.log("entered AMBER period");
                }
                break;
            case amberTime:
                amberTimeLeft -= dtInSeconds;
                if (amberTimeLeft <= 0) {
                    if (this.isNextPhaseSameAsCurrentOne()) {
                        this.stepSMEREngine();
                    } else {
                        clearanceTimeLeft = CLEARANCE_DURATION;
                        this.phaseState = PhaseState.clearanceTime;
                        ReportManager.log("entered ALLRED period");
                    }
                }
                break;
            case clearanceTime:
                clearanceTimeLeft -= dtInSeconds;
                if (clearanceTimeLeft <= 0) {
                    this.stepSMEREngine();
                }
                break;
        }
    }

    private void stepSMEREngine() throws SMERSignalControllerException {
        /*
         * Se vai mudar para a próxima fase, é preciso atualizar
         * a engine para que ela reverta as arestas dos nós
         * que estavam executando.
         */
        try {
            this.getEngine().step();
        } catch (SMERException e) {
            throw new SMERSignalControllerException("SMERSignalControllerException: " + e.getMessage());
        }

        this.setPhasePosition(0);
        this.phaseState = PhaseState.minimumPeriod;
        ReportManager.log(String.format("%s entered minimum period",
                this.getCurrentPhase().getConnections().get(0).getIncomingRoad().getName()));
    }

    @Override
    public void vehiclePassageEvent(VehiclePassageEvent event) {
        /*
         * Se recebeu um evento de passagem de veículo vindo de uma via que tem
         * sinal verde na fase atual E a fase atual ainda não chegou o tempo
         * máximo de período, coloca um pedido de extensão:
         */
        if (this.isGreen(event.getRoad()) && (this.phaseState != PhaseState.timedOut)) {
            isExtensionCallPlaced = true;
            ReportManager.log("extension placed");
        }
        /*
         * Caso o evento tenha vindo de uma via que não tem sinal verde,
         * coloca um pedido de recall (mudança de fase):
         */
        else if (!this.isGreen(event.getRoad())) {
            isRecallPlaced = true;
            ReportManager.log("recall placed");
        }

    }

    @Override
    public Light getLightColor(RoadConnection rc) {
        if (isGreen(rc.getIncomingRoad(), rc.getOutgoingRoad())) {
            if (this.phaseState == PhaseState.amberTime) {
                return Light.AMBER;
            } else if (this.phaseState != PhaseState.clearanceTime) {
                return Light.GREEN;
            }
        }

        return Light.RED;
    }

    @Override
    public boolean isGreen(Road incoming, Road outgoing) {
        SignalPhase sp = this.getCurrentPhase();
        if (sp.hasConnection(incoming, outgoing)) {
            /*
             * Descubro se devo ou não entrar na etapa "all red". Caso
             * a mesma fase venha a ficar ativa na próxima iteração do
             * grafo SMER, não deixo que o sinal entre no "all red", pois
             * isso vai causar uma interrupção desnecessária do fluxo que
             * continuaria logo em seguida.
             */
            if (willBeGreenInNextPhase(incoming, outgoing)) {
                return true; // ignora etapa de "all red" pois continuará com a mesma fase ativa na próxima etapa
            } else {
                return this.phaseState != PhaseState.clearanceTime;
            }
        }
        return false;
    }

    @Override
    public boolean isGreen(Road incoming) {
        SignalPhase sp = this.getCurrentPhase();
        if (sp.hasConnection(incoming, null)) {
            /*
             * Descubro se devo ou não entrar na etapa "all red". Caso
             * a mesma fase venha a ficar ativa na próxima iteração do
             * grafo SMER, não deixo que o sinal entre no "all red", pois
             * isso vai causar uma interrupção desnecessária do fluxo que
             * continuaria logo em seguida.
             */
            if (willBeGreenInNextPhase(incoming)) {
                return true; // ignora etapa de "all red" pois continuará com a mesma fase ativa na próxima etapa
            } else {
                return this.phaseState != PhaseState.clearanceTime; // this.getPhasePosition() < PREDEFINED_GREEN_AMBER_TIME; // retorna false se estiver na etapa de "all red"
            }
        }
        return false;
    }
}
