package br.ufrj.lam.microlam.data.loader;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import br.ufrj.lam.microlam.core.RoadConnection;
import br.ufrj.lam.microlam.core.Scenario;
import br.ufrj.lam.microlam.exceptions.MalformedScenarioFileException;
import br.ufrj.lam.microlam.signalcontrol.PreemptiveSMERSignalController;
import br.ufrj.lam.microlam.signalcontrol.PretimedSignalController;
import br.ufrj.lam.microlam.signalcontrol.SMERCoordinatedSignalController;
import br.ufrj.lam.microlam.signalcontrol.SMERSignalController;
import br.ufrj.lam.microlam.signalcontrol.SMERSignalControllerException;
import br.ufrj.lam.microlam.signalcontrol.SignalController;

/**
 * @author Flavio
 */
public final class SignalControllerFactory extends ScenarioEntityFactory {
    static final String TAG_SIGNALCONTROLLER = "signalcontroller";
    private static final String TAG_SIGNALPHASE = "signalphase";
    private static final String TAG_LEGALMOVEMENT = "legalmovement";

    static {
        ScenarioEntityFactoryMaker factoryMaker = ScenarioEntityFactoryMaker.getInstance();
        ScenarioEntityFactory factory = new SignalControllerFactory();

        factoryMaker.addMapFactory(TAG_SIGNALCONTROLLER, factory);
        factoryMaker.addMapFactory(TAG_SIGNALPHASE, factory);
        factoryMaker.addMapFactory(TAG_LEGALMOVEMENT, factory);
    }

    private SignalControllerFactory() {
    }

    @Override
    public void buildEntity(Node node, Scenario scenario)
            throws MalformedScenarioFileException {
        String tag = node.getNodeName();

        if (TAG_SIGNALCONTROLLER.equals(tag)) {
            buildController(node, scenario);
        } else if (TAG_SIGNALPHASE.equals(tag)) {
            buildPhase(node, scenario);
        } else if (!TAG_LEGALMOVEMENT.equals(tag)) {
            throw new MalformedScenarioFileException("Tag desconhecida: " + tag);
        }
    }

    private void buildController(Node node, Scenario scenario)
            throws MalformedScenarioFileException {
        SignalController controller;

        NamedNodeMap attrs = node.getAttributes();
        String strType = attrs.getNamedItem("type").getNodeValue();

        int id = getIntAttribute(node, "id");

        // pré-temporizado:
        if ("pretimed".equalsIgnoreCase(strType)) {
            Element signalController = (Element) node;

            // Recupera nó com o offset desejado para este controlador, caso especificado:
            Double offset = getDoubleAttribute(signalController, "offset", false);
            if (offset == null || offset < 0d)
                offset = (double) 0;

            controller = new PretimedSignalController(id, offset);
        }
        // SMER:
        else if ("smer".equalsIgnoreCase(strType)) {
            controller = new SMERSignalController(id);
        }
        // Coordinated SMER:
        else if ("coordsmer".equalsIgnoreCase(strType)) {
            controller = new SMERCoordinatedSignalController(id);
        }
        // Preemptive SMER:
        else if ("psmer".equalsIgnoreCase(strType)) {
            controller = new PreemptiveSMERSignalController(id);
        } else {
            String msg = "Atributo type nao encontrado para a tag " + node.getNodeName();
            throw new MalformedScenarioFileException(msg);
        }

        scenario.add(id, controller);
    }

    private void buildPhase(Node node, Scenario scenario)
            throws MalformedScenarioFileException {
        int controllerId = getIntAttribute(node, TAG_SIGNALCONTROLLER);
        SignalController controller = scenario.getSignalControllerById(controllerId);
        RoadConnection[] legalMovements = buildLegalMovements(node, scenario);

        // semáforo pré-temporizado:
        if (controller instanceof PretimedSignalController) {
            int greenTime = getIntAttribute(node, "green");
            int amberTime = getIntAttribute(node, "amber");
            int allRedTime = getIntAttribute(node, "allred");

            PretimedSignalController signalController = (PretimedSignalController) controller;
            signalController.addPhase(greenTime, amberTime, allRedTime, legalMovements);
        }
        // semáforo SMER:
        else if (controller instanceof SMERSignalController) {
            int reversibility = getIntAttribute(node, "reversibility");
            SMERSignalController signalController = (SMERSignalController) controller;

            try {
                signalController.addPhase(reversibility, legalMovements);
            } catch (SMERSignalControllerException ex) {
                throw new RuntimeException(ex);
            }
        }
        // semáforo CoordSMER:
        else if (controller instanceof SMERCoordinatedSignalController) {
            int reversibility = getIntAttribute(node, "reversibility");
            SMERCoordinatedSignalController signalController = (SMERCoordinatedSignalController) controller;

            try {
                signalController.addPhase(reversibility, legalMovements);
            } catch (SMERSignalControllerException ex) {
                throw new RuntimeException(ex);
            }
        } else {
            String msg = "Fase referenciando SignalController inválido: " + controllerId;
            throw new MalformedScenarioFileException(msg);
        }
    }

    // recupera os legal movements que devem estar listados como filhos da fase sendo lida do XML:
    private RoadConnection[] buildLegalMovements(Node node, Scenario scenario)
            throws MalformedScenarioFileException {
        List<RoadConnection> rcs = new ArrayList<>();
        NodeList children = node.getChildNodes();

        for (int i = 0; i < children.getLength(); i++) {
            // recupera lista de legal movements:
            Node legalMove = children.item(i);
            if (!legalMove.getNodeName().equals(SignalControllerFactory.TAG_LEGALMOVEMENT))
                continue;
            int connectionId = getIntAttribute(legalMove, RoadConnectionFactory.TAG_ROADCONNECTION);

            rcs.add(scenario.getRoadConnectionById(connectionId));
        }
        return rcs.toArray(new RoadConnection[0]);
    }
}
