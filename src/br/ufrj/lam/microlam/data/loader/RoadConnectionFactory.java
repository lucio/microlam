package br.ufrj.lam.microlam.data.loader;

import org.w3c.dom.Node;

import br.ufrj.lam.microlam.core.Intersection;
import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.RoadConnection;
import br.ufrj.lam.microlam.core.Scenario;
import br.ufrj.lam.microlam.exceptions.MalformedScenarioFileException;

/**
 * @author Flavio
 */
public final class RoadConnectionFactory extends ScenarioEntityFactory {

    static final String TAG_ROADCONNECTION = "roadconnection";

    static {
        ScenarioEntityFactoryMaker.getInstance().addMapFactory(TAG_ROADCONNECTION, new RoadConnectionFactory());
    }

    private RoadConnectionFactory() {

    }

    @Override
    public void buildEntity(Node node, Scenario scenario)
            throws MalformedScenarioFileException {
        // recupera atributos que vieram do XML:
        int id = getIntAttribute(node, "id");
        int intersectionId = getIntAttribute(node, "intersection");
        int incomingId = getIntAttribute(node, "incoming");
        int outgoingId = getIntAttribute(node, "outgoing");
        float p = getFloatAttribute(node, "probability");

        // recupera instâncias do cenário relacionadas com esta road connection:
        Road incRoad = scenario.getRoadById(incomingId);
        Road outgRoad = scenario.getRoadById(outgoingId);
        Intersection intersection = scenario.getIntersectionById(intersectionId);

        // cria road connection:
        RoadConnection rc = intersection.addRoadConnection(id, incRoad, outgRoad, p);

        // adiciona ao cenário:
        scenario.add(id, rc);
    }
}
