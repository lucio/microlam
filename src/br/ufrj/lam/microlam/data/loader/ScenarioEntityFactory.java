package br.ufrj.lam.microlam.data.loader;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import br.ufrj.lam.microlam.core.Scenario;
import br.ufrj.lam.microlam.exceptions.MalformedScenarioFileException;

public abstract class ScenarioEntityFactory {

    ScenarioEntityFactory() {
    }

    private String getNodeValue(Node node, String name, boolean isMandatory) throws MalformedScenarioFileException {
        NamedNodeMap attrs = node.getAttributes();
        Node attrNode = attrs.getNamedItem(name);
        if (attrNode == null) {
            if (isMandatory) {
                String msg = "Atributo " + name + " nao encontrado em " + node.getNodeName();
                throw new MalformedScenarioFileException(msg);
            }
            return null;
        }
        return attrNode.getNodeValue();
    }

    private Integer getIntAttribute(Node node, String name, boolean isMandatory) throws MalformedScenarioFileException {
        try {
            String nodeValue = getNodeValue(node, name, isMandatory);
            if (nodeValue != null)
                return Integer.parseInt(nodeValue);
            else
                return null;
        } catch (RuntimeException ex) {
            throw new MalformedScenarioFileException("Valor de atributo invalido: " + name);
        }
    }

    Long getLongAttribute(Node node, String name, boolean isMandatory) throws MalformedScenarioFileException {
        try {
            String nodeValue = getNodeValue(node, name, isMandatory);
            if (nodeValue != null)
                return Long.parseLong(nodeValue);
            else
                return null;
        } catch (RuntimeException ex) {
            throw new MalformedScenarioFileException("Valor de atributo invalido: " + name);
        }
    }

    private Float getFloatAttribute(Node node, String name, boolean isMandatory) throws MalformedScenarioFileException {
        try {
            String nodeValue = getNodeValue(node, name, isMandatory);
            if (nodeValue != null)
                return Float.parseFloat(nodeValue);
            else
                return null;
        } catch (RuntimeException ex) {
            throw new MalformedScenarioFileException("Valor de atributo invalido: " + name);
        }
    }

    Double getDoubleAttribute(Node node, String name, boolean isMandatory) throws MalformedScenarioFileException {
        try {
            String nodeValue = getNodeValue(node, name, isMandatory);
            if (nodeValue != null)
                return Double.parseDouble(nodeValue);
            else
                return null;
        } catch (RuntimeException ex) {
            throw new MalformedScenarioFileException("Valor de atributo invalido: " + name);
        }
    }

    String getStringAttribute(Node node, String name, boolean isMandatory) throws MalformedScenarioFileException {
        try {
            return getNodeValue(node, name, isMandatory);
        } catch (Exception ex) {
            throw new MalformedScenarioFileException("Valor de atributo invalido: " + name);
        }
    }

    protected String getStringAttribute(Node node, String name) throws MalformedScenarioFileException {
        return getStringAttribute(node, name, true);
    }

    Integer getIntAttribute(Node node, String name) throws MalformedScenarioFileException {
        return getIntAttribute(node, name, true);
    }

    protected Long getLongAttribute(Node node, String name) throws MalformedScenarioFileException {
        return getLongAttribute(node, name, true);
    }

    Float getFloatAttribute(Node node, String name) throws MalformedScenarioFileException {
        return getFloatAttribute(node, name, true);
    }

    Double getDoubleAttribute(Node node, String name) throws MalformedScenarioFileException {
        return getDoubleAttribute(node, name, true);
    }

    public abstract void buildEntity(Node node, Scenario sc) throws MalformedScenarioFileException;
}
