package br.ufrj.lam.microlam.data.loader;

import java.awt.geom.Point2D;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import br.ufrj.lam.microlam.core.ConstantVehicleInflowPolicy;
import br.ufrj.lam.microlam.core.InflowIntersection;
import br.ufrj.lam.microlam.core.ExponentialVehicleInflowPolicy;
import br.ufrj.lam.microlam.core.Intersection;
import br.ufrj.lam.microlam.core.OutflowIntersection;
import br.ufrj.lam.microlam.core.ProgrammableVehicleGenerationPolicy;
import br.ufrj.lam.microlam.core.Scenario;
import br.ufrj.lam.microlam.core.SignalizedIntersection;
import br.ufrj.lam.microlam.core.UniformVehicleGenerationPolicy;
import br.ufrj.lam.microlam.core.UnsignalizedIntersection;
import br.ufrj.lam.microlam.core.VehicleGenerationPolicy;
import br.ufrj.lam.microlam.core.VehicleInflowPolicy;
import br.ufrj.lam.microlam.core.VehicleInflowRateFunction;
import br.ufrj.lam.microlam.core.VehicleInflowRateFunctionConst;
import br.ufrj.lam.microlam.core.VehicleInflowRateFunctionSin;
import br.ufrj.lam.microlam.exceptions.MalformedScenarioFileException;
import br.ufrj.lam.microlam.signalcontrol.SignalController;

/**
 * @author Flavio
 */
public final class IntersectionFactory extends ScenarioEntityFactory {
    private static final String TAG_INTERSECTION = "intersection";
    private static final String TAG_VEHICLEINFLOWPOLICY = "vehicleinflowpolicy";
    private static final String TAG_VEHICLEGENERATIONPOLICY = "vehiclegenerationpolicy";
    private static final String TAG_RATE = "rate";
    private static final String TAG_VEHICLETYPE = "vehicletype";

    /**
     * Tipos de política de fluxo de entrada de veículos.
     */
    private static final String INFLOWPOLICY_EXPONENTIAL = "exponential";
    private static final String INFLOWPOLICY_CONSTANT = "constant";

    /**
     * Tipos de política de geração de veículos.
     */
    private static final String GENERATIONPOLICY_PROGRAMMABLE = "programmable";
    private static final String GENERATIONPOLICY_UNIFORM = "uniform";

    /**
     * Tipos de variação da taxa de entrada em função do tempo
     **/
    private static final String RATEFUNCTION_CONST = "const";
    private static final String RATEFUNCTION_SIN = "sin";

    static {
        ScenarioEntityFactoryMaker.getInstance().addMapFactory(TAG_INTERSECTION, new IntersectionFactory());
        ScenarioEntityFactoryMaker.getInstance().addMapFactory(TAG_VEHICLEINFLOWPOLICY, new IntersectionFactory());
    }

    private IntersectionFactory() {

    }

    @Override
    public void buildEntity(Node node, Scenario scenario)
            throws MalformedScenarioFileException {
        NamedNodeMap attrs = node.getAttributes();
        Intersection intersection;

        // recupera atributos vindos do XML:
        int id = getIntAttribute(node, "id");
        int x = getIntAttribute(node, "x");
        int y = getIntAttribute(node, "y");
        Point2D.Double location = new Point2D.Double(x, y);

        // verifica qual é o tipo da interseção sendo criada:
        String type = attrs.getNamedItem("type").getNodeValue();

        // interseção que representa um ponto de entrada de veículos no sistema:
        if ("inflow".equalsIgnoreCase(type)) {
            VehicleInflowPolicy vip = parseVehicleInflowPolicy(node);

            intersection = new InflowIntersection(id, location, vip);
        }
        // interseção que representa um ponto de saída de veículos do sistema:
        else if ("outflow".equalsIgnoreCase(type)) {
            intersection = new OutflowIntersection(id, location);
        }
        // interseção sinalizada:
        else if ("signalized".equalsIgnoreCase(type)) {
            // recupera signal controller relacionado a esta intersection (e que já deve ter sido criado):
            int controllerId = getIntAttribute(node, SignalControllerFactory.TAG_SIGNALCONTROLLER);
            SignalController sc = scenario.getSignalControllerById(controllerId);

            // cria interseção:
            intersection = new SignalizedIntersection(id, location, sc);
        } else if ("unsignalized".equalsIgnoreCase(type)) {
            intersection = new UnsignalizedIntersection(id, location);
        } else {
            String msg = "Atributo type nao encontrado para tag " + node.getNodeName();
            throw new MalformedScenarioFileException(msg);
        }
        scenario.add(intersection);
    }

    private VehicleInflowPolicy parseVehicleInflowPolicy(Node node) throws MalformedScenarioFileException {
        Element element = (Element) node;
        VehicleInflowPolicy vip;

        // Recupera nó com a política de fluxo de entrada de veículos
        NodeList childNodes = element.getElementsByTagName(TAG_VEHICLEINFLOWPOLICY);
        if (childNodes.getLength() != 1) {
            throw new MalformedScenarioFileException("XML do cenário está incorreto. Todo nó '" + TAG_INTERSECTION +
                    "' precisa ter um - e apenas um - nó filho '" + TAG_VEHICLEINFLOWPOLICY + "'.");
        }

        // Recupera o tipo do fluxo de entrada
        Element vipElement = (Element) childNodes.item(0);
        String policyType = vipElement.getAttribute("type");

        //Recupera política de geração de veículos
        VehicleGenerationPolicy vgp = parseVehicleGenerationPolicy(vipElement);

        // TODO tornar essa leitura do tipo de inflow policy um mecanismo dinâmico que permite novas policies sem precisar recompilar código
        if (INFLOWPOLICY_EXPONENTIAL.equalsIgnoreCase(policyType)) {
//			Double inflow = getDoubleAttribute(vipElement, "rate");
            Long vehicleCount = getLongAttribute(vipElement, "limit", false);
            if (vehicleCount == null)
                vehicleCount = VehicleInflowPolicy.VEHICLE_INFLOW_UNLIMITED;

            VehicleInflowRateFunction virf = parseVehicleInflowRateFunction(vipElement);

            vip = new ExponentialVehicleInflowPolicy(vgp, virf, vehicleCount);
        } else if (INFLOWPOLICY_CONSTANT.equalsIgnoreCase(policyType)) {
            Double headway = getDoubleAttribute(vipElement, "headway");
            Long vehicleCount = getLongAttribute(vipElement, "limit", false);
            if (vehicleCount == null)
                vehicleCount = VehicleInflowPolicy.VEHICLE_INFLOW_UNLIMITED;

            vip = new ConstantVehicleInflowPolicy(vgp, headway, vehicleCount);
        } else {
            throw new MalformedScenarioFileException("Tipo inválido de '" + TAG_VEHICLEINFLOWPOLICY + "': '" + policyType + "'");
        }

        return vip;
    }

    private VehicleInflowRateFunction parseVehicleInflowRateFunction(Element element) throws MalformedScenarioFileException {
        // Recupera nó com a política de fluxo de entrada de veículos
        NodeList childNodes = element.getElementsByTagName(TAG_RATE);
        if (childNodes.getLength() != 1) {
            throw new MalformedScenarioFileException("XML do cenário está incorreto. Todo nó '" + TAG_VEHICLEINFLOWPOLICY + "' precisa ter um - e apenas um - nó filho '" + TAG_RATE + "'.");
        }

        Element rateElement = (Element) childNodes.item(0);

        String rateFunction = rateElement.getAttribute("function");

        if (RATEFUNCTION_CONST.equalsIgnoreCase(rateFunction)) {
            String rateValue = rateElement.getAttribute("value");

            return new VehicleInflowRateFunctionConst(Long.parseLong(rateValue));
        } else if (RATEFUNCTION_SIN.equalsIgnoreCase(rateFunction)) {
            String ratePhase = rateElement.getAttribute("phase");
            String ratePeriod = rateElement.getAttribute("period");
            String rateLow = rateElement.getAttribute("low");
            String rateHigh = rateElement.getAttribute("high");

            return new VehicleInflowRateFunctionSin(Long.parseLong(ratePhase), Long.parseLong(ratePeriod), Long.parseLong(rateLow), Long.parseLong(rateHigh));
        } else {
            throw new MalformedScenarioFileException(String.format("Erro no XML do cenário. A função '%s' não é reconhecida como uma rate function válida", rateFunction));
        }
    }

    private VehicleGenerationPolicy parseVehicleGenerationPolicy(Element element) throws MalformedScenarioFileException {
        // Recupera nó com a política de fluxo de entrada de veículos
        NodeList childNodes = element.getElementsByTagName(TAG_VEHICLEGENERATIONPOLICY);
        if (childNodes.getLength() != 1) {
            throw new MalformedScenarioFileException("XML do cenário está incorreto. Todo nó '" + TAG_VEHICLEINFLOWPOLICY + "' precisa ter um - e apenas um - nó filho '" + TAG_VEHICLEGENERATIONPOLICY + "'.");
        }

        // Recupera o tipo do fluxo de entrada
        Element vgpElement = (Element) childNodes.item(0);
        String policyType = vgpElement.getAttribute("type");

        // Política de geração programável
        if (GENERATIONPOLICY_PROGRAMMABLE.equalsIgnoreCase(policyType)) {
            return parseProgramableVehicleGenerationPolicy(vgpElement);
        }
        // Política de geração aleatória com distribuição uniforme
        else if (GENERATIONPOLICY_UNIFORM.equalsIgnoreCase(policyType)) {
            return parseUniformVehicleGenerationPolicy(vgpElement);
        }
        // Tipo inválido de política de geração
        else {
            throw new MalformedScenarioFileException("Tipo inválido de '" + TAG_VEHICLEGENERATIONPOLICY + "': '" + policyType + "'");
        }
    }

    private UniformVehicleGenerationPolicy parseUniformVehicleGenerationPolicy(Element element) throws MalformedScenarioFileException {
        // Recupera os nós com os tipos de veículo
        NodeList childNodes = element.getElementsByTagName(TAG_VEHICLETYPE);
        if (childNodes.getLength() <= 0) {
            throw new MalformedScenarioFileException("XML do cenário está incorreto. Todo nó '" + TAG_VEHICLEGENERATIONPOLICY + "' precisa ter pelo menos um nó filho '" + TAG_VEHICLETYPE + "'.");
        }

        UniformVehicleGenerationPolicy vgp = new UniformVehicleGenerationPolicy();

        // Adiciona próximos tipos de veículo à fila da política de geração
        for (int i = 0; i < childNodes.getLength(); i++) {
            // Recupera o tipo de veículo
            Element vtElement = (Element) childNodes.item(i);
            String className = vtElement.getAttribute("classname");

            // Recupera a quantidade do tipo de veículo
            double ratio = getDoubleAttribute(vtElement, "ratio");
            if (ratio <= 0 || ratio > 1) {
                throw new MalformedScenarioFileException("XML do cenário está incorreto. Campo 'ratio' em '" + TAG_VEHICLETYPE + "' precisa ter valor no intervalo ]0,1].");
            }

            // Adiciona à fila
            try {
                vgp.addVehicleType(className, ratio);
            } catch (ClassNotFoundException e) {
                throw new MalformedScenarioFileException("XML do cenário está incorreto. A classe '" + className + "' não foi encontrada.");
            }
        }

        return vgp;
    }

    private ProgrammableVehicleGenerationPolicy parseProgramableVehicleGenerationPolicy(Element element) throws MalformedScenarioFileException {
        // Recupera os nós com os tipos de veículo
        NodeList childNodes = element.getElementsByTagName(TAG_VEHICLETYPE);
        if (childNodes.getLength() <= 0) {
            throw new MalformedScenarioFileException("XML do cenário está incorreto. Todo nó '" + TAG_VEHICLEGENERATIONPOLICY + "' precisa ter pelo menos um nó filho '" + TAG_VEHICLETYPE + "'.");
        }

        // Recupera o tipo de veículo
        Element vtElement = (Element) childNodes.item(0);
        String className = vtElement.getAttribute("classname");

        // Recupera a quantidade do tipo de veículo
        int quantity = getIntAttribute(vtElement, "quantity");
        if (quantity <= 0) {
            throw new MalformedScenarioFileException("XML do cenário está incorreto. Quantidade em '" + TAG_VEHICLETYPE + "' precisa ser maior que zero.");
        }

        // Instancia a política de geração de veículo
        ProgrammableVehicleGenerationPolicy pvgp;
        try {
            pvgp = new ProgrammableVehicleGenerationPolicy(className, quantity);
        } catch (ClassNotFoundException e) {
            throw new MalformedScenarioFileException("XML do cenário está incorreto. A classe '" + className + "' não foi encontrada.");
        }

        // Adiciona próximos tipos de veículo à fila da política de geração
        for (int i = 1; i < childNodes.getLength(); i++) {
            // Recupera o tipo de veículo
            vtElement = (Element) childNodes.item(i);
            className = vtElement.getAttribute("classname");

            // Recupera a quantidade do tipo de veículo
            quantity = getIntAttribute(vtElement, "quantity");
            if (quantity <= 0) {
                throw new MalformedScenarioFileException("XML do cenário está incorreto. Quantidade em '" + TAG_VEHICLETYPE + "' precisa ser maior que zero.");
            }

            // Adiciona à fila
            try {
                pvgp.addVehicleType(className, quantity);
            } catch (ClassNotFoundException e) {
                throw new MalformedScenarioFileException("XML do cenário está incorreto. A classe '" + className + "' não foi encontrada.");
            }
        }

        return pvgp;
    }
}
