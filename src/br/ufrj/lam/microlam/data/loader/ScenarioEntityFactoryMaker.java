package br.ufrj.lam.microlam.data.loader;

import java.util.HashMap;

/**
 * Singleton que guarda um mapa de factories para cada tipo de objeto
 * que pode ser instanciado num {@link br.ufrj.lam.microlam.core.Scenario}.
 * <p>
 * São as próprias factories que se "cadastram" nesse mapa, durante a
 * inicialização. Como exemplo, ver {@link RoadFactory}.
 *
 * @author Flavio
 */
public final class ScenarioEntityFactoryMaker {

    private static ScenarioEntityFactoryMaker sInstance;
    private HashMap<String, ScenarioEntityFactory> mFactories;

    private ScenarioEntityFactoryMaker() {
        mFactories = new HashMap<>();
    }

    static synchronized ScenarioEntityFactoryMaker getInstance() {
        if (sInstance == null) {
            sInstance = new ScenarioEntityFactoryMaker();
        }
        return sInstance;
    }

    void addMapFactory(String tag, ScenarioEntityFactory factory) {
        mFactories.put(tag, factory);
    }

    ScenarioEntityFactory getMapFactory(String tag) {
        return mFactories.get(tag);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (String key : mFactories.keySet()) {
            result.append(key).append("\n");
        }
        return result.toString();
    }
}
