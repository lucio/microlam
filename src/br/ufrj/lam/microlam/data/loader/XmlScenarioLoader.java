package br.ufrj.lam.microlam.data.loader;

import java.io.File;
import java.io.FileNotFoundException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import br.ufrj.lam.microlam.core.Scenario;
import br.ufrj.lam.microlam.exceptions.ScenarioLoadException;

/**
 * Classe responsável por carregar um cenário a partir de um arquivo XML.
 *
 * @author Flávio Faria
 * @see Scenario
 * @since Feb-05-2012
 */
public class XmlScenarioLoader {

    /**
     * Lê o cenário armazenado no arquivo passado em {@code scenarioPath}.
     *
     * @param scenarioPath Caminho do XML a ser lido.
     *                     no término da leitura.
     * @return o cenário lido.
     */
    public static Scenario load(String scenarioPath) throws ScenarioLoadException, FileNotFoundException {
        // TODO tornar esse passo abaixo dinâmico. Não pode ficar marretado aqui,
        // caso contrário factories de plugins futuros não poderão ser carregados
        // sem recompilar o código todo.
        try {
            /*
             * Inicializa os factories dos componentes do mapa. Necessário
             * para executar os static block initializers.
             */
            Class.forName(SignalControllerFactory.class.getName());
            Class.forName(RoadFactory.class.getName());
            Class.forName(IntersectionFactory.class.getName());
            Class.forName(RoadConnectionFactory.class.getName());
            Class.forName(CoordinatorFactory.class.getName());
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
            throw new ScenarioLoadException("Erro ao tentar iniciar factories.");
        }

        Scenario sc = new Scenario();
        try {
            File file = new File(scenarioPath);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);
            doc.getDocumentElement().normalize();

            Node root = doc.getElementsByTagName("scenario").item(0);
            NodeList children = root.getChildNodes();

            loadScenarioAttributes((Element) root, sc);

            ScenarioEntityFactoryMaker facMaker = ScenarioEntityFactoryMaker.getInstance();

            // para cada nó do XML:
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (!child.hasAttributes() && !child.hasChildNodes())
                    continue;

                // recupera a factory correspondente ao objeto do cenário correspondente à tag em questão:
                ScenarioEntityFactory mef = facMaker.getMapFactory(child.getNodeName());

                if (mef == null) {
                    throw new Exception("Tag '" + child.getNodeName() + "' desconhecida pelo XmlScenarioLoader!");
                }

                // cria novo objeto no cenário:
                mef.buildEntity(child, sc);
            }
        } catch (java.io.FileNotFoundException e) {
            throw e;
        } catch (Exception ex) {
            throw new ScenarioLoadException("Erro ao carregar cenario.", ex);
        }
        return sc;
    }

    private static void loadScenarioAttributes(Element root, Scenario scenario) throws ScenarioLoadException {
        // nome do cenário:
        String scenarioName = root.getAttribute("name");
        if (scenarioName == null)
            scenarioName = "Untitled";
        scenario.setName(scenarioName);

        // seed para o random number generator:
        String scenarioStrSeed = root.getAttribute("seed");
        Long scenarioSeed;
        try {
            scenarioSeed = Long.parseLong(scenarioStrSeed);
        } catch (NumberFormatException e) {
            scenarioSeed = null;
        }
        scenario.setSeed(scenarioSeed);

        // duração da simulação:
        String durationStr = root.getAttribute("duration");
        Long duration;
        try {
            duration = Long.parseLong(durationStr);
        } catch (NumberFormatException e) {
            duration = null;
        }
        scenario.setSimulationDuration(duration);

        // velocidade limite padrão das vias do cenário:
        String defaultSpeedLimitStr = root.getAttribute("defaultspeedlimit");
        if (defaultSpeedLimitStr.isEmpty())
            throw new ScenarioLoadException("Cenário precisa ter uma velocidade máxima padrão definida.");
        Double defaultSpeedLimit;
        try {
            defaultSpeedLimit = Double.parseDouble(defaultSpeedLimitStr);
        } catch (NumberFormatException e) {
            defaultSpeedLimit = null;
        }
        scenario.setDefaultSpeedLimit(defaultSpeedLimit);
    }
}
