package br.ufrj.lam.microlam.data.loader;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import br.ufrj.lam.microlam.core.Intersection;
import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.Scenario;
import br.ufrj.lam.microlam.exceptions.MalformedScenarioFileException;

/**
 * @author Flavio
 */
public final class RoadFactory extends ScenarioEntityFactory {
    private static final String TAG_ROAD = "road";

    static {
        ScenarioEntityFactoryMaker.getInstance().addMapFactory(TAG_ROAD, new RoadFactory());
    }

    private RoadFactory() {
    }

    @Override
    public void buildEntity(Node node, Scenario scenario) throws MalformedScenarioFileException {

        // recupera atributos vindos do XML:
        int id = getIntAttribute(node, "id");
        int upstreamId = getIntAttribute(node, "upstream");
        int downstreamId = getIntAttribute(node, "downstream");

        // recupera intersections que se ligam a esta road:
        Intersection usi = scenario.getIntersectionById(upstreamId);
        Intersection dsi = scenario.getIntersectionById(downstreamId);

        Road road = new Road(id, usi, dsi);

        String speedLimitStr = ((Element) node).getAttribute("speedlimit");
        if (speedLimitStr != null) {
            try {
                double speedLimit = Double.parseDouble(speedLimitStr);
                road.setSpeedLimit(speedLimit);
            } catch (NumberFormatException e) {
                // se não conseguir ler valor, ignora e vai em frente
            }
        }

        // cria road e adiciona ao cenário:
        scenario.add(road);
    }
}
