package br.ufrj.lam.microlam.data.loader;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import br.ufrj.lam.microlam.coordination.SMERCoordinator;
import br.ufrj.lam.microlam.coordination.SMERCorridor;
import br.ufrj.lam.microlam.coordination.SMERCorridorNode;
import br.ufrj.lam.microlam.core.Intersection;
import br.ufrj.lam.microlam.core.Road;
import br.ufrj.lam.microlam.core.Scenario;
import br.ufrj.lam.microlam.core.SignalizedIntersection;
import br.ufrj.lam.microlam.exceptions.MalformedScenarioFileException;
import br.ufrj.lam.microlam.signalcontrol.SMERCoordinatedSignalController;

public class CoordinatorFactory extends ScenarioEntityFactory {

    private static final String TAG_COORDINATOR = "smercoordinator";
    private static final String TAG_CORRIDOR = "corridor";
    private static final String TAG_CORRIDOR_INTERSECTION = "intersection";
    private static final String ATTR_STARTS_AT_ROAD = "startsatroad";
    private static final String ATTR_ID = "id";
    private static final String ATTR_NAME = "name";

    static {
        ScenarioEntityFactoryMaker.getInstance().addMapFactory(TAG_COORDINATOR, new CoordinatorFactory());
    }

    /**
     * Processa uma seção {@code <smercorridor>} dentro do XML do cenário.
     */
    @Override
    public void buildEntity(Node node, Scenario sc)
            throws MalformedScenarioFileException {
        // cria um sistema coordenador:
        SMERCoordinator coordinator = new SMERCoordinator();

        NodeList children = node.getChildNodes();

        /*
         * Este loop itera para cada corredor a ser criado no
         * sistema de coordenação.
         *
         * Cada corredor é necessariamente uma árvore, já que
         * um ciclo no corredor implica na impossibilidade de
         * se gerar offsets corretamente para cada interseção.
         */
        for (int i = 0; i < children.getLength(); i++) {
            Node corridorNode = children.item(i);
            if (!corridorNode.getNodeName().equals(TAG_CORRIDOR))
                continue;

            // recupera info sobre a interseção de início do corredor:
            int startRoadId = getIntAttribute(corridorNode, ATTR_STARTS_AT_ROAD);
            Road startRoad = sc.getRoadById(startRoadId);

            // captura o nome do corredor:
            String corridorName = getStringAttribute(corridorNode, ATTR_NAME, false);
            if (corridorName == null) corridorName = "unnamed corridor";

            // confirma que interseção downstream de road é do tipo certo:
            if (startRoad.getDownstreamIntersection() instanceof SignalizedIntersection &&
                    ((SignalizedIntersection) startRoad.getDownstreamIntersection()).getSignalController() instanceof SMERCoordinatedSignalController) {

                // pega a primeira interseção do corredor:
                Node firstIntersectionNode = null;
                NodeList corridorNodeChildren = corridorNode.getChildNodes();
                for (int j = 0; j < corridorNodeChildren.getLength(); j++) {
                    Node tempIntersectionNode = corridorNodeChildren.item(j);
                    if (!tempIntersectionNode.getNodeName().equals(TAG_CORRIDOR_INTERSECTION))
                        continue;

                    firstIntersectionNode = tempIntersectionNode;
                }

                if (firstIntersectionNode == null)
                    throw new MalformedScenarioFileException("O corredor '" + corridorName + "' precisa ter ao menos uma tag <intersection>.");

                int firstIntersectionId = getIntAttribute(firstIntersectionNode, ATTR_ID);
                Intersection firstIntersection = sc.getIntersectionById(firstIntersectionId);

                // verifica se a primeira interseção condiz com a interseção downstream da via de entrada do corredor
                // eu sei que há uma redundância aqui e não seria necessário explicitar essa primeira interseção no XML,
                // porém eu julguei que seria mais correto fazer assim, pois seria esquisito se somente a primeira inter-
                // seção não aparecesse no XML.
                if (startRoad.getDownstreamIntersection().getId() != firstIntersectionId)
                    throw new MalformedScenarioFileException("No corredor '" + corridorName + "', a interseção downstream da via 'startsatroad' não condiz com a primeira tag <intersection> encontrada.");

                // cria nó que será a raiz desse corredor:
                SMERCorridorNode rootNode = new SMERCorridorNode((SignalizedIntersection) firstIntersection); //, startRoad);
                // cria o corredor passando como parâmetro o nó raiz:
                SMERCorridor corridor = new SMERCorridor(corridorName, rootNode, startRoad);
                // faz recursão para montar a árvore do corredor:
                buildCorridor(corridor, rootNode, firstIntersectionNode, sc);
                // adiciona corredor ao sistema de coordenação:
                coordinator.addCorridor(corridor);
            } else {
                throw new MalformedScenarioFileException("Corredor '" + corridorName + "' só pode ser formado por interseções sinalizadas cujo controlador seja do tipo SMERCoordinatedSignalController.");
            }
        }

        sc.add(coordinator);
    }

    /**
     * Método recursivo para construção de um corredor SMER (SMERCorridor).
     *
     * @param corridor        Corredor que está sendo construído
     * @param parentNode      Nó que vai receber os filhos que serão processados nesta iteração da recursão
     * @param corridorXMLNode Nó XML que contém a informação crua ainda sobre os filhos a processar
     * @param scenario        Objeto Scenario usado para recuperar a Road cujo id é referenciado no XML
     */
    private void buildCorridor(SMERCorridor corridor, SMERCorridorNode parentNode, Node corridorXMLNode,
                               Scenario scenario) throws MalformedScenarioFileException {
        NodeList children = corridorXMLNode.getChildNodes();
        // para cada interseção downstream discriminada no XML:
        for (int i = 0; i < children.getLength(); i++) {
            Node corridorIntersection = children.item(i);
            if (!corridorIntersection.getNodeName().equals(TAG_CORRIDOR_INTERSECTION))
                continue;

            // pega interseção no Scenario:
            int intersectionId = getIntAttribute(corridorIntersection, ATTR_ID);
            Intersection intersection = scenario.getIntersectionById(intersectionId);

            // verifica se a interseção é do tipo certo:
            if (intersection instanceof SignalizedIntersection &&
                    ((SignalizedIntersection) intersection).getSignalController() instanceof SMERCoordinatedSignalController) {
                // seta flag na Road avisando que ela é coordenada - a única utilidade disso é mudar a cor das ondas verdes quando elas forem criadas numa via coordenada
                Road road = parentNode.getIntersection().getRoadDownstreamTo(intersection);
                road.setIsCoordinated(true);

                // cria nó:
                SMERCorridorNode currentNode = new SMERCorridorNode((SignalizedIntersection) intersection);
                corridor.addDownstreamNode(parentNode, currentNode);
                buildCorridor(corridor, currentNode, corridorIntersection, scenario);
            } else {
                throw new MalformedScenarioFileException("Corredor só pode ser formado por interseções sinalizadas cujo controlador seja do tipo SMERCoordinatedSignalController.");
            }
        }
    }
}
