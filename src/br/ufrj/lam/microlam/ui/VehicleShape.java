package br.ufrj.lam.microlam.ui;

import java.awt.geom.AffineTransform;

import br.ufrj.lam.microlam.core.VehicleDrawingInfo;

public class VehicleShape extends EntityRectangle {

    private VehicleDrawingInfo vehicle;
    private double positionX;
    private double positionY;
    private double rearPositionX;
    private double rearPositionY;

    private double length;

    public VehicleShape(VehicleDrawingInfo vehicle) {
        this.vehicle = vehicle;
        init();
    }

    private void init() {
        width = vehicle.getWidth();
        length = vehicle.getLength();

        update();
    }

    void update() {
        positionX = vehicle.getPosition().getX();
        positionY = vehicle.getPosition().getY();

        calculateRearBumperPosition();

        dx = positionX;
        dy = positionY;
        ux = rearPositionX;
        uy = rearPositionY;
        // passei a criar o retângulo fundamental NÃO rotacionado... a rotação vai ser feita via AffineTransform agora!
        makeRectangle(0); //vehicle.getAngle());

        AffineTransform tr = new AffineTransform();
        double halflen = (length / 2.d);
        // translada origem até o centro do veículo...
        tr.translate(positionX + halflen * Math.cos(vehicle.getAngle()), positionY + halflen * Math.sin(vehicle.getAngle()));
        // e só então rotaciona, para que ele gire em torno do seu centro:
        tr.rotate(vehicle.getAngle());
        // finalmente, retorna origem ao ponto inicial:
        tr.translate(-positionX, -positionY);

        shape.reset();
        shape.moveTo(leftUpstream.x, leftUpstream.y);
        shape.lineTo(leftDownstream.x, leftDownstream.y);
        shape.lineTo(rightDownstream.x, rightDownstream.y);
        shape.lineTo(rightUpstream.x, rightUpstream.y);
        shape.closePath();
        shape.transform(tr);
    }

    // A posição principal do veículo (fornecida pela engine) refere-se ao centro
    // do parachoque dianteiro.
    // Nesta função calculamos o ponto do centro do parachoque TRASEIRO.
    // Imagine o veículo visto por cima, com a traseira apontada para Oeste e a dianteira para Leste.
    // Posicionado dessa maneira, ele está com ângulo zero no círculo trigonométrico.
    private void calculateRearBumperPosition() {
        // modificado para usar AffineTransform acima:
        rearPositionX = positionX - length;
        rearPositionY = positionY;
    }

    @Override
    public double getWidth() {
        return width;
    }
}
