package br.ufrj.lam.microlam.ui;

import java.awt.Shape;

import br.ufrj.lam.microlam.detector.VehiclePointDetectorDrawingInfo;

public class VehicleDetectorShape extends EntityRectangle {

    private VehiclePointDetectorDrawingInfo vehicleDetectorDrawingInfo;

    VehicleDetectorShape(VehiclePointDetectorDrawingInfo vehicleDetectorDrawingInfo) {
        this.vehicleDetectorDrawingInfo = vehicleDetectorDrawingInfo;

        this.ux = vehicleDetectorDrawingInfo.getUpstreamPosition().getX();
        this.uy = vehicleDetectorDrawingInfo.getUpstreamPosition().getY();
        this.dx = vehicleDetectorDrawingInfo.getDownstreamPosition().getX();
        this.dy = vehicleDetectorDrawingInfo.getDownstreamPosition().getY();

        this.width = 0.5 * this.vehicleDetectorDrawingInfo.getRoadWidth();
        makeRectangle(this.vehicleDetectorDrawingInfo.getAngle());
    }

    @Override
    public double getWidth() {
        return this.width;
    }

    @Override
    public Shape getShape() {
        double x[] = {
                getRightUpstream().x,
                getLeftUpstream().x,
                getLeftDownstream().x,
                getRightDownstream().x
        };
        double y[] = {
                getRightUpstream().y,
                getLeftUpstream().y,
                getLeftDownstream().y,
                getRightDownstream().y
        };

        shape.reset();
        shape.moveTo(x[0], y[0]);
        shape.lineTo(x[1], y[1]);
        shape.lineTo(x[2], y[2]);
        shape.lineTo(x[3], y[3]);
        shape.closePath();

        return shape;
    }
}
