package br.ufrj.lam.microlam.ui;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 * Este {@link JFrame} é o entrypoint da aplicação para quem desejar executá-la
 * como uma aplicação desktop. É responsável por encapsular
 * o {@link MicroLAMPanel}.
 *
 * @author Flávio Faria
 * @see MicroLAMPanel
 * @since Feb-05-2012
 */
public class MicroLAMDesktopApp extends JFrame implements ComponentListener {

    /**
     * Membro que referencia a instância do {@link MicroLAMPanel}.
     */
    private MicroLAMPanel mPanel;

    /**
     * Construtor básico responsável por realizar as inicializações
     * básicas e setar os tamanhos dos componentes.
     */
    public MicroLAMDesktopApp() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        addComponentListener(this);
        Dimension d = new Dimension(800, 600);
        setPreferredSize(d);
        mPanel = new MicroLAMPanel();
        mPanel.setPreferredSize(d);
        add(mPanel);
        pack();
        mPanel.init();
    }

    @Override
    public void componentHidden(ComponentEvent arg0) {
    }

    @Override
    public void componentMoved(ComponentEvent arg0) {
    }

    /**
     * Implementação do
     * {@link ComponentListener#componentResized(ComponentEvent)}.
     * É usado para resetar o tamanho do {@link MicroLAMPanel }
     * contido neste {@link MicroLAMDesktopApp}.
     */
    @Override
    public void componentResized(ComponentEvent e) {
        mPanel.setPreferredSize(new Dimension(getWidth(), getHeight()));
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    /**
     * Entrypoint da aplicação para quem a executa como uma aplicação
     * desktop.
     */
    public static void main(String... args) {
        // Dispara a execução do loop do Swing (lá ele)
        java.awt.EventQueue.invokeLater(() -> {
            try {
                // Seta o look & feel padrão do sistema
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                // Apresenta na tela a janela principal
                new MicroLAMDesktopApp().setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        });
    }
}
