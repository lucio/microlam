package br.ufrj.lam.microlam.ui;

import java.awt.geom.Point2D;

class CurveShape extends EntityShape {

    CurveShape(RoadShape incomingRoad, RoadShape outgoingRoad) {
        super();

        // encontra os pontos de interseção das duas calçadas das vias que vão se ligar para formar a curva.
        // existem exatamente 2 vias: uma via por onde o fluxo chega e outra por onde ele sai.
        // quando falo em lado esquerdo e direito abaixo, me refiro ao lado das calçadas da via, do ponto de vista de quem segue no sentido do fluxo

        // interseção entre as calçadas esquerdas das vias
        Point2D.Double pLeftIntersection = calculateIntersection(incomingRoad.getLeftUpstream(), incomingRoad.getLeftDownstream(),
                outgoingRoad.getLeftUpstream(), outgoingRoad.getLeftDownstream());
        // interseção entre as calçadas direitas das vias
        Point2D.Double pRightIntersection = calculateIntersection(incomingRoad.getRightUpstream(), incomingRoad.getRightDownstream(),
                outgoingRoad.getRightUpstream(), outgoingRoad.getRightDownstream());

        // calcula o shape do cruzamento no sentido horário, começando pelo ponto da calçada esquerda
        // de rua de chegada onde começa a curva do cruzamento:
        shape.moveTo(incomingRoad.leftDownstream.x, incomingRoad.leftDownstream.y);
        shape.quadTo(pLeftIntersection.x, pLeftIntersection.y, outgoingRoad.leftUpstream.x, outgoingRoad.leftUpstream.y);
        shape.lineTo(outgoingRoad.rightUpstream.x, outgoingRoad.rightUpstream.y);
        shape.quadTo(pRightIntersection.x, pRightIntersection.y, incomingRoad.rightDownstream.x, incomingRoad.rightDownstream.y);
        shape.closePath();
    }

    /**
     * Rotina para calcular o ponto de interseção entre duas retas.
     * A reta 1 é definida pelos pontos inroadUp, inroadDn
     * A reta 2, por outroadUp, outroadDn.
     * <p>
     * Rotina adaptada a partir de
     * http://www.ahristov.com/tutorial/geometry-games/intersection-segments.html
     */
    private Point2D.Double calculateIntersection(Point2D.Double inroadUp, Point2D.Double inroadDn,
                                                 Point2D.Double outroadUp, Point2D.Double outroadDn) {
        double x1 = inroadUp.x;
        double y1 = inroadUp.y;
        double x2 = inroadDn.x;
        double y2 = inroadDn.y;

        double x3 = outroadUp.x;
        double y3 = outroadUp.y;
        double x4 = outroadDn.x;
        double y4 = outroadDn.y;

        double d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        if (d == 0) {  // se d==0, significa que as linhas são paralelas
            // A geometria analítica diz que as linhas podem ser coincidentes ou não;
            // porém, nesse caso elas só podem ser coincidentes (serão não estariam
            // ligadas à mesma interseção). Nesse caso, retorna inroadDn ou outroadUp (tanto faz)
            return new Point2D.Double(inroadDn.x, inroadDn.y);
        }

        double xi = (((x3 - x4) * (x1 * y2 - y1 * x2) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d);
        double yi = (((y3 - y4) * (x1 * y2 - y1 * x2) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d);

        return new Point2D.Double(xi, yi);
    }
}
