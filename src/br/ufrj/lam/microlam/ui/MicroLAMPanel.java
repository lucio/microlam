package br.ufrj.lam.microlam.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import br.ufrj.lam.microlam.MicroLAMApplication;
import br.ufrj.lam.microlam.core.Engine;
import br.ufrj.lam.microlam.core.GreenWaveSegmentDrawingInfo;
import br.ufrj.lam.microlam.core.IntersectionDrawingInfo;
import br.ufrj.lam.microlam.core.RoadConnectionDrawingInfo;
import br.ufrj.lam.microlam.core.RoadDrawingInfo;
import br.ufrj.lam.microlam.core.RouteDrawingInfo;
import br.ufrj.lam.microlam.core.SignalizedConnectionDrawingInfo;
import br.ufrj.lam.microlam.core.SignalizedIntersectionDrawingInfo;
import br.ufrj.lam.microlam.core.Utils;
import br.ufrj.lam.microlam.core.VehicleDrawingInfo;
import br.ufrj.lam.microlam.data.loader.XmlScenarioLoader;
import br.ufrj.lam.microlam.detector.VehiclePointDetectorDrawingInfo;
import br.ufrj.lam.microlam.exceptions.InvalidRoadIdException;
import br.ufrj.lam.microlam.exceptions.MicroLAMUnkownException;
import br.ufrj.lam.microlam.exceptions.RoutePickingException;
import br.ufrj.lam.microlam.exceptions.UnknownVehicleContainerException;
import br.ufrj.lam.microlam.exceptions.VehicleCopyConstructorMissingException;
import br.ufrj.lam.microlam.report.ReportManager;
import br.ufrj.lam.microlam.signalcontrol.SignalControllerException;
import br.ufrj.lam.microlam.ui.hud.HUDInfoWindow;
import br.ufrj.lam.microlam.ui.hud.HUDSignalizedIntersectionInfo;
import br.ufrj.lam.microlam.ui.hud.HUDVehicleInfoWindow;
import br.ufrj.lam.microlam.ui.hud.HUDWindow;
import br.ufrj.lam.microlam.ui.hud.MicroLAMHUDManager;

/**
 * Panel que mostra uma animação do resultado da simulação da MicroLAMEngine.
 * Caso queira apenas rodar uma simulação sem visualização, execute o MicroLAMApplication.
 *
 * @author Lucio Paiva
 * @see MicroLAMApplication
 * @since Mar-08-2011
 */
public class MicroLAMPanel extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener, KeyListener {

    private static final long serialVersionUID = 1L;
    private static final boolean ANTI_ALIASING = true;

    private static final int TIME_WARP = 1;
    private static int timeWarp = TIME_WARP;
    private static boolean isPaused = false;

    private static final Color backgroundColor = new Color(0x409040);
    private static final Color mapFontColor = new Color(0x004000);
    private static final Font mapFont = new Font(null, Font.PLAIN, 6);
    private static final Color roadColor = new Color(0x707070);
    private static final Color greenWaveColor = new Color(0x00D500);
    private static final Color detectorColor = new Color(0xFF70FF);
    private static final Stroke detectorStroke = new BasicStroke(0.1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    private static final Color selectedRoadColor = new Color(0x858585);
    private static final Color intersectionColor = new Color(0x757575);
    private static final Color carOnIntersectionColor = new Color(0x9090ff);
    private static final Color selectedCarColor = new Color(0xFFFF00);
    private static final Color selectedBackCarColor = new Color(0xFF0000);
    private static final Color selectedFrontCarColor = new Color(0x00FF00);

    private static final Font defaultFont = new Font("SanSerif", Font.PLAIN, 12);

    private static int appletWidth, appletHeight;
    private static Image roadLayer;
    private static Image greenWaveLayer;
    private static Image routeLayer;
    private static Image carsLayer;
    private static Image scene;
    private static MicroLAMHUDManager hudManager;
    private static HUDInfoWindow hudScenarioInfoWindow;
    private static HUDSignalizedIntersectionInfo hudSignalizedIntersectionInfoWindow;
    private static HUDVehicleInfoWindow hudVehicleInfoWindow;
    private static Graphics2D buffGraphics;

    private static boolean backgroundChanged;
    private static boolean flagTranslate;
    private static int mouseOrgX, mouseOrgY;
    private static double translateX, translateY, translatePrevX, translatePrevY;

    private static boolean flagScale;
    private static double canvasScale;
    private static double canvasScalePrev;
    private static final double SCALE_STEP = 0.010;

    private static final double FRAME_DURATION_MS = 1000d / 60d; // tempo que leva mostrando 1 frame (o inverso de fps), em ms
    private static long fps, fpsCount, fpsDisplay;
    private static long processingTime, pTimeAvg, pTimeDisplay;
    private static long selectedRoadId = 1;
    private static VehicleDrawingInfo selectedVehicle;
    private static boolean changedSelectedVehicle = false;
    private static SignalizedIntersectionDrawingInfo selectedIntersection;
    private static boolean changedSelectedIntersection = false;
    private static ArrayList<Long> selectedVehicleRearVehicleIds = new ArrayList<>();
    private static long selectedVehicleFrontVehicleId = -1;

    private static Engine engine;

    MicroLAMPanel() throws HeadlessException {
    }

    void init() {
        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
        setFocusable(true);
        addKeyListener(this);
        setFocusTraversalKeysEnabled(false);

        // inicia engine -------------------------------------------
        try {
//			engine = new Engine(XmlScenarioLoader.load("scenarios/controlador_var_amp.xml"), FRAME_DURATION_MS);
//			engine = new Engine(XmlScenarioLoader.load("scenarios/controlador_const_heter_200_200.xml"), FRAME_DURATION_MS);
//			engine = new Engine(XmlScenarioLoader.load("scenarios/cruzamento_smer.xml"), FRAME_DURATION_MS);
            engine = new Engine(XmlScenarioLoader.load("scenarios/manhattan_smer.xml"), FRAME_DURATION_MS);
//		    engine = new Engine(XmlScenarioLoader.load("scenarios/singlecar.xml"), FRAME_DURATION_MS);
//			engine = new Engine(XmlScenarioLoader.load("scenarios/two_corridors_coord_smer.xml"), FRAME_DURATION_MS);
//			engine = new Engine(XmlScenarioLoader.load("scenarios/three_corridors_coord_smer.xml"), FRAME_DURATION_MS);
//			engine = new Engine(XmlScenarioLoader.load("scenarios/corridor_two_way_coord_smer.xml"), FRAME_DURATION_MS);
            ReportManager.setupManager(engine, null, null);

            /*
             * Os parametros abaixo têm que ser
             * setados antes de iniciar a engine, pois eles vão participar do
             * calculo dos offsets dos corredores coordenados. se esses parametros
             * forem setado depois, os corredores não vão funcionar como esperado.
             */
            engine.setSeed(42);
            engine.setGlobalSpeedLimit(60);
            engine.setGlobalControllerPhaseTiming(27, 2, 1);
            engine.setCoordinatedStartupLostTime(2.0);
            engine.setCoordinatedGreenWaveSpeedFactor(1.0);

            engine.start();

            /*
             * Os demais argumentos devem ser setados após o início da engine.
             */
            engine.setGlobalSMERControlReversibilityChangePeriod(5);
            engine.setGlobalSMERCoordinationReversibilityChangePeriod(10);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        initDraw();

        translateX = translateY = 0;

        Thread t = new Thread(() -> {
            while (true) {
                try {
                    drawScene();
                    stepEngine();
                } catch (InterruptedException e) {
                    return;
                }
            }
        });
        t.setDaemon(true);
        t.start();
    }

    private synchronized void initDraw() {
        appletWidth = getPreferredSize().width;
        appletHeight = getPreferredSize().height;

        // prepara layer das vias ----------------------------------
        roadLayer = this.createImage(appletWidth, appletHeight);
        buffGraphics = getGraphics(roadLayer);
        buffGraphics.clearRect(0, 0, appletWidth, appletHeight);

        // desenha layer das vias ----------------------------------
        drawRoadLayer();

        // prepara layer das ondas verdes --------------------------
        greenWaveLayer = this.createImage(appletWidth, appletHeight);
        buffGraphics = getGraphics(greenWaveLayer);
        buffGraphics.clearRect(0, 0, appletWidth, appletHeight);

        // desenha layer das ondas verdes --------------------------
        drawGreenWaveLayer();

        // prepara layer da rota do veículo selecionado ------------
        routeLayer = this.createImage(appletWidth, appletHeight);
        buffGraphics = getGraphics(routeLayer);
        buffGraphics.clearRect(0, 0, appletWidth, appletHeight);

        // prepara layer da rota do veículo selecionado ------------
        drawRouteLayer();

        // prepara layer dos veículos ------------------------------
        carsLayer = this.createImage(appletWidth, appletHeight);
        buffGraphics = getGraphics(carsLayer);
        buffGraphics.clearRect(0, 0, appletWidth, appletHeight);

        // prepara a imagem que vai receber todas as sobreposições de layers:
        scene = this.createImage(appletWidth, appletHeight);
        buffGraphics = getGraphics(scene);
        buffGraphics.clearRect(0, 0, appletWidth, appletHeight);

        buffGraphics.setFont(defaultFont);

        // prepara HUD manager
        hudManager = new MicroLAMHUDManager(scene, appletWidth, appletHeight);
        hudScenarioInfoWindow = new HUDInfoWindow("Scenario Info", hudManager, 10, 10);
        hudManager.addWindow(hudScenarioInfoWindow);
        hudVehicleInfoWindow = new HUDVehicleInfoWindow(hudManager, 320, 10);
        hudManager.addWindow(hudVehicleInfoWindow);
        if (engine.getSignalizedIntersectionsDrawingInfo().size() > 0) {
            selectedIntersection = engine.getSignalizedIntersectionsDrawingInfo().get(0);
            hudSignalizedIntersectionInfoWindow = new HUDSignalizedIntersectionInfo("Signalized Intersection", hudManager, 10, 150);
            hudSignalizedIntersectionInfoWindow.setSignalizedIntersection(selectedIntersection);
        }
        hudManager.addWindow(hudSignalizedIntersectionInfoWindow);

        fps = 0;
        fpsCount = 0;
        pTimeAvg = 0;

        backgroundChanged = true;
        flagScale = false;
        flagTranslate = false;
        canvasScale = 1;
    }

    private Graphics2D getGraphics(Image layer) {
        Graphics2D g = (Graphics2D) layer.getGraphics();
        if (ANTI_ALIASING)
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        return g;
    }

    @Override
    public void setPreferredSize(Dimension preferredSize) {
        super.setPreferredSize(preferredSize);
        if (engine != null) {
            initDraw();
        }
    }

    private synchronized void drawScene() {
        AffineTransform tx = getCanvasTransformation();

        // evita refazer o fundo desnecessariamente; checa antes a flag que diz se algo mudou desde o último render
        if (backgroundChanged) {
            /*
             * Layer das vias.
             * Na maior parte do tempo ele permanece constante,
             * logo poucas vezes será redesenhado.
             */
            buffGraphics = getGraphics(roadLayer);
            // começa com um canvas com a cor de fundo padrão e mais nada:
            buffGraphics.setBackground(backgroundColor);
            buffGraphics.clearRect(0, 0, appletWidth, appletHeight);

            buffGraphics.transform(tx);

            drawRoadLayer();

            backgroundChanged = false;
        }

        /*
         * Layer das ondas verdes.
         * Pega layer das vias e desenha em cima dele.
         */
        buffGraphics = getGraphics(greenWaveLayer);
        AffineTransform orgwavetx = buffGraphics.getTransform();
        {
            buffGraphics.drawImage(roadLayer, 0, 0, this);
            buffGraphics.transform(tx);
            drawGreenWaveLayer();
        }
        buffGraphics.setTransform(orgwavetx);

        /*
         *  Layer das rotas do veículo selecionado.
         */
        buffGraphics = getGraphics(routeLayer);
        AffineTransform routetx = buffGraphics.getTransform();
        {
            buffGraphics.drawImage(greenWaveLayer, 0, 0, this);
            buffGraphics.transform(tx);
            drawRouteLayer();
        }
        buffGraphics.setTransform(routetx);

        /*
         * Layer dos veículos e dos semáforos.
         * Pega layer das ondas verdes e desenha em cima dele
         * (com o desenho das ondas verdes já feito por cima
         * do desenho das vias).
         */
        buffGraphics = getGraphics(carsLayer);
        AffineTransform orgtx = buffGraphics.getTransform();
        {
            // copia o desenho das vias com ondas verdes que já foi previamente renderizado:
            buffGraphics.drawImage(routeLayer, 0, 0, this);
            buffGraphics.transform(tx);
            // desenha posição atual dos veículos:
            drawVehiclesLayer();
            drawTrafficLights();
        }
        buffGraphics.setTransform(orgtx);

        /*
         * Imprime tudo no buffer da cena final.
         */
        buffGraphics = getGraphics(scene);
        buffGraphics.drawImage(carsLayer, 0, 0, this);

        /*
         * Por fim aplica o layer do HUD.
         */
        updateScenarioHUDInfo();
        updateSignalizedIntersectionHUDInfo();
        updateVehicleHUDInfo();
        hudManager.drawHUD();

        /*
         * Ao final temos a seguinte pilha de layers (topo da pilha em cima):
         *
         * HUD
         * Semáforos
         * Veículos
         * Ondas verdes
         * Vias
         * Background
         *
         */

        repaint();
    }

    private void updateVehicleHUDInfo() {
        boolean forceUpdate = false;
        if (changedSelectedVehicle) {
            changedSelectedVehicle = false;
            hudVehicleInfoWindow.setVehicle(selectedVehicle);
            if (isPaused) forceUpdate = true;
        }
        hudVehicleInfoWindow.updateInfo(engine.getElapsedTimeInSeconds(), forceUpdate);
    }

    private void updateSignalizedIntersectionHUDInfo() {
        if (changedSelectedIntersection) {
            changedSelectedIntersection = false;
            hudSignalizedIntersectionInfoWindow.setSignalizedIntersection(selectedIntersection);
        }
        hudSignalizedIntersectionInfoWindow.updateInfo(engine.getElapsedTimeInSeconds());
    }

    private void updateScenarioHUDInfo() {
        ArrayList<String> infos = new ArrayList<>();

        infos.add(getTimeStr());
        infos.add(getFramesPerSecondAndProcTimeStr());

        String roadDensity;
        try {
            roadDensity = String.format("Road %d density: %.2f", selectedRoadId, engine.getRoadCapacity(selectedRoadId));
        } catch (InvalidRoadIdException e) {
            roadDensity = "Invalid road id";
        }
        infos.add(roadDensity);

        infos.add(getSeedStr());

        hudScenarioInfoWindow.updateInfo(infos);
    }

    /**
     * @return string formatada mostrando o tempo total de execução até o momento, em hh:mm:ss, seguido do time warp sendo usado atualmente.
     * Exemplo: 'Elapsed time: 0h10m57s (5x)'
     */
    private String getTimeStr() {
        String timeStr = Utils.getTimeStr((long) engine.getElapsedTimeInSeconds());

        String timeWarpStr = isPaused ? "paused" : String.valueOf(timeWarp) + "x";

        return String.format("Elapsed time: %s (%s)", timeStr, timeWarpStr);
    }

    /**
     * @return string com FPS e tempo de processamento necessário para gerar o estado mais recente da simulação.
     * Exemplo: '58 fps | procTime: 486us'
     */
    private String getFramesPerSecondAndProcTimeStr() {
        if (fpsCount > 20) {
            fpsCount = 0;
            fps /= 20;
            fpsDisplay = (long) (1000d / (double) fps);  // converte de periodo para frequencia
            pTimeAvg /= 20;
            pTimeDisplay = pTimeAvg / 1000; // converte para microssegundos
            fps = 0;
            pTimeAvg = 0;
        }
        return fpsDisplay + "fps | procTime: " + pTimeDisplay + "us";
    }

    /**
     * @return string com o valor sendo usado pelo seed do gerador de números pseudo-aleatórios da engine.
     */
    private String getSeedStr() {
        return "Seed = " + engine.getSeed();
    }

    /**
     * Aplica zoom e translação do sistema no canvas.
     *
     * @return matriz com as transformações a serem executadas
     */
    private AffineTransform getCanvasTransformation() {
        AffineTransform tx = new AffineTransform();

        // Inicialmente translada origem do sistema para o centro do canvas, de modo que o scale
        // que vem a seguir seja baseado nesse ponto (experimente comentar esse translate inicial
        // para ver como o scaling seguinte se comporta):
        double centerX = (appletWidth / 2d);
        double centerY = (appletHeight / 2d);
        tx.translate(centerX, centerY);

        // Converte origem do canvas do java (que fica no canto superior esquerdo) para origem do
        // plano cartesiano (canto inferior esquerdo) ao multiplicar eixo y por -1.
        // Além disso, aplica escala:
        tx.scale(canvasScale, -canvasScale); //tx.scale(1, -1);

        // retorna o sistema para a origem após o scaling, mas soma em cima o drag do usuario:
        tx.translate(-centerX + translateX, -centerY + translateY);

        return tx;
    }

    private void drawGreenWaveLayer() {
        for (GreenWaveSegmentDrawingInfo gwi : engine.getGreenWavesDrawingInfo()) {
            GreenWaveSegmentShape wave = new GreenWaveSegmentShape(gwi);

            drawGreenWave(wave.getShape(), gwi.getColor());
        }
    }

    /**
     * Rotina que cuida do render da parte estática da cena (ruas, cruzamentos
     * e demais objetos que não se movem).
     */
    private void drawRoadLayer() {

        // desenha vias:
        for (RoadDrawingInfo rdi : engine.getRoadsDrawingInfo()) {
            RoadShape road = new RoadShape(rdi);

            drawRoad(road, rdi.getId() == selectedRoadId);

            // desenha sensores da via:
            for (VehiclePointDetectorDrawingInfo detector : rdi.getVehiclePointDetectorsDrawingInfo()) {
                VehicleDetectorShape detectorShape = new VehicleDetectorShape(detector);

                drawDetector(detectorShape.getShape());
            }
        }

        // desenha interseções:
        for (IntersectionDrawingInfo idi : engine.getIntersectionsDrawingInfo()) {
            for (RoadConnectionDrawingInfo rcdi : idi.getRoadConnectionsDrawingInfo()) {
                // as RoadConnections de entrada do sistema não possuem incomingRoad. Checo antes para
                // ver se é null, pois se for, não queremos desenhar:
                if (rcdi.getIncomingRoadPosition() != null) {
                    RoadShape r1 = new RoadShape(rcdi.getIncomingRoadPosition());
                    RoadShape r2 = new RoadShape(rcdi.getOutgoingRoadPosition());
                    CurveShape curve = new CurveShape(r1, r2);

                    drawIntersectionPart(curve);
                }
            }
            drawIntersectionName(idi);
        }
    }

    private void drawRouteLayer() {
        if (selectedVehicle != null) {
            Color roadsBehindColor = new Color(1, 1, 0, 0.35f);
            Color roadsAheadColor = new Color(0, 1, 1, 0.35f);

            RouteDrawingInfo routedi = selectedVehicle.getRouteDrawingInfo();
            List<RoadDrawingInfo> roadsBehind = routedi.getRoadsBehind();
            for (RoadDrawingInfo roaddi : roadsBehind) {
                RoadShape roadShape = new RoadShape(roaddi);
                drawRouteSegment(roadShape.getShape(), roadsBehindColor);
            }
            List<RoadDrawingInfo> roadsAhead = routedi.getRoadsAhead();
            for (RoadDrawingInfo roaddi : roadsAhead) {
                RoadShape roadShape = new RoadShape(roaddi);
                drawRouteSegment(roadShape.getShape(), roadsAheadColor);
            }
        }
    }

    private void drawRouteSegment(Shape shape, Color c) {
        drawVehicleContainer(shape, c);
    }

    private void drawIntersectionName(IntersectionDrawingInfo idi) {
        AffineTransform tx = buffGraphics.getTransform();
        buffGraphics.translate(idi.getPosition().getX(), idi.getPosition().getY());
        buffGraphics.scale(1, -1); // reverte o eixo y para que o texto nao saia de cabeça pra baixo
        buffGraphics.setPaint(mapFontColor);
        buffGraphics.setFont(mapFont);
        buffGraphics.drawString(idi.getName(), -3, 15);
        buffGraphics.setTransform(tx);
    }

    private void drawIntersectionPart(CurveShape curve) {
        drawVehicleContainer(curve.getShape(), intersectionColor);
    }

    private void drawGreenWave(Shape shape, Color color) {
        drawVehicleContainer(shape, color == null ? greenWaveColor : color);
    }

    private void drawRoad(RoadShape road, boolean isSelected) {
        drawVehicleContainer(road.getShape(), isSelected ? selectedRoadColor : roadColor);

        AffineTransform tx = buffGraphics.getTransform();
        buffGraphics.translate(road.ux, road.uy);
        buffGraphics.rotate(road.getAngle());
        buffGraphics.scale(1, -1); // reverte o eixo y para que o texto nao saia de cabeça pra baixo
        buffGraphics.setPaint(mapFontColor);
        buffGraphics.setFont(mapFont);
        buffGraphics.drawString(road.getName(), 10, -5);
        buffGraphics.setTransform(tx);
    }

    private void drawVehicleContainer(Shape shape, Color color) {
        buffGraphics.setColor(color);
        buffGraphics.fill(shape);
    }

    private void drawDetector(Shape shape) {
        buffGraphics.setPaint(detectorColor);
        buffGraphics.setStroke(detectorStroke);
        buffGraphics.draw(shape);
    }

    /**
     * Rotina que cuida do render de tudo que se move na cena do simulador (basicamente,
     * todos os veículos).
     */
    private void drawVehiclesLayer() {
        if (selectedVehicle != null) {
            if (engine.isVehicleStillOnTheMap(selectedVehicle.getId())) {
                selectedVehicleFrontVehicleId = engine.getVehiclesFrontVehicleId(selectedVehicle.getId());
                selectedVehicleRearVehicleIds = engine.getVehiclesBackVehicleIds(selectedVehicle.getId());
            } else {
                changedSelectedVehicle = true;
                selectedVehicle = null;
                selectedVehicleFrontVehicleId = -1;
                selectedVehicleRearVehicleIds = null;
            }
        } else {
            /*
             * Seleciona um veículo ao acaso se não houver nenhum selecionado.
             */
            if (engine.getVehiclesDrawingInfo().size() > 0) {
                changedSelectedVehicle = true;
                selectedVehicle = engine.getVehiclesDrawingInfo().get(0);
            }
        }

        for (VehicleDrawingInfo vdi : engine.getVehiclesDrawingInfo()) {
            VehicleShape vehicleShape = vdi.getVehicleShape();
            vehicleShape.update();

            Color drawColor = vdi.getColor();
            if ((selectedVehicle != null) && (vdi.getId() == selectedVehicle.getId()))
                drawColor = selectedCarColor;
            else if ((selectedVehicleRearVehicleIds != null) && selectedVehicleRearVehicleIds.contains(vdi.getId()))
                drawColor = selectedBackCarColor;
            else if (vdi.getId() == selectedVehicleFrontVehicleId)
                drawColor = selectedFrontCarColor;
            else if (vdi.isOnIntersection())
                drawColor = carOnIntersectionColor;

            drawVehicle(vehicleShape.getShape(), drawColor);
        }

    }

    private void drawVehicle(Shape shape, Color carColor) {
        buffGraphics.setColor(carColor);
        buffGraphics.fill(shape);
    }

    private void drawTrafficLights() {
        // para cada interseção sinalizada
        for (SignalizedIntersectionDrawingInfo sidi : engine.getSignalizedIntersectionsDrawingInfo()) {
            // para cada conexão da interseção
            for (SignalizedConnectionDrawingInfo scdi : sidi.getConnectionsInfo()) {
                Color col;

                switch (scdi.getLight()) {
                    case GREEN:
                        col = Color.GREEN;
                        break;
                    case AMBER:
                        col = Color.YELLOW;
                        break;
                    default:
                        col = Color.RED;
                }

                drawTrafficLight(scdi.getIncomingRoadInfo(), col);
            }
        }
    }

    private static final double LIGHT_DIAM = 2.2;
    private static final double LIGHT_RADIUS = LIGHT_DIAM / 2d;

    private void drawTrafficLight(RoadDrawingInfo road, Color col) {
        Point2D.Double pos = new Point2D.Double(road.getDownstreamPosition().x, road.getDownstreamPosition().y);

        pos.x -= LIGHT_RADIUS;
        pos.y -= LIGHT_RADIUS;

        double dist = road.getRoadWidth() / 2d + 2;

        pos.x += dist * Math.sin(road.getAngle());
        pos.y -= dist * Math.cos(road.getAngle());

        buffGraphics.setColor(col);
        Ellipse2D trafficLight = new Ellipse2D.Double(pos.x, pos.y, LIGHT_DIAM, LIGHT_DIAM);


        buffGraphics.fill(trafficLight);
    }

    private void stepEngine() throws InterruptedException {
        double dt = System.currentTimeMillis();

        try {
            processingTime = System.nanoTime();

            if (!isPaused) {
                for (int i = 0; i < timeWarp; i++)
                    engine.step();
            }

            processingTime = System.nanoTime() - processingTime;
            pTimeAvg += processingTime;

        } catch (RoutePickingException | UnknownVehicleContainerException | SignalControllerException e) {
            e.printStackTrace();
        }

        // calcula o tempo de sleep em tempo real de maneira que se tenha um fluxo o mais próximo possível de 60fps:
        long howLongToSleep = (long) (FRAME_DURATION_MS - processingTime / (double) (1000 * 1000));
        if (howLongToSleep < 0) howLongToSleep = 0;
        Thread.sleep(howLongToSleep);

        dt = System.currentTimeMillis() - dt;
        fps += dt;
        fpsCount++;
    }

    public void paint(Graphics g) {
        g.drawImage(scene, 0, 0, this);
    }

    public void update(Graphics g) {
        paint(g);
    }

    /**
     * Interação de clique com os objetos do cenário.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        Point clickPoint = e.getPoint();
        Point tPoint = new Point();

        try {
            getCanvasTransformation().inverseTransform(clickPoint, tPoint);
        } catch (NoninvertibleTransformException e1) {
            e1.printStackTrace();
        }

        // testa por clique em veículo:
        for (VehicleDrawingInfo vdi : engine.getVehiclesDrawingInfo()) {
            VehicleShape vehicle = new VehicleShape(vdi);
            if (vehicle.getShape().contains(tPoint)) {
                selectedVehicle = vdi;
                changedSelectedVehicle = true;
                e.consume();
                return;
            }
        }

        // testa por clique em via:
        for (RoadDrawingInfo rdi : engine.getRoadsDrawingInfo()) {
            RoadShape road = new RoadShape(rdi);
            if (road.getShape().contains(tPoint)) {
                selectedRoadId = rdi.getId();
                backgroundChanged = true;
                e.consume();
                return;
            }
        }

        // testa por clique em cruzamento sinalizado:
        for (SignalizedIntersectionDrawingInfo ip : engine.getSignalizedIntersectionsDrawingInfo()) {
            for (SignalizedConnectionDrawingInfo rcdi : ip.getConnectionsInfo()) {
                RoadShape r1 = new RoadShape(rcdi.getIncomingRoadInfo());
                RoadShape r2 = new RoadShape(rcdi.getOutgoingRoadInfo());
                CurveShape curve = new CurveShape(r1, r2);
                if (curve.getShape().contains(tPoint)) {
                    selectedIntersection = ip;
                    changedSelectedIntersection = true;
                    e.consume();
                    return;
                }
            }
        }

        e.consume();
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // primeiro checa se mouse foi pressionado sobre uma janela do HUD
        HUDWindow window = hudManager.getWindowAt(e.getX(), e.getY());
        if (window != null) {
            hudManager.startWindowDrag(window, e.getX(), e.getY());
        } else {
            // caso não tenha sido pressionado sobre uma janela do HUD, trata interação de zoom/translação com a cena:
            if (e.getButton() == MouseEvent.BUTTON1) {
                flagTranslate = true;
                mouseOrgX = e.getX();
                mouseOrgY = e.getY();
                translatePrevX = translateX;
                translatePrevY = translateY;

                setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
            } else if (e.getButton() == MouseEvent.BUTTON3) {
                flagScale = true;
                mouseOrgY = e.getY();
                canvasScalePrev = canvasScale;

                setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
            }
        }

        e.consume();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            flagTranslate = false;
            hudManager.stopWindowDrag();
        } else if (e.getButton() == MouseEvent.BUTTON2) {
            flagScale = false;
        }
        setCursor(Cursor.getDefaultCursor());

        e.consume();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (hudManager.isDraggingWindow()) {
            hudManager.dragWindow(e.getX(), e.getY());
        } else {
            if (flagTranslate) {
                translateX = translatePrevX + (e.getX() - mouseOrgX) / canvasScale;
                translateY = translatePrevY + (-1) * (e.getY() - mouseOrgY) / canvasScale;

                backgroundChanged = true;
            } else if (flagScale) {
                canvasScale = canvasScalePrev + (e.getY() - mouseOrgY) * SCALE_STEP;
                if (canvasScale < SCALE_STEP) canvasScale = SCALE_STEP;

                backgroundChanged = true;
            }
        }

        e.consume();
    }

    @Override
    public void mouseMoved(MouseEvent arg0) {
    }


    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        canvasScale += e.getWheelRotation() * SCALE_STEP * -1;
        if (canvasScale < SCALE_STEP) canvasScale = SCALE_STEP;

        backgroundChanged = true;
        e.consume();
    }

    private static void simulationPauseToggle() {
        isPaused = !isPaused;
    }

    private static void simulationSpeedSlower() {
        if (timeWarp > 1)
            timeWarp /= 2;
        else {
            timeWarp = 1;
            isPaused = true;
        }
    }

    private static void simulationSpeedFaster() {
        if (isPaused)
            isPaused = false;
        else {
            if (timeWarp < 128)
                timeWarp *= 2;
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyChar()) {
            case '[':
                simulationSpeedSlower();
                break;
            case ']':
                simulationSpeedFaster();
                break;
            case 'p':
                simulationPauseToggle();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
