package br.ufrj.lam.microlam.ui.hud;

import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

public abstract class HUDWindow {
    private static final int MIN_HEIGHT = 10;
    private static final int MIN_WIDTH = 10;

    private MicroLAMHUDManager hudManager;
    private Rectangle rect;
    private AffineTransform affineTransform;
    private String windowTitle;

    String getWindowTitle() {
        return windowTitle;
    }

    HUDWindow(String windowTitle, MicroLAMHUDManager hudManager, int leftPos, int topPos) {
        this.windowTitle = windowTitle;
        this.hudManager = hudManager;
        this.rect = new Rectangle(leftPos, topPos, 0, 0);

        resetAffineTransform();
    }

    public abstract void drawWindow();

    int getLeftPos() {
        return this.rect.x;
    }

    void setLeftPos(int x) {
        if (x < 0) {
            x = 0;
        } else if (x > hudManager.getAppletWidth() - getWidth()) {
            x = hudManager.getAppletWidth() - getWidth();
        }

        this.rect.x = x;

        resetAffineTransform();
    }

    int getTopPos() {
        return this.rect.y;
    }

    void setTopPos(int y) {
        if (y < 0) {
            y = 0;
        } else if (y > hudManager.getAppletHeight() - getHeight()) {
            y = hudManager.getAppletHeight() - getHeight();
        }

        this.rect.y = y;

        resetAffineTransform();
    }

    public int getWidth() {
        return this.rect.width;
    }

    protected void setWidth(int width) {
        if (width < MIN_WIDTH)
            this.rect.width = MIN_WIDTH;
        else
            this.rect.width = width;
    }

    void setHeight(int height) {
        if (height < MIN_HEIGHT)
            this.rect.height = MIN_HEIGHT;
        else
            this.rect.height = height;
    }

    int getHeight() {
        return this.rect.height;
    }

    MicroLAMHUDManager getHudManager() {
        return hudManager;
    }

    boolean isPointInsideMe(int x, int y) {
        return this.rect.contains(x, y);
    }

    private void resetAffineTransform() {
        this.affineTransform = new AffineTransform();
        this.affineTransform.translate(getLeftPos(), getTopPos());
    }

    AffineTransform getWindowAffineTransform() {
        return this.affineTransform;
    }
}
