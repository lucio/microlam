package br.ufrj.lam.microlam.ui.hud;

import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

/**
 * Gerencia as janelas do HUD do MicroLAM applet.
 *
 * @author luciopaiva
 * @since 22-Jan-2012
 */
public class MicroLAMHUDManager {
    private Image scene;
    private int appletWidth, appletHeight;
    private boolean isDraggingWindow;
    private HUDWindow windowToDrag;
    private int orgMouseLeft, orgMouseTop;

    private ArrayList<HUDWindow> windows;

    public MicroLAMHUDManager(Image scene, int appletWidth, int appletHeight) {
        super();

        this.scene = scene;
        this.appletWidth = appletWidth;
        this.appletHeight = appletHeight;
        this.windows = new ArrayList<>();
    }

    public void addWindow(HUDWindow window) {
        windows.add(window);
    }

    public void drawHUD() {
        // desenha subjanelas
        for (HUDWindow window : windows) {
            window.drawWindow();
        }
    }

    public HUDWindow getWindowAt(int x, int y) {
        for (HUDWindow window : windows) {
            if (window.isPointInsideMe(x, y)) {
                return window;
            }
        }
        return null;
    }

    public void startWindowDrag(HUDWindow windowToDrag, int orgMouseX, int orgMouseY) {
        this.windowToDrag = windowToDrag;
        this.isDraggingWindow = true;
        this.orgMouseLeft = orgMouseX;
        this.orgMouseTop = orgMouseY;
    }

    public void stopWindowDrag() {
        this.windowToDrag = null;
        this.isDraggingWindow = false;
    }

    public void dragWindow(int x, int y) {
        if (isDraggingWindow && windowToDrag != null) {
            int deltaX = x - orgMouseLeft;
            int deltaY = y - orgMouseTop;
            windowToDrag.setLeftPos(windowToDrag.getLeftPos() + deltaX);
            windowToDrag.setTopPos(windowToDrag.getTopPos() + deltaY);
            orgMouseLeft = x;
            orgMouseTop = y;
        }
    }

    int getAppletWidth() {
        return appletWidth;
    }

    int getAppletHeight() {
        return appletHeight;
    }

    Graphics2D getGraphics() {
        return (Graphics2D) this.scene.getGraphics();
    }

    public boolean isDraggingWindow() {
        return isDraggingWindow;
    }
}
