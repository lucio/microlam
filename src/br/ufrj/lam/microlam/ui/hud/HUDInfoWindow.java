package br.ufrj.lam.microlam.ui.hud;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.util.ArrayList;

public class HUDInfoWindow extends HUDWindow {
    private ArrayList<String> infos = new ArrayList<>();

    public HUDInfoWindow(String windowTitle, MicroLAMHUDManager hudManager, int leftPos, int topPos) {
        super(windowTitle, hudManager, leftPos, topPos);

        setWidth(300);
        setHeight(130);
    }

    public void updateInfo(ArrayList<String> infos) {
        this.infos = infos;
    }

    @Override
    public void drawWindow() {
        Graphics2D bg = getHudManager().getGraphics();

        AffineTransform orgtx = bg.getTransform();
        AffineTransform newtx = getWindowAffineTransform();

        bg.transform(newtx);

        Path2D path = new Path2D.Float();

        // window bar
        final int BAR_HEIGHT = 20;
        path.moveTo(0, 0);
        path.lineTo(getWidth(), 0);
        path.lineTo(getWidth(), BAR_HEIGHT);
        path.lineTo(0, BAR_HEIGHT);
        path.closePath();
        bg.setColor(Color.WHITE);
        bg.fill(path);
        bg.setPaint(Color.BLACK);
        bg.drawString(this.getWindowTitle(), 10, 15);

        // window background
        path = new Path2D.Float();
        path.moveTo(0, BAR_HEIGHT);
        path.lineTo(getWidth(), BAR_HEIGHT);
        path.lineTo(getWidth(), getHeight() - BAR_HEIGHT);
        path.lineTo(0, getHeight() - BAR_HEIGHT);
        path.closePath();
        bg.setColor(new Color(0.3f, 0.3f, 1.f, 0.5f));
        bg.fill(path);

        int strX = 10;
        int strY = 40;

        bg.setColor(Color.WHITE);
        for (String info : infos) {
            bg.drawString(info, strX, strY);
            strY += 20;
        }

        bg.setTransform(orgtx);
    }
}
