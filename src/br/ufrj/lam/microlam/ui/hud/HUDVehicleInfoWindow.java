package br.ufrj.lam.microlam.ui.hud;

import java.util.ArrayList;

import br.ufrj.lam.microlam.core.VehicleDrawingInfo;

public class HUDVehicleInfoWindow extends HUDInfoWindow {

    private VehicleDrawingInfo vehicle;

    private double lastUpdate;
    private static final double UPDATE_STEP = 0.1;

    public HUDVehicleInfoWindow(MicroLAMHUDManager hudManager, int leftPos, int topPos) {
        super("Vehicle Info", hudManager, leftPos, topPos);

        this.setHeight(200);
    }

    public void setVehicle(VehicleDrawingInfo vehicle) {
        this.vehicle = vehicle;
    }

    public void updateInfo(double elapsedTimeInSeconds, boolean forceUpdate) {
        // atualiza apenas 1 vez por segundo:
        if ((this.lastUpdate + UPDATE_STEP < elapsedTimeInSeconds) || forceUpdate) {
            this.lastUpdate = elapsedTimeInSeconds;

            ArrayList<String> infos = new ArrayList<>();
            if (this.vehicle != null) {
                infos.add(String.format("Vehicle id: %d", this.vehicle.getId()));
                infos.add(String.format("-> Vel.: %.1fkm/h", this.vehicle.getVelocity()));
                infos.add(String.format("-> Acc.: %.2fm/s^2", this.vehicle.getAcceleration()));
                if (this.vehicle.getFrontVehicleDrawingInfo() != null) {
                    VehicleDrawingInfo frontVehicle = this.vehicle.getFrontVehicleDrawingInfo();
                    infos.add(String.format("Front Vehicle id: %d", frontVehicle.getId()));
                    infos.add(String.format("-> Distance: %.1fm", this.vehicle.getDistanceToFrontVehicle()));
                    infos.add(String.format("-> Vel.: %.1fkm/h", frontVehicle.getVelocity()));
                    infos.add(String.format("-> Acc.: %.2fm/s^2", frontVehicle.getAcceleration()));
                }
                super.updateInfo(infos);
            } else {
                super.updateInfo(infos);
            }
        }
    }
}
