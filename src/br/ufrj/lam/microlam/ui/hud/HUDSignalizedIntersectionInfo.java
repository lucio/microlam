package br.ufrj.lam.microlam.ui.hud;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.HashMap;

import br.ufrj.lam.microlam.core.SignalizedConnectionDrawingInfo;
import br.ufrj.lam.microlam.core.SignalizedIntersection.Light;
import br.ufrj.lam.microlam.core.SignalizedIntersectionDrawingInfo;
import br.ufrj.lam.microlam.core.VehicleDrawingInfo;

public class HUDSignalizedIntersectionInfo extends HUDWindow {

    private SignalizedIntersectionDrawingInfo intersection;

    private HashMap<Integer, SignalizedConnectionDrawingInfo> roadConnections;
    /**
     * Mapa <road_connection_id, array de histórico de cor do semáforo de road_connection_id ao longo do tempo>
     */
    private HashMap<Integer, ArrayList<Light>> lightHistory;
    /**
     * Matriz tridimensional.
     * A primeira dimensão corresponde a cada um dos road connections (id da connection).
     * A segunda dimensão é a visão daquele road connection num determinado instante de tempo (array).
     * A terceira dimensão é a coleção da posição dos veículos naquele determinado instante (mapa de <vehicle id, position>).
     */
    private HashMap<Integer, ArrayList<HashMap<Long, Double>>> vehiclePositionHistory;

    private double lastUpdate;
    private static final double UPDATE_STEP = 0.1;

    /**
     * Quantos segundos devem ser desenhados no mesmo instante de visualização.
     */
    private static final int TIME_WINDOW = 20;
    private static final int TIME_RESOLUTION = (int) (TIME_WINDOW / (float) UPDATE_STEP);

    public HUDSignalizedIntersectionInfo(String windowTitle, MicroLAMHUDManager hudManager, int leftPos, int topPos) {
        super(windowTitle, hudManager, leftPos, topPos);

        this.roadConnections = new HashMap<>();
        this.lightHistory = new HashMap<>();
        this.vehiclePositionHistory = new HashMap<>();
        this.lastUpdate = 0;

        setWidth(300);
        setHeight(300);
    }

    @Override
    public void drawWindow() {
        Graphics2D bg = getHudManager().getGraphics();

        AffineTransform orgtx = bg.getTransform();
        AffineTransform newtx = getWindowAffineTransform();

        bg.transform(newtx);

        Path2D path = new Path2D.Float();

        // window bar
        final int BAR_HEIGHT = 20;
        path.moveTo(0, 0);
        path.lineTo(getWidth(), 0);
        path.lineTo(getWidth(), BAR_HEIGHT);
        path.lineTo(0, BAR_HEIGHT);
        path.closePath();
        bg.setColor(Color.WHITE);
        bg.fill(path);
        bg.setPaint(Color.BLACK);
        bg.drawString(this.getWindowTitle(), 10, 15);

        // window background
        path = new Path2D.Float();
        path.moveTo(0, BAR_HEIGHT);
        path.lineTo(getWidth(), BAR_HEIGHT);
        path.lineTo(getWidth(), getHeight() - BAR_HEIGHT);
        path.lineTo(0, getHeight() - BAR_HEIGHT);
        path.closePath();
        bg.setColor(new Color(0.3f, 0.3f, 1.f, 0.5f));
        bg.fill(path);

        int strX = 10;
        int strY = 40;
        bg.setColor(Color.WHITE);
        String name = String.format("Intersection %s", this.intersection.getName());
        bg.drawString(name, strX, strY);

        float ypos = 50;
        for (Integer connectionId : this.lightHistory.keySet()) {
            ArrayList<Light> lights = this.lightHistory.get(connectionId);
            ArrayList<HashMap<Long, Double>> vehiclePositionHistory = this.vehiclePositionHistory.get(connectionId);
            drawPhaseHistory(bg, ypos, connectionId, lights, vehiclePositionHistory);
            ypos += 105;
        }

        bg.setTransform(orgtx);
    }

    private void drawPhaseHistory(Graphics2D g, float ypos, int connectionId, ArrayList<Light> lightHistory,
                                  ArrayList<HashMap<Long, Double>> vehiclePositionHistory) {
        Path2D path = new Path2D.Float();

        float margin_left = 20;
        float margin_right = 10;
        float canvas_height = 100;

        /*
         * Escreve nome da connection.
         */
        String incomingRoadName = this.roadConnections.get(connectionId).getIncomingRoadInfo().getName();
        AffineTransform tx = g.getTransform();
        {
            g.translate(margin_left - 2, ypos + canvas_height / 2 + 20);
            g.rotate(3 * Math.PI / 2f);
            g.setColor(Color.ORANGE);
            g.setFont(new Font("Arial", Font.PLAIN, 11));
            g.drawString(incomingRoadName, 0, 0);
        }
        g.setTransform(tx);

        /*
         * pinta fundo branco
         */
        path.moveTo(margin_left, ypos);
        path.lineTo(getWidth() - margin_right, ypos);
        path.lineTo(getWidth() - margin_right, ypos + canvas_height);
        path.lineTo(margin_left, ypos + canvas_height);
        path.closePath();
        g.setColor(Color.WHITE);
        g.fill(path);

        /*
         * desenha segmentos representando cada segundo da cor do sinal desta road connection
         */
        float x = margin_left;
        float step = (getWidth() - margin_left - margin_right) / (float) TIME_RESOLUTION;
        float y = ypos + 84;
        float signalbarheight = 6;
        for (Light light : lightHistory) {
            float nextX = x + step;
            path = new Path2D.Float();
            path.moveTo(x, y);
            path.lineTo(nextX, y);
            path.lineTo(nextX, y + signalbarheight);
            path.lineTo(x, y + signalbarheight);
            path.closePath();
            Color color;
            switch (light) {
                case GREEN:
                    color = Color.GREEN;
                    break;
                case AMBER:
                    color = Color.YELLOW;
                    break;
                case RED:
                default:
                    color = Color.RED;
                    break;
            }
            g.setColor(color);
            g.fill(path);
            x += step;
        }

        /*
         * desenha caminhos percorridos pelos veículos que
         * se aproximam da interseção downstream.
         */
        x = margin_left;
        step = (getWidth() - margin_left - margin_right) / (float) TIME_RESOLUTION;
        y = ypos;
        float h = canvas_height - signalbarheight - 10;
        final float MAXDIST = 150; // enxerga até 150 metros upstream
        for (int i = 0; i < vehiclePositionHistory.size() - 1; i++) {
            HashMap<Long, Double> vehiclePositions = vehiclePositionHistory.get(i);
            HashMap<Long, Double> nextVehiclePositions = vehiclePositionHistory.get(i + 1);

            for (Long vehicleId : vehiclePositions.keySet()) {
                float initialPosition = vehiclePositions.get(vehicleId).floatValue();
                if (initialPosition > MAXDIST)
                    continue;

                float finalPosition;
                if (nextVehiclePositions.containsKey(vehicleId)) {
                    finalPosition = nextVehiclePositions.get(vehicleId).floatValue();
                } else { // veículo sumiu no step seguinte porque atravessou o semáforo
                    finalPosition = -20;
                }

                /*
                 * converte distância para a interseção downstream (em metros)
                 * para altura no gráfico (em pixels)
                 */
                float y1 = y + h * (MAXDIST - initialPosition) / MAXDIST;
                float y2 = y + h * (MAXDIST - finalPosition) / MAXDIST;

                path = new Path2D.Float();
                path.moveTo(x, y1);
                path.lineTo(x + step, y2);
                g.setColor(Color.BLUE);
                g.draw(path);
            }
            x += step;
        }
    }

    /**
     * Método chamado quando se deseja mudar a interseção sinalizada
     * que está sendo analisada na janela atual.
     */
    public void setSignalizedIntersection(SignalizedIntersectionDrawingInfo signalizedIntersectionDrawingInfo) {
        this.intersection = signalizedIntersectionDrawingInfo;
        this.lightHistory.clear();
        this.vehiclePositionHistory.clear();
        this.roadConnections.clear();

        for (SignalizedConnectionDrawingInfo connection : this.intersection.getConnectionsInfo()) {
            // inicia arrays:

            this.roadConnections.put(connection.getId(), connection);

            ArrayList<Light> lights = new ArrayList<>();
            this.lightHistory.put(connection.getId(), lights);

            ArrayList<HashMap<Long, Double>> vehiclePositions = new ArrayList<>();
            this.vehiclePositionHistory.put(connection.getId(), vehiclePositions);
        }
    }

    public void updateInfo(double elapsedTimeInSeconds) {
        // atualiza apenas 1 vez por segundo:
        if (this.lastUpdate + UPDATE_STEP < elapsedTimeInSeconds) {
            this.lastUpdate = elapsedTimeInSeconds;

            // atualiza array com o histórico da cor do sinal para cada road connection da interseção:
            for (SignalizedConnectionDrawingInfo connection : this.intersection.getConnectionsInfo()) {
                ArrayList<Light> lights = this.lightHistory.get(connection.getId());
                lights.add(connection.getLight());
                /*
                 * mantém um histórico de TIME_RESOLUTION segundos
                 */
                if (lights.size() > TIME_RESOLUTION)
                    lights.remove(0);
                this.lightHistory.put(connection.getId(), lights);

                // pega a coleção de histórico de veículos para esta connection:
                ArrayList<HashMap<Long, Double>> vehiclePositionHistory = this.vehiclePositionHistory.get(connection.getId());
                HashMap<Long, Double> vehiclePositions = new HashMap<>();
                for (VehicleDrawingInfo vehicle : connection.getIncomingRoadInfo().getVehiclesDrawingInfo()) {
                    double distanceToDownstream = vehicle.getDistanceToDownstream();
                    vehiclePositions.put(vehicle.getId(), distanceToDownstream);
                }
                vehiclePositionHistory.add(vehiclePositions);
                /*
                 * mantém um histórico de TIME_RESOLUTION segundos
                 */
                if (vehiclePositionHistory.size() > TIME_RESOLUTION)
                    vehiclePositionHistory.remove(0);
                this.vehiclePositionHistory.put(connection.getId(), vehiclePositionHistory);
            }
        }
    }
}
