package br.ufrj.lam.microlam.ui;

import java.awt.Shape;

import br.ufrj.lam.microlam.core.RoadDrawingInfo;

public class RoadShape extends EntityRectangle {

    private RoadDrawingInfo road;
    private double length = -1;

    RoadShape(RoadDrawingInfo road) {
        this.ux = road.getUpstreamPosition().getX();
        this.uy = road.getUpstreamPosition().getY();
        this.dx = road.getDownstreamPosition().getX();
        this.dy = road.getDownstreamPosition().getY();

        this.road = road;

        this.width = road.getRoadWidth();
        makeRectangle(road.getAngle());
    }

    public double getLength() {
        if (length < 0)
            recalculateLength();
        return length;
    }

    private void recalculateLength() {
        double a = this.dx - this.ux;
        double b = this.dy - this.uy;
        length = Math.sqrt(a * a + b * b);
    }

    @Override
    public double getWidth() {
        return this.width;
    }

    @Override
    public Shape getShape() {
        double x[] = {
                getRightUpstream().x,
                getLeftUpstream().x,
                getLeftDownstream().x,
                getRightDownstream().x
        };
        double y[] = {
                getRightUpstream().y,
                getLeftUpstream().y,
                getLeftDownstream().y,
                getRightDownstream().y
        };

        shape.reset();
        shape.moveTo(x[0], y[0]);
        shape.lineTo(x[1], y[1]);
        shape.lineTo(x[2], y[2]);
        shape.lineTo(x[3], y[3]);
        shape.closePath();

        return shape;
    }

    public double getAngle() {
        return road.getAngle();
    }

    public String getName() {
        return road.getName();
    }
}
