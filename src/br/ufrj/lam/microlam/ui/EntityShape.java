package br.ufrj.lam.microlam.ui;

import java.awt.Shape;
import java.awt.geom.Path2D;

public abstract class EntityShape {

    Path2D.Double shape;

    EntityShape() {
        shape = new Path2D.Double();
    }

    public Shape getShape() {
        return shape;
    }
}
