package br.ufrj.lam.microlam.ui;

import java.awt.geom.Point2D;

public abstract class EntityRectangle extends EntityShape {

    double ux, uy, dx, dy;
    protected double width;

    Point2D.Double rightUpstream, leftUpstream, rightDownstream, leftDownstream;

    /**
     * Cálcula um retângulo baseado na posição e ângulo de inclinação do objeto na simulação.
     * <p>
     * Considere os dois exemplos relatados abaixo: uma via e um veículo.
     * <p>
     * A via é representada por dois pontos: (ux,uy), ponto "mais upstream" da via, colocado
     * exatamente no centro da largura da via; (dx,dy), ponto "mais downstream", também
     * centrado em relação à largura da via.
     * <p>
     * O veículo é representado também por dois pontos: (ux,uy), centro do parachoque dianteiro;
     * (dx,dy), centro do parachoque traseiro.
     *
     * @param angle valor previamente calculado do ângulo do objeto (essa informação poderia ser
     *              extraída dos dois pontos que chegam como parâmetro; porém, como o ângulo já teve que
     *              ser calculado antes para o veículo, aproveito o cálculo.
     */
    void makeRectangle(double angle) {
        double WDIV2 = width / 2.0d;

        // ponto direito upstream
        double rux = ux + WDIV2 * Math.sin(angle);
        double ruy = uy - WDIV2 * Math.cos(angle);

        // ponto esquerdo upstream
        double lux = ux - WDIV2 * Math.sin(angle);
        double luy = uy + WDIV2 * Math.cos(angle);

        // ponto esquerdo downstream
        double ldx = dx - WDIV2 * Math.sin(angle);
        double ldy = dy + WDIV2 * Math.cos(angle);

        // ponto direito downstream
        double rdx = dx + WDIV2 * Math.sin(angle);
        double rdy = dy - WDIV2 * Math.cos(angle);

        rightUpstream = new Point2D.Double(rux, ruy);
        leftUpstream = new Point2D.Double(lux, luy);
        rightDownstream = new Point2D.Double(rdx, rdy);
        leftDownstream = new Point2D.Double(ldx, ldy);
    }

    public abstract double getWidth();

    Point2D.Double getRightUpstream() {
        return rightUpstream;
    }

    Point2D.Double getLeftUpstream() {
        return leftUpstream;
    }

    Point2D.Double getRightDownstream() {
        return rightDownstream;
    }

    Point2D.Double getLeftDownstream() {
        return leftDownstream;
    }
}
