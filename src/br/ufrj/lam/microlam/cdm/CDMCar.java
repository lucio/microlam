package br.ufrj.lam.microlam.cdm;

import br.ufrj.lam.microlam.idm.IDMCar;

public class CDMCar extends CDMVehicle {

    public CDMCar(IDMCar source) {
        super(source);
    }

    public CDMCar(long id) {
        super(id);
    }
}
