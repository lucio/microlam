package br.ufrj.lam.microlam.cdm;

import java.awt.Color;

import br.ufrj.lam.microlam.idm.IDMCar;

public abstract class CDMVehicle extends IDMCar {

    CDMVehicle(IDMCar source) {
        super(source);
    }

    CDMVehicle(long id) {
        super(id);
    }

    @Override
    public Color getColor() {
        return Color.ORANGE;
    }
}
