package br.ufrj.lam.microlam.core;

import java.util.Vector;

public interface SignalizedIntersectionDrawingInfo {

    Vector<SignalizedConnectionDrawingInfo> getConnectionsInfo();

    String getName();

    long getId();
}
