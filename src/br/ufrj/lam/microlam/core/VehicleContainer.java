package br.ufrj.lam.microlam.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;
import java.awt.geom.Point2D;

/**
 * Abstração de um elemento de via que contém veículos.
 * Pode ser uma rua ou uma interseção de ruas, por exemplo.
 *
 * @author Lucio
 * @since 12-Mar-2011
 */
public abstract class VehicleContainer {
    protected long id;
    protected String name = null;
    private boolean hasSpeedLimit;
    /**
     * Velocidade máxima na via, em metros por segundo.
     */
    private double speedLimit;
    private PaceVehicle speedLimitVehicle;

    /**
     * Veículos contidos nesta instância.
     **/
    Vector<Vehicle> vehicles;

    VehicleContainer(int id) {
        this.id = id;
        vehicles = new Vector<>();
        hasSpeedLimit = false;
        speedLimitVehicle = new PaceVehicle();
    }

    public abstract Vehicle getFrontVehicle(Vehicle me);

    public abstract Vehicle getMostUpstreamVehicle(Vehicle vehicle);

    /**
     * Calcula a posição absoluta do veículo do plano cartesiano do sistema,
     * baseado na posição dele ao longo do comprimento da via.
     *
     * @param vehicle Veículo cuja posição será calculada
     * @return ponto [x,y] do veículo no plano do sistema
     */
    public abstract Point2D.Double getVehiclePosition(Vehicle vehicle);

    /**
     * Adiciona um veículo à via.
     *
     * @param vehicle Veículo a ser adicionado
     */
    // TODO verificar antes se há espaço para inserir o veículo; talvez retornar Exception se não der
    // TODO:LANE decidir em qual faixa o veículo entra - atualmente o veículo decide qual é e pronto (não pergunta se pode entrar)
    // TODO caso o carro queira entrar numa posição específica, precisa antes perguntar se o espaço está disponível
    // TODO adicionar ordenadamente - é complicado pq quem detem a posição do veículo é o próprio veículo; se alguém muda a posição dele do nada, um evento teria que ser disparado para reordenar a lista da via!
    void addVehicle(Vehicle vehicle) {
        vehicles.add(vehicle);
    }

    /**
     * Método para cancelar a cronometragem do headway atual.
     * Usado para não gerar headway quando houver um sinal fechado,
     * pois vai gerar uma leitura incorreta.
     */
    void resetHeadwayTime() {
        previousVehicleDepartureTime = 0;
    }

    /**
     * Registro da saída de um veículo da via.
     *
     * @param vehicle       Veículo que deixou a via.
     * @param departureTime Timestamp do momento em que o veículo saiu.
     */
    void removeVehicle(Vehicle vehicle, double departureTime) {
        boolean wasRemoved = vehicles.remove(vehicle);

        if (wasRemoved) {
            /*
             * Atualmente estou interessado apenas em gerar estatísticas para ruas.
             * Se no futuro for necessário gerar para interseções, vai ser preciso
             * tirar esta condição daqui.
             */
            if (this instanceof Road)
                updateVehicleStatistics(vehicle, departureTime);
        }
    }

    private ArrayList<Double> stoppedTimes = new ArrayList<>();
    private ArrayList<Double> travelTimes = new ArrayList<>();
    private ArrayList<Double> averageSpeeds = new ArrayList<>();
    private double maximumObservedSpeed = Double.MIN_VALUE;
    private double minimumObservedSpeed = Double.MAX_VALUE;
    private long numberOfVehiclesSinceBeginningOfSimulation = 0;
    private double previousVehicleDepartureTime = 0;
    private ArrayList<Double> headways = new ArrayList<>();

    private void updateVehicleStatistics(Vehicle vehicle, double departureTime) {
        numberOfVehiclesSinceBeginningOfSimulation++;
        stoppedTimes.add(vehicle.getLocalStoppedTime());
        travelTimes.add(vehicle.getLocalTravelTime());
        averageSpeeds.add(vehicle.getLocalAverageSpeed());
        if (vehicle.getLocalMaxObservedSpeed() > maximumObservedSpeed)
            maximumObservedSpeed = vehicle.getLocalMaxObservedSpeed();
        if (vehicle.getLocalMinObservedSpeed() < minimumObservedSpeed)
            minimumObservedSpeed = vehicle.getLocalMinObservedSpeed();

        /*
         * Calcula headway (intervalo entre a saída deste veículo e a do anterior a ele).
         */
        if (previousVehicleDepartureTime != 0) // se está com uma cronometragem em aberto, ou seja, se houve uma passagem de veículo anterior válida para gerar headway
        {
            double headway = departureTime - previousVehicleDepartureTime;
            headways.add(headway);
        }
        previousVehicleDepartureTime = departureTime;
    }

    long getNumberOfVehiclesSinceBeginningOfSimulation() {
        return this.numberOfVehiclesSinceBeginningOfSimulation;
    }

    ArrayList<Double> getHeadways() {
        return this.headways;
    }

    ArrayList<Double> getStoppedTimes() {
        return this.stoppedTimes;
    }

    ArrayList<Double> getTravelTimes() {
        return this.travelTimes;
    }

    ArrayList<Double> getAverageSpeeds() {
        return this.averageSpeeds;
    }

    public long getId() {
        return id;
    }

    public abstract String getIdStr();

    @Override
    public String toString() {
        return this.getIdStr();
    }

    public Collection<Vehicle> getVehicles() {
        return vehicles;
    }

    /**
     * @param speedInKmPerHour velocidade limite em quilômetros por hora. Se for igual a zero, desliga o limitador de velocidade desta via.
     */
    public void setSpeedLimit(double speedInKmPerHour) {
        if (speedInKmPerHour > 0) {
            hasSpeedLimit = true;
            this.speedLimit = speedInKmPerHour / 3.6d;
            this.speedLimitVehicle.setCurrentVelocity(this.speedLimit);
        } else
            hasSpeedLimit = false;
    }

    public double getSpeedLimit() {
        if (hasSpeedLimit)
            return this.speedLimit;
        else
            return Double.POSITIVE_INFINITY;
    }

    PaceVehicle getPaceVehicle(double position) {
        // TODO o pace car está entrando 10m à frente da posição do veiculo que pede o pace car (ou em relacao ao inicio da via,
        // caso o veículo que está pedindo esteja numa via anterior)... isso estah marretado e deveria usar o s0 do IDM como referencia
        this.speedLimitVehicle.setCurrentContainerPosition(position + 10);
        return this.speedLimitVehicle;
    }

    boolean hasSpeedLimit() {
        return this.hasSpeedLimit;
    }

    public String getName() {
        return name;
    }
}
