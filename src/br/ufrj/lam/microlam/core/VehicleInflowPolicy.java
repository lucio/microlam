package br.ufrj.lam.microlam.core;

public abstract class VehicleInflowPolicy {

    public static final long VEHICLE_INFLOW_UNLIMITED = -1;

    public abstract double generateNextArrivalTime(double executionTime);

    public abstract Class<? extends Vehicle> nextArrivingVehicleType();

    public abstract double getCurrentArrivalRateInVehiclesPerHour();
}
