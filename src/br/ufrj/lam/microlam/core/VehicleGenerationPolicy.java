package br.ufrj.lam.microlam.core;

/**
 * Indica qual é o próximo tipo de veículo que será criado pela política de um fluxo de entrada.
 *
 * @author Daniel
 */
public abstract class VehicleGenerationPolicy {
    public abstract Class<? extends Vehicle> nextVehicleType();
}
