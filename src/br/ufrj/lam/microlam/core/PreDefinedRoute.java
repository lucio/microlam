package br.ufrj.lam.microlam.core;

import java.util.ArrayList;
import java.util.List;

import br.ufrj.lam.microlam.exceptions.RoutePickingException;

/**
 * Rota pré-definida.
 * <p>
 * Guarda a rota que vai ser atravessada pelo veículo ao longo do percurso.
 * Cada vez que o veículo é inserido num nó de entrada, uma nova rota é criada,
 * completa, até um nó de saída.
 * Admite um ciclo infinito na composição da rota.
 *
 * @author Daniel
 */
public class PreDefinedRoute extends Route {
    private static final int NO_ENDLESS_CYCLE = -1;

    private ArrayList<VehicleContainer> routeToVisit = new ArrayList<>();
    private int endlessCycleStart = NO_ENDLESS_CYCLE;
    private int currentContainerIndex = -1;

    /**
     * Iterador para a rota pré-definida.
     *
     * @author Daniel
     */
    public class PreDefinedRouteIterator extends RouteIterator {
        private int index;
        private int endOpenBoundIndex;

        private PreDefinedRouteIterator() {
            index = currentContainerIndex;

            // Se está dentro de um ciclo infinito, mas não no primeiro container do mesmo,
            // basta procurar no comprimento do ciclo
            if (hasAnEndlessCycle() && currentContainerIndex > endlessCycleStart) {
                endOpenBoundIndex = currentContainerIndex + getEndlessCycleSize();
            }
            // Caso esteja no primeiro container do ciclo infinito ou não esteja em um ciclo infinito,
            // basta procurar até o final da rota
            else {
                endOpenBoundIndex = routeToVisit.size();
            }
        }

        @Override
        public VehicleContainer next() {
            if ((index + 1) < endOpenBoundIndex) {
                return getContainer(++index);
            }

            return null;
        }

        @Override
        public boolean hasNext() {
            return (index + 1) < endOpenBoundIndex;
        }
    }

    /**
     * Traça rota que vai ser atravessada pelo veículo ao longo do percurso.
     * Cada vez que o veículo é inserido num nó de entrada, uma nova rota
     * é calculada.
     * Esta rotina já toma a decisão sobre qual caminho tomar numa interseção
     * onde há mais de um caminho possível a seguir.
     * Caso a rota possua pelo menos um trecho, o trecho atual é definido como o primeiro trecho.
     * Admite um ciclo infinito na composição da rota.
     * <br /><br />
     * Caso um laço infinito seja encontrado no fim da tripa da rota, a variável
     * {@link PreDefinedRoute#endlessCycleStart} aponta para o índice da primeira {@link Road} que
     * aparece no laço.
     *
     * <i>Lucio em 26/Fev/2012:</i><br />
     * O código do Daniel originalmente marcava uma interseção como início de
     * ciclo infinito. Além disso, na hora de perguntar para uma interseção se
     * ela só possuia uma saída, esquecia de filtrar pela via de entrada. A in-
     * terseção de saída pode ter duas saídas, porém pode ser que só uma esteja
     * disponível para a via de entrada em questão. Se isto não for tratado, o
     * seguinte caso vai causar um overflow na memória:
     *
     * <pre>    4
     *       o--<--o
     *       |     ^3
     *      5v     |
     * o-->--o-->--o
     *   1   |  2
     *      6v
     *       o
     * </pre>
     * <p>
     * Digamos que a via 1 possa seguir para 2 ou virar para 6. Sendo assim, a
     * interseção downstream de 1 tem duas saídas e não marca o início de um
     * possível ciclo. No caso da via 5, ela só possui uma saída para 2 (não é
     * permitido a ela que siga em frente). O código antigo, porém, respondia
     * que existem duas saídas, já que ele não filtrava pela via de entrada.
     * Dessa maneira, o código não detectava um laço e simplesmente continuava
     * aumentando o array de VehicleContainers eternamente na rota.
     * <p>
     * No código atual, eu marco a via por default como candidata a início de
     * ciclo (culpado até que se prove o contrário). Se a interseção em seguida
     * me disser que existem duas saídas possíveis, eu cancelo a acusação da via.
     * Se não, eu mantenho a acusação e continuo montando a rota. Se por ventura
     * eu encontrar novamente aquela via acusada e nenhuma interseção no meio do
     * caminho desmentiu a acusação, eu sei condeno a via como um início de ciclo
     * infinito.
     */
    PreDefinedRoute(Road road) throws RoutePickingException {
        int possibleEndlessCycleStart = NO_ENDLESS_CYCLE;

        // se rota == null, não há mais rota possível - chegou a um nó de saída do sistema
        while (road != null) {
            if (road == getContainer(possibleEndlessCycleStart)) {
                endlessCycleStart = possibleEndlessCycleStart;
                return;
            }

            // adiciona via na lista de vias da rota
            routeToVisit.add(road);
            currentContainerIndex = 0;

            if (possibleEndlessCycleStart == NO_ENDLESS_CYCLE) {
                // marca road como uma via candidata a estar dentro um ciclo infinito:
                possibleEndlessCycleStart = routeToVisit.size() - 1;
            }

            // pega nó downstream da via
            Intersection intersection = road.getDownstreamIntersection();

            // adiciona interseção na lista da rota
            routeToVisit.add(intersection);

            if (!intersection.hasOnlyOneWayOutWhenComingFrom(road))
                possibleEndlessCycleStart = NO_ENDLESS_CYCLE;

            // escolhe aleatoriamente um sentido possível de passagem pela interseção
            road = intersection.pickRoute(road);
        }
    }

    /**
     * Construtor de cópia.
     */
    private PreDefinedRoute(PreDefinedRoute source) {
        super(source);
        routeToVisit = new ArrayList<>(source.routeToVisit);
        endlessCycleStart = source.endlessCycleStart;
        currentContainerIndex = source.currentContainerIndex;
    }

    /**
     * Usa o construtor de cópia para retornar uma cópia de si mesmo.
     */
    @Override
    public Route copy() {
        return new PreDefinedRoute(this);
    }

    /**
     * Verifica se a rota possui um ciclo infinito.
     */
    private boolean hasAnEndlessCycle() {
        return endlessCycleStart >= 0;
    }

    /**
     * Retorna o tamanho do ciclo infinito.
     * Se não há um ciclo infinito, então retorna zero.
     */
    private int getEndlessCycleSize() {
        if (hasAnEndlessCycle()) {
            return routeToVisit.size() - endlessCycleStart;
        }
        return 0;
    }

    /**
     * Retorna o container associado ao índice na rota.
     * Esse método deve ser usado sempre, porque rotas com ciclo infinito admitem um índice contínuo.
     */
    private VehicleContainer getContainer(int index) {
        // Se índice negativo, retornar nulo:
        if (index < 0) {
            return null;
        }
        // se índice está ultrapassando o limite do tamanho do array:
        else if (index >= routeToVisit.size()) {
            // Se existir um ciclo infinito, retornar índice "normalizado":
            if (hasAnEndlessCycle()) {
                index = ((index - endlessCycleStart) % getEndlessCycleSize()) + endlessCycleStart;
            }
            // Se não existe ciclo infinito, index não era pra ter vindo apontando para uma posição maior que o array!
            else {
                return null;
            }
        }

        // Caso normal em que índice é não-negativo e está dentro do tamanho do array.
        // Também chegará aqui caso tenha passado pela normalização do ciclo infinito acima.
        return routeToVisit.get(index);
    }

    /**
     * Retorna um iterador para a rota apontando para o container atual.
     */
    @Override
    public RouteIterator iterator() {
        return new PreDefinedRouteIterator();
    }

    @Override
    public VehicleContainer getCurrentContainer() {
        return getContainer(currentContainerIndex);
    }

    @Override
    public VehicleContainer getNextContainer() {
        return getContainer(currentContainerIndex + 1);
    }

    @Override
    public VehicleContainer getPreviousContainer() {
        return getContainer(currentContainerIndex - 1);
    }

    /**
     * Chamado pelo veículo para atualizar sua rota quando ele
     * passa para um novo container.
     */
    @Override
    public VehicleContainer moveToNextContainer() {
        // Evita adicionar containers à rota visitada se ela tiver um ciclo infinito
        if (currentContainerIndex < routeToVisit.size()) {
            routeVisited.add(getCurrentContainer());
        }

        return getContainer(++currentContainerIndex);
    }

    @Override
    public boolean hasEnded() {
        // A rota terminou se o trecho atual é maior que o total de trechos a serem visitados
        // e não existir um ciclo infinito
        return currentContainerIndex > routeToVisit.size() && !hasAnEndlessCycle();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        for (VehicleContainer aRouteToVisit : routeToVisit) {
            result.append(aRouteToVisit.getId()).append("; ");
        }

        if (hasAnEndlessCycle()) {
            result.append("; ").append(routeToVisit.get(endlessCycleStart).getId()).append(" <endless loop>");
        }

        return result.toString();
    }

    /**
     * Retorna o índice da primeira ocorrência do container na rota a partir do container atual.
     */
    private int getContainerIndex(VehicleContainer container) {
        int endOpenBoundIndex;

        // Se está dentro de um ciclo infinito, mas não no primeiro container do mesmo,
        // basta procurar no comprimento do ciclo
        if (hasAnEndlessCycle() && currentContainerIndex > endlessCycleStart) {
            endOpenBoundIndex = currentContainerIndex + getEndlessCycleSize();
        }
        // Caso esteja no primeiro container do ciclo infinito ou não esteja em um ciclo infinito,
        // basta procurar até o final da rota
        else {
            endOpenBoundIndex = routeToVisit.size();
        }

        // Encontra container na rota
        for (int i = currentContainerIndex; i < endOpenBoundIndex; i++) {
            if (getContainer(i) == container) {
                return i;
            }
        }

        // Retorna índice inválido se container não foi encontrado
        return -1;
    }

    /**
     * Retorna o container anterior à primeira ocorrência do container na rota a partir do container atual.
     */
    @Override
    public VehicleContainer getContainerInRouteBefore(VehicleContainer container) {
        int index = getContainerIndex(container);
        if (index > 0) {
            return getContainer(index - 1);
        }
        return null;
    }

    /**
     * Retorna o container seguinte à primeira ocorrência do container na rota a partir do container atual.
     */
    @Override
    public VehicleContainer getContainerInRouteAfter(VehicleContainer container) {
        int index = getContainerIndex(container);
        if (index >= 0) {
            return getContainer(index + 1);
        }
        return null;
    }

    /**
     * Busca a primeira ocorrência da interceção na rota a partir do container atual
     * e retorna a conexão que ele irá usar na interseção.
     */
    @Override
    public RoadConnection getRoadConnectionInIntersectionAhead(Intersection intersection) {
        int index = getContainerIndex(intersection);
        if (index > 0) {
            VehicleContainer incoming = getContainer(index - 1);
            VehicleContainer outgoing = getContainer(index + 1);

            if (incoming instanceof Road && outgoing instanceof Road) {
                return intersection.getRoadConnection((Road) incoming, (Road) outgoing);
            }
        }

        return null;
    }

    @Override
    public List<RoadDrawingInfo> getRoadsAhead() {
        List<RoadDrawingInfo> roads = new ArrayList<>();
        for (VehicleContainer vc : routeToVisit) {
            if (vc instanceof Road)
                roads.add((RoadDrawingInfo) vc);
        }
        return roads;
    }
}
