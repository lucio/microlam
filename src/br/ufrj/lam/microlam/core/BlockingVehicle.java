package br.ufrj.lam.microlam.core;

import java.awt.Color;

public class BlockingVehicle extends Vehicle {

    BlockingVehicle() {
        super(-1);

        this.parkVehicle();
    }

    @Override
    public void calculateAccelerationAndVelocity(double dt) {
    }

    @Override
    public void calculateStartingVelocity() {
    }

    @Override
    public double getLength() {
        return 0;
    }

    @Override
    public double getMinimumDistanceToFrontVehicle() {
        return 0;
    }

    @Override
    public double getWidth() {
        return 0;
    }

    @Override
    public Color getColor() {
        return null;
    }

    @Override
    public String toString() {
        return "Blocking Vehicle";
    }

}
