package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D;
import java.util.Collection;

public class Utils {

    static double distanceBetweenPoints(Point2D.Double p1, Point2D.Double p2) {
        double x = (p2.x - p1.x);
        double y = (p2.y - p1.y);

        return Math.sqrt(x * x + y * y);
    }

    // http://stackoverflow.com/questions/4201860/how-to-find-gcf-lcm-on-a-set-of-numbers
    public static long gcd(long a, long b) {
        while (b > 0) {
            long temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    public static long gcd(Long[] input) {
        long result = input[0];
        for (int i = 1; i < input.length; i++)
            result = gcd(result, input[i]);
        return result;
    }

    private static long lcm(long a, long b) {
        return a * (b / gcd(a, b));
    }

    public static long lcm(Long[] input) {
        long result = input[0];
        for (int i = 1; i < input.length; i++)
            result = lcm(result, input[i]);
        return result;
    }

    static String quoteStr(String str) {
        return "\"" + str + "\"";
    }

    /**
     * @param values Coleção com valores do tipo double
     * @return Média dos valores passados na coleção
     */
    static double calculateAverage(Collection<Double> values) {
        double result = 0d;
        if (values != null && values.size() > 0) {
            for (double value : values) {
                result += value;
            }
            result /= (double) values.size();
        }
        return result;
    }

    static double min(Collection<Double> values) {
        double result = Double.MAX_VALUE;
        if (values != null && values.size() > 0) {
            for (double value : values) {
                if (value < result)
                    result = value;
            }
        }
        return result;
    }

    public static double max(Collection<Double> values) {
        double result = Double.MIN_VALUE;
        if (values != null && values.size() > 0) {
            for (double value : values) {
                if (value > result)
                    result = value;
            }
        }
        return result;
    }

    static double calculateStandardDeviation(Collection<Double> values) {
        double result = 0d;
        if (values != null && values.size() > 0) {
            double mean = calculateAverage(values);
            for (double value : values) {
                result += (value - mean) * (value - mean);
            }
            result /= (double) (values.size() - 1); // n-1 remove viés por conta do uso de média da amostra em vez de da população
            result = Math.sqrt(result);
        }
        return result;
    }

    /**
     * Método que converte um intervalo de tempo em segundos para uma string que indica a
     * quantidade de horas, minutos e segundos contidas naquele intervalo.
     *
     * @return string formatada em hh:mm:ss
     * Exemplo: '0h10m57s'
     */
    public static String getTimeStr(long timeInSeconds) {
        long seconds = timeInSeconds;
        long minutes = (seconds / 60) % 60;
        long hours = (seconds / (60 * 60));
        seconds %= 60;

        return String.format("%dh%02dm%02ds", hours, minutes, seconds);
    }
}
