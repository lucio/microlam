package br.ufrj.lam.microlam.core;

import java.awt.Color;
import java.awt.geom.Point2D;

import br.ufrj.lam.microlam.ui.VehicleShape;

public interface VehicleDrawingInfo {

    Point2D.Double getPosition();

    double getAngle();

    double getLength();

    double getWidth();

    Color getColor();

    boolean isOnIntersection();

    long getId();

    double getDistanceToDownstream();

    double getVelocity();

    double getAcceleration();

    VehicleDrawingInfo getFrontVehicleDrawingInfo();

    VehicleShape getVehicleShape();

    double getDistanceToFrontVehicle();

    RouteDrawingInfo getRouteDrawingInfo();
}
