package br.ufrj.lam.microlam.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import br.ufrj.lam.microlam.coordination.SMERCoordinator;
import br.ufrj.lam.microlam.signalcontrol.SignalController;

/**
 * Esta classe representa a abstração de um cenário qualquer.
 * É nela em que as ruas, interseções e outros componentes devem
 * ser adicionados.
 * <p>
 * Nenhum tipo de cálculo da simulação é feito aqui. Os cálculos
 * são feitos pela {@link Engine}.
 *
 * @author Flávio Faria
 * @see Engine
 * @since Feb-05-2012
 */
public class Scenario {

    private static final long SIMULATION_DURATION_15MIN = 60 * 15;

    /**
     * vetor de todas as vias do sistema
     */
    private Vector<Road> mRoads;
    /**
     * vetor de todos os cruzamentos do sistema
     */
    private Vector<Intersection> mIntersections;

    /**
     * Lista de elementos coordenadores de cruzamento.
     */
    private ArrayList<SMERCoordinator> mCoordinators;

    private HashMap<Integer, SignalController> mControllers;

    private HashMap<Integer, RoadConnection> mConnections;
    /**
     * contador de veículos
     */
    private int mVehicleIndex = 1;
    /**
     * seed usada no gerador de numeros pseudo-aleatorios
     **/
    private Long seed;
    private String name;
    private long simulationDuration;
    private boolean hasDefaultSpeedLimit = false;
    private double defaultSpeedLimit;

    /**
     * Construtor básico. Responsável apenas por iniciar os membros da classe.
     */
    public Scenario() {
        mRoads = new Vector<>();
        mIntersections = new Vector<>();
        mControllers = new HashMap<>();
        mConnections = new HashMap<>();
        mCoordinators = new ArrayList<>();
    }

    /**
     * Reseta o cenário, removendo todos os seus componentes.
     */
    protected void resetScenario() {
        mVehicleIndex = 1;
        mRoads.clear();
        mIntersections.clear();
        mCoordinators.clear();
    }

    Vector<Intersection> getIntersections() {
        return mIntersections;
    }

    Vector<Road> getRoads() {
        return mRoads;
    }

    ArrayList<SMERCoordinator> getCoordinators() {
        return mCoordinators;
    }

    public void add(SMERCoordinator coordinator) {
        if (!mCoordinators.contains(coordinator)) {
            mCoordinators.add(coordinator);
        }
    }

    public void add(Road road) {
        if (!mRoads.contains(road)) {
            if (!road.hasSpeedLimit() && this.hasDefaultSpeedLimit)
                road.setSpeedLimit(this.defaultSpeedLimit);
            mRoads.add(road);
        }
    }

    public void add(Intersection intersection) {
        if (!mIntersections.contains(intersection)) {
            if (!intersection.hasSpeedLimit() && this.hasDefaultSpeedLimit)
                intersection.setSpeedLimit(this.defaultSpeedLimit);
            mIntersections.add(intersection);
        }
    }

    /**
     * Sobrescreve todos os limites de velocidade do cenário
     * pelo limite passado como parâmetro.
     *
     * @param speedLimitInKmPerHour velocidade limite, em km/h
     */
    void setGlobalSpeedLimit(double speedLimitInKmPerHour) {
        for (Road road : this.mRoads) {
            road.setSpeedLimit(speedLimitInKmPerHour);
        }
        for (Intersection intersection : this.mIntersections) {
            intersection.setSpeedLimit(speedLimitInKmPerHour);
        }
    }

    public void add(int id, SignalController controller) {
        mControllers.put(id, controller);
    }

    public void add(int id, RoadConnection connection) {
        mConnections.put(id, connection);
    }

    /**
     * Recupera uma {@link Road} do cenário a partir do seu id.
     *
     * @param id o id da {@link Road}.
     * @return A {@link Road} referente ao id passado como parâmetro.
     */
    public Road getRoadById(int id) {
        for (Road r : mRoads) {
            if (id == r.getId())
                return r;
        }
        return null;
    }

    /**
     * Recupera uma {@link Intersection} do cenário a partir do seu id.
     *
     * @param id o id da {@link Intersection}.
     * @return A {@link Intersection} referente ao id passado como parâmetro.
     */
    public Intersection getIntersectionById(int id) {
        for (Intersection i : mIntersections) {
            if (id == i.getId())
                return i;
        }
        return null;
    }

    /**
     * Recupera um {@link SignalController} do cenário a partir do seu id.
     *
     * @param id o id do {@link SignalController}.
     * @return O {@link SignalController} referente ao id passado como parâmetro.
     */
    public SignalController getSignalControllerById(int id) {
        return mControllers.get(id);
    }

    /**
     * Recupera uma {@link RoadConnection} do cenário a partir do seu id.
     *
     * @param id o id da {@link RoadConnection}.
     * @return A {@link RoadConnection} referente ao id passado como parâmetro.
     */
    public RoadConnection getRoadConnectionById(int id) {
        return mConnections.get(id);
    }

    long getNextFreeVehicleIndex() {
        return mVehicleIndex++;
    }

    public void setName(String scenarioName) {
        if (scenarioName == null)
            scenarioName = "Untitled";
        this.name = scenarioName;
    }

    public void setSeed(Long scenarioSeed) {
        this.seed = scenarioSeed;
        Rand.setSeed(this.getSeed());
    }

    public Long getSeed() {
        return this.seed;
    }

    String getScenarioName() {
        return this.name;
    }

    public void setSimulationDuration(Long duration) {
        if (duration == null)
            duration = SIMULATION_DURATION_15MIN;
        simulationDuration = duration;
    }

    /**
     * Tempo total em segundos que a simulação irá rodar.
     *
     * @return tempo total em segundos.
     */
    long getSimulationDuration() {
        return simulationDuration;
    }

    /**
     * @param defaultSpeedLimit valor em km/h
     */
    public void setDefaultSpeedLimit(Double defaultSpeedLimit) {
        this.hasDefaultSpeedLimit = true;
        this.defaultSpeedLimit = defaultSpeedLimit;
    }
}
