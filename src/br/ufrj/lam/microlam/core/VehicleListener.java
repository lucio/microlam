package br.ufrj.lam.microlam.core;

public interface VehicleListener {

    void vehicleEvent(VehicleEvent vehicleEvent);
}
