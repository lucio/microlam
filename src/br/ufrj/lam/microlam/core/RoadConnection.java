package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D;
import java.util.Vector;

/**
 * Representação de uma ligação entre duas vias.
 * Ao conectar duas vias, esta classe provoca uma "indentação" das mesmas,
 * fazendo com que o comprimento delas diminua ligeiramente de maneira que
 * haja espaço para que a curva que liga as duas vias seja projetada.
 * <p>
 * A trajetória que o veículo fará ao atravessar a curva é calculada através
 * de uma bezier.
 *
 * @author Lucio Paiva
 * @since 12-Mar-2011
 */
public class RoadConnection implements RoadConnectionDrawingInfo, MapEntity {

    private Road incomingRoad, outgoingRoad;
    private double probability;
    private Bezier curvePath;
    private int id;

    public RoadConnection(int id, Road incomingRoad, Road outgoingRoad, double probability) {
        this.id = id;
        this.incomingRoad = incomingRoad;
        this.outgoingRoad = outgoingRoad;
        this.probability = probability;

        if (incomingRoad != null) {
            incomingRoad.indentDownstream();
            outgoingRoad.indentUpstream();

            /*
             * Calcula o caminho que o veículo vai percorrer ao atravessar a curva que liga as duas vias.
             * Parâmetros:
             * 		1. ponto mais downstream da via de chegada, considerando o recuo devido à criação da curva
             * 		2. ponto central da interseção, onde as duas vias de tocas
             * 		3. ponto mais upstream da via de saída, considerando o recuo devido à criação da curva
             */
            curvePath = new Bezier(incomingRoad.getDownstreamPosition(), incomingRoad.getOriginalDownstreamPosition(), outgoingRoad.getUpstreamPosition());
        } else
            curvePath = null;
    }

    Point2D.Double getPathStartingPoint() {
        return incomingRoad.getDownstreamPosition();
    }

    Point2D.Double getPathEndingPoint() {
        return outgoingRoad.getUpstreamPosition();
    }

    public RoadConnection(int id, Road outgoingRoad, double probability) {
        this(id, null, outgoingRoad, probability);
    }

    public Road getIncomingRoad() {
        return incomingRoad;
    }

    public Road getOutgoingRoad() {
        return outgoingRoad;
    }

    public double getProbability() {
        return probability;
    }

    public double getLength() {
        return curvePath.getLength();
    }

    /**
     * Calcula a posição [x,y] do veículo no plano cartesiano, baseado em que
     * ponto da curva ele se encontra. A curva não é contínua, mas sim quebrada
     * em pequenos segmentos de reta (gerados através do cálculo da bezier, com
     * resolução finita).
     *
     * @param vehicle Veículo cuja posição será calculada
     * @return posição absoluta [x,y] do veículo no plano cartesiano
     */
    Point2D.Double getVehiclePosition(Vehicle vehicle) {

        return curvePath.linearToCartesianPosition(vehicle.getCurrentPosition());

    }

    double getVehicleAngle(Vehicle vehicle) {
        return curvePath.linearToCartesianAngle(vehicle.getCurrentPosition());
    }

    @Override
    public RoadDrawingInfo getIncomingRoadPosition() {
        return incomingRoad;
    }

    @Override
    public RoadDrawingInfo getOutgoingRoadPosition() {
        return outgoingRoad;
    }

    /**
     * Dado um vetor "connections" de RoadConnections, descobre se existe alguma RoadConnection
     * nele cuja incomingRoad seja "incoming" e cuja outgoingRoad seja "outgoing".
     *
     * @return true se existe a tupla <incoming,outgoing> no vetor connections passado como parâmetro
     **/
    public static boolean isConnectionInRoadConnectionVector(Road incoming, Road outgoing, Vector<RoadConnection> connections) {
        for (RoadConnection rc : connections) {
            if ((rc.getIncomingRoad() == incoming) && (rc.getOutgoingRoad() == outgoing)) {
                return true;
            }
        }

        return false;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "RC" + this.getId() + "/[" + this.incomingRoad + "," + this.outgoingRoad + "]/" + this.probability;
    }
}
