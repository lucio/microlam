package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D.Double;

public class UnsignalizedIntersection extends Intersection {

    public UnsignalizedIntersection(int id, Double location) {
        super(id, location);
    }

}
