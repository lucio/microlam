package br.ufrj.lam.microlam.core;

/**
 * Gera a chegada de veículos segundo um processo de Poisson,
 * cuja taxa de chegada segue uma distribuição exponencial.
 *
 * @author Lucio Paiva
 */
public class ExponentialVehicleInflowPolicy extends VehicleInflowPolicy {
    private VehicleGenerationPolicy vehicleGenerationPolicy;
    private VehicleInflowRateFunction vehicleInflowRateFunction;

    private boolean limitedAmountOfVehicles;
    private long numVehiclesLeft;

    /**
     * Construtor.
     *
     * @param vehicleGenerationPolicy Política para geração de veículos.
     */
    public ExponentialVehicleInflowPolicy(VehicleGenerationPolicy vehicleGenerationPolicy, VehicleInflowRateFunction vehicleInflowRateFunction) {
        this(vehicleGenerationPolicy, vehicleInflowRateFunction, VEHICLE_INFLOW_UNLIMITED);
    }

    /**
     * Construtor.
     *
     * @param vehicleGenerationPolicy Política para geração de veículos.
     * @param vehicleCount            se for positivo, obriga um limite máximo de veículos que devem entrar.
     *                                Quando o limite for atingido, nenhum outro veículo entrará no sistema através deste gerador.
     *                                Se este parâmetro for igual a VEHICLE_INFLOW_UNLIMITED, nenhum limite será imposto.
     */
    public ExponentialVehicleInflowPolicy(VehicleGenerationPolicy vehicleGenerationPolicy, VehicleInflowRateFunction vehicleInflowRateFunction, long vehicleCount) {
        this.vehicleGenerationPolicy = vehicleGenerationPolicy;
        this.limitedAmountOfVehicles = (vehicleCount != VEHICLE_INFLOW_UNLIMITED);
        this.numVehiclesLeft = vehicleCount;
        this.vehicleInflowRateFunction = vehicleInflowRateFunction;
    }

    /**
     * @param executionTime Tempo atual da simulação, em milissegundos
     * @return Quando ocorrerá a próxima chegada (em milissegundos desde o início da simulação)
     */
    @Override
    public double generateNextArrivalTime(double executionTime) {
        // se está limitando a entrada de veículos e ainda não completou a meta:
        if (limitedAmountOfVehicles && (numVehiclesLeft > 0)) {
            numVehiclesLeft--;
            return executionTime + 1000.0d * Rand.exponential(vehicleInflowRateFunction.getRate(executionTime));
        }
        // se a entrada de veículos está ilimitada:
        else if (!limitedAmountOfVehicles) {
            return executionTime + 1000.0d * Rand.exponential(vehicleInflowRateFunction.getRate(executionTime));
        }
        // caso tenha chegado aqui, já estourou o limite de veículos - proibe que novos veículos entrem:
        return Double.POSITIVE_INFINITY;
    }

    @Override
    public Class<? extends Vehicle> nextArrivingVehicleType() {
        return vehicleGenerationPolicy.nextVehicleType();
    }

    @Override
    public double getCurrentArrivalRateInVehiclesPerHour() {
        return this.vehicleInflowRateFunction.getRateInVehiclesPerHour();
    }
}
