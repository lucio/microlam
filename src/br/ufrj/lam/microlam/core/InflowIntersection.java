package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D;

import br.ufrj.lam.microlam.report.ReportManager;

/**
 * Tipo especial de Intersection que introduz fluxo de veículos no sistema.
 * <p>
 * A Engine chama a cada iteração o método step() das InflowIntersections de
 * modo a atualizá-las a respeito do tempo decorrido e dar a elas a possibi-
 * lidade de sortear os intervalos entre chegadas de veículos no sistema.
 * <p>
 * Inicialmente a InflowIntersection sorteia a hora de chegada do primeiro
 * veículo e entra em stanby até que este tempo se esgote. Esgotado, ela in-
 * dica à Engine que o veículo deve ser inserido naquele momento e e também
 * cuida de sortear a hora de chegada do veículo seguinte.
 *
 * @author Lucio Paiva
 * @since 13-Mar-2011
 */
public class InflowIntersection extends Intersection {
    private static final double MINIMUM_GAP_TO_ENTER_ROAD = 17; // metros - valor estimado empiricamente!

    private VehicleInflowPolicy policy;  // política de chegada de veículos
    private double executionTime;  // tempo total de execução dessa Intersection (em milissegundos)
    private double nextArrivalTime;  // tempo de chegada do próximo veículo (em milissegundos)

    private int waitingVehiclesCount;  // contabiliza veículos que não conseguiram entrar na pista no momento sorteado e estão esperando para entrar

    /**
     * @param id       identificador desta interseção
     * @param location posição desta interseção no plano cartesiano
     */
    public InflowIntersection(int id, Point2D.Double location, VehicleInflowPolicy policy) {
        super(id, location);

        this.policy = policy;
        this.executionTime = 0.0d;
        this.nextArrivalTime = generateNextArrivalTime(executionTime);
        this.waitingVehiclesCount = 0;
    }

    /**
     * @param outgoingRoad via para onde vão escoar os veículos que chegarem por este nó de entrada
     */
    void addRoadConnection(Road outgoingRoad) {
        RoadConnection rc;

        rc = new RoadConnection(0, outgoingRoad, 1d);

        // para o caso especial de InflowIntersection, só pode haver uma conexão desta Intersection com uma via,
        // que é a via por onde vai escoar o fluxo de entrada:
        if (connections.size() > 0) {
            connections.clear();
        }

        connections.add(rc);
    }

    private double generateNextArrivalTime(double previousArrivalTime) {
        return policy.generateNextArrivalTime(previousArrivalTime);
    }

    /**
     * Executa uma iteração da simulação da InflowIntersection.
     * <p>
     * O trabalho da InflowIntersection é disparar um timer com uma variável aleatória
     * com distribuição exponencial e ficar aguardando o momento da chegada de um
     * veículo.
     * <p>
     * Ao chegar, o veículo deve ser inserido no sistema. Caso haja espaço na via de
     * escoamento, coloca o veículo lá. Se não houver, dois poderão ser os motivos:
     * <p>
     * 1. a via está congestionada
     * 2. um carro acabou de entrar e está ocupando o espaço inicial da via
     * <p>
     * No caso 1 temos um descarte, já que a via está congestionada e não há possibilidade
     * de alocar mais um carro;
     * <p>
     * No caso 2, a via ainda não atingiu sua capacidade. Por isso, sabemos que o carro
     * pode entrar, mas ainda assim ele terá que esperar o carro que está à frente dele.
     * Nesse caso, colocamos o carro na fila de espera da InflowIntersection.
     *
     * @param dt Tempo decorrido desde a última iteração (em milissegundos)
     */
    public void step(double dt) {
        executionTime += dt;

        // Verifica se um novo veículo quer entrar na via
        while (executionTime >= nextArrivalTime) {
            // Incrementa a quantidade de veículos esperando para entrar
            waitingVehiclesCount++;

            // Descobre o próximo tempo de chegada
            nextArrivalTime = generateNextArrivalTime(nextArrivalTime);
        }
    }

    boolean wantsToIntroduceNewVehicle() {
        // Tenta colocar um veículo na via, caso haja algum esperando para entrar
        if (waitingVehiclesCount > 0) {
            Road road = getOutgoingRoad();
            Vehicle frontVehicle = road.getMostUpstreamVehicle();

            // verifica se há um veículo à frente na via:
            if (frontVehicle != null) {
                // FIXME constante marretada! Implementar método que determina dinamicamente a distância mínima para inserir um novo veículo baseado no tamanho desse novo veículo
                if (frontVehicle.getCurrentPosition() >= MINIMUM_GAP_TO_ENTER_ROAD) {
                    return true;
                } else // caso não haja espaço para inserir
                {
                    // se o motivo é a rua estar congestionada:
                    if (road.isCongested()) {
                        // TODO implementar mecanismo que avalia a taxa de descarte do sistema
                        if (Engine.DEBUG_WARNINGS)
                            ReportManager.err("Ocorreu um descarte! Implementar avaliação da taxa de descarte");
                    }
                }

            } else {
                // caso não exista veículo à frente, tem garantia de espaço para entrar:
                return true;
            }
        }

        return false;
    }

    /**
     * Chamado pela engine quando ela retira da fila de espera
     * um dos veículos desta interseção e coloca no sistema.
     */
    Class<? extends Vehicle> removeWaitingVehicle() {
        // Decrementa a fila de espera e indica o tipo do veículo
        if (waitingVehiclesCount > 0) {
            waitingVehiclesCount--;
            return policy.nextArrivingVehicleType();
        }

        return null;
    }

    private Road getOutgoingRoad() {
        return connections.get(0).getOutgoingRoad();
    }

    public double getCurrentArrivalRateInVehiclesPerHour() {
        return this.policy.getCurrentArrivalRateInVehiclesPerHour();
    }
}
