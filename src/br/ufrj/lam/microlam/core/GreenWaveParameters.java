package br.ufrj.lam.microlam.core;

/**
 * Classe auxiliar usada para informar à onda verde sendo criada
 * qual a janela de tempo que ela tem para chegar na interseção
 * downstream.
 *
 * @author lucio
 * @since 30 Abr 2012
 */
public class GreenWaveParameters {

    /**
     * Parâmetro de quanto tempo falta para a próxima
     * etapa verde alcançável (isto é, o próximo verde que esta
     * onda, viajando na velocidade programada para ela, consegue
     * alcançar) na interseção downstream.
     */
    private double timeToNextReachableGreenPhase;
    /**
     * Seta o tempo de verde que estará disponível para a onda
     * assim que ela chegar na interseção downstream. Este não
     * necessariamente ditará a banda da onda, já que o tempo de
     * verde upstream pode ser menor (a banda da onda é o menor
     * destes dois tempos).
     */
    private double availableTimespan;

    public GreenWaveParameters() {
    }

    double getTimeToNextReachableGreenPhase() {
        return timeToNextReachableGreenPhase;
    }

    /**
     * @param timeToNextReachableGreenPhase tempo para a próxima janela verde downstream, em segundos
     */
    public void setTimeToNextReachableGreenPhase(
            double timeToNextReachableGreenPhase) {
        this.timeToNextReachableGreenPhase = timeToNextReachableGreenPhase;
    }

    double getAvailableTimespan() {
        return availableTimespan;
    }

    /**
     * @param availableTimespan tamanho da próxima janela verde downstream, em segundos
     */
    public void setAvailableTimespan(double availableTimespan) {
        this.availableTimespan = availableTimespan;
    }

}
