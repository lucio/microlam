package br.ufrj.lam.microlam.core;

public class ConstantVehicleInflowPolicy extends VehicleInflowPolicy {
    private boolean limitedAmountOfVehicles;
    private long numVehiclesLeft;
    private VehicleGenerationPolicy vehicleGenerationPolicy;

    /**
     * Intervalo entre a chegada de um veículo e a do próximo (em segundos), aka "headway"
     **/
    private double headway;

    /**
     * Construtor.
     *
     * @param vehicleGenerationPolicy Política para geração de veículos.
     */
    public ConstantVehicleInflowPolicy(VehicleGenerationPolicy vehicleGenerationPolicy, double headway) {
        this(vehicleGenerationPolicy, headway, VEHICLE_INFLOW_UNLIMITED);
    }

    /**
     * Construtor.
     *
     * @param vehicleGenerationPolicy Política para geração de veículos.
     * @param headway                 intervalo de chegada entre dois veículos consecutivos, em segundos.
     * @param vehicleCount            se for positivo, obriga um limite máximo de veículos que devem entrar.
     *                                Quando o limite for atingido, nenhum outro veículo entrará no sistema através deste gerador.
     *                                Se este parâmetro for igual a VEHICLE_INFLOW_UNLIMITED, nenhum limite será imposto.
     */
    public ConstantVehicleInflowPolicy(VehicleGenerationPolicy vehicleGenerationPolicy, double headway, long vehicleCount) {
        this.vehicleGenerationPolicy = vehicleGenerationPolicy;
        this.limitedAmountOfVehicles = (vehicleCount != VEHICLE_INFLOW_UNLIMITED);
        this.numVehiclesLeft = vehicleCount;
        this.headway = headway; // veiculos/segundo -> segundos/veiculo
    }

    @Override
    public double generateNextArrivalTime(double executionTime) {
        // se está limitando a entrada de veículos e ainda não completou a meta:
        if (limitedAmountOfVehicles && (numVehiclesLeft > 0)) {
            numVehiclesLeft--;
            return executionTime + 1000.0d * this.headway;
        }
        // se a entrada de veículos está ilimitada:
        else if (!limitedAmountOfVehicles) {
            return executionTime + 1000.0d * this.headway;
        }
        // caso tenha chegado aqui, já estourou o limite de veículos - proibe que novos veículos entrem:
        return Double.POSITIVE_INFINITY;
    }

    @Override
    public Class<? extends Vehicle> nextArrivingVehicleType() {
        return vehicleGenerationPolicy.nextVehicleType();
    }

    @Override
    public double getCurrentArrivalRateInVehiclesPerHour() {
        return 3600d / this.headway;
    }
}
