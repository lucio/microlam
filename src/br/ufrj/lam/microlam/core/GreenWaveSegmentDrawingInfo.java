package br.ufrj.lam.microlam.core;

import java.awt.Color;
import java.awt.geom.Point2D;

public interface GreenWaveSegmentDrawingInfo {

    double getRoadWidth();

    double getAngle();

    Point2D.Double getHeadPosition();

    Point2D.Double getTailPosition();

    Color getColor();
}
