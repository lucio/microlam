package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import br.ufrj.lam.microlam.detector.VehiclePointDetectorDrawingInfo;

/**
 * Interface criada especialmente para não expor a classe Road à visualização do Applet.
 * O Applet precisa obter a posição no plano cartesiano da via. Para isso ele deve
 * iterar sobre cada via do sistema. Se ele recebesse um objeto da classe Road, estaria
 * livre para chamar métodos que só a Engine deveria ver. Por isso foi criada essa
 * interface!
 *
 * @author Lucio Paiva
 * @since 14-Mar-2011
 */
public interface RoadDrawingInfo {

    Point2D.Double getUpstreamPosition();

    Point2D.Double getDownstreamPosition();

    double getRoadWidth();

    double getAngle();

    long getId();

    ArrayList<VehiclePointDetectorDrawingInfo> getVehiclePointDetectorsDrawingInfo();

    String getName();

    ArrayList<VehicleDrawingInfo> getVehiclesDrawingInfo();
}
