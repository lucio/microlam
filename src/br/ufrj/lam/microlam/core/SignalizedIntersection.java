package br.ufrj.lam.microlam.core;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Vector;

import br.ufrj.lam.microlam.signalcontrol.SignalController;
import br.ufrj.lam.microlam.signalcontrol.SignalControllerEvent;
import br.ufrj.lam.microlam.signalcontrol.SignalControllerException;
import br.ufrj.lam.microlam.signalcontrol.SignalControllerListener;

public class SignalizedIntersection extends Intersection implements SignalizedIntersectionDrawingInfo, SignalControllerListener {

    public enum Light {
        GREEN, AMBER, RED
    }

    private SignalController signalController;
    private BlockingVehicle blockingVehicle;

    public SignalizedIntersection(int id, Point2D.Double location, SignalController sc) {
        super(id, location);

        this.signalController = sc;
        this.signalController.addEventListener(this);
        this.blockingVehicle = new BlockingVehicle();
    }

    public void step(double dt) throws SignalControllerException {
        this.signalController.step((dt / 1000.d));
    }

    /**
     * Este método é chamado quando o SignalController dispara um evento
     * indicando que houve uma mudança de fase.
     * <p>
     * Para cada via de saída cujo RoadConnection está ativo
     * na fase atual, começa uma onda verde.
     *
     * @param greenConnections      conexões de saída por onde serão propagadas as ondas verdes
     * @param upstreamPhaseDuration informação vinda do SignalController que diz quanto tempo este sinal ficará aberto
     */
    private void startGreenWaveSegments(ArrayList<RoadConnection> greenConnections, double upstreamPhaseDuration) {

        for (RoadConnection roadConnection : greenConnections) {
            Road incomingRoad = roadConnection.getIncomingRoad();
            Road outgoingRoad = roadConnection.getOutgoingRoad();

            /*
             * Antes de criar a onda, garante que a roadconnection pertence mesmo
             * a esta interseção (comecei a ter problemas com isso quando implementei
             * a coordenação de vias):
             */
            if (this.getRoadConnection(incomingRoad, outgoingRoad) == null)
                continue;

            // se a interseção downstream também for sinalizada:
            if (outgoingRoad.getDownstreamIntersection() instanceof SignalizedIntersection) {
                SignalizedIntersection downstreamIntersection = (SignalizedIntersection) outgoingRoad.getDownstreamIntersection();

                // distancia a ser percorrida (travessia do cruzamento + travessia da via downstream):
                double crossingDistance = this.getLength(incomingRoad, outgoingRoad);
                double distance = crossingDistance + outgoingRoad.getLength();
                /*
                 * Calcula parâmetros da onda verde.
                 * O tempo {@code minimumReachableTime} corresponde ao tempo mínimo em que a onda, partindo da
                 * interseção upstream, consegue alcançar a interseção downstream. Esse tempo é limitado pela
                 * velocidade máxima da via. Ele é passado para a função que retorna a próxima fase verde na
                 * interseção downstream para que ela possa calcular o próximo verde "alcançável" pela onda.
                 */
                double minimumReachableTime = distance / outgoingRoad.getSpeedLimit(); // tempo em segundos
                GreenWaveParameters gwp = downstreamIntersection.getTimeToNextReachableGreenPhase(outgoingRoad, minimumReachableTime);
                /*
                 * Verifica se é possível criar uma onda verde. Se o retorno foi null, significa que não há nenhuma expectativa
                 * de curto prazo de poder gerar uma onda verde nesse momento.
                 */
                if (gwp != null) {
                    /*
                     * a velocidade da onda é ajustada levando em conta o tempo que vai
                     * levar pro sinal downstream ficar verde. perceba que, se levar
                     * muito tempo, a onda viajará a uma velocidade muito baixa e provavelmente
                     * só os veículos inteligentes serão capazes de seguí-la, enquanto
                     * os outros a ultrapassarão.
                     * Por outro lado, eu posso assumir que ela não vai viajar mais rá-
                     * pido do que os veículos podem viajar, pois minimumReachableTime
                     * garante que estou respeitando o limite de velocidade da via.
                     */
                    double nrgp = gwp.getTimeToNextReachableGreenPhase();
                    double greenWaveVelocity = distance / nrgp; // velocidade = distancia / tempo

                    Color waveColor = outgoingRoad.isCoordinated() ? new Color(0x00D500) : new Color(0xE055E0);

                    /*
                     * Cria segmento de onda verde.
                     */
                    GreenWaveSegment gws = new GreenWaveSegment(
                            outgoingRoad,
                            greenWaveVelocity,
                            upstreamPhaseDuration,
                            gwp.getAvailableTimespan(),
                            crossingDistance,
                            waveColor
                    );

                    /*
                     * Passa para a via a tarefa de fazer o upkeep dessa onda verde até
                     * que ela encontre a interseção downstream, vá perdendo comprimento
                     * e finalmente morra.
                     * Mais de um segmento de onda verde pode trafegar ao mesmo tempo
                     * por uma mesma via.
                     */
                    outgoingRoad.addGreenWaveSegment(gws);
                }
            }
        }
    }

    /**
     * Usado por uma interseção upstream que está para começar uma nova
     * janela verde para calcular qual deve ser a velocidade constante
     * do deslocamento da janela.
     *
     * @param incoming             via de chegada da onda.
     * @param minimumReachableTime tempo mínimo (em segundos) em que a onda consegue chegar na interseção. É pautado pela velocidade máxima da via.
     * @return GreenWaveParameters indicando quanto tempo falta para a próxima fase verde alcançável pela onda a ser criada e qual será a duração da janela.
     */
    private GreenWaveParameters getTimeToNextReachableGreenPhase(Road incoming, double minimumReachableTime) {
        return this.signalController.getTimeToNextReachableGreenPhase(incoming, minimumReachableTime);
    }

    /**
     * Essa função é chamada por um veículo que está atualmente em outro container, mas
     * que possui esta SignalizedIntersection na rota dele. Ele invoca esta função quando
     * está procurando pelo veículo imediatamente à frente dele na rota.
     * <p>
     * Primeiro, a funçao verifica se ele tem sinal verde para prosseguir. Se não tiver,
     * a função cria um BlockingVehicle e o retorna como resposta.
     * <p>
     * Caso o sinal esteja verde, ainda assim é possível que haja veículos após o sinal,
     * mas ainda dentro da interseção, que estejam no caminho. Nesse caso, a função olha
     * pra todos os veículos que vieram da mesma via por onde o veículo em questão vai
     * vir e pega o que está mais atrasado (upstream).
     * <p>
     * Finalmente, se mesmo assim não for encontrado nenhum veículo, retorna null.
     *
     * @param vehicle Veículo que quer saber se há algum outro veículo neste container que esteja no caminho
     * @return veja descrição acima.
     */
    @Override
    public Vehicle getMostUpstreamVehicle(Vehicle vehicle) {

        Road incoming = (Road) vehicle.getContainerInRouteBefore(this);
        Road outgoing = (Road) vehicle.getContainerInRouteAfter(this);

        if (!this.signalController.isGreen(incoming, outgoing)) {
            // antes de enviar um BlockingVehicle, verifica se não há nenhum veículo com a traseira exposta:
            Vehicle frontVehicle = findAnyVehicleWhichHasntFullyCrossedTheBoundaryBetweenMyRoadAndThisIntersection(vehicle);

            if (frontVehicle != null)
                return frontVehicle;

            return this.blockingVehicle;
        }

        // Caso esteja verde, verifica se há algum veículo à frente.
        // "Veículo à frente" significa qualquer veículo que tenha vindo de vehicle.getContainerInRouteBefore(this),
        // independente de qual é o destino dele. Estou considerando que, em qualquer dos casos de destino, o veículo
        // de qualquer jeito, por ter vindo da mesma via, está no caminho (até que atinja a via final)
        return super.getMostUpstreamVehicle(vehicle);
    }

    @Override
    public Vector<SignalizedConnectionDrawingInfo> getConnectionsInfo() {
        Vector<SignalizedConnectionDrawingInfo> signalizedConnectionsInfo = new Vector<>();

        for (RoadConnection rc : connections) {
            Light light = this.signalController.getLightColor(rc);

            SignalizedConnectionDrawingInfo sci = new SignalizedConnectionDrawingInfo(rc, light);

            signalizedConnectionsInfo.add(sci);
        }

        return signalizedConnectionsInfo;
    }

    @Override
    public void signalControllerEvent(SignalControllerEvent signalControllerEvent) {

        if (signalControllerEvent.getEventKind() == SignalControllerEvent.EventKind.signalControllerPhaseChange) {
            startGreenWaveSegments(signalControllerEvent.getGreenConnections(), signalControllerEvent.getPhaseDuration());
        }

    }

    public SignalController getSignalController() {
        return signalController;
    }

    void setPhaseTiming(long greenTime, long amberTime, long clearanceTime) {
        this.signalController.setPhaseTiming(greenTime, amberTime, clearanceTime);
    }

}
