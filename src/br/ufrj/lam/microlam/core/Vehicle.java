package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import br.ufrj.lam.microlam.exceptions.RoutePickingException;
import br.ufrj.lam.microlam.exceptions.UnknownVehicleContainerException;
import br.ufrj.lam.microlam.ui.VehicleShape;

/**
 * Implementação da classe base de um veículo que trafega pelo sistema da simulação.
 * Esta classe carrega apenas a estrutura básica para representação de um veículo.
 * A inteligência dele fica a cargo da classe DriverModel
 *
 * @author Lucio Paiva
 * @since Mar-08-2011
 */
public abstract class Vehicle implements VehicleDrawingInfo {

    protected long id;
    private ArrayList<VehicleListener> listeners;
    /**
     * Posição que está na via atual, a contar do início da via do ponto da
     * faixa em que está. Assim, se está numa faixa "DOWNSTREAM", a posição
     * zero está no ponto p1 da Road.
     *
     * @see Road
     */
    private double currentContainerPosition;
    /**
     * Velocidade atual do veículo, em metros por segundo.
     */
    private double currentVelocity;
    private double currentAcceleration;
    private int currentRoadLane;

    /**
     * Variáveis auxiliares para atualização do estado do veículo.
     */
    private boolean autoCommit;
    private double uncommitedCurrentContainerPosition;
    private double uncommitedCurrentVelocity;
    private double uncommitedCurrentAcceleration;
    private int uncommitedCurrentRoadLane;
    /**
     * tempo decorrido no último step executado
     **/
    private double uncommitedDtInSeconds;
    private boolean uncommitedAdvanceInRoute;
    private Route uncommitedRoute = null;
    private VehicleContainer uncommitedPreviousContainer;

    private VehicleContainer previousContainer;

    private boolean isParked;

    private Vehicle frontVehicle;
    private double distanceToFrontVehicle;

    /**
     * Amostras de velocidade acumuladas para geração de estatística de velocidade média
     **/
    private double accumulatedSpeedSamples = 0, accumulatedSpeedSampleCount = 0;
    private double currentContainerAccumulatedSpeedSamples, currentContainerAccumulatedSpeedSampleCount;
    /**
     * Velocidade máxima instantânea obtida durante o percurso
     **/
    private double maxObservedSpeed = 0, currentContainerMaxObservedSpeed, currentContainerMinObservedSpeed;

    /*
     * Medidas de interesse para geração de estatísticas.
     */
    /**
     * flag que mostra se o veículo precisou parar em algum momento
     **/
    private boolean hadToStop = false, currentContainerHadToStop;
    /**
     * tempo de viagem (há quanto tempo o veículo se encontra no sistema)
     **/
    private double travelTime, currentContainerTravelTime;
    /**
     * tempo parado (quanto tempo o veículo passou com velocidade zero desde que entrou no sistema
     **/
    private double stoppedTime, currentContainerStoppedTime;

    /**
     * Lista de interseções/vias pelas quais o veículo vai passar ao longo da execução.
     */
    private Route route = null;

    private VehicleShape vehicleShape;

    /**
     * Método de cálculo da aceleração. Depende exclusivamente da classe de inteligência que vai estender Vehicle.
     */
    public abstract void calculateAccelerationAndVelocity(double dt);

    /**
     * O objetivo desta função é definir a velocidade inicial de um
     * veículo entrando na via. Se não houvesse esse cálculo, o veículo
     * entraria numa velocidade qualquer (no caso deste simulador, ele
     * entrava com velocidade 0).
     * <p>
     * A velocidade inicial é calculada imaginando-se que a via de
     * entrada é infinitamente longa e o veículo vinha ganhando
     * velocidade até chegar a uma velocidade de equilibrio, em que
     * a aceleração tornou-se zero.
     * <p>
     * Isso é feito utilizando a mesma fórmula para acelerar o veículo,
     * porém ela é iterada num loop até que a aceleração seja zero.
     */
    public abstract void calculateStartingVelocity();

    /**
     * Dá o comprimento do veículo (metros). Deve ser implementado de preferência pelo neto dessa classe
     * (já que o pai deve ser uma classe abstrata que implementa um tipo de motorista, sem
     * ainda especificar detalhes sobre o veículo.
     */
    @Override
    public abstract double getLength();

    /**
     * Dá a largura do veículo (metros). Deve ser implementado de preferência pelo neto dessa classe
     * (já que o pai deve ser uma classe abstrata que implementa um tipo de motorista, sem
     * ainda especificar detalhes sobre o veículo.
     */
    @Override
    public abstract double getWidth();

    /**
     * Copy constructor de um veículo
     *
     * @param source Veículo a ser clonado (sem trocadilhos)
     */
    public Vehicle(Vehicle source) {
        this.id = source.id;
        this.listeners = source.listeners;
        this.currentContainerPosition = source.currentContainerPosition;
        this.currentVelocity = source.currentVelocity;
        this.currentAcceleration = source.currentAcceleration;
        this.currentRoadLane = source.currentRoadLane;

        this.autoCommit = source.autoCommit;
        this.uncommitedCurrentContainerPosition = source.uncommitedCurrentContainerPosition;
        this.uncommitedCurrentVelocity = source.uncommitedCurrentVelocity;
        this.uncommitedCurrentAcceleration = source.uncommitedCurrentAcceleration;
        this.uncommitedCurrentRoadLane = source.uncommitedCurrentRoadLane;
        this.uncommitedDtInSeconds = source.uncommitedDtInSeconds;
        this.uncommitedAdvanceInRoute = source.uncommitedAdvanceInRoute;
        this.uncommitedRoute = source.uncommitedRoute;
        this.uncommitedPreviousContainer = source.uncommitedPreviousContainer;

        this.previousContainer = source.previousContainer;
        this.isParked = source.isParked;

        this.frontVehicle = source.frontVehicle;
        this.distanceToFrontVehicle = source.distanceToFrontVehicle;

        this.accumulatedSpeedSamples = source.accumulatedSpeedSamples;
        this.accumulatedSpeedSampleCount = source.accumulatedSpeedSampleCount;
        this.currentContainerAccumulatedSpeedSamples = source.currentContainerAccumulatedSpeedSamples;
        this.currentContainerAccumulatedSpeedSampleCount = source.currentContainerAccumulatedSpeedSampleCount;
        this.maxObservedSpeed = source.maxObservedSpeed;
        this.currentContainerMaxObservedSpeed = source.currentContainerMaxObservedSpeed;
        this.currentContainerMinObservedSpeed = source.currentContainerMinObservedSpeed;
        this.hadToStop = source.hadToStop;
        this.currentContainerHadToStop = source.currentContainerHadToStop;
        this.travelTime = source.travelTime;
        this.currentContainerTravelTime = source.currentContainerTravelTime;
        this.stoppedTime = source.stoppedTime;
        this.currentContainerStoppedTime = source.currentContainerStoppedTime;

        this.route = source.route.copy();

        this.distanceToPaceVehicle = source.distanceToPaceVehicle;
    }

    public Vehicle(long id) {
        this.id = id;
        listeners = new ArrayList<>();

        resetVehicle();
    }

    /**
     * Usar quando o veículo estiver precisando de
     * um reset, mas vai continuar a ser usado no
     * cenário.
     */
    private void resetVehicle() {
        currentContainerPosition = 0.0d;
        currentVelocity = 0.0d;
        currentAcceleration = 0.0d;
        currentRoadLane = 0;

        autoCommit = false;
        uncommitedAdvanceInRoute = false;
        uncommitedCurrentAcceleration = 0.d;
        uncommitedCurrentContainerPosition = 0.d;
        uncommitedCurrentVelocity = 0.d;
        uncommitedDtInSeconds = 0.d;
        uncommitedRoute = null;
        uncommitedPreviousContainer = null;

        stoppedTime = 0.d;
        travelTime = 0.d;

        frontVehicle = null;
        distanceToFrontVehicle = 0;

        route = null;

        isParked = false;
    }

    /**
     * Rotina chamada toda vez que o veículo é colocado sobre uma nova via.
     */
    private void advanceInRoute() {
        uncommitedPreviousContainer = uncommitedRoute.getCurrentContainer();
        if (uncommitedRoute.getNextContainer() instanceof OutflowIntersection) {
            // pula para a outflow intersection:
            uncommitedRoute.moveToNextContainer();
            // e automaticamente pula para fora do sistema:
            uncommitedRoute.moveToNextContainer();
        } else {
            uncommitedRoute.moveToNextContainer();
        }

        uncommitedAdvanceInRoute = true;
    }

    private void broadcastEvent(VehicleEvent vehicleEvent) {
        for (VehicleListener listener : listeners) {
            listener.vehicleEvent(vehicleEvent);
        }
    }

    /**
     * Executa uma iteração da simulação.
     *
     * @param dt tempo em milissegundos desde a última iteração executada.
     * @see Engine#step()
     */
    public void step(double dt) {
        if (!isParked && route.getCurrentContainer() != null) {
            /* atualiza informação sobre quem é o veículo da frente e a que distância ele se encontra */
            updateFrontVehicle();

            // calcula nova aceleração e velocidade:
            calculateAccelerationAndVelocity(dt);

            // atualiza posição com a velocidade anteriormente calculada:
            double deltaS = getCurrentVelocity() * (dt / 1000.d);
            setCurrentContainerPosition(getCurrentContainerPosition() + deltaS);

            if ((getCurrentContainer() != null) && (uncommitedCurrentContainerPosition > getCurrentContainerLength())) {
                uncommitedRoute = route.copy();

                // se chegou ao final da via, troca para próxima da rota
                while ((uncommitedRoute != null) &&
                        (uncommitedRoute.getCurrentContainer() != null) &&
                        (uncommitedCurrentContainerPosition > getContainerLength(uncommitedRoute.getCurrentContainer()))
                ) {
                    // caso tenha andado um pouco além da via, adianta o carro ao entrar na via nova
                    setCurrentContainerPosition(uncommitedCurrentContainerPosition - getContainerLength(uncommitedRoute.getCurrentContainer()));

                    // como já esgotou o percurso do trecho atual, avança com o veículo para o próximo trecho da rota:
                    advanceInRoute();
                }
            }
        }

        uncommitedDtInSeconds = dt / 1000.d;
    }

    /**
     * Concretiza os cálculos feitos n último step.
     * <p>
     * A engine atualiza a posição/velocidade/aceleração de todos os
     * veículos de uma só vez. Só depois de fazer isso para todos,
     * ela chama este método para validar os novos valores para as
     * variáveis do veículo. Isto é necessário para evitar inconsis-
     * tências durante os cáculos das novas posições e garantir que
     * todos os veículos serão atulizados sincronizadamente.
     */
    void commitStep() {
        currentAcceleration = uncommitedCurrentAcceleration;
        currentVelocity = uncommitedCurrentVelocity;
        currentContainerPosition = uncommitedCurrentContainerPosition;
        currentRoadLane = uncommitedCurrentRoadLane;

        // atualiza estatísticas gerais e da via atual:
        updateStatistics();

        if (uncommitedAdvanceInRoute) {
            previousContainer = uncommitedPreviousContainer;
            route = uncommitedRoute;
            uncommitedRoute = null;
            uncommitedAdvanceInRoute = false;

            VehicleEvent vehicleEvent = new VehicleEvent(VehicleEvent.EventKind.vehicleAdvancedInRoute, this, previousContainer, getCurrentContainer());
            broadcastEvent(vehicleEvent);

            clearCurrentContainerStatistics();
        }
    }

    private void updateStatistics() {
        /*
         * Velocidade mínima aferível por um sensor de passagem.
         * Se o veículo estiver mais lento que essa velocidade,
         * o sensor de passagem não consegue perceber a passagem
         * do veículo. Atualmente esta variável está servindo
         * justamente para o oposto: para detectar que o veículo
         * está parado (sensor de presença). A cada momento em que
         * o veículo estiver abaixo desta velocidade, será entendi-
         * do que ele está parado.
         */
        // 8km/h, convertido para metros por segundo
        double SENSOR_MINIMUM_MEASURABLE_SPEED = 8d /*km/h*/ / 3.6d;
        if (getCurrentVelocity() < SENSOR_MINIMUM_MEASURABLE_SPEED || isParked) {
            hadToStop = true;
            currentContainerHadToStop = true;
            stoppedTime += uncommitedDtInSeconds;
            currentContainerStoppedTime += uncommitedDtInSeconds;
        }
        travelTime += uncommitedDtInSeconds;
        currentContainerTravelTime += uncommitedDtInSeconds;
        accumulatedSpeedSamples += currentVelocity;
        currentContainerAccumulatedSpeedSamples += currentVelocity;
        accumulatedSpeedSampleCount++;
        currentContainerAccumulatedSpeedSampleCount++;
        if (currentVelocity > maxObservedSpeed) maxObservedSpeed = currentVelocity;
        if (currentVelocity > currentContainerMaxObservedSpeed) currentContainerMaxObservedSpeed = currentVelocity;
        if (currentVelocity < currentContainerMinObservedSpeed) currentContainerMinObservedSpeed = currentVelocity;
    }

    /**
     * Limpa estatísticas do container atual.
     * Chamado quando o veículo avança na rota, indo para um novo container
     * e começando a rodar a amostragem novamente do zero.
     */
    private void clearCurrentContainerStatistics() {
        currentContainerHadToStop = false;
        currentContainerStoppedTime = 0d;
        currentContainerTravelTime = 0d;
        currentContainerAccumulatedSpeedSamples = 0d;
        currentContainerAccumulatedSpeedSampleCount = 0;
        currentContainerMaxObservedSpeed = Double.MIN_VALUE;
        currentContainerMinObservedSpeed = Double.MAX_VALUE;
    }

    double getLocalStoppedTime() {
        return this.currentContainerStoppedTime;
    }

    double getLocalTravelTime() {
        return this.currentContainerTravelTime;
    }

    double getLocalAverageSpeed() {
        if (this.currentContainerAccumulatedSpeedSampleCount > 0)
            return this.currentContainerAccumulatedSpeedSamples / this.currentContainerAccumulatedSpeedSampleCount;
        else
            return 0d;
    }

    double getLocalMaxObservedSpeed() {
        return currentContainerMaxObservedSpeed;
    }

    double getLocalMinObservedSpeed() {
        return currentContainerMinObservedSpeed;
    }

    private double distanceToPaceVehicle;

    protected double getDistanceToPaceVehicle() {
        return distanceToPaceVehicle;
    }

    /**
     * Verifica se a via atual possui limite de velocidade, ou se
     * qualquer outra via da rota possui. Retorna um {@link PaceVehicle}
     * para a via mais próxima na rota que possui limite de velocidade, ou
     * null se nenhuma via à frente possui limite.
     *
     * @return {@link PaceVehicle} ou {@code null}.
     */
    protected PaceVehicle checkForSpeedLimit() {
        // se a via atual tem limite:
        if (this.getCurrentContainer().hasSpeedLimit()) {
            PaceVehicle paceVehicle = this.getCurrentContainer().getPaceVehicle(this.currentContainerPosition);
            distanceToPaceVehicle = paceVehicle.getCurrentContainerPosition() - this.getCurrentContainerPosition();
            return paceVehicle;
        }

        // caso a via atual não tenha limite, acumula o comprimento restante até o fim dela:
        distanceToPaceVehicle = this.getCurrentContainerLength() - this.getCurrentContainerPosition();

        // para cada via restante na rota:
        for (Route.RouteIterator it = route.iterator(); it.hasNext(); ) {
            VehicleContainer vc = it.next();

            if (vc.hasSpeedLimit())  // se a via possui limite
            {
                PaceVehicle pv = vc.getPaceVehicle(0); //this.currentContainerPosition);
                distanceToPaceVehicle += pv.getCurrentContainerPosition();
                return pv;
            } else // caso contrário, continua acumulando o comprimento das vias percorridas
                distanceToPaceVehicle += this.getContainerLength(vc);
        }

        // se chegou até aqui é porque nenhuma via até o fim da rota possui limite de velocidade:
        distanceToPaceVehicle = 0d;
        return null;
    }

    /**
     * Verifica se o veículo se encontra dentro de uma onda verde.
     *
     * @return GreenWaveSegment, caso esteja dentro de uma onda verde.
     */
    protected GreenWaveSegment getContainingGreenWave() {
        if (this.getCurrentContainer() instanceof Road) {
            Road road = (Road) this.getCurrentContainer();
            return road.getContainingGreenWave(this.currentContainerPosition);
        } else
            return null;
    }

    /**
     * Atualiza as variáveis Vehicle.frontVehicle e Vehicle.distanceToFrontVehicle
     * baseado em qual veículo encontra-se atualmente à frente deste.
     * <p>
     * Caso não haja nenhum veículo à frente na mesma via, a rotina busca nas vias
     * seguintes a serem percorridas pela rota traçada para este veículo.
     * <p>
     * Se mesmo assim não encontrar nada, frontVehicle é setado para null e
     * distanceToFrontVehicle é a distância total a ser percorrida até o final da rota.
     */
    private void updateFrontVehicle() {
        // distância de mim até o veículo à minha frente, ou até o final da via onde estou, caso não haja mais veículos
        double s_from_current_road;

        // caso o veículo à minha frente esteja em outra via, esta variável guarda a soma dos comprimentos de todas as vias à frente,
        // até chegar no veículo (exclui a distância de mim até o final da via onde estou)
        double s_from_all_roads_downstream = 0;

        // caso updateFrontVehicle tenha sido chamado num momento em que o veículo já não tem mais rota:
        if (route == null || route.getCurrentContainer() == null)
            return;

        /* ********************************* PASSO 1 DE 2 **************************************/
        // procura pelo próximo veículo à frente (na mesma faixa) na mesma via:
        Vehicle _frontVehicle = route.getCurrentContainer().getFrontVehicle(this);

        /* ********************************* PASSO 2 DE 2 **************************************/
        // caso frontVehicle == null, tenta nas vias seguintes da rota do veículo:
        if (_frontVehicle == null) {
            s_from_current_road = getCurrentContainerLength();

            for (Route.RouteIterator it = route.iterator(); it.hasNext(); ) {
                VehicleContainer vc = it.next();

                // TODO aqui na verdade eu deveria estar procurando pelo veículo que está na faixa que eu vou ENTRAR!
                // Por enquanto está procurando em todas as faixas da via!
                // como saber a faixa dele? Caso eu esteja numa via de 1 faixa que vai encontrar uma de 2 faixas,
                // não sei em qual faixa procurar. Atualmente estou procurando em todas as faixas, mas isso não é
                // realista. Não preciso frear por causa de um carro que está numa faixa se posso ir para outra que
                // está vazia. Caso existam duas faixas de saída possíveis, talvez eu devesse escolher a que tem
                // mais espaço, porém isso faz parte do algoritmo de mudança de faixa a ser implementado no futuro.
                _frontVehicle = vc.getMostUpstreamVehicle(this);

                if (_frontVehicle != null) {
                    s_from_all_roads_downstream += getContainerPositionForVehicleAhead(_frontVehicle); //_frontVehicle.getCurrentPosition();
                    break;
                } else {
                    s_from_all_roads_downstream += getContainerLength(vc);
                }
            }

        } else {
            s_from_current_road = getContainerPositionForVehicleAhead(_frontVehicle); //_frontVehicle.getCurrentPosition();
        }

        this.frontVehicle = _frontVehicle;
        /*
         * Casos possíveis para o cálculo da distância entre mim e o carro à minha frente:
         * CASO 1: carro está na mesma via que eu, na mesma faixa:
         *     Simplesmente calculo a distância entre mim e ele usando getCurrentPosition().
         *
         *     s_from_current_road = frontVehicle.getCurrentPosition();
         *     s = 0 + s_from_current_road - currentRoadPosition - this.getLength();
         *     	ou seja:
         *     s = frontVehicle.getCurrentPosition() - currentRoadPosition - this.getLength();
         *
         * CASO 2: carro está em outra via à frente, em qualquer faixa*:
         *     Caso não haja veículo à minha frente na minha via atual, percorre minha
         *     rota até encontrar o próximo carro na minha frente.
         *     * ver TO DO acima, na parte onde é procurado o carro na via à frente
         *
         *     s_from_all_roads_downstream = somatório de todos os comprimentos de vias que existem
         *     	entre a via que estou e a via em que está o veículo da frente + posição do veículo
         *      na via dele.
         *     s_from_current_road = distância que falta até chegar ao final da via onde estou
         *     s = s_from_all_roads_downstream + (s_from_current_road - currentRoadPosition - this.getLength())
         *
         * CASO 3: não há mais nenhum carro à minha frente até o final da rota:
         *     Nesse caso frontVehicle == null e o Java não entra nesse if, passando direto para o
         *     cálculo da equação de free way abaixo.			 *
         */
        double frontVehicleLength = (frontVehicle == null) ? 0 : frontVehicle.getLength();
        this.distanceToFrontVehicle = s_from_all_roads_downstream +
                (s_from_current_road - this.getCurrentContainerPosition() - frontVehicleLength);
    }

    /**
     * Retorna a posição relativa* de determinado veículo à frente.
     * *relativa ao ponto mais upstream do container onde ELE se encontra.
     * <p>
     * No caso normal em que ele está numa Road, o valor retornado é a posição dele naquela Road.
     * Se estiver em algum tipo de interseção, porém, o valor retornado é outro: é a distância,
     * em linha reta no plano cartesiano, do vehicleAhead até o nosso veículo (this).
     * <p>
     * Obs: no caso do vehicleAhead ser um BlockingVehicle, ele não tem posição [x,y] no mundo,
     * então pegamos a posição dele com getCurrentPosition() que vai retornar 0.
     */
    private double getContainerPositionForVehicleAhead(Vehicle vehicleAhead) {
        if (vehicleAhead instanceof BlockingVehicle || vehicleAhead.route.getCurrentContainer() instanceof Road) {
            return vehicleAhead.getCurrentPosition();
        }

        return ((Intersection) vehicleAhead.getCurrentContainer()).getDistanceBetweenVehicles(this, vehicleAhead);
    }

    private double getContainerLength(VehicleContainer container) {
        if (container instanceof Road) {
            return ((Road) container).getLength();
        } else if (container instanceof Intersection) {
            return ((Intersection) container).getLength(this);
        } else {
            throw new UnknownVehicleContainerException("Impossible to get length of unknow container type!");
        }
    }

    private double getCurrentContainerLength() {
        return getContainerLength(route.getCurrentContainer());
    }

    /**
     * Coloca o veículo num nó de entrada do sistema. O veículo normalmente vem
     * após ter sido retirado do limbo.
     *
     * @param incomingIntersection O nó de entrada, ponto de início para estabelecer a rota a ser percorrida
     */
    void placeMe(InflowIntersection incomingIntersection) throws RoutePickingException {
        resetVehicle();
        // pega primeira via a partir do nó de entrada
        Road road = incomingIntersection.pickRoute(null);
        route = createRoute(road);

        VehicleEvent vehicleEvent = new VehicleEvent(VehicleEvent.EventKind.vehicleAdvancedInRoute, this, incomingIntersection, getCurrentContainer());
        broadcastEvent(vehicleEvent);
    }

    /**
     * Coloca o veículo num nó de entrada do sistema. O veículo normalmente vem
     * após ter sido retirado do limbo.
     *
     * @param incomingRoad A via de entrada, ponto de início para estabelecer a rota a ser percorrida
     * @return Via em que acabou de entrar.
     */
    public Road placeMe(Road incomingRoad) throws RoutePickingException {
        resetVehicle();

        route = createRoute(incomingRoad);  // programa a rota a ser seguida

        return (Road) route.getCurrentContainer();  // após ter programado toda a rota, pega a primeira via a ser percorrida
    }

    /**
     * Cria uma rota para este veículo. Por padrão, a rota criada é {@link PreDefinedRoute}.
     * Caso deseje uma rota diferente, sobrescreva este método.
     *
     * @param incomingRoad A via de entrada, ponto de início para estabelecer a rota
     *                     a ser percorrida.
     * @throws RoutePickingException caso haja qualquer inconsistência na rota criada.
     */
    private Route createRoute(Road incomingRoad)
            throws RoutePickingException {
        // programa a rota a ser seguida
        return new PreDefinedRoute(incomingRoad);
    }

    /**
     * Converte o valor da velocidade do veículo de m/s para km/h
     *
     * @return Velocidade do veículo em km/h
     */
    private double getVelocityAsKmPerHour() {
        return this.currentVelocity * 3.6;
    }

    /**
     * Para o veículo onde está imediatamente, ignorando as leis da inércia.
     */
    void parkVehicle() {
        isParked = true;
        currentAcceleration = 0.d;
        currentVelocity = 0.d;
    }

    String toCSV() {
        String output;
        output = "V" + this.id + ";" +
                route.getCurrentContainer().getIdStr() + ";" +
                Engine.formatDouble(this.getCurrentContainerPosition()) + ";" +
                Engine.formatDouble(getVelocityAsKmPerHour()) + ";" +
                Engine.formatDouble(this.getCurrentAcceleration());
        return output;
    }

    public String toString() {
        String output;
        output = "Vehicle #" + this.id + "\n" +
                "\tRoad: " + route.getCurrentContainer().getIdStr() + "\n" +
                "\tPosition: " + Engine.formatDouble(this.getCurrentContainerPosition()) + "m\n" +
                "\tVelocity: " + Engine.formatDouble(getVelocityAsKmPerHour()) + "km/h\n" +
                "\tAcceleration: " + Engine.formatDouble(this.getCurrentAcceleration()) + "m/s^2";
        return output;
    }

    public double getCurrentPosition() {
        return currentContainerPosition;
    }

    int getCurrentLane() {
        return currentRoadLane;
    }

    public void setCurrentLane(int currentLane) {
        if (autoCommit)
            this.currentRoadLane = currentLane;
        else
            this.uncommitedCurrentRoadLane = currentLane;
    }

    public double getCurrentVelocity() {
        return currentVelocity;
    }

    public boolean completedRoute() {
        return route == null || route.hasEnded();
    }

    protected VehicleContainer getCurrentContainer() {
        if (route == null)
            return null;
        return route.getCurrentContainer();
    }

    String routeToString() {
        return route.toString();
    }

    String getIdStr() {
        return "V" + id;
    }

    public long getId() {
        return id;
    }

    @Override
    public Point2D.Double getPosition() {
        return route.getCurrentContainer().getVehiclePosition(this);
    }

    /**
     * Pega ângulo do veículo em relação ao plano cartesiano.
     * Serve de auxílio para o applet mostrar o veículo devidamente rotacionado na cena.
     */
    @Override
    public double getAngle() {
        if (route.getCurrentContainer() instanceof Road) {
            return ((Road) route.getCurrentContainer()).getAngle();
        } else {
            return ((Intersection) route.getCurrentContainer()).getVehicleAngle(this);
        }
    }

    /**
     * Diz se o veículo está parado na via
     *
     * @return true se veículo está parado
     */
    public boolean isStopped() {
        return (currentVelocity == 0.0); // && (currentAcceleration == 0.0);
    }

    /**
     * @return Distância mínima que o veículo deseja manter do veículo à frente dele.
     */
    public abstract double getMinimumDistanceToFrontVehicle();

    /**
     * Busca a primeira ocorrência da interceção na rota a partir do container atual
     * e retorna a conexão que ele irá usar na interseção.
     */
    RoadConnection getRoadConnectionInIntersectionAhead(Intersection intersection) {
        return route.getRoadConnectionInIntersectionAhead(intersection);
    }

    /**
     * Retorna o container anterior à primeira ocorrência do container na rota a partir do container atual.
     */
    VehicleContainer getContainerInRouteBefore(VehicleContainer thisVC) {
        return route.getContainerInRouteBefore(thisVC);
    }

    /**
     * Retorna o container seguinte à primeira ocorrência do container na rota a partir do container atual.
     */
    VehicleContainer getContainerInRouteAfter(VehicleContainer thisVC) {
        return route.getContainerInRouteAfter(thisVC);
    }

    /**
     * Retorna true se veículo estiver em algum tipo de interseção.
     */
    public boolean isOnIntersection() {
        // tudo que não é Road, é um tipo de interseção:
        return !(route.getCurrentContainer() instanceof Road);
    }

    protected void setCurrentVelocity(double currentVelocity) {
        if (autoCommit)
            this.currentVelocity = currentVelocity;
        else
            this.uncommitedCurrentVelocity = currentVelocity;
    }

    protected double getCurrentContainerPosition() {
        return currentContainerPosition;
    }

    void setCurrentContainerPosition(double currentContainerPosition) {
        if (autoCommit)
            this.currentContainerPosition = currentContainerPosition;
        else
            this.uncommitedCurrentContainerPosition = currentContainerPosition;
    }

    private double getCurrentAcceleration() {
        return currentAcceleration;
    }

    protected void setCurrentAcceleration(double currentAcceleration) {
        if (autoCommit)
            this.currentAcceleration = currentAcceleration;
        else
            this.uncommitedCurrentAcceleration = currentAcceleration;
    }

    protected int getCurrentRoadLane() {
        return currentRoadLane;
    }

    protected Vehicle getFrontVehicle() {
        return frontVehicle;
    }

    public double getDistanceToFrontVehicle() {
        return distanceToFrontVehicle;
    }

    protected VehicleContainer getPreviousContainer() {
        return previousContainer;
    }

    void addEventListener(VehicleListener listener) {
        listeners.add(listener);
    }

    Double getStoppedTime() {
        return this.stoppedTime;
    }

    Double getTravelTime() {
        return this.travelTime;
    }

    Double getAverageSpeed() {
        return accumulatedSpeedSamples / accumulatedSpeedSampleCount;
    }

    double getMaxObservedSpeed() {
        return maxObservedSpeed;
    }

    void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }

    /**
     * Usado pelo visualizador para desenhar a linha do veículo
     * se aproximando da interseção downstream no HUD da inter-
     * seção sinalizada.
     */
    public double getDistanceToDownstream() {
        return this.getCurrentContainerLength() - this.getCurrentContainerPosition();
    }

    /**
     * Usado pelo visualizador para desenhar o veículo no mapa.
     * Anteriormente o próprio visualizador estava se encarregando
     * de criar o VehicleShape, porém julguei ser mais eficiente
     * deixar aqui dentro, pois assim o shape é gerado apenas uma
     * vez durante a existência do veículo.
     */
    public VehicleShape getVehicleShape() {
        if (this.vehicleShape == null)
            this.vehicleShape = new VehicleShape(this);
        return this.vehicleShape;
    }

    /**
     * Retorna a velocidade do veículo em km/h.
     * Usado pelo visualizador para mostrar infos do veículo na tela.
     */
    public double getVelocity() {
        return this.currentVelocity * 3.6;
    }

    /**
     * Retorna a aceleração do veículo em m/s^2.
     * Usado pelo visualizador para mostrar infos do veículo na tela.
     */
    public double getAcceleration() {
        return this.currentAcceleration;
    }

    /**
     * Retorna o drawing info do veículo à frente, ou null caso ele não exista.
     * Usado pelo visualizador para mostrar infos do veículo na tela.
     */
    public VehicleDrawingInfo getFrontVehicleDrawingInfo() {
        return this.frontVehicle;
    }

    /**
     * Retorna o drawing info da rota deste veículo.
     * Usado pelo visualizador para mostrar infos da rota na tela.
     */
    @Override
    public RouteDrawingInfo getRouteDrawingInfo() {
        return route;
    }
}
