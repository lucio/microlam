package br.ufrj.lam.microlam.core;

public class VehicleInflowRateFunctionConst implements VehicleInflowRateFunction {

    private double inflow;

    public VehicleInflowRateFunctionConst(long rate) {
        this.inflow = rate / 3600.0d; // converte de veículos/hora para veículos/segundo
    }

    public double getRate(double executionTime) {
        return this.inflow;
    }

    @Override
    public double getRateInVehiclesPerHour() {
        return this.inflow * 3600d;
    }
}
