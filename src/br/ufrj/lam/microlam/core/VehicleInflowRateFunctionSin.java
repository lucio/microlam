package br.ufrj.lam.microlam.core;


public class VehicleInflowRateFunctionSin implements VehicleInflowRateFunction {
    /**
     * parâmetro de defasamento do seno. Valor em graus
     **/
    private long phase;
    /**
     * período em milissegundos que um ciclo leva
     **/
    private double period;
    private double rateLow;
    private double rateHigh;
    private double averageRate;
    private double currentRate;

    public VehicleInflowRateFunctionSin(long phase, long periodInHours, long low, long high) {
        this.phase = phase % 360;
        this.period = periodInHours * 60 * 60 * 1000; // horas para milissegundos
        this.rateLow = Math.min(low, high) / 3600.0d; // converte de veículos/hora para veículos/segundo
        this.rateHigh = Math.max(low, high) / 3600.0d; // converte de veículos/hora para veículos/segundo
        this.averageRate = rateLow + (rateHigh - rateLow) / 2d;

        // inicia currentRate:
        this.getRate(0);
    }

    public double getRate(double executionTime) {
        double period_percent = (executionTime % period) / period;

        double angle = 360 * period_percent + (double) phase;

        double delta = rateHigh - averageRate;

        double mathSin = Math.sin(Math.toRadians(angle));
        currentRate = mathSin * delta + averageRate;

        return currentRate;
    }

    @Override
    public double getRateInVehiclesPerHour() {
        return this.currentRate * 3600d;
    }
}
