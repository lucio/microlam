package br.ufrj.lam.microlam.core;

public interface RoadConnectionDrawingInfo {

    RoadDrawingInfo getIncomingRoadPosition();
    RoadDrawingInfo getOutgoingRoadPosition();
}
