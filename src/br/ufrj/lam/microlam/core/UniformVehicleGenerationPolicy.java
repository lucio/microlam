package br.ufrj.lam.microlam.core;

import java.util.HashMap;

/**
 * Define uma política de sorteio de tipos de veículos seguindo uma taxa
 * de ocorrência para cada um, seguindo uma distribuição uniforme.
 *
 * @author Lucio
 */
public class UniformVehicleGenerationPolicy extends VehicleGenerationPolicy {
    // Tipos de veículos que serão criados em ordem
    private HashMap<Class<? extends Vehicle>, Double> generationPool = new HashMap<>();

    /**
     * Construtor.
     */
    public UniformVehicleGenerationPolicy() {
    }

    /**
     * Adiciona um novo tipo de veículo ao pool.
     *
     * @param className Nome da classe do tipo de veículo que entrará na fila.
     * @param ratio     Taxa de ocorrência do tipo.
     */
    public void addVehicleType(String className, double ratio) throws ClassNotFoundException {
        // tenta recuperar classe passada como parâmetro:
        Class<? extends Vehicle> vehicleClass = Class.forName(className).asSubclass(Vehicle.class);
        // adiciona par <tipo de veículo, taxa de ocorrência>:
        generationPool.put(vehicleClass, ratio);
    }

    /**
     * Determina a classe do próximo tipo de veículo que deve ser criado.
     */
    @Override
    public Class<? extends Vehicle> nextVehicleType() {
        double p = 0d;
        double u = Rand.uniform();

        for (Class<? extends Vehicle> vehicleClass : generationPool.keySet()) {
            p += generationPool.get(vehicleClass);
            if (u < p) {
                return vehicleClass;
            }
        }
        return null;
    }
}
