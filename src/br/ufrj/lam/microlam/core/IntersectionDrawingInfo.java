package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D;
import java.util.Vector;

public interface IntersectionDrawingInfo {

    Point2D.Double getPosition();
    Vector<RoadConnectionDrawingInfo> getRoadConnectionsDrawingInfo();
    String getName();
}
