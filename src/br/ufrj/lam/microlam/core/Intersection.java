package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Vector;

import br.ufrj.lam.microlam.exceptions.RoutePickingException;
import br.ufrj.lam.microlam.report.ReportManager;

/**
 * Representação de uma interseção de vias no sistema da simulação.
 * Liga-se necessariamente a instâncias da classe Road.
 *
 * @author Lucio Paiva
 * @see Road
 * @since Mar-08-2011
 */
public abstract class Intersection extends VehicleContainer implements IntersectionDrawingInfo, MapEntity {

    ArrayList<RoadConnection> connections;
    private Point2D.Double location;

    public Intersection(int id, Point2D.Double location) {
        super(id);
        this.name = String.format("i%d", this.id);

        this.location = location;

        connections = new ArrayList<>();
    }

    public double getX() {
        return location.getX();
    }

    public double getY() {
        return location.getY();
    }

    /**
     * Monta um array de possíveis saídas para o veículo que vem de {@code incomingRoad}.
     *
     * @return lista de {@link RoadConnection}s.
     */
    private ArrayList<RoadConnection> getPossibleRoadConnectionsWhenComingFrom(Road incomingRoad) {
        ArrayList<RoadConnection> result = new ArrayList<>();

        for (RoadConnection rc : connections)
            if (rc.getIncomingRoad() == incomingRoad)
                result.add(rc);

        return result;
    }

    /**
     * Usado na implementação do sistema de coordenação para
     * conhecer a árvore que se estende a partir desta interseção.
     *
     * @return coleção de interseções downstream em relação a esta interseção
     */
    public ArrayList<Road> getDownstreamRoadsWhenComingFrom(Road incomingRoad) {
        ArrayList<Road> result = new ArrayList<>();

        for (RoadConnection rc : connections)
            if (rc.getIncomingRoad() == incomingRoad)
                result.add(rc.getOutgoingRoad());

        return result;
    }

    /**
     * Usado por {@link Route} para identificar um possível início de ciclo infinito
     * numa rota.
     * <br /><br />
     *
     * @param road via de entrada de quem está perguntando
     * @return true se só existe uma saída para o tráfego que vem de {@code road}.
     */
    boolean hasOnlyOneWayOutWhenComingFrom(Road road) {
        return getPossibleRoadConnectionsWhenComingFrom(road).size() == 1;
    }

    public RoadConnection addRoadConnection(int id, Road incomingRoad, Road outgoingRoad, double p) {

        RoadConnection rc;

        rc = new RoadConnection(id, incomingRoad, outgoingRoad, p);

        connections.add(rc);
        return rc;
    }

    /**
     * Você dá a via por onde vai entrar e o método sorteia uma via por onde
     * você pode sair. Usado por {@link Route} para construir uma rota baseada
     * nas probabilidades de saída definidas pelo modelo do cenário.
     * <br/><br/>
     * --------------------------------------------------------------------
     * <br/><br/>
     * Caso incomingRoad seja null, assume-se que esta seja uma InflowIntersection.
     * Neste caso, o veículo está vindo de uma origem anônima e só há uma via de
     * destino possível (porque foi definido assim na arquitetura do simulador).
     * <p>
     * O veículo possui um conjunto de conexões por onde pode prosseguir. Para
     * cada conexão, há uma probabilidade p do veículo seguir por ela. É impor-
     * tante observar que a soma das probabilidades dessas conexões seja igual
     * a 1, pois caso contrário é possível que nenhuma delas seja escolhida. Caso
     * isso ocorra, a função lança uma Exception.
     *
     * @param incomingRoad via pela qual o veículo chegou a esta Intersection
     * @return via pela qual o veículo deve prosseguir em sua rota
     */
    Road pickRoute(Road incomingRoad) throws RoutePickingException {
        Road result = null;
        boolean foundConnection = false;
        boolean foundPossibleConnection = false;

        // se sou um InflowIntersection, só há uma saída possível:
        if (this instanceof InflowIntersection) {
            if (this.connections.size() > 1)
                throw new RoutePickingException("InflowIntersection id='" + this.id + "' tem mais de uma saída possível!");

            result = connections.get(0).getOutgoingRoad();
        } else if (incomingRoad != null) {
            double p = 0d;
            double u = Rand.uniform();

            // para cada conexão desta interseção que possue incomingRoad como origem:
            for (RoadConnection rc : getPossibleRoadConnectionsWhenComingFrom(incomingRoad)) {
                foundPossibleConnection = true;

                // pega probabilidade de entrar nessa conexão
                p += rc.getProbability();

                // testa a sorte:
                if (u < p) {
                    result = rc.getOutgoingRoad();
                    foundConnection = true;
                    break;
                }

                // Caso não tenha entrado nessa conexão, a probabilidade p acumula para a próxima.
                // Dessa maneira, caso o algoritmo chegue na última opção possível, chegará com
                // probabilidade 1 e vai pegar a conexão. No final teremos montado uma reta com
                // segmentos representando cada uma das possbilidades:
                //
                //     p1               p2               ...            pn
                // |--------|------------------------|---------|------------------|
                // 0                                                              1
                //
            }

            // Num caso extremo em que o cenário da simulação tenha sido montado errado e a soma
            // das probabilidades não dê 1, a função lança uma Exception:
            if (foundPossibleConnection && !foundConnection) {
                throw new RoutePickingException("A soma das probabilidades não deu 1 para a Intersection " + this.getId() + " chegando pela Road " + incomingRoad.getId());
            }
        }

        // caso tenha chegado aqui, estamos num nó de saída do sistema, pois não foi
        // possível encontrar nenhum destino a partir da via de entrada. Retorna null
        // para que o algoritmo na rotina que me chamou possa saber que a rota desse
        // veículo chegou ao fim:
        return result;
    }

    /**
     * Dada uma Intersection destination, verifica se existe
     * alguma rota desta Intersection para destination. Caso
     * exista, retorna a via que leva até lá.
     * <p>
     * Note que o sistema não prevê o caso de interseções que
     * estão ligadas por mais de uma via. Se por algum motivo
     * for desejado fazer isso, existe o seguinte workaround:
     * <p>
     * Dadas as Intersections A e B
     * Crie uma via direta ligando A a B: A->B
     * Crie a via secundária ligando A a B através de uma
     * interseção extra I: A->I->B
     * De tal forma que o fluxo que chega a I através de A->I
     * vá para I->B com probabilidade 1.
     *
     * @param destination Intersection de destino
     * @return via que conecta esta Intersection à destination, ou null caso não exista conexão direta.
     */
    public Road getRoadDownstreamTo(Intersection destination) {

        // para cada conexão:
        for (RoadConnection rc : connections) {
            // acesse a via de saída desse nó para verificar qual é o nó
            // que está na outra ponta da via; caso o nó seja destination,
            // retorna a via:
            if (rc.getOutgoingRoad().getOtherEnd(this) == destination) {
                return rc.getOutgoingRoad();
            }
        }

        return null;
    }

    /**
     * Mesma lógica do getRoadDownstreamTo(), porém sobe
     * as vias no sentido contrário ao fluxo procurando pela
     * interseção {@code source} upstream.
     */
    public Road getRoadUpstreamTo(Intersection source) {
        for (RoadConnection rc : connections) {
            if (rc.getIncomingRoad().getOtherEnd(this) == source) {
                return rc.getIncomingRoad();
            }
        }

        return null;
    }

    @Override
    public Point2D.Double getPosition() {
        return location;
    }

    @Override
    public String getIdStr() {
        return "I" + Long.toString(id);
    }

    @Override
    public Point2D.Double getVehiclePosition(Vehicle vehicle) {
        RoadConnection rc = vehicle.getRoadConnectionInIntersectionAhead(this);

        if (rc != null)
            return rc.getVehiclePosition(vehicle);
        else
            return null;
    }

    RoadConnection getRoadConnection(Road incoming, Road outgoing) {
        for (RoadConnection rc : connections) {
            if (rc.getIncomingRoad() == incoming && rc.getOutgoingRoad() == outgoing)
                return rc;
        }
        return null;
    }

    /**
     * Calcula a distância a ser percorrida pelo veículo dentro da interseção.
     * O cálculo da distância leva em conta a rua de origem e a rua de destino,
     * já que o desenho da curva depende disso.
     *
     * @param vehicle Veículo que vai cruzar a interseção
     * @return Distância que ele vai percorrer dentro da interseção
     */
    public double getLength(Vehicle vehicle) {
        RoadConnection rc = vehicle.getRoadConnectionInIntersectionAhead(this);

        return rc.getLength();
    }

    /**
     * Mesmo princípio que {@code getLength(Vehicle)}, porém nesse caso
     * são passadas a via de entrada e a via de saída.
     *
     * @return Comprimento em metros do caminho a ser percorrido dentro da via para entrar por incomingRoad e sair por outgoingRoad
     */
    public double getLength(Road incoming, Road outgoing) {
        RoadConnection conn = getRoadConnection(incoming, outgoing);
        if (conn != null)
            return conn.getLength();
        else
            return Double.POSITIVE_INFINITY;
    }

    /**
     * Calcula ângulo de inclinação do veículo em relação à origem do plano cartesiano.
     * No caso da interseção, o ângulo de um veículo depende basicamente da posição que
     * ele se encontra na travessia da curva. Essa posição vai dizer em qual segmento
     * da curva bezier ele está, e assim a função na verdade retorna o ângulo desse
     * segmento.
     *
     * @param vehicle Veículo cujo ângulo será calculado
     * @return Ângulo do veículo
     */
    double getVehicleAngle(Vehicle vehicle) {
        RoadConnection rc = vehicle.getRoadConnectionInIntersectionAhead(this);

        return rc.getVehicleAngle(vehicle);
    }

    /**
     * Obtém o veículo que está à frente de 'vehicle'.
     * A função procura entre todos os veículos que chegaram a esta
     * Intersection através da mesma via pela qual 'vehicle' está
     * vindo. Se nada for encontrado, retorna null.
     */
    @Override
    public Vehicle getMostUpstreamVehicle(Vehicle vehicle) {
        // FIXME não é pra adicionar TODOS os veículos; só aqueles que vão para o mesmo destino que eu!
        ArrayList<Vehicle> candidateVehicles = new ArrayList<>(vehicles);
        candidateVehicles.add(vehicle);

        // TODO getContainerInRouteAfter precisa usar tb a informação do vc em que se encontra o veiculo agora - o veiculo pode passar por esta intersecao n vezes. estou querendo saber sobre a proxima passada dele.
        Road downstreamRoad = (Road) vehicle.getContainerInRouteAfter(this);
        if (downstreamRoad != null)
            if (!downstreamRoad.canContain(candidateVehicles)) {
                // antes de enviar um BlockingVehicle, verifica se não há nenhum veículo com a traseira exposta:
                Vehicle frontVehicle = findAnyVehicleWhichHasntFullyCrossedTheBoundaryBetweenMyRoadAndThisIntersection(vehicle);

                if (frontVehicle != null)
                    return frontVehicle;

                return new BlockingVehicle();
            }

        // CHECK 2 - PERGUNTA QUEM ESTÁ MAIS PRÓXIMO DELE DENTRO DA INTERSEÇÃO

        return findNearestVehicle(vehicle);
    }

    /**
     * Rotina similar à Road.getFrontVehicle(). A diferença é que
     * nessa ainda temos que filtrar retirando os veículos que não vêm
     * da mesma via de origem que 'me', pois estes não estão efetivamente
     * na frente de 'me'.
     */
    @Override
    public Vehicle getFrontVehicle(Vehicle me) {
        return findNearestVehicle(me);
    }

    /**
     * Acha o veículo mais próximo de mim no cruzamento e que esteja necessariamente à minha frente.
     * Essa rotina é usada para evitar colisões durante engarrafamentos. Normalmente o following model
     * só olha pra frente... mas nesse caso, podem vir carros pelos lados!
     */
    private Vehicle findNearestVehicle(Vehicle myself) {
        Vehicle frontVehicle = null;

        /*
         * A primeira parte consiste em calcular a distância de mim até o ponto final da
         * trajetória da minha curva por este cruzamento:
         **/
        Point2D.Double myPosition = myself.getPosition();
        RoadConnection rc = myself.getRoadConnectionInIntersectionAhead(this);
        if (rc == null)
            return frontVehicle; // se rc == null, esta é uma Intersection de saída do sistema (não tem outgoingRoad e retorna null por causa disso)
        Point2D.Double endOfMyPath = rc.getPathEndingPoint();
        double searchRadius = Utils.distanceBetweenPoints(myPosition, endOfMyPath);

        /*
         * A variável searchRadius me garante que só vou encontrar veículos que estejam à minha
         * frente no cruzamento. Isso evita que eu acidentalmente me guie por um carro que está
         * atrás de mim, o que pode causar complicações (ver arquivo "doc/Engarrafamento em
         * cruzamentos.txt"), na parte "ADENDO 1 DA SOLUÇÃO 3".
         */

        double mindist = Double.MAX_VALUE;
        // itera por todos os veículos que estão na Intersection atualmente:
        for (Vehicle otherVehicle : vehicles) {
            if (otherVehicle == myself) continue;

            // ignora os veículos que estão mais longe que eu do ponto final da minha curva:
            if (Utils.distanceBetweenPoints(otherVehicle.getPosition(), endOfMyPath) > searchRadius)
                continue;

            double dist = getDistanceBetweenVehicles(myself, otherVehicle);

            // se essa distância é menor do que a menor distância que havia encontrado até agora:
            if (dist < mindist) {
                // se este veículo já não estiver me seguindo (evita deadlocks!):
                if (otherVehicle.getFrontVehicle() != myself) {
                    frontVehicle = otherVehicle;
                    mindist = dist;
                }
            }
        }

        return frontVehicle;
    }

    /**
     * O objetivo desta rotina é encontrar veículos nessa interseção à minha frente que estejam com a
     * traseira ainda para fora dela, na via onde estou. Caso isso aconteça, tenho que saber para evitar
     * bater nele (o que pode acontecer se o sinal estiver fechado, quando então o veículo não olharia
     * mais adiante para saber que na verdade há um carro parado bem no sinal, com a traseira um pouco
     * pra trás).
     * <p>
     *
     * @param me Veículo que está indagando
     * @return Veículo que está com a traseira na via - ou null se não houver um
     */
    Vehicle findAnyVehicleWhichHasntFullyCrossedTheBoundaryBetweenMyRoadAndThisIntersection(Vehicle me) {
        RoadConnection rc = me.getRoadConnectionInIntersectionAhead(this);
        if (rc == null) {
            return null; // se rc == null, esta é uma Intersection de saída do sistema (não tem outgoingRoad e retorna null por causa disso
        }

        for (Vehicle v : vehicles) {
            double curdist = getDistanceBetweenVehicles(me, v);

            if (curdist < v.getLength())
                return v;
        }

        return null;
    }

    /**
     * Calcula a distância entre dois veículos dentro da interseção.
     * É possível, porém, que meu veículo esteja do lado de fora. Nesse caso,
     * a função é usada para calcular a distância do carro dentro da interseção
     * que está mais próximo do ponto inicial da curva que o meu veículo fará
     * quando finalmente entrar no cruzamento.
     * <p>
     * OBS: não é trabalho dessa rotina subtrair o comprimento do veículo da
     * frente. Se for necessário, isso deve ser feito FORA dela!
     */
    double getDistanceBetweenVehicles(Vehicle v1, Vehicle v2) {
        // caso os dois veículos estejam dentro do cruzamento, basta retornar a distância entre seus dois pontos:
        if ((v1.getCurrentContainer() == this) && (v2.getCurrentContainer() == this)) {
            return Utils.distanceBetweenPoints(v1.getPosition(), v2.getPosition());
        }
        // se ambos não estiverem contidos, gera erro:
        else if ((v1.getCurrentContainer() != this) && (v2.getCurrentContainer() != this)) {
            ReportManager.err("Intersection.getDistanceBetweenVehicles() error!");
            return 0;
        }
        // caso em que um deles está do lado de fora:
        else {
            // descobre quem é local e quem está do lado de fora:
            Vehicle foreigner = (v1.getCurrentContainer() == this) ? v2 : v1;
            Vehicle local = (v1.getCurrentContainer() == this) ? v1 : v2;

            // pega o ponto inicial da curva do veículo que está do lado de fora:
            RoadConnection rc = foreigner.getRoadConnectionInIntersectionAhead(this);
            if (rc == null) return 0;
            Point2D.Double org = rc.getPathStartingPoint();
            // usa esse ponto inicial para calcular a distância do veículo que está dentro da interseção:
            return Utils.distanceBetweenPoints(org, local.getPosition());
        }

    }

    @Override
    public Vector<RoadConnectionDrawingInfo> getRoadConnectionsDrawingInfo() {
        return new Vector<>(connections);
    }
}
