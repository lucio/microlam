package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D;
import java.util.Vector;

public class Bezier {

    private static final int RESOLUTION = 20;
    private static final int N = 3; // número de pontos iniciais - como é uma bezier quadrática, N é necessariamente 3

    private Vector<Point2D.Double> points;
    private Vector<Double> segmentLengths;
    private Vector<Double> segmentAngles;
    private double totalLength;

    private double Px[], Py[];

    Bezier(Point2D.Double p1, Point2D.Double p2, Point2D.Double p3) {
        Px = new double[]{p1.x, p2.x, p3.x};
        Py = new double[]{p1.y, p2.y, p3.y};

        points = new Vector<>();
        segmentLengths = new Vector<>();
        segmentAngles = new Vector<>();

        drawCurve();
    }

    /**
     * Converte uma posição relativa ao comprimento da curva para uma posição
     * no plano cartesiano onde a curva está inserida.
     *
     * @param currentPosition posição relativa ao comprimento da curva
     * @return posição absoluta do ponto fornecido como parâmetro
     */
    Point2D.Double linearToCartesianPosition(double currentPosition) {

        // para cada segmento da bezier:
        for (int i = 0; i < segmentLengths.size(); i++) {
            // verifica se ponto passado como parâmetro está contido neste segmento:
            if (currentPosition < segmentLengths.get(i)) {
                // achou o segmento onde o ponto se encontra atualmente:
                double r = currentPosition / segmentLengths.get(i);
                double x = r * (points.get(i + 1).getX() - points.get(i).getX()) + points.get(i).getX();
                double y = r * (points.get(i + 1).getY() - points.get(i).getY()) + points.get(i).getY();
                return new Point2D.Double(x, y);
            } else
                // subtrai comprimento do segmento atual de currentPosition
                currentPosition -= segmentLengths.get(i);
        }

        return new Point2D.Double(points.lastElement().getX(), points.lastElement().getY());

    }

    /**
     * Converte uma posição relativa ao comprimento da curva para uma posição
     * no plano cartesiano onde a curva está inserida.
     *
     * @param currentPosition posição relativa ao comprimento da curva
     * @return posição absoluta do ponto fornecido como parâmetro
     */
    double linearToCartesianAngle(double currentPosition) {
        // para cada segmento da bezier:
        for (int i = 0; i < segmentLengths.size(); i++) {
            // verifica se ponto passado como parâmetro está contido neste segmento:
            if (currentPosition < segmentLengths.get(i)) {
                // achou o segmento onde o ponto se encontra atualmente:
                return segmentAngles.get(i);
            } else
                // subtrai comprimento do segmento atual de currentPosition
                currentPosition -= segmentLengths.get(i);
        }

        return segmentAngles.lastElement();
    }

    // Interactive 2D Bezier splines,  Evgeny Demidov 16 June 2001
    /*
     * Código refatorado por Lucio Paiva <luciopaiva@gmail.com> em 09 March 2011
     * Original em http://ibiblio.org/e-notes/Splines/Bezier.htm
     * Especificamente aqui: http://ibiblio.org/e-notes/Splines/Bezier.java
     * Que cheguei a partir desta página:
     *    An Interactive Introduction to Splines
     *    http://ibiblio.org/e-notes/Splines/Intro.htm
     */
    private void drawCurve() {
        totalLength = 0;
        segmentLengths.clear();
        segmentAngles.clear();
        points.clear();

        points.add(new Point2D.Double(Px[0], Py[0]));

        double Xold = Px[0];
        double Yold = Py[0];
        double X, Y;

        double[] Pxi = new double[N];
        double[] Pyi = new double[N];

        double step = 1. / (double) RESOLUTION;
        double t = step;

        // cada iteração desse loop externo constroi mais uma mini-reta que vai formar a curva.
        // quanto mais iterações (resolution) forem rodadas, menores serão os segmentos e mais
        // definida ficará a curva final
        for (int k = 1; k < RESOLUTION; k++) {

            System.arraycopy(Px, 0, Pxi, 0, N); // copia Px -> Pxi
            System.arraycopy(Py, 0, Pyi, 0, N); // copia Py -> Pyi

            // anda no array de trás para frente e não itera para o elemento 0
            for (int j = N - 1; j > 0; j--) {
                // anda no array de frente para trás e itera até j-1
                for (int i = 0; i < j; i++) {
                    Pxi[i] = (1 - t) * Pxi[i] + t * Pxi[i + 1];
                    Pyi[i] = (1 - t) * Pyi[i] + t * Pyi[i + 1];
                }
            }

            X = Pxi[0];
            Y = Pyi[0];

            points.add(new Point2D.Double(X, Y));
            calculateSegmentLengthAndAngle(Xold, Yold, X, Y);

            Xold = X;
            Yold = Y;
            t += step;
        }
        // lucio: o algoritmo original não desenha a última linha ligando ao ponto final
        // o autor provavelmente não se importou pq colocou uma resolução muito alta;
        // porém, meu propósito é usar esse algoritmo com baixa resolução, então a
        // última linha importa:
        points.add(new Point2D.Double(Px[N - 1], Py[N - 1]));
        calculateSegmentLengthAndAngle(Xold, Yold, Px[N - 1], Py[N - 1]);
    }

    /**
     * Calcula a distância entre dois pontos.
     */
    private void calculateSegmentLengthAndAngle(double x1, double y1, double x2, double y2) {
        double x = (x2 - x1);
        double y = (y2 - y1);
        double seglen = Math.sqrt(x * x + y * y);
        double angle;

        if (x2 - x1 != 0) {
            angle = Math.atan2((y2 - y1), (x2 - x1));
        } else {
            if (y2 < y1) {
                angle = Math.toRadians(90);
            } else {
                angle = Math.toRadians(270);
            }
        }

        totalLength += seglen;
        segmentLengths.add(seglen);
        segmentAngles.add(angle);
    }

    public double getLength() {
        return totalLength;
    }

}
