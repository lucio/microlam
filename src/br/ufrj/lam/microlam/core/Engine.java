package br.ufrj.lam.microlam.core;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Vector;

import br.ufrj.lam.microlam.coordination.SMERCoordinator;
import br.ufrj.lam.microlam.exceptions.InvalidRoadIdException;
import br.ufrj.lam.microlam.exceptions.RoutePickingException;
import br.ufrj.lam.microlam.exceptions.StepTooBigException;
import br.ufrj.lam.microlam.exceptions.UnknownVehicleContainerException;
import br.ufrj.lam.microlam.exceptions.VehicleCopyConstructorMissingException;
import br.ufrj.lam.microlam.signalcontrol.SMERSignalController;
import br.ufrj.lam.microlam.signalcontrol.SignalControllerException;

/**
 * Implementa a base para o simulador operar. Enumera ruas e interseções do sistema. Todos os objetos
 * estáticos da simulação são manipulados aqui.
 * Nada do que diz respeito aos veículos e como eles se comportam está nessa parte.
 * Além disso, esta classe também não trata de como os sinais se comportam.
 *
 * @author Lucio Paiva
 * @since Mar-08-2011
 */
public class Engine implements VehicleListener {
    static final boolean DEBUG_WARNINGS = false;
    private static final long MAX_STEP = 100;

    /**
     * Se true, métodos toString() dos objetos passam a imprimir seguindo a formatação de um arquivo CSV (comma separated value)
     */
    private boolean debugModeCSV = true;
    private static java.text.DecimalFormat df = new java.text.DecimalFormat("#.##");

    private int nextRoadIndex;
    private int nextIntersectionIndex;

    private Scenario mScenario;

    private double stepDuration;

    private double simulationTime; // tempo decorrido da simulação, em milissegundos
    /**
     * número total de veículos que passaram pelo sistema desde o início da simulação
     **/
    private long totalNumberOfVehiclesSinceBeginningOfSimulation;

    private double statisticsInterval = 1000.d; // intervalo entre duas coletas, em ms
    private double statisticsNextSample = statisticsInterval;

    // geração de estatísticas:
    private ArrayList<Double> stoppedTimes = new ArrayList<>();
    private ArrayList<Double> travelTimes = new ArrayList<>();
    private ArrayList<Double> averageSpeeds = new ArrayList<>();
    private double maximumObservedSpeed = 0;

    /**
     * Executa uma iteração da simulação.
     */
    public void step() throws RoutePickingException,
            UnknownVehicleContainerException, SignalControllerException {
        // atualiza interseções de entrada e interseções sinalizadas
        stepTrafficLights(this.stepDuration);
        introduceNewVehicles(this.stepDuration);

        // atualiza ondas verdes nas vias:
        stepRoads(this.stepDuration);

        // atualiza posição dos veículos no sistema
        stepVehicles(this.stepDuration);

        // gera estatísticas:
        runStatistics();

        simulationTime += this.stepDuration;
    }

    /**
     * Gera estatísticas em cima do cenário. As estatísticas são coletadas
     * segundo o intervalo ditado por {@link Engine#statisticsInterval}.
     */
    private void runStatistics() {
        if (simulationTime < statisticsNextSample) {
            return;
        }

        statisticsNextSample += statisticsInterval;
    }

    /**
     * Atualmente é usado apenas para atualizar
     * a posição das ondas verdes contidas nas
     * vias.
     */
    private void stepRoads(double dt) {
        for (Road r : this.mScenario.getRoads()) {
            r.step(dt);
        }
    }

    /**
     * Atualiza estados dos semáforos.
     */
    private void stepTrafficLights(double dt) throws SignalControllerException {
        for (Intersection in : this.mScenario.getIntersections()) {
            if (in instanceof SignalizedIntersection) {
                SignalizedIntersection si = (SignalizedIntersection) in;
                si.step(dt);
            }
        }
    }

    /**
     * Atualiza a posição de cada veículo rodando no sistema atualmente
     */
    private void stepVehicles(double dt) throws UnknownVehicleContainerException {
        /*
         * A primeira passada faz os cálculos de deslocamento
         * dos veículos, porém não sobrescreve ainda os valores
         * atuais. Se os valores fossem sobrescritos, teríamos
         * um estado global inconsistente pois como há interação
         * entre os veículos, o veículo i poderia estar se
         * baseando na posição futura do veículo i-1 que foi
         * calculado na iteração anterior do for.
         */
        for (Vehicle vehicle : getActiveVehicles()) {
            vehicle.step(dt);
        }

        /*
         * Aqui sim, o novo estado dos veículos é armazenado
         * definitivamente.
         */
        for (Vehicle vehicle : getActiveVehicles())
            vehicle.commitStep();
    }

    /**
     * Cria uma cópia exata do Vehicle 'oldVehicle'. Essa função é chamada durante a fase de
     * atualização dos veículos no sistema, assim que um step() é invocado na engine.
     *
     * @param oldVehicle Veículo a ser clonado
     * @return Clone de 'oldVehicle'
     */
    @SuppressWarnings("unused")
    private Vehicle cloneVehicle(Vehicle oldVehicle) throws VehicleCopyConstructorMissingException {
        Vehicle newVehicle = null;
        Class<? extends Vehicle> cl = oldVehicle.getClass();   // captura classe dele (que será uma subclasse de Vehicle)
        try {
            java.lang.reflect.Constructor<? extends Vehicle> ct = cl.getConstructor(cl);  // captura o copy constructor da classe dele

            newVehicle = ct.newInstance(oldVehicle);  // invoca copy constructor do veículo para criar um clone dele no novo estado global
        } catch (java.lang.NoSuchMethodException e) {
            throw new VehicleCopyConstructorMissingException("A classe " + cl.getName() + " precisa implementar o construtor " + cl.getName() + "(Vehicle).");
        } catch (IllegalArgumentException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return newVehicle;
    }

    /**
     * @return Veículo com o id requisitado ou null se ele não pertence ao array passado como parâmetro.
     */
    private Vehicle getVehicleById(ArrayList<Vehicle> vehicleList, long id) {
        for (Vehicle v : vehicleList) {
            if (v.getId() == id)
                return v;
        }
        return null;
    }

    /**
     * Introduz novos carros no sistema, de acordo com a necessidade de cada entrada.
     */
    private void introduceNewVehicles(double dt) throws RoutePickingException {
        for (Intersection intersection : mScenario.getIntersections()) {
            if (intersection instanceof InflowIntersection) {
                InflowIntersection inflowIntersection = (InflowIntersection) intersection;

                inflowIntersection.step(dt);

                // após o step(), pode ser que haja veículos novos a serem introduzidos no sistema:
                while (inflowIntersection.wantsToIntroduceNewVehicle()) {
                    placeNewVehicle(inflowIntersection);
                }
            }
        }
    }

    /**
     * (Re)inicia parâmetros da engine para começar uma execução limpa.
     */
    public void start() {
        simulationTime = 0.0d;
        statisticsNextSample = statisticsInterval;
        // caso o seed de mScenario seja null, escrevemos nele a seed gerada:
        this.mScenario.setSeed(Rand.getSeed());

        /*
         * Dispara inicialização dos corredores SMER.
         */
        initializeCoordinators();
    }

    /**
     * Acorda todos os coordenadores de cruzamentos avisando que a engine vai
     * começar a operar e dá a chance para que eles iniciem seus sistemas.
     */
    // TODO desacoplar SMERCoordinator da Engine (foi feito às pressas por conta do meu deadline no mestrado!)
    private void initializeCoordinators() {
        double maxOffset = 0;
        for (SMERCoordinator coordinator : this.mScenario.getCoordinators()) {
            double offset = coordinator.initialize();
            if (offset > maxOffset)
                maxOffset = offset;
        }
        /*
         * Avança no eixo do tempo, colocando a origem do eixo em cima do offset da interseção coordenada mais avançada de todas.
         */
        this.simulationTime += maxOffset * 1000d;
    }

    public Engine(Scenario sc, double stepDuration) throws StepTooBigException {
        if (stepDuration > MAX_STEP) {
            throw new StepTooBigException("Engine step cannot be greater than " + MAX_STEP + "ms, otherwise there will be inconsistencies.");
        }

        this.clearEngine();
        this.mScenario = sc;
        this.stepDuration = stepDuration;
    }

    /**
     * Limpa todas as listas e reinicia contadores.
     */
    private void clearEngine() {
        mScenario = null;
        debugModeCSV = false;
        nextIntersectionIndex = 1;
        nextRoadIndex = 1;
        totalNumberOfVehiclesSinceBeginningOfSimulation = 0;
    }

    public long getSeed() {
        return mScenario.getSeed();
    }

    public void setSeed(long seed) {
        this.mScenario.setSeed(seed);
    }

    private ArrayList<GreenWaveSegment> getGreenWaveSegments() {
        ArrayList<GreenWaveSegment> greenWaveSegments = new ArrayList<>();

        for (Road r : mScenario.getRoads()) {
            greenWaveSegments.addAll(r.getGreenWaveSegments());
        }

        return greenWaveSegments;
    }

    /**
     * @return Lista completa dos veículos que estão no sistema
     */
    private ArrayList<Vehicle> getActiveVehicles() {
        ArrayList<Vehicle> activeVehicles = new ArrayList<>();

        for (Road r : mScenario.getRoads()) {
            activeVehicles.addAll(r.getVehicles());
        }

        for (Intersection r : mScenario.getIntersections()) {
            activeVehicles.addAll(r.getVehicles());
        }

        return activeVehicles;
    }

    static String formatDouble(double d) {
        return df.format(d);
    }

    public ArrayList<GreenWaveSegmentDrawingInfo> getGreenWavesDrawingInfo() {
        return new ArrayList<>(getGreenWaveSegments());
    }

    public Vector<RoadDrawingInfo> getRoadsDrawingInfo() {
        return new Vector<>(mScenario.getRoads());
    }

    public Vector<IntersectionDrawingInfo> getIntersectionsDrawingInfo() {
        return new Vector<>(mScenario.getIntersections());
    }

    public Vector<VehicleDrawingInfo> getVehiclesDrawingInfo() {
        return new Vector<>(getActiveVehicles());
    }

    public double getRoadCapacity(long id) throws InvalidRoadIdException {
        for (Road r : mScenario.getRoads()) {
            if (r.getId() == id) {
                return r.getDensity();
            }
        }

        throw new InvalidRoadIdException("Via " + Long.toString(id) + " inexistente!");
    }

    /**
     * Retorna tempo total decorrido de simulação em SEGUNDOS
     **/
    public double getElapsedTimeInSeconds() {
        return simulationTime / 1000d;
    }

    public Vector<SignalizedIntersectionDrawingInfo> getSignalizedIntersectionsDrawingInfo() {
        Vector<SignalizedIntersectionDrawingInfo> result = new Vector<>();

        for (Intersection in : mScenario.getIntersections()) {
            if (in instanceof SignalizedIntersection) {
                result.add((SignalizedIntersection) in);
            }
        }

        return result;
    }

    private Vehicle getFreeVehicle(Class<? extends Vehicle> clazz) {
        Vehicle newVehicle = null;

        try {
            // para sermos capazes de passar um parametro para o construtor do veiculo, precisamos usar a classe Constructor<> do lang.reflect
            Constructor<? extends Vehicle> ct = clazz.getConstructor(Long.TYPE);  // <-- fazendo uso de variable arguments // cl.getConstructor(new Class[] {Integer.TYPE} );
            newVehicle = ct.newInstance(mScenario.getNextFreeVehicleIndex());  // <-- fazendo uso de variable arguments // ct.newInstance(new Object[]{ this.getNextVehicleIndex() });
            newVehicle.addEventListener(this);

        } catch (SecurityException | NoSuchMethodException | InstantiationException | IllegalArgumentException |
                IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return newVehicle;
    }

    private void placeNewVehicle(InflowIntersection intersection) throws RoutePickingException {
        // Cria um novo veículo
        Vehicle vehicle = this.getFreeVehicle(intersection.removeWaitingVehicle());

        // Coloca o veículo na pista
        vehicle.placeMe(intersection);

        vehicle.calculateStartingVelocity();
        vehicle.commitStep();

        totalNumberOfVehiclesSinceBeginningOfSimulation++;
    }

    /**
     * Método invocado toda vez que um veículo dispara um evento.
     */
    @Override
    public void vehicleEvent(VehicleEvent vehicleEvent) {
        switch (vehicleEvent.getEventKind()) {
            case vehicleAdvancedInRoute:  // veículo avançou para próximo container em sua rota

                Vehicle vehicle = vehicleEvent.getVehicle();

                /*
                 * Atualiza containers com a nova posição do veículo que avançou sua rota.
                 */
                if (vehicleEvent.getOldContainer() != null)
                    vehicleEvent.getOldContainer().removeVehicle(vehicle, this.getElapsedTimeInSeconds());
                if (vehicleEvent.getNewContainer() != null)
                    vehicleEvent.getNewContainer().addVehicle(vehicle);

                // se o veículo acabou de sair do sistema, atualiza estatísticas:
                if (vehicleEvent.getNewContainer() == null) {
                    addStatisticsForVehicle(vehicle);
                }
                break;
        }
    }

    private void addStatisticsForVehicle(Vehicle vehicle) {
        stoppedTimes.add(vehicle.getStoppedTime());
        travelTimes.add(vehicle.getTravelTime());
        averageSpeeds.add(vehicle.getAverageSpeed());
        if (vehicle.getMaxObservedSpeed() > maximumObservedSpeed)
            maximumObservedSpeed = vehicle.getMaxObservedSpeed();
    }

    public String printReportSummaryHeaderToCSV() {
        /*
         * para cada via, gerar:
         * - contagem de veiculos: quantos veiculos passaram pela via durante toda a simulacao
         * - tempo parado: quanto tempo um veiculo passa parado dentro da via - media, max, min, dp
         * - tempo de viagem: quanto tempo um veiculo passa dentro da via - media, max, min, dp
         * - velocidade: velocidade dos veiculos que passaram pela via - media, max, min, dp
         * - headway: intervalo entre chegadas de veículos consecutivos - media, max, min, dp
         * - ondas verdes
         * 		- quantas ondas foram geradas
         * 		- tempo de duração (bandwidth) - media, max, min, dp
         * 		- comprimento (bandwidth tb?) - media, max, min, dp
         * 		- eficiencia (duracao da onda / tempo do ciclo) - media, max, min, dp
         *
         * no final o report terá uma seção "Sistema" com todas as medidas acima e, para cada via, haverá uma seção extra
         * com essas medidas restritas àquela via
         */

        String header =
                Utils.quoteStr("Tempo de Simulação (s)") +
                        ";" + Utils.quoteStr("Contagem de Veículos") +
                        ";" + Utils.quoteStr("Tempo Parado (Min, s)") +
                        ";" + Utils.quoteStr("Tempo Parado (Média, s)") +
                        ";" + Utils.quoteStr("Tempo Parado (Max, s)") +
                        ";" + Utils.quoteStr("Tempo Parado (DP, s)") +
                        ";" + Utils.quoteStr("Tempo de Viagem (Min, s)") +
                        ";" + Utils.quoteStr("Tempo de Viagem (Média, s)") +
                        ";" + Utils.quoteStr("Tempo de Viagem (Max, s)") +
                        ";" + Utils.quoteStr("Tempo de Viagem (DP, s)") +
                        ";" + Utils.quoteStr("Vel. (Min, km/h)") +
                        ";" + Utils.quoteStr("Vel. (Média, km/h)") +
                        ";" + Utils.quoteStr("Vel. (Max, km/h)") +
                        ";" + Utils.quoteStr("Vel. (DP, km/h)") +
                        ";" + Utils.quoteStr("Headway (Min, s)") +
                        ";" + Utils.quoteStr("Headway (Média, s)") +
                        ";" + Utils.quoteStr("Headway (Max, s)") +
                        ";" + Utils.quoteStr("Headway (DP, s)");
        for (Road road : this.mScenario.getRoads()) {
            //noinspection StringConcatenationInLoop
            header +=
                    ";" + Utils.quoteStr(road.getIdStr() + " Contagem de Veículos") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Tempo Parado (Min, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Tempo Parado (Media, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Tempo Parado (Max, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Tempo Parado (DP, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Tempo de Viagem (Min, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Tempo de Viagem (Media, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Tempo de Viagem (Max, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Tempo de Viagem (DP, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Vel. (Min, km/h)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Vel. (Media, km/h)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Vel. (Max, km/h)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Vel. (DP, km/h)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Headway (Min, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Headway (Media, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Headway (Max, s)") +
                            ";" + Utils.quoteStr(road.getIdStr() + " Headway (DP, s)");
        }

        return header;
    }

    private String printSimulationIterationCSV_global() {
        String report = "";

        /*
         * Estatísticas globais.
         */
        report += String.format("%d", this.totalNumberOfVehiclesSinceBeginningOfSimulation);

        report += ";" + String.format("%.2f", Utils.min(this.stoppedTimes));
        report += ";" + String.format("%.2f", Utils.calculateAverage(this.stoppedTimes));
        report += ";" + String.format("%.2f", Utils.max(this.stoppedTimes));
        report += ";" + String.format("%.2f", Utils.calculateStandardDeviation(this.stoppedTimes));

        report += ";" + String.format("%.2f", Utils.min(this.travelTimes));
        report += ";" + String.format("%.2f", Utils.calculateAverage(this.travelTimes));
        report += ";" + String.format("%.2f", Utils.max(this.travelTimes));
        report += ";" + String.format("%.2f", Utils.calculateStandardDeviation(this.travelTimes));

        /*
         * Nota sobre velocidades:
         * O sistema calcula, para cada veículo, qual foi sua velocidade MÉDIA assim que ele abandona o sistema.
         * A Engine acumula um vetor de velocidades MÉDIAS de cada veículo. É importante frizar que, no relatório,
         * MIN e MAX representam a velocidade média mínima e a velocidade média máxima registradas. Observe que
         * MIN e MAX não mostram e velocidade instantânea mínima e velocidade instantânea máxima alcançada por
         * um veículo qualquer no sistema. Estamos falando sempre de médias aqui.
         */
        report += ";" + String.format("%.2f", 3.6d/* km/h */ * Utils.min(this.averageSpeeds));
        report += ";" + String.format("%.2f", 3.6d/* km/h */ * Utils.calculateAverage(this.averageSpeeds));
        report += ";" + String.format("%.2f", 3.6d/* km/h */ * Utils.max(this.averageSpeeds));
        report += ";" + String.format("%.2f", 3.6d/* km/h */ * Utils.calculateStandardDeviation(this.averageSpeeds));

        ArrayList<Double> headways = new ArrayList<>();
        for (Road road : this.mScenario.getRoads()) {
            if (road.getDownstreamIntersection() instanceof OutflowIntersection)
                continue;
            headways.addAll(road.getHeadways());
        }
        report += ";" + String.format("%.2f", Utils.min(headways));
        report += ";" + String.format("%.2f", Utils.calculateAverage(headways));
        report += ";" + String.format("%.2f", Utils.max(headways));
        report += ";" + String.format("%.2f", Utils.calculateStandardDeviation(headways));

        return report;
    }

    private String printSimulationIterationCSV_local(Road road) {
        String report = "";

        report += String.format("%d", road.getNumberOfVehiclesSinceBeginningOfSimulation());

        report += ";" + String.format("%.2f", Utils.min(road.getStoppedTimes()));
        report += ";" + String.format("%.2f", Utils.calculateAverage(road.getStoppedTimes()));
        report += ";" + String.format("%.2f", Utils.max(road.getStoppedTimes()));
        report += ";" + String.format("%.2f", Utils.calculateStandardDeviation(road.getStoppedTimes()));

        report += ";" + String.format("%.2f", Utils.min(road.getTravelTimes()));
        report += ";" + String.format("%.2f", Utils.calculateAverage(road.getTravelTimes()));
        report += ";" + String.format("%.2f", Utils.max(road.getTravelTimes()));
        report += ";" + String.format("%.2f", Utils.calculateStandardDeviation(road.getTravelTimes()));

        report += ";" + String.format("%.2f", 3.6d/* km/h */ * Utils.min(road.getAverageSpeeds()));
        report += ";" + String.format("%.2f", 3.6d/* km/h */ * Utils.calculateAverage(road.getAverageSpeeds()));
        report += ";" + String.format("%.2f", 3.6d/* km/h */ * Utils.max(road.getAverageSpeeds()));
        report += ";" + String.format("%.2f", 3.6d/* km/h */ * Utils.calculateStandardDeviation(road.getAverageSpeeds()));

        report += ";" + String.format("%.2f", road.getDownstreamIntersection() instanceof OutflowIntersection ? 0d : Utils.min(road.getHeadways()));
        report += ";" + String.format("%.2f", road.getDownstreamIntersection() instanceof OutflowIntersection ? 0d : Utils.calculateAverage(road.getHeadways()));
        report += ";" + String.format("%.2f", road.getDownstreamIntersection() instanceof OutflowIntersection ? 0d : Utils.max(road.getHeadways()));
        report += ";" + String.format("%.2f", road.getDownstreamIntersection() instanceof OutflowIntersection ? 0d : Utils.calculateStandardDeviation(road.getHeadways()));

        return report;
    }

    public String printSimulationIterationCSV() {
        StringBuilder report = new StringBuilder();

        /*
         * Estatísticas globais.
         */
        report.append(String.format("%d", (long) getSimulationTimeInSeconds()));
        report.append(";").append(printSimulationIterationCSV_global());

        /*
         * Estatísticas locais.
         */
        for (Road road : this.mScenario.getRoads()) {
            report.append(";").append(printSimulationIterationCSV_local(road));
        }

        return report.toString();
    }

    /**
     * @return String com a saída do report da simulação.
     */
    public String printSimulationResultsCSV() {
        StringBuilder report =
                new StringBuilder(Utils.quoteStr("Escopo") +
                        ";" + Utils.quoteStr("Contagem de Veículos") +
                        ";" + Utils.quoteStr("Tempo Parado (Min, s)") +
                        ";" + Utils.quoteStr("Tempo Parado (Média, s)") +
                        ";" + Utils.quoteStr("Tempo Parado (Max, s)") +
                        ";" + Utils.quoteStr("Tempo Parado (DP, s)") +
                        ";" + Utils.quoteStr("Tempo de Viagem (Min, s)") +
                        ";" + Utils.quoteStr("Tempo de Viagem (Média, s)") +
                        ";" + Utils.quoteStr("Tempo de Viagem (Max, s)") +
                        ";" + Utils.quoteStr("Tempo de Viagem (DP, s)") +
                        ";" + Utils.quoteStr("Vel. (Min, km/h)") +
                        ";" + Utils.quoteStr("Vel. (Média, km/h)") +
                        ";" + Utils.quoteStr("Vel. (Max, km/h)") +
                        ";" + Utils.quoteStr("Vel. (DP, km/h)") +
                        ";" + Utils.quoteStr("Headway (Min, s)") +
                        ";" + Utils.quoteStr("Headway (Média, s)") +
                        ";" + Utils.quoteStr("Headway (Max, s)") +
                        ";" + Utils.quoteStr("Headway (DP, s)"));

        report.append("\n").append(Utils.quoteStr("Global")).append(";").append(printSimulationIterationCSV_global());

        for (Road road : this.mScenario.getRoads()) {
            report.append("\n").append(Utils.quoteStr(road.getIdStr())).append(";").append(printSimulationIterationCSV_local(road));
        }

        return report.toString();
    }

    /**
     * @return String com a saída do report da simulação.
     */
    public String printSimulationSummary() {
        String report = "";

        report += "\n+------------------------------------------------------------------------------+";
        report += "\n|                            RESUMO DA SIMULAÇÃO                               |";
        report += "\n+------------------------------------------------------------------------------+";
        report += String.format("\n> Tempo de simulação: %ds", getSimulationTimeInSeconds());
        report += String.format("\n> Cenário: '%s'", mScenario.getScenarioName());
        report += String.format("\n> Seed do gerador de números pseudo-aleatórios: %d", mScenario.getSeed());
        report += String.format("\n> Contagem total de veículos distintos: %d", this.totalNumberOfVehiclesSinceBeginningOfSimulation);
        report += String.format("\n> Tempo médio parado: %.2fs", Utils.calculateAverage(this.stoppedTimes));
        report += String.format("\n> Tempo médio de viagem: %.2fs", Utils.calculateAverage(this.travelTimes));
        report += String.format("\n> Velocidade média: %.2fkm/h", 3.6d/* km/h */ * Utils.calculateAverage(this.averageSpeeds));
        report += String.format("\n> Máxima velocidade observada: %.2fkm/h", getMaximumObservedSpeed());
        report += "\n+------------------------------------------------------------------------------+";

        return report;
    }

    /**
     * @return Tempo de simulação até agora, em segundos.
     */
    private long getSimulationTimeInSeconds() {
        return (long) (this.simulationTime / 1000d);
    }

    private Double getMaximumObservedSpeed() {
        return maximumObservedSpeed * 3.6d;
    }

    /**
     * Quanto tempo a simulação vai rodar desde que começou até o momento de parar.
     *
     * @return tempo total em segundos.
     */
    public long getSimulationTotalDuration() {
        return mScenario.getSimulationDuration();
    }

    /**
     * Pega o id do veículo que está SENDO SEGUIDO pelo veículo de id {@code vehicleId}.
     *
     * @return id do veículo sendo seguido ou -1 se ele não existe.
     */
    public long getVehiclesFrontVehicleId(long vehicleId) {
        long frontId = -1;
        Vehicle me = this.getVehicleById(getActiveVehicles(), vehicleId);
        if (me != null) {
            for (Vehicle vehicle : getActiveVehicles()) {
                if (me.getFrontVehicle() == vehicle) {
                    frontId = vehicle.getId();
                    break;
                }
            }
        }
        return frontId;
    }

    /**
     * Pega o(s) veículo(s) que está(ão) SEGUINDO o veículo de id {@code vehicleId}.
     * Pode haver mais de um veículo pois em interseções, onde fluxos se encontram,
     * podem haver dois fluxos que estão convergindo numa saída. Neste caso, é pos-
     * sível que num determinado momento haja dois ou mais veículos, vindos de vias
     * distintas, que estão seguindo o mesmo veículo à frente.
     *
     * @return array com ids dos veículos seguindo ou -1 se não há nenhum.
     */
    public ArrayList<Long> getVehiclesBackVehicleIds(long vehicleId) {
        ArrayList<Long> following = new ArrayList<>();
        Vehicle me = this.getVehicleById(getActiveVehicles(), vehicleId);
        if (me != null) {
            for (Vehicle vehicle : getActiveVehicles()) {
                if (vehicle.getFrontVehicle() == me) {
                    following.add(vehicle.getId());
                }
            }
        }
        return following;
    }

    /**
     * @return true se veículo de id {@code selectedVehicleId} ainda está no mapa.
     */
    public boolean isVehicleStillOnTheMap(long selectedVehicleId) {
        return (this.getVehicleById(getActiveVehicles(), selectedVehicleId) != null);
    }

    public void setSimulationDuration(long duration) {
        this.mScenario.setSimulationDuration(duration);
    }

    /**
     * Seta a temporização de todas as fases de todas as interseções sinalizadas
     * do cenário.
     */
    public void setGlobalControllerPhaseTiming(long greenTime, long amberTime, long clearanceTime) {
        for (Intersection i : this.mScenario.getIntersections()) {
            if (i instanceof SignalizedIntersection) {
                SignalizedIntersection si = (SignalizedIntersection) i;
                si.setPhaseTiming(greenTime, amberTime, clearanceTime);
            }
        }
    }

    public void setGlobalSpeedLimit(double speedLimitInKmPerHour) {
        this.mScenario.setGlobalSpeedLimit(speedLimitInKmPerHour);
    }

    /**
     * Método marretado para setar o tempo de intervalo entre
     * mudanças de reversibilidade nos coordenadores SMER.
     *
     * @param changePeriod período em número de fases de controlador
     */
    // TODO A Engine não deveria conhecer os componentes SMER diretamente! Foi feito às pressas por conta do deadline do mestrado.
    public void setGlobalSMERCoordinationReversibilityChangePeriod(int changePeriod) {
        /*
         * Corredores.
         */
        for (SMERCoordinator coordinator : this.mScenario.getCoordinators()) {
            coordinator.setReversibilityChangePeriod(changePeriod);
        }
    }

    /**
     * Método marretado para setar o tempo de intervalo entre
     * mudanças de reversibilidade nos controladores SMER.
     *
     * @param changePeriod período em minutos
     */
    // TODO A Engine não deveria conhecer os componentes SMER diretamente! Foi feito às pressas por conta do deadline do mestrado.
    public void setGlobalSMERControlReversibilityChangePeriod(int changePeriod) {
        /*
         * Interseções cujo controlador for do tipo SMERSignalController.
         */
        for (Intersection i : this.mScenario.getIntersections()) {
            if (i instanceof SignalizedIntersection) {
                SignalizedIntersection si = (SignalizedIntersection) i;
                // as intersecoes cujo controller for do tipo SMERCoordinatedSignalController ja vao ser setadas via setGlobalSMERCoordinationReversibilityChangePeriod()
                if (si.getSignalController() instanceof SMERSignalController) {
                    SMERSignalController ssi = (SMERSignalController) si.getSignalController();
                    ssi.setReversibilityChangePeriod(changePeriod);
                }
            }
        }
    }

    public void setCoordinatedStartupLostTime(double startupLostTime) {
        for (SMERCoordinator coordinator : this.mScenario.getCoordinators()) {
            coordinator.setCoordinatedStartupLostTime(startupLostTime);
        }
    }

    public void setCoordinatedGreenWaveSpeedFactor(double speedFactor) {
        for (SMERCoordinator coordinator : this.mScenario.getCoordinators()) {
            coordinator.setCoordinatedGreenWaveSpeedFactor(speedFactor);
        }
    }
}
