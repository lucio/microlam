package br.ufrj.lam.microlam.core;

/**
 * Evento gerado por {@link Vehicle} para informar seus {@link VehicleListener}s de algum ocorrido.
 *
 * @author Lucio Paiva
 */
public class VehicleEvent {
    private EventKind eventKind;
    private Vehicle vehicle;
    private VehicleContainer oldContainer, newContainer;

    public enum EventKind {
        vehicleAdvancedInRoute
    }

    VehicleEvent(EventKind event, Object... params) {
        eventKind = event;
        switch (event) {
            case vehicleAdvancedInRoute:
                if ((params.length == 3) &&
                        (params[0] instanceof Vehicle) &&
                        (params[1] instanceof VehicleContainer || params[1] == null) &&
                        (params[2] instanceof VehicleContainer || params[2] == null)) {
                    vehicle = (Vehicle) params[0];
                    oldContainer = (VehicleContainer) params[1];
                    newContainer = (VehicleContainer) params[2];
                }
                break;
        }
    }

    VehicleContainer getOldContainer() {
        return oldContainer;
    }

    VehicleContainer getNewContainer() {
        return newContainer;
    }

    EventKind getEventKind() {
        return eventKind;
    }

    protected Vehicle getVehicle() {
        return vehicle;
    }
}
