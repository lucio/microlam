package br.ufrj.lam.microlam.core;

public interface VehicleContainerListener {

    void vehicleContainerEvent(VehicleContainerEvent vehicleContainerEvent);
}
