package br.ufrj.lam.microlam.core;

import java.awt.Color;
import java.awt.geom.Point2D;

/**
 * GreenWaveSegment
 * <p>
 * Representação de uma onda verde que viaja em um determinado segmento de via.
 * O segmento de via é um objeto Road que liga dois objetos SignalizedIntersection.
 * As interseções precisam ser do tipo SignalizedIntersection por definição, já que
 * só faz sentido pensar em ondas verdes em vias cujas interseções são sinalizadas.
 *
 * @author lucio
 * @since 22-abr-2012
 */
public class GreenWaveSegment implements GreenWaveSegmentDrawingInfo {
    /**
     * Via por onde este segmento de onda verde vai viajar.
     */
    private Road road;
    /**
     * Velocidade de viagem da onda verde, em metros por segundo.
     * A velocidade é a mesma para a cabeça e a cauda, logo o comprimento
     * da onda se mantém o mesmo ao longo da viagem.
     * Neste modelo a velocidade da onda é sempre constante.
     */
    private double velocity; // m/s
    /**
     * Posição da cabeça (a.k.a. "crista") da onda em relação ao
     * início da via (ou seja, em relação ao ponto inicial em rela-
     * ção à interseção upstream).
     */
    private double headPosition; // metros percorridos pela cabeça da onda
    private double tailPosition;

    private boolean reachedEnd;

    private Color color;

    /**
     * Construtor.
     * <p>
     * A onda será criada na via {@code road} viajando com velocidade constante {@code velocity}.
     * O comprimento da onda verde será o menor dentre os parâmetros downstreamBandwidth e upstreamBandwidth.
     * O parâmetro {@code upstreamIntersectionLength} é usado para atrasar o início da onda verde compensando
     * pelo fato de que o veículo que está cruzando o sinal nesse momento veria a onda começar já no início da
     * via downstream (ela não teve que fazer a travessia do cruzamento - já começou adiantada), fora do
     * alcance dele.
     *
     * @param road                       via por onde a onda verde vai viajar
     * @param velocity                   velocidade de cruzeiro
     * @param upstreamBandwidth          banda da interseçao upstream
     * @param downstreamBandwidth        banda da interseçao downstream
     * @param upstreamIntersectionLength comprimento da travessia do cruzamento upstream
     */
    GreenWaveSegment(Road road, double velocity, double upstreamBandwidth, double downstreamBandwidth, double upstreamIntersectionLength, Color color) {
        /*
         * A largura de banda de uma onda é afetada pelo startup lost time,
         * que é uma medida conhecida de engenheria de tráfego representada
         * pelo tempo que leva para os veículos que estavam parados no cru-
         * zamento possam vencer a inércia e ganhar velocidade de cruzeiro.
         * Ela é tradicionalmente tem o valor de 2 segundos, valor este re-
         * tirado como média em vários países por diversas décadas, até hoje
         * se mostrando válido.
         *
         * O valor está zero aqui atualmente porque eu já estou levando em
         * conta esse tempo na hora de gerar os offsets de cruzamentos coor-
         * denados.
         */
        final int STARTUP_LOST_TIME = 0;

        this.road = road;
        this.velocity = velocity;

        this.headPosition = -upstreamIntersectionLength - (this.velocity * STARTUP_LOST_TIME);
        this.reachedEnd = false;

        /*
         * O comprimento da onda é ditado pelo menor tempo de verde entre os dois
         * semáforos (upstream e downstream), ou seja, a menor das bandas é que
         * dita a vazão.
         */
        /*
         * Comprimento da onda. Ele foi chamado de "max" pois no início
         * e no fim do deslocamento da onda ela possui um comprimento
         * menor.
         * <p>
         * O------------------O
         * O=-----------------O
         * O==----------------O
         * O===---------------O
         * O====--------------O
         * O-====-------------O
         * .  .  .
         * O-------------====-O
         * O--------------====O
         * O---------------===O
         * O----------------==O
         * O-----------------=O
         * O------------------O
         */
        double maxLength = this.velocity * Math.min(upstreamBandwidth, downstreamBandwidth) - (this.velocity * STARTUP_LOST_TIME);
        this.tailPosition = -maxLength - upstreamIntersectionLength;

        this.color = color;
    }

    /**
     * Atualiza a posição da cabeça da onda.
     *
     * @param dt Tempo decorrido desde o último step, em milissegundos.
     */
    public void step(double dt) {
        double ds = this.velocity * (dt / 1000d);

        if (this.tailPosition > this.road.getLength()) {
            this.reachedEnd = true;
        } else {
            this.headPosition += ds;
            this.tailPosition += ds;
        }
    }

    public Point2D.Double getHeadPosition() {
        return this.road.convertRoadPositionToCartesianPosition(getAbsoluteHeadPosition());
    }

    public Point2D.Double getTailPosition() {
        return this.road.convertRoadPositionToCartesianPosition(getAbsoluteTailPosition());
    }

    public double getAbsoluteHeadPosition() {
        return Math.max(0, Math.min(this.headPosition, this.road.getLength()));
    }

    double getAbsoluteTailPosition() {
        // o legal seria que o rabo da onda começasse em -this.upstreamIntersectionLength,
        // porém isso é dentro da interseção e dependendo da geometria, o retangulo pode
        // ficar desencaixado do cruzamento e ficar feio. sendo assim, estou optando por
        // deixar a onda invisivel enquanto ela estiver atravessando a interseção:
        return Math.max(this.tailPosition, 0);
    }

    public double getVelocity() {
        return this.velocity;
    }

    public double getRoadWidth() {
        return this.road.getRoadWidth();
    }

    public double getAngle() {
        return this.road.getAngle();
    }

    boolean reachedEnd() {
        return this.reachedEnd;
    }

    @Override
    public Color getColor() {
        return this.color;
    }
}
