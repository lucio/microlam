package br.ufrj.lam.microlam.core;

import java.util.Random;

public final class Rand {

    private static Random rnd;//  = new Random();
    private static long seed;

    static {
        rnd = new Random();
    }

    /**
     * Reinicia o gerador de numeros aleatorios com uma nova seed.
     * Caso o valor de seed seja null, reinicia com um seed aleatório.
     *
     * @param seed nova seed para o gerador
     */
    public static void setSeed(Long seed) {
        if (seed == null) {
            // caso seed não tenha sido especificada, usa o próprio
            // Random pra gerar uma seed, grava essa seed e realimenta
            // ele com a seed.
            // Essa solução foi encontrada em:
            // http://stackoverflow.com/questions/6001368/how-do-i-get-the-seed-from-a-random-in-java
            // respondido pelo usuário "smci".
            rnd = new Random();
            Rand.seed = rnd.nextLong();
        } else
            Rand.seed = seed;

        rnd.setSeed(Rand.seed);
    }

    public static long getSeed() {
        return Rand.seed;
    }

    private static double next() {
        return rnd.nextDouble();
    }

    public static double uniform() {
        return next();
    }

    public static long uniformLong() {
        return (long) (next() * ((double) Long.MAX_VALUE));
    }

    /**
     * Função usada para obter intervalos entre chegadas de veículos. Assumindo
     * que os intervalos entre as chegadas são memoryless, temos que a variável
     * aleatória "intervalo entre 2 chegadas de veículo" segue a distribuição
     * exponencial.
     * <p>
     * Gera uma variável aleatória com distribuição exponencial a partir da
     * variável aleatória com distribuição uniforme U gerada pela API do
     * Java.
     * <p>
     * A função -Math.log(U)/lambda é obtida através do método da transformada
     * inversa.
     * <p>
     * Referências:
     * http://en.wikipedia.org/wiki/Exponential_distribution
     * http://en.wikipedia.org/wiki/Uniform_distribution_%28continuous%29
     * http://en.wikipedia.org/wiki/Inverse_transform_sampling
     *
     * @param lambda Taxa de chegada de veículos, em veículos/segundo
     * @return Tempo em segundos até a próxima chegada
     */
    public static double exponential(double lambda) {
        double U = next();
        return -Math.log(U) / lambda;
    }
}
