package br.ufrj.lam.microlam.core;

import java.util.List;

public interface RouteDrawingInfo {

    List<RoadDrawingInfo> getRoadsBehind();

    List<RoadDrawingInfo> getRoadsAhead();
}
