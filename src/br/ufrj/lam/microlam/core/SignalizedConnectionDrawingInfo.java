package br.ufrj.lam.microlam.core;

public class SignalizedConnectionDrawingInfo {

    private RoadConnection connection;
    private SignalizedIntersection.Light light;

    SignalizedConnectionDrawingInfo(RoadConnection connection, SignalizedIntersection.Light light) {
        this.connection = connection;
        this.light = light;
    }

    public RoadDrawingInfo getIncomingRoadInfo() {
        return connection.getIncomingRoadPosition();
    }

    public RoadDrawingInfo getOutgoingRoadInfo() {
        return connection.getOutgoingRoadPosition();
    }

    public SignalizedIntersection.Light getLight() {
        return light;
    }

    public Integer getId() {
        return connection.getId();
    }
}
