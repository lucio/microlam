package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import br.ufrj.lam.microlam.detector.VehiclePointDetectorDrawingInfo;
import br.ufrj.lam.microlam.exceptions.BadLaneCountException;
import br.ufrj.lam.microlam.signalcontrol.VehicleDetector;

/**
 * <p>Classe base para representação das vias de tráfego de veículos na simulação.
 * Cada ponta dessa via, por definição da arquitetura do simulador, deve se ligar
 * a um objeto da classe Intersection.</p>
 *
 * <p>A via é representada espacialmente por 2 pontos num plano cartesiano.
 * Assim que estes pontos são definidos, a classe calcula o comprimento da via. Fica
 * definido que o quilômetro zero está no ponto 'upstream'. Assim, quando um carro for inserido
 * na altura de 100m, por exemplo, serão 100m a contar de 'upstream'.
 * UPSTREAM: ponto de onde o fluxo nesta via está se originando.
 * DOWNSTREAM: ponto para onde o fluxo está se dirigindo.
 * </p>
 *
 * <p>As faixas desta via (lanes) são numeradas da direita para a esquerda (mais lenta para
 * a mais rápida nos sistemas viários reais), começando pelo índice 0. Assim, a pista mais
 * à direita tem sempre índice 0.
 * </p>
 *
 * <p>Via de duas mãos: originalmente esta classe foi modelada como podendo ter duas mãos;
 * porém, ao longo da implementação percebi que a complexidade estava sendo aumentada sem
 * necessidade. Resolvi então quebrar a classe Road em objetos de apenas uma mão, já que as
 * duas mãos nunca se comunicam e não há necessidade de interação direta; além disso, nem
 * todas as vias possuem 2 mãos, então criar vias de 1 mão estava mais complexo do que devia.
 * Assim, atualmente, para criar vias de 2 mãos basta definir duas vias: uma que parte de A e
 * vai para B e outra que parte de B e vai para A.
 * Do ponto de vista do sistema como um grafo, é como se agora as vias fossem arestas dire-
 * cionadas.
 *
 * @author Lucio Paiva
 * @since Mar-08-2011
 */
public class Road extends VehicleContainer implements RoadDrawingInfo, MapEntity {

    private static final double JAM_DENSITY_PARAMETER = 0.7d;
    private static final double LANE_WIDTH = 4; // metros

    private int laneCount;
    private double laneWidth;

    private double length;
    private Intersection upstreamIntersection, downstreamIntersection;
    private Point2D.Double upstreamIndent, downstreamIndent;
    private Double angle; // ângulo de inclinação da via em relação ao plano cartesiano

    private ArrayList<GreenWaveSegment> greenWaveSegments;
    private ArrayList<VehicleDetector> vehicleDetectors;

    private boolean isCoordinated;

    public Road(int id, Intersection upstreamIntersection, Intersection downstreamIntersection, int laneCount) {
        super(id);
        this.name = String.format("Road %d", this.id);

        if (laneCount < 0) {
            throw new BadLaneCountException("Numero de faixas nao pode ser negativo.");
        }

        this.laneCount = laneCount;
        this.laneWidth = LANE_WIDTH;
        this.upstreamIntersection = upstreamIntersection;
        this.downstreamIntersection = downstreamIntersection;
        this.upstreamIndent = null;
        this.downstreamIndent = null;
        this.angle = null;
        this.greenWaveSegments = new ArrayList<>();
        this.vehicleDetectors = new ArrayList<>();

        if (upstreamIntersection instanceof InflowIntersection) {
            InflowIntersection inflow = (InflowIntersection) upstreamIntersection;
            inflow.addRoadConnection(this);
        }

        recalculateLength();
    }

    /**
     * Cria vias com uma única faixa
     **/
    public Road(int id, Intersection upstreamIntersection, Intersection downstreamIntersection) {
        this(id, upstreamIntersection, downstreamIntersection, 1);
    }

    /**
     * Atualiza coisas relacionadas à via.
     *
     * @param dt tempo decorrido desde o último step da engine, em milissegundos.
     */
    public void step(double dt) {
        /*
         * Invalida qualquer cronometragem de headway em andamento caso
         * a interseção downstream esteja fechada. Com isso, eliminamos
         * os reports de headway irreais por conta de um veículo que parou
         * em sinal vermelho e só quando estiver verde vai conseguir
         * atravessar.
         */
        if (this.getDownstreamIntersection() instanceof SignalizedIntersection) {
            SignalizedIntersection intersection = (SignalizedIntersection) this.getDownstreamIntersection();
            if (!intersection.getSignalController().isGreen(this))
                this.resetHeadwayTime();
        } else if (this.getDownstreamIntersection() instanceof OutflowIntersection) {
            /*
             * Não gera estatística de headway para vias de saída.
             */
            this.resetHeadwayTime();
        }

        /*
         * Atualiza segmentos de onda verde que estejam viajando pela via.
         */
        for (GreenWaveSegment gws : new ArrayList<>(this.greenWaveSegments)) {
            gws.step(dt);
            if (gws.reachedEnd())
                this.greenWaveSegments.remove(gws);
        }

        /*
         * Atualiza sensores.
         */
        for (VehicleDetector detector : this.vehicleDetectors) {
            detector.step(dt);
        }
    }

    /**
     * Calcula o comprimento da via. Caso ela tenha sido indentada por uma interseção,
     * o cálculo vai levar isso em conta.
     */
    private void recalculateLength() {
        double x, y;

        x = getDownstreamPosition().getX() - getUpstreamPosition().getX();
        y = getDownstreamPosition().getY() - getUpstreamPosition().getY();

        length = Math.sqrt(x * x + y * y);
    }

    /**
     * Determina qual veículo está à frente de "me", considerando a mesma faixa.
     * O algoritmo atual é pouco eficiente porque busca numa lista desordenada.
     * TODO após evoluir o método addVehicle(), refatorar essa rotina aqui!
     *
     * @param me Veículo que quer saber qual é o outro veículo que está à frente.
     * @return Veículo que está à frente de "me", ou null caso não haja ninguém.
     */
    public Vehicle getFrontVehicle(Vehicle me) {
        Vehicle frontVehicle = null;

        for (Vehicle cur : vehicles) {
            // ignora se está atrás de mim:
            if (cur.getCurrentPosition() <= me.getCurrentPosition())
                continue;

            // ignora se não está na mesma faixa que eu:
            if (cur.getCurrentLane() != me.getCurrentLane())
                continue;

            // se já havia encontrado outro anteriormente:
            if (frontVehicle != null) {
                // se está mais próximo que o último veículo encontrado:
                if (cur.getCurrentPosition() < frontVehicle.getCurrentPosition())
                    frontVehicle = cur;
            } else {
                frontVehicle = cur;
            }
        }

        return frontVehicle;
    }

    /**
     * Este método é chamado por um veículo para saber se ele está dentro de uma
     * onda verde nesta Road. Em caso positivo, retorna o GreenWaveSegment; caso
     * contrário, retorna null.
     * <p>
     * 27/Ago/2012 - adicionada lógica para levar em conta que o veículo pode estar
     * dentro de mais de uma onda verde ao mesmo tempo. Pode acontecer se o ciclo
     * do sinal downstream for muito longo comparado com o ciclo do upstream, pois
     * neste caso o upstream enviará diversas ondas verdes com temp de chegada igual,
     * e elas fatalmente irão se tocar no meio do caminho. Quando este caso acontecer,
     * a via vai retornar para o veículo a onda que esteja viajando mais rápido.
     *
     * @param position Posição na via que queremos saber se está dentro de uma onda verde.
     * @return Em caso positivo, retorna o GreenWaveSegment; caso
     * contrário, retorna null.
     */
    GreenWaveSegment getContainingGreenWave(double position) {
        GreenWaveSegment bestWave = null;
        for (GreenWaveSegment gws : this.greenWaveSegments) {
            if (gws.getAbsoluteHeadPosition() > position && // Cabeça deve estar à frente...
                    gws.getAbsoluteTailPosition() < position)   // ...e cauda atrás.
                if (bestWave == null || gws.getVelocity() > bestWave.getVelocity())
                    bestWave = gws;
        }
        return bestWave;
    }

    public Intersection getDownstreamIntersection() {
        return downstreamIntersection;
    }

    public Intersection getUpstreamIntersection() {
        return upstreamIntersection;
    }

    Intersection getOtherEnd(Intersection thisend) {
        if (thisend == upstreamIntersection) {
            return downstreamIntersection;
        } else if (thisend == downstreamIntersection) {
            return upstreamIntersection;
        } else {
            return null;
        }
    }

    public double getLength() {
        return length;
    }

    /**
     * Faz o que o nome diz. É usada por veículos que estão em outras vias e
     * passarão por esta via em algum momento de sua rota. Chamam esta função
     * para observar se há algum veículo à frente.
     *
     * @return Veículo mais atrasado (mais "upstream") que está nessa via; null caso não haja veículos
     */
    @Override
    public Vehicle getMostUpstreamVehicle(Vehicle vehicle) {
        return getMostUpstreamVehicle();
    }

    Vehicle getMostUpstreamVehicle() {
        // TODO:LANE filtrar veículos de acordo com a faixa 'laneNumber'

        Vehicle mostUpstreamVehicle = null;

        if (vehicles.size() > 0) {
            mostUpstreamVehicle = vehicles.firstElement();
            for (int i = 1; i < vehicles.size(); i++) {
                Vehicle cur = vehicles.get(i);
                if (cur.getCurrentPosition() < mostUpstreamVehicle.getCurrentPosition())
                    mostUpstreamVehicle = cur;
            }
        }

        return mostUpstreamVehicle;
    }

    /**
     * Implementa um algoritmo para determinar se a via está congestionada.
     * Este método é usado para saber se um carro que está tentando entrar na via e não conseguiu, não
     * obteve sucesso porque a via está engarrafada (o outro motivo seria porque um carro acabou de
     * entrar, mas não há tráfego à frente dele).
     *
     * @return true se a via está engarrafada.
     */
    // TODO:LANE levar em conta o caso de ter múltiplas faixas
    boolean isCongested() {
        // Verifico o quanto do comprimento da via está ocupado por veículos. Se esse percentual igual ou maior que JAM_DENSITY_PARAMETER, a via é considerada congestionada.
        // TODO validar essa constante. Verificar se este é realmente o melhor método de saber se uma rua está congestionada... o valor foi gerado empiricamente.
        return (this.getDensity()) >= JAM_DENSITY_PARAMETER;
    }

    public int getVehicleCount() {
        return vehicles.size();
    }

    @Override
    public Point2D.Double getUpstreamPosition() {
        return upstreamIndent == null ? upstreamIntersection.getPosition() : upstreamIndent;
    }

    @Override
    public Point2D.Double getDownstreamPosition() {
        return downstreamIndent == null ? downstreamIntersection.getPosition() : downstreamIndent;
    }

    Point2D.Double getOriginalDownstreamPosition() {
        return downstreamIntersection.getPosition();
    }

    public Point2D.Double getOriginalUpstreamPosition() {
        return upstreamIntersection.getPosition();
    }

    @Override
    public String getIdStr() {
        return "R" + Long.toString(id);
    }

    /**
     * Calcula a densidade atual da via.
     * <p>
     * O cálculo é feito somando-se o comprimento de todos os carros e dividindo pelo (tamanho da via * o número de faixas).
     * Conforme o retorno se aproxima de 1, temos a indicação de que a via está congestionando.
     */
    double getDensity() {
        double totalUtilizedSpace = 0.d;

        for (Vehicle v : vehicles) {
            totalUtilizedSpace += v.getLength() + v.getMinimumDistanceToFrontVehicle();
        }
        return totalUtilizedSpace / this.getLength();
    }

    @Override
    public double getRoadWidth() {
        return laneWidth * laneCount;
    }

    public Point2D.Double convertRoadPositionToCartesianPosition(double roadPosition) {
        Point2D.Double p1 = this.getUpstreamPosition();
        Point2D.Double p2 = this.getDownstreamPosition();

        double r = roadPosition / this.getLength();
        double x = r * (p2.getX() - p1.getX()) + p1.getX();
        double y = r * (p2.getY() - p1.getY()) + p1.getY();

        return new Point2D.Double(x, y);
    }

    @Override
    public Point2D.Double getVehiclePosition(Vehicle vehicle) {
        return this.convertRoadPositionToCartesianPosition(vehicle.getCurrentPosition());
    }

    void indentDownstream() {
        downstreamIndent = downstreamIndent == null ? calculateIndentationPoint(getDownstreamPosition(), getUpstreamPosition()) : downstreamIndent;
        recalculateLength();
    }

    void indentUpstream() {
        upstreamIndent = upstreamIndent == null ? calculateIndentationPoint(getUpstreamPosition(), getDownstreamPosition()) : upstreamIndent;
        recalculateLength();
    }

    /**
     * Funciona com lazy initialization. Só calcula angle quando alguém pedir.
     * Isso evita calcular angle sem necessidade durante a instaciação do objeto.
     * Além disso, uma vez calculado, ele nunca mais refaz o cálculo.
     */
    private void calculateMyAngle() {
        if (angle == null) {
            double dx = downstreamIntersection.getX();
            double dy = downstreamIntersection.getY();
            double ux = upstreamIntersection.getX();
            double uy = upstreamIntersection.getY();

            if ((dx - ux != 0) && (dy - uy != 0))
                angle = Math.atan2((dy - uy), (dx - ux));
            else if (dy - uy == 0) {
                if (ux < dx)
                    angle = Math.toRadians(0);
                else
                    angle = Math.toRadians(180);
            } else {
                if (uy < dy)
                    angle = Math.toRadians(90);
                else
                    angle = Math.toRadians(270);
            }
        }
    }

    /**
     * Calcula um ponto com um recuo de valor "indentation" em relação ao ponto (x2,y2).
     * Essa rotina é usada para liberar um espaço na ponta da via que vai se ligar a um
     * cruzamento, permitindo assim com que esse cruzamento possa ter espaço físico.
     * Escrevi o cálculo baseado numa fórmula para encontrar o ponto médio num
     * segmento de reta:
     * <p>
     * x = (x1 + x2) / 2
     * y = (y1 + y2) / 2
     * <p>
     * Porém, em vez do ponto médio, eu quero um ponto mais próximo do ponto (x2,y2);
     * no caso, com uma distância específica de "indentation". Para isso eu passo
     * a pensar no cálculo do ponto médio como tendo pesos para cada extremo:
     * <p>
     * x = 0.5x1 + 0.5x2
     * y = 0.5y1 + 0.5y2
     * <p>
     * E regular os pesos de acordo com a proximidade do ponto:
     * <p>
     * x = (w)*x1 + (1-w)*x2
     * x = (w)*y1 + (1-w)*y2
     */
    private Point2D.Double calculateIndentationPoint(Point2D.Double nearPoint, Point2D.Double farPoint) {
        double x1 = farPoint.x;
        double y1 = farPoint.y;
        double x2 = nearPoint.x;
        double y2 = nearPoint.y;

        double f = getIndentationLength() / getLength();
        double x = f * x1 + (1 - f) * x2;
        double y = f * y1 + (1 - f) * y2;

        return new Point2D.Double(x, y);
    }

    /**
     * Estipula um comprimento (em metros) que deve ser seguido ao calcular o recuo da via para encaixe numa interseção.
     */
    private double getIndentationLength() {
        return laneWidth * 1.5;
    }

    @Override
    public double getAngle() {
        calculateMyAngle();
        return angle;
    }

    boolean canContain(ArrayList<Vehicle> candidateVehicles) {
        // FIXME coloquei esse fator porque getMinimumDistanceToFrontVehicle() dá um comprimento maior do que realmente
        // os carros usam. O resultado disso é que totalAvailableSpace não reflete corretamente o espaço disponível na
        // via (sempre há mais espaço do que a variável diz). O fator abaixo reduz o valor para 3/4 do original, que
        // observei ser uma boa fração pra representar a distância real entre os carros parados. De qualquer jeito, isso
        // não deveria ser feito dessa maneira! Descobrir um jeito melhor.
        // 27/Ago/2012: Observei que se eu for mais conservador aqui (deixando o fator = 1 em vez de tentar torná-lo mais
        // eficiente) eu consigo acabar com o problema de que mesmo assim em situações de muito congestionamento os veículos
        // continuavam obstruindo interseções.
        final double MAGIC_DISTANCE_FACTOR = 1; //0.75;

        // começamos com todo o comprimento da via disponível...
        double totalAvailableSpace = this.getLength();

        // agora subtraímos o espaço ocupado pelos carros que estão nela atualmente (levando em conta a distância mínima que eles desejam manter do veículo da frente):
        for (Vehicle v : vehicles) {
            totalAvailableSpace -= v.getLength() + MAGIC_DISTANCE_FACTOR * v.getMinimumDistanceToFrontVehicle();
        }

        // finalmente, subtraímos o espaço a ser ocupado pelos veículos candidatos a entrar na via:
        for (Vehicle v : candidateVehicles) {
            totalAvailableSpace -= v.getLength() + MAGIC_DISTANCE_FACTOR * v.getMinimumDistanceToFrontVehicle();
        }
        // se totalAvailableSpace não for positivo, a via não suporta os veículos candidatos:
        return (totalAvailableSpace > 0);
    }

    ArrayList<GreenWaveSegment> getGreenWaveSegments() {
        return this.greenWaveSegments;
    }

    void addGreenWaveSegment(GreenWaveSegment gws) {
        if (!this.greenWaveSegments.contains(gws))
            this.greenWaveSegments.add(gws);
    }

    public void addVehicleDetector(VehicleDetector detector) {
        if (!this.vehicleDetectors.contains(detector))
            this.vehicleDetectors.add(detector);
    }

    @Override
    public ArrayList<VehiclePointDetectorDrawingInfo> getVehiclePointDetectorsDrawingInfo() {
        return new ArrayList<>(this.vehicleDetectors);
    }

    @Override
    public ArrayList<VehicleDrawingInfo> getVehiclesDrawingInfo() {
        return new ArrayList<>(this.vehicles);
    }

    public void setIsCoordinated(boolean isCoordinated) {
        this.isCoordinated = isCoordinated;
    }

    boolean isCoordinated() {
        return this.isCoordinated;
    }
}
