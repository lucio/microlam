package br.ufrj.lam.microlam.core;

/**
 * As classes que implementam esta interface devem ser capazes de,
 * dado o tempo atual de simulação, dizer qual deve ser a taxa de
 * entrada de veículos no momento.
 *
 * @author lucio
 * @since 07-apr-2012
 */
public interface VehicleInflowRateFunction {
    /**
     * @param executionTime tempo decorrido de simulação até agora.
     * @return taxa de veículos POR SEGUNDO.
     */
    double getRate(double executionTime);

    double getRateInVehiclesPerHour();
}
