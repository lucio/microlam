package br.ufrj.lam.microlam.core;

import java.util.ArrayList;

/**
 * Política programável que permite configurar uma ordem
 * recorrente com os tipos de veículos que devem ser criados.
 *
 * @author Daniel
 */
public class ProgrammableVehicleGenerationPolicy extends VehicleGenerationPolicy {
    // Tipos de veículos que serão criados em ordem
    private ArrayList<Class<? extends Vehicle>> generationOrder = new ArrayList<>();

    // Índice do próximo tipo de veículo que deve ser criado
    private int nextVehicleTypeIndex;

    /**
     * Construtor.
     *
     * @param className Nome da classe do tipo de veículo que entrará na fila de criação.
     */
    public ProgrammableVehicleGenerationPolicy(String className) throws ClassNotFoundException {
        generationOrder.add(Class.forName(className).asSubclass(Vehicle.class));
        nextVehicleTypeIndex = 0;
    }

    /**
     * Construtor.
     *
     * @param className Nome da classe do tipo de veículo que entrará na fila de criação.
     * @param quantity  Quantidade de vezes que este tipo entrará na fila.
     */
    public ProgrammableVehicleGenerationPolicy(String className, int quantity) throws ClassNotFoundException {
        Class<? extends Vehicle> clazz = Class.forName(className).asSubclass(Vehicle.class);

        for (int i = 0; i < quantity; i++) {
            generationOrder.add(clazz);
        }

        nextVehicleTypeIndex = 0;
    }

    /**
     * Adiciona um novo tipo de veículo à fila da ordem de criação.
     */
    public void addVehicleType(String className) throws ClassNotFoundException {
        generationOrder.add(Class.forName(className).asSubclass(Vehicle.class));
    }

    /**
     * Adiciona um novo tipo de veículo à fila da ordem de criação uma determinada quantidade de vezes.
     *
     * @param quantity Quantidade de vezes que este tipo entrará na fila.
     */
    public void addVehicleType(String className, int quantity) throws ClassNotFoundException {
        Class<? extends Vehicle> clazz = Class.forName(className).asSubclass(Vehicle.class);

        for (int i = 0; i < quantity; i++) {
            generationOrder.add(clazz);
        }
    }

    /**
     * Determina a classe do próximo tipo de veículo que deve ser criado.
     */
    @Override
    public Class<? extends Vehicle> nextVehicleType() {
        if (nextVehicleTypeIndex >= generationOrder.size()) {
            nextVehicleTypeIndex = 0;
        }

        return generationOrder.get(nextVehicleTypeIndex++);
    }
}
