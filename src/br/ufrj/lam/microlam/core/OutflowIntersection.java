package br.ufrj.lam.microlam.core;

import java.awt.geom.Point2D;

public class OutflowIntersection extends Intersection {

    public OutflowIntersection(int id, Point2D.Double location) {
        super(id, location);
    }

    /**
     * A distância a ser percorrida por um veículo dentro da interseção de saída é sempre ZERO.
     *
     * @param vehicle Veículo que vai cruzar a interseção
     * @return Distância que ele vai percorrer dentro da interseção
     */
    @Override
    public double getLength(Vehicle vehicle) {
        return 0;
    }
}
