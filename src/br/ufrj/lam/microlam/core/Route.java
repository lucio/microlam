package br.ufrj.lam.microlam.core;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public abstract class Route implements RouteDrawingInfo {

    Queue<VehicleContainer> routeVisited;

    public abstract class RouteIterator {
        public abstract VehicleContainer next();

        public abstract boolean hasNext();
    }

    Route() {
        routeVisited = new LinkedList<>();
    }

    Route(Route source) {
        routeVisited = new LinkedList<>(source.routeVisited);
    }

    @Override
    public List<RoadDrawingInfo> getRoadsBehind() {
        List<RoadDrawingInfo> roads = new ArrayList<>();
        for (VehicleContainer vc : routeVisited) {
            if (vc instanceof Road)
                roads.add((Road) vc);
        }
        return roads;
    }

    public abstract Route copy();

    public abstract RouteIterator iterator();

    public abstract VehicleContainer getCurrentContainer();

    public abstract VehicleContainer getNextContainer();

    public abstract VehicleContainer getPreviousContainer();

    public abstract VehicleContainer moveToNextContainer();

    public abstract boolean hasEnded();

    public abstract RoadConnection getRoadConnectionInIntersectionAhead(Intersection intersection);

    public abstract VehicleContainer getContainerInRouteBefore(VehicleContainer container);

    public abstract VehicleContainer getContainerInRouteAfter(VehicleContainer container);
}
