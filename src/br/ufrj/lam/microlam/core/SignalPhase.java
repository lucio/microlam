package br.ufrj.lam.microlam.core;

import java.util.ArrayList;

/**
 * Classe que representa uma fase de um determinado cruzamento sinalizado.
 * Possui um vetor que guarda todas as conexões que estarão ativas (ou seja, com sinal verde) durante essa fase.
 * Aqui não serão especificados tempos de verde, amarelo e vermelho - isso é um trabalho do SignalController.
 * O SignalPhase apenas agrupa RoadConnections que estarão ativas num mesmo momento.
 **/
public class SignalPhase {

    /**
     * conexões que estarão ativas durante essa fase do semáforo
     */
    private ArrayList<RoadConnection> connections;

    /**
     * greenTime: tempo de verde efetivo
     * amberTime: tempo de amarelo
     * allRedTime: tempo do sinal que poderia estar sendo usado para o verde, mas é dedicado ao vermelho para
     * provocar um momento "all red", onde todos os sinais estão fechados. Isso é comum para permitir que
     * veículos que ainda estejam trafegando dentro da interseção possam escoar antes que o tráfego seja
     * aberto para a outra via.
     */
    public SignalPhase(RoadConnection... connections) {
        this.connections = new ArrayList<>();

        for (RoadConnection rc : connections) {
            this.addConnection(rc);
        }
    }

    public ArrayList<RoadConnection> getConnections() {
        return connections;
    }

    private RoadConnection getConnection(Road incomingRoad, Road outgoingRoad) {
        for (RoadConnection rc : connections) {
            if ((rc.getIncomingRoad() == incomingRoad) && (rc.getOutgoingRoad() == outgoingRoad))
                return rc;
        }
        return null;
    }

    public ArrayList<RoadConnection> getConnections(Road incomingRoad) {
        ArrayList<RoadConnection> result = new ArrayList<>();
        for (RoadConnection rc : connections) {
            if (rc.getIncomingRoad() == incomingRoad)
                result.add(rc);
        }
        return result;
    }

    public ArrayList<RoadConnection> removeConnections(Road incomingRoad) {
        ArrayList<RoadConnection> result = new ArrayList<>();
        ArrayList<RoadConnection> copyOfMyConnections = new ArrayList<>(connections);
        for (RoadConnection rc : copyOfMyConnections) {
            if (rc.getIncomingRoad() == incomingRoad) {
                this.connections.remove(rc);
                result.add(rc);
            }
        }
        return result;
    }

    private void addConnection(RoadConnection connection) {
        connections.add(connection);
    }

    public void addConnections(ArrayList<RoadConnection> connections) {
        this.connections.addAll(connections);
    }

    /**
     * verifica se uma determinada conexão de vias faz parte dessa fase
     **/
    public boolean hasConnection(Road incoming, Road outgoing) {
        if (outgoing == null)
            return hasConnectionFrom(incoming);
        else
            return this.getConnection(incoming, outgoing) != null;
    }

    /**
     * verifica se uma determinada via de entrada faz parte de alguma das RoadConnections desta fase
     **/
    private boolean hasConnectionFrom(Road incoming) {
        for (RoadConnection rc : connections) {
            if (rc.getIncomingRoad() == incoming)
                return true;
        }
        return false;
    }

    /**
     * verifica se uma determinada via de saída faz parte de alguma das RoadConnections desta fase
     **/
    public boolean hasConnectionTo(Road outgoing) {
        for (RoadConnection rc : connections) {
            if (rc.getOutgoingRoad() == outgoing)
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (RoadConnection connection : this.connections) {
            result.append(connection).append(", ");
        }
        return result.toString();
    }
}
