package br.ufrj.lam.microlam.report;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import br.ufrj.lam.microlam.core.Engine;
import br.ufrj.lam.microlam.core.Utils;

public class ReportManager {

    private static Engine engine;
    private static BufferedWriter logFile = null;
    private static BufferedWriter errFile = null;

    /**
     * Constutor forçado para private (escondido).
     */
    private ReportManager() {
    }

    public static void setupManager(Engine engine, String logFilename, String errFilename) {
        ReportManager.engine = engine;

        try {
            if (logFilename == null)
                logFile = null;
            else
                logFile = new BufferedWriter(new FileWriter(logFilename));
        } catch (IOException e1) {
            System.err.println("Não foi possível criar o arquivo '" + logFilename + "' para escrita.");
            System.exit(1);
        }

        try {
            if (errFilename == null)
                errFile = null;
            else
                errFile = new BufferedWriter(new FileWriter(errFilename));
        } catch (IOException e1) {
            System.err.println("Não foi possível criar o arquivo '" + errFilename + "' para escrita.");
            System.exit(1);
        }
    }

    public static void err(String message) {
        String logMessage = prepareMessage(message);
        if (errFile != null) {
            try {
                errFile.write(logMessage + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.err.println(logMessage);
        }
    }

    public static void log(String message) {
        String logMessage = prepareMessage(message);
        if (logFile != null) {
            try {
                logFile.write(logMessage + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println(logMessage);
        }
    }

    private static String prepareMessage(String message) {
        String timestamp;
        if (engine != null) {
            timestamp = Utils.getTimeStr((long) engine.getElapsedTimeInSeconds());
        } else {
            timestamp = "?";
        }
        return String.format("%s: %s", timestamp, message);
    }

    public static void logTitle(String message) {
        log("-------------------------" + message + "-------------------------");
    }

    public static void closeReport() {
        try {
            logFile.close();
            errFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logFile = null;
        errFile = null;
    }

}
