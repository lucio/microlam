package br.ufrj.lam.microlam.idm;

public class IDMTruck extends IDMVehicle {

    private static final double LENGTH = 12.0d;
    private static final double WIDTH = 2.1d;

    public IDMTruck(IDMTruck source) {
        super(source);
    }

    public IDMTruck(long id) {
        super(id);

        v0 = 80.0 / 3.6; // 120km/h para m/s
        a = 0.4;
        b = 4.0;
        s0 = 2.0;
        T = 1.7;
        length = LENGTH;
        width = WIDTH;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getWidth() {
        return width;
    }

}
