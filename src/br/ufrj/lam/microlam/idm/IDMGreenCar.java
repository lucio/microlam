package br.ufrj.lam.microlam.idm;

public class IDMGreenCar extends IDMGreenVehicle {

    // medidas retiradas de http://en.wikipedia.org/wiki/Fiat_Uno
    private static final double LENGTH = 3.7d;
    private static final double WIDTH = 1.6d;

    public IDMGreenCar(IDMGreenCar source) {
        super(source);

        v0 = source.v0;
        a = source.a;
        b = source.b;
        s0 = source.s0;
        T = source.T;
        length = source.length;
        width = source.width;
    }

    public IDMGreenCar(long id) {
        super(id);

        v0 = 60.0 / 3.6; // 60km/h para m/s
        a = 0.5;
        b = 3.0;
        s0 = 2.0;
        T = 1.5;
        length = LENGTH;
        width = WIDTH;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getWidth() {
        return width;
    }

}
