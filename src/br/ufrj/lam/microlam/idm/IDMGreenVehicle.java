package br.ufrj.lam.microlam.idm;

import java.awt.Color;

import br.ufrj.lam.microlam.core.GreenWaveSegment;
import br.ufrj.lam.microlam.core.PaceVehicle;
import br.ufrj.lam.microlam.core.Road;

/**
 * <p>IDM Green Vehicle</p>
 * <p>Variante "verde" do IDMVehicle, este veículo enxerga as ondas verdes
 * que circulam pelas vias e procura sempre se manter dentro de uma,
 * respeitando a velocidade ditada por elas.</p>
 *
 * @author Lucio Paiva
 * @see IDMVehicle
 * @since Aug-17-2012
 */
public abstract class IDMGreenVehicle extends IDMVehicle {

    IDMGreenVehicle(IDMVehicle source) {
        super(source);
    }

    IDMGreenVehicle(long id) {
        super(id);
    }

    @Override
    public void calculateAccelerationAndVelocity(double dt) {
        // FIXME Precisa de uma refatoração séria. O código deste método é uma cópia
        // do mesmo método em IDMVehicle, apenas adicionando a etapa de verificação
        // do brakingDueToGreenWave. Qualquer hora o código em IDMVehicle será modificado
        // e este ficará quebrado/desatualizado!!!

        double v = getCurrentVelocity();
        double brakingDeceleration = 0.0d;
        double brakingDueToFrontVehicle = 0d;
        double brakingDueToPaceVehicle = 0d;
        double brakingDueToGreenWave = 0d;

        // caso haja um veículo à frente, o termo brakingDeceleration será maior que zero:
        if (getFrontVehicle() != null) {
            brakingDueToFrontVehicle = calculateBreakingDeceleration(getFrontVehicle().getCurrentVelocity(), getDistanceToFrontVehicle());
        }

        // caso a via tenha limite de velocidade, calculamos o braking deceleration também para o "pacing car":
        PaceVehicle paceVehicle = this.checkForSpeedLimit();
        if (paceVehicle != null) {
            brakingDueToPaceVehicle = calculateBreakingDeceleration(paceVehicle.getCurrentVelocity(), getDistanceToPaceVehicle());
        }

        // O veículo verde deve averiguar se está dentro de uma onda verde e, em caso afirmativo, manter-se dentro dela:
        GreenWaveSegment gws = this.getContainingGreenWave();
        if (gws != null) {
            if (this.getCurrentContainer() instanceof Road) {
                Road road = (Road) this.getCurrentContainer();
                /*
                 * Caso a onda verde já tenha atingido o comprimento máximo da via,
                 * não queremos calcular a desaceleração pois isso vai fazer com que
                 * o veículo pare ao encontrar o cruzamento downstream (ele vai achar
                 * que a cabeça da onda verde é um carro parado).
                 */
                if (gws.getAbsoluteHeadPosition() != road.getLength()) // verdadeiro apenas se a cabeça da onda ainda não chegou no ponto máximo downstream
                {
                    double distanceToHead = gws.getAbsoluteHeadPosition() - this.getCurrentContainerPosition();
                    brakingDueToGreenWave = calculateBreakingDeceleration(gws.getVelocity(), distanceToHead);
                }
            }
        }

        // ao final dos cálculos, escolho o braking deceleration mais agressivo, que é
        // consequentemente o necessário para tratar o pior caso e ao mesmo tempo evitar
        // colisões, garantir respeito ao limite de velocidade e manter-se dentro da onda verde:
        if (!Double.isInfinite(brakingDueToPaceVehicle) ||
                !Double.isInfinite(brakingDueToFrontVehicle) ||
                !Double.isInfinite(brakingDueToGreenWave))
            brakingDeceleration = Math.max(Math.max(brakingDueToFrontVehicle, brakingDueToPaceVehicle), brakingDueToGreenWave);

        // calcula nova aceleração:
        double newAcceleration = a * (1 - Math.pow(v / v0, delta) - brakingDeceleration);
        setCurrentAcceleration(newAcceleration);
        // calcula nova velocidade:
        double deltaV = newAcceleration * (dt / 1000.0);
        double newVelocity = getCurrentVelocity() + deltaV;
        setCurrentVelocity(newVelocity);
        // evita que o carro comece a andar de ré - pode acontecer caso haja um veículo à frente, pois a equação de cálculo de aceleração vai continuar mandando acelerar negativamente mesmo depois do carro ter parado:
        if (newVelocity < 0) {
            setCurrentVelocity(0);
        }
    }

    @Override
    public Color getColor() {
        return Color.YELLOW;
    }
}
