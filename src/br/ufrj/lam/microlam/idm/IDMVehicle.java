package br.ufrj.lam.microlam.idm;

import java.awt.Color;

import br.ufrj.lam.microlam.core.PaceVehicle;
import br.ufrj.lam.microlam.core.Vehicle;

/**
 * <p>Intelligent Driver Model</p>
 * <p>Modelo do tipo car-following criado por Treiber et al. Usado para simular
 * o comportamento de um veículo no sistema.</p>
 *
 * @author Lucio Paiva
 * @see <a href="http://www.mtreiber.de/">http://www.mtreiber.de/</a>
 * @see <a href="http://www.traffic-simulation.de/">http://www.traffic-simulation.de/</a>
 * @see <a href="http://en.wikipedia.org/wiki/Intelligent_Driver_Model">http://en.wikipedia.org/wiki/Intelligent_Driver_Model</a>
 * @see <a href="http://www.vwi.tu-dresden.de/~treiber/MicroApplet/IDM.html">http://www.vwi.tu-dresden.de/~treiber/MicroApplet/IDM.html</a>
 * @since Mar-08-2011
 */
public abstract class IDMVehicle extends Vehicle {

    /*
     * Abaixo seguem os parâmetros usados pelo IDM na fórmula do cálculo da aceleração.
     * Todos os valores estão em metros e segundos, a menos que seja indicado o contrário.
     */

    /**
     * Velocidade que o motorista considera ideal caso não haja trânsito à sua
     * frente - não confundir v0 com velocidade inicial!
     **/
    double v0;
    /**
     * Headway desejado (em segundos) em relação ao veículo da frente.
     **/
    double T;
    /**
     * distância mínima que o motorista quer manter em relação ao veículo da frente
     **/
    double s0;
    /**
     * aceleração normal do dia-a-dia
     **/
    protected double a;
    protected double b; // parametro de desaceleracao

    /**
     * expoente de aceleração - parâmetro de calibração da fórmula
     **/
    double delta;
    /**
     * distancia atual em relacao ao carro da frente
     **/
    private double s;
    /**
     * sqrt(a*b) - valor pré-calculado
     **/
    private double sqrtab;

    protected double length;
    protected double width;

    IDMVehicle(long id) {
        super(id);

        v0 = 60.0 / 3.6; // 60km/h para m/s
        delta = 4.0;
        a = 0.5;
        b = 3.0;
        s0 = 2.0;
        T = 1.5;
        sqrtab = Math.sqrt(a * b);
    }

    IDMVehicle(IDMVehicle source) {
        super(source);

        this.v0 = source.v0;
        this.T = source.T;
        this.s0 = source.s0;
        this.a = source.a;
        this.b = source.b;
        this.delta = source.delta;
        this.s = source.s;
        this.sqrtab = source.sqrtab;
        this.length = source.length;
    }

    /**
     * O chamado "breaking term" da fórmula de aceleração do IDM.
     */
    double calculateBreakingDeceleration(double frontVehicleVelocity, double frontVehicleDistance) {
        s = frontVehicleDistance;
        double v = getCurrentVelocity();

        double deltaV = v - frontVehicleVelocity;
        double sStar = s0 + (v * T + (v * deltaV) / (2 * sqrtab));
        // nao deixa que sStar seja menor que s0, que é a distância mínima desejada em relação ao veículo da frente:
        if (sStar < s0) sStar = s0;

        return Math.pow(sStar / s, 2);
    }

    /**
     * Fórmula de aceleração do IDM.
     * Detalhes em http://www.vwi.tu-dresden.de/~treiber/MicroApplet/IDM.html
     */
    public void calculateAccelerationAndVelocity(double dt) {

        double v = getCurrentVelocity();
        double brakingDeceleration = 0.0d;
        double brakingDueToFrontVehicle = 0d; //Double.POSITIVE_INFINITY;
        double brakingDueToPaceVehicle = 0d; //Double.POSITIVE_INFINITY;

        // caso haja um veículo à frente, o termo brakingDeceleration será maior que zero:
        if (getFrontVehicle() != null) {
            brakingDueToFrontVehicle = calculateBreakingDeceleration(getFrontVehicle().getCurrentVelocity(), getDistanceToFrontVehicle());
        }

        // caso a via tenha limite de velocidade, calculamos o braking deceleration também para o "pacing car":
        PaceVehicle paceVehicle = this.checkForSpeedLimit();
        if (paceVehicle != null &&
                /*
                 * Passei a só calcular a desaceleração aqui quando a velocidade do
                 * veículo for efetivamente maior que o limite da via. Estava acontecendo
                 * um efeito indesejável por conta da fórmula do IDM que fazia com que ele
                 * nunca atingisse a velocidade limite da via e ficasse sempre abaixo dela.
                 */
                (getCurrentVelocity() > paceVehicle.getCurrentVelocity())) {
            brakingDueToPaceVehicle = calculateBreakingDeceleration(paceVehicle.getCurrentVelocity(), getDistanceToPaceVehicle());
        }

        // ao final dos cálculos, escolho o braking deceleration mais agressivo, que é
        // consequentemente o necessário para tratar o pior caso e ao mesmo tempo evitar
        // colisões e garantir respeito ao limite de velocidade:
        if (!Double.isInfinite(brakingDueToPaceVehicle) || !Double.isInfinite(brakingDueToFrontVehicle))
            brakingDeceleration = Math.max(brakingDueToFrontVehicle, brakingDueToPaceVehicle);

        // calcula nova aceleração:
        double newAcceleration = a * (1 - Math.pow(v / v0, delta) - brakingDeceleration);
        setCurrentAcceleration(newAcceleration);
        // calcula nova velocidade:
        double deltaV = newAcceleration * (dt / 1000.0);
        double newVelocity = getCurrentVelocity() + deltaV;
        setCurrentVelocity(newVelocity);
        // evita que o carro comece a andar de ré - pode acontecer caso haja um veículo à frente, pois a equação de
        // cálculo de aceleração vai continuar mandando acelerar negativamente mesmo depois do carro ter parado:
        if (newVelocity < 0) {
            setCurrentVelocity(0);
        }
    }

    // FIXME calcular a velocidade de equilibrio para entrada no sistema
    @Override
    public void calculateStartingVelocity() {
        this.setCurrentVelocity(30 / 3.6d); // 30km/h
    }

    @Override
    public double getMinimumDistanceToFrontVehicle() {
        return s0;
    }

    @Override
    public Color getColor() {
        return Color.BLUE;
    }
}
