package br.ufrj.lam.microlam;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.ufrj.lam.microlam.core.Engine;
import br.ufrj.lam.microlam.data.loader.XmlScenarioLoader;
import br.ufrj.lam.microlam.exceptions.ScenarioLoadException;
import br.ufrj.lam.microlam.exceptions.StepTooBigException;
import br.ufrj.lam.microlam.report.ReportManager;

/**
 * Programa principal do MicroLAM.
 * Passa parâmetros para a engine e a executa, e então mostra os resultados.
 * Há também o <a href="br.ufrj.lam.MicroLamApplet">MicroLamApplet</a> que permite visualizar uma animação da simulação.
 *
 * @author Lucio Paiva
 * @since Mar-08-2011
 */
public class MicroLAMApplication {

    private static Engine engine;
    private static final double INTERVAL_1_MIN = 1000d * 60 * 1;
    private static double intervalBetweenExtractions;
    /**
     * Quantos milissegundos cada iteração representa
     */
    private static final double FRAME_DURATION_MS = 1000d / 60d; // tempo que leva mostrando 1 frame (o inverso de fps), em ms

    private static BufferedWriter csv_output_file, csv_verbose_output_file;

    private static void usage() {
        System.out.println("Modo de Uso: java MicroLAMApplication.class <xml_arquivo_cenario> <interval> <duration> <green> <amber> <clearance> <speedlimit> <smercontrolrevchange> <smercoordrevchange> <startuplosttime> <greenwavespeedfactor> <seed> <suffix>");
        System.out.println("\tOpções:");
        System.out.println("\t\t<interval>\tIntervalo entre as extrações para o arquivo .VERBOSE.CSV (em minutos).");
        System.out.println("\t\t<duration>\tSobrescreve o valor da duração da simulação passado via XML (em minutos).");
        System.out.println("\t\t<green>\tTempo de verde dos sinais do cenário (em segundos).");
        System.out.println("\t\t<amber>\tTempo de amarelo dos sinais do cenário (em segundos).");
        System.out.println("\t\t<clearance>\tClearance time dos sinais do cenário (em segundos).");
        System.out.println("\t\t<speedlimit>\tLimite de velocidade padrão das vias (em km/h).");
        System.out.println("\t\t<smercontrolrevchange>\tIntervalo entre mudanças de reversibilidade dos controladores SMER (em minutos).");
        System.out.println("\t\t<smercoordrevchange>\tIntervalo entre mudanças de reversibilidade dos coordenadores SMER (em número de fases dos seus controladores).");
        System.out.println("\t\t<startuplosttime>\tTempo que leva para um pelotão parado ganhar headway constante (em segundos). Usado no SMERCoordinator para calculo dos offsets.");
        System.out.println("\t\t<greenwavespeedfactor>\tFator que dita a velocidade de uma onda verde do SMERCoordinator em relação ao limite de velocidade da via, variando no intervalo ]0..1].");
        System.out.println("\t\t<seed>\tValor inteiro usado para alimentar o gerador de números pseudo-aleatórios.");
        System.out.println("\t\t<suffix>\tSufixo a ser usado na geração dos arquivos de output para auxiliar na diferenciação de outras execuções.");
    }

    private static void parsePreEngineStartArguments(String[] args) {
        // <speedlimit>
        try {
            double speedLimit = Double.parseDouble(args[6]); // km/h
            engine.setGlobalSpeedLimit(speedLimit);
            ReportManager.log(String.format("Velocidade limite padrão: %.1fkm/h", speedLimit));
        } catch (NumberFormatException e) {
            System.err.println("O valor <speedlimit>='" + args[6] + "' não é válido.");
            usage();
            System.exit(1);
        }

        // <green> <amber> <clearance>
        try {
            long greenTime = Long.parseLong(args[3]); // segundos
            long amberTime = Long.parseLong(args[4]); // segundos
            long clearanceTime = Long.parseLong(args[5]); // segundos
            engine.setGlobalControllerPhaseTiming(greenTime, amberTime, clearanceTime);
            ReportManager.log(String.format("Tempos da fase de um controlador: verde=%ds, amarelo=%ds, clearance=%ds", greenTime, amberTime, clearanceTime));
        } catch (NumberFormatException e) {
            System.err.println("A combinação <green>='" + args[3] + "', <amber>='" + args[4] + "', <clearance>='" + args[5] + "' não é válida para os tempos dos controladores.");
            usage();
            System.exit(1);
        }

        // <startuplosttime>
        try {
            double startupLostTime = Double.parseDouble(args[9]); // segundos
            engine.setCoordinatedStartupLostTime(startupLostTime);
            ReportManager.log(String.format("Startup Lost Time: %.1fs", startupLostTime));
        } catch (NumberFormatException e) {
            System.err.println("O valor <startuplosttime>='" + args[9] + "' não é válido.");
            usage();
            System.exit(1);
        }

        // <greenwavespeedfactor>
        try {
            double greenWaveSpeedFactor = Double.parseDouble(args[10]); // segundos
            engine.setCoordinatedGreenWaveSpeedFactor(greenWaveSpeedFactor);
            ReportManager.log(String.format("Fator de velocidade da onda verde: %.1fs", greenWaveSpeedFactor));
        } catch (NumberFormatException e) {
            System.err.println("O valor <greenwavespeedfactor>='" + args[10] + "' não é válido.");
            usage();
            System.exit(1);
        }

        // <greenwavespeedfactor>
        try {
            long seed = Long.parseLong(args[11]); // valor inteiro
            engine.setSeed(seed);
            ReportManager.log(String.format("Seed da simulação: %d", seed));
        } catch (NumberFormatException e) {
            System.err.println("O valor <seed>='" + args[11] + "' não é válido.");
            usage();
            System.exit(1);
        }
    }

    private static void parsePostEngineStartArguments(String[] args) {
        ReportManager.logTitle("CONFIGURACOES GERAIS DO SIMULADOR");

        // <interval> lê parâmetro que dita o intervalo entre os outputs do verbose.csv:
        try {
            intervalBetweenExtractions = Double.parseDouble(args[1]) * INTERVAL_1_MIN;
            ReportManager.log(String.format("Intervalo entre extrações do relatório: %.1f min", intervalBetweenExtractions / INTERVAL_1_MIN));
        } catch (NumberFormatException e) {
            System.err.println("O valor <interval>='" + args[1] + "' não é válido para o intervalo entre as extrações.");
            usage();
            System.exit(1);
        }

        // <duration> lê parâmetro com a duração da simulação
        try {
            long duration = Long.parseLong(args[2]) * 60; // minutos para segundos
            engine.setSimulationDuration(duration);
            ReportManager.log(String.format("Duração da simulação: %d min", duration / 60));
        } catch (NumberFormatException e) {
            System.err.println("O valor <duration>='" + args[2] + "' não é válido para a duração da simulação.");
            usage();
            System.exit(1);
        }

        // <smercontrolrevchange>
        try {
            int changePeriod = Integer.parseInt(args[7]); // em minutos
            engine.setGlobalSMERControlReversibilityChangePeriod(changePeriod);
            ReportManager.log(String.format("Intervalo de tempo entre mudanças de reversibilidade do SMERSignalController: %d min", changePeriod));
        } catch (NumberFormatException e) {
            System.err.println("O valor <smercontrolrevchange>='" + args[7] + "' não é válido.");
            usage();
            System.exit(1);
        }

        // <smercoordrevchange>
        try {
            int changePeriod = Integer.parseInt(args[8]); // em numero de fases
            engine.setGlobalSMERCoordinationReversibilityChangePeriod(changePeriod);
            ReportManager.log(String.format("Intervalo de tempo entre mudanças de reversibilidade do SMERCoordinatedSignalController: %d fases", changePeriod));
        } catch (NumberFormatException e) {
            System.err.println("O valor <smercoordrevchange>='" + args[8] + "' não é válido.");
            usage();
            System.exit(1);
        }
    }

    private static void prepareScenarioAndOutputFiles(String[] args) {
        String scenarioFilename = args[0];
        String outputSuffix = args[12];

        try {
            // extrai nome do arquivo XML e usa para compor os arquivos .CSV e .VERBOSE.CSV
            Pattern extension = Pattern.compile("\\.[^.]*$");
            Matcher extmatcher = extension.matcher(scenarioFilename);
            String csv_output = extmatcher.replaceFirst("_" + outputSuffix + ".csv");
            String csv_verbose_output = extmatcher.replaceFirst("_" + outputSuffix + ".verbose.csv");
            String log_output = extmatcher.replaceFirst("_" + outputSuffix + ".log");
            String err_output = extmatcher.replaceFirst("_" + outputSuffix + ".err");

            // abre arquivo .CSV para escrita
            try {
                csv_output_file = new BufferedWriter(new FileWriter(csv_output));
            } catch (IOException e1) {
                System.err.println("Não foi possível criar o arquivo '" + csv_output + "' para escrita.");
                System.exit(1);
            }

            // abre arquivo .VERBOSE.CSV para escrita
            try {
                csv_verbose_output_file = new BufferedWriter(new FileWriter(csv_verbose_output));
            } catch (IOException e1) {
                System.err.println("Não foi possível criar o arquivo '" + csv_verbose_output + "' para escrita.");
                System.exit(1);
            }

            engine = new Engine(XmlScenarioLoader.load(scenarioFilename), FRAME_DURATION_MS);

            ReportManager.setupManager(engine, log_output, err_output);

            ReportManager.log(String.format("Arquivo de cenário \"%s\" carregado.", scenarioFilename));

            /*
             * Os parametros abaixo têm que ser
             * setados antes de iniciar a engine, pois eles vão participar do
             * calculo dos offsets dos corredores coordenados. se esses parametros
             * forem setado depois, os corredores não vão funcionar como esperado.
             */
            parsePreEngineStartArguments(args);

            engine.start();

            /*
             * Os demais argumentos devem ser setados após o início da engine.
             */
            parsePostEngineStartArguments(args);

            ReportManager.log("Iniciando simulação...");
            ReportManager.logTitle("INICIO DA SIMULACAO");

        } catch (FileNotFoundException | ScenarioLoadException | StepTooBigException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        if (args.length == 13) {
            prepareScenarioAndOutputFiles(args);
        } else {
            usage();
            System.exit(1);
        }

        try {
            long numberOfSteps = Math.round(engine.getSimulationTotalDuration() / (FRAME_DURATION_MS / 1000.d));
            long dtStart = System.currentTimeMillis();
            double elapsedTime = 0;

            csv_verbose_output_file.write(engine.printReportSummaryHeaderToCSV() + "\n");

            for (long i = 0; i < numberOfSteps; i++) {
                engine.step();

                elapsedTime += FRAME_DURATION_MS;

                if (elapsedTime > intervalBetweenExtractions) {
                    csv_verbose_output_file.write(engine.printSimulationIterationCSV() + "\n");
                    elapsedTime = 0;
                }
            }
            long dtEnd = System.currentTimeMillis();

            csv_output_file.write(engine.printSimulationResultsCSV());

            showExecutionTimeStatistics(dtStart, dtEnd, numberOfSteps);

            ReportManager.log(engine.printSimulationSummary());

            csv_output_file.close();
            csv_verbose_output_file.close();
            ReportManager.closeReport();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void showExecutionTimeStatistics(long dtStart, long dtEnd, long numberOfSteps) {
        long dtDelta = (dtEnd - dtStart);
        double avgStepTimeUs = 1000 * (dtDelta / (double) numberOfSteps);
        double stepsPerSec = (1 / (avgStepTimeUs / (1000.d * 1000.d)));
        double secsInSimPerSecsInRealTime = stepsPerSec * (FRAME_DURATION_MS / 1000.d);
        long dtDeltaSecs = Math.round(dtDelta / 1000.d);

        String result = "";
        result += "\n+------------------------------------------------------------------------------+";
        result += "\n|                          ESTATÍSTICAS DE EXECUÇÃO                            |";
        result += "\n+------------------------------------------------------------------------------+";
        result += "\n> " + String.format("%d iterações processadas em %d segundos de tempo real.", numberOfSteps, dtDeltaSecs);
        result += "\n> " + String.format("Média de %.2f microssegundos de tempo real de processamento por iteração.", avgStepTimeUs);
        result += "\n> " + String.format("%.2f iterações processadas por segundo de tempo real.", stepsPerSec);
        result += "\n> " + String.format("Speedup: %.2f segundos de tempo de simulação por segundo de tempo real.", secsInSimPerSecsInRealTime);
        ReportManager.log(result);
    }
}
